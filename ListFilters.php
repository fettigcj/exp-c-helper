<?php

include 'Functions.php';
include "$pathToPan_Configurator/lib/misc-classes/RQuery.php";
$availableFilters = RQuery::$defaultFilters['rule'];

print "<HTML>
<HEAD>
	<TITLE>Pan-Configurator Helper</TITLE>
        <STYLE>
                table
                {border-collapse: collapse;}
                .child:nth-of-type(even)
                {background-color: khaki;}
                .item:nth-of-type(even)
                {background-color: lemonchiffon;}
                tr
		{border-bottom: 1px solid black;}
		/*.child:hover
                {background-color: gray; }*/
                /*.item:hover
                {background-color: gray; }*/
                DIV.invisible
                {display: none;}
        </STYLE>
</HEAD>
<BODY>
<CENTER>";

print "<!--Debug-->
<DIV CLASS=\"invisible\">
";
print_r($availableFilters);
print "<!--EndDebug-->
</DIV>";

print "<div style =\"overflow-x:auto;\">
<H2>
Pan-Configurator Helper - List Filters
</H2>
<TABLE>
	<TR>
		<TH ALIGN=Left WIDTH=20%>Filter For</TH>
		<TH ALIGN=Left>Filter Criteria</TH>
	</TR>
<!--EndHeaderRow-->
";

    ksort(RQuery::$defaultFilters['rule']);
    foreach(RQuery::$defaultFilters['rule'] as $index => &$filter )
    {
        print "<!--StartFilter-->
	<TR CLASS=\"item\">
	<TD><B>".$index."</B></TD>
<!--StartCriteria-->
	<TD>";
        ksort( $filter['operators'] );
	$count=1;
        foreach( $filter['operators'] as $oindex => &$operator)
        {
//            $output = "    - $oindex";
	print "<DIV CLASS=\"child\">";
	print "#$count  -  $oindex";
	print "</DIV>";
	$count++;
//";
//          print $output."\n";
        }
        print "<!--EndCriteria-->
	</TD>
</TR>
<!--EndFilter-->


";
    }

print "
</TABLE>
</CENTER>
</BODY>
</HTML>
";

?>
