<?php

if (isset($_POST['targetInfo']))
{$selectedTargetInfo = $_POST['targetInfo'];}

print "

<DIV ID=\"targetInfo\" class=\"sectionTargetInfo\" STYLE=\"clear: left\">

<CENTER><H3>Set target firewall / panorama (API or XML) to be changed</H3></CENTER>
<TABLE CLASS=\"border\">
<TR>
<TD>
	Target Type:<BR>
	<SELECT ID=\"targetType\" name=\"targetInfo[targetType]\">\n";

if (isset($_POST['targetInfo']['targetType'])) //Check if user has submitted target type. If so list their choices and re-build the section to allow for changes.
{
	foreach ($availableTargetTypes as $key => $targetType) 
	{
		if ($selectedTargetInfo['targetType'] == $targetType)
			{
				print "	<OPTION value=\"$targetType\" selected=\"selected\">$targetType</OPTION>";
			}
		else
			{
				print "	<OPTION value=\"$targetType\">$targetType</OPTION>";
			}		
	}
}
else // if the user has NOT selected anything simply build the TargetInfo section.
{
	foreach ($availableTargetTypes as $key => $targetType)
	{
		if ($targetType == 'PanoramaAPI') //default to PanoramaAPI selected.
		{print "	<OPTION value=\"$targetType\" selected=\"selected\">$targetType</OPTION>";}
		else
		{print "	<OPTION value=\"$targetType\">$targetType</OPTION>";}
	}
}
print "	</SELECT>
</TD>
<TD>

	Rule Type:<BR>
	<SELECT ID=\"ruleType\" name=\"targetInfo[ruleType]\">\n";
if (isset($_POST['targetInfo']['ruleType'])) //Check if user has submitted target info. If so list their choices and re-build the section to allow for changes.
{
	foreach ($availableRuleTypes as $key => $ruleType) 
	{
		if ($selectedTargetInfo['ruleType'] == $ruleType) // If user previously selected a target type select it.
			{
				print "	<OPTION value=\"$ruleType\" selected=\"selected\">$ruleType</OPTION>";
			}
		else
			{
				if ($ruleType == 'Security') //default to security rules.
				{print "	<OPTION value=\"$ruleType\" selected =\"selected\">$ruleType</OPTION>";}
				else
				{print "	<OPTION value=\"$ruleType\">$ruleType</OPTION>";}
			}		
	}
}
else // if the user has NOT selected a ruletype simply build the TargetInfo section.
{
	foreach ($availableRuleTypes as $key => $ruleType)
	{
		if ($ruleType == 'Security')
		{print "	<OPTION value=\"$ruleType\" selected=\"selected\">$ruleType</OPTION>";}
		else
		{print "	<OPTION value=\"$ruleType\">$ruleType</OPTION>";}
	}
}

print "	</SELECT>
</TD>
<TD>
Display statistics after performing actions?<BR><CENTER>
<INPUT TYPE=\"hidden\" NAME=\"targetInfo[displayStats]\" value=\"0\">
";
if (isset($_POST['targetInfo']['displayStats']))
	{
		if ($selectedTargetInfo['displayStats'] == "1")
		{print "	<INPUT TYPE=\"checkbox\" class=\"BigCheckbox\" NAME=\"targetInfo[displayStats]\" VALUE=\"1\" checked>";}
		else
		{print "	<INPUT TYPE=\"checkbox\" class=\"BigCheckbox\" NAME=\"targetInfo[displayStats]\" VALUE=\"1\">";}
	}
else
{print "	<INPUT TYPE=\"checkbox\" class=\"BigCheckbox\" NAME=\"targetInfo[displayStats]\" VALUE=\"1\">";}


print "
</CENTER>
</TD>
<TD>
Read Panorama Objects/Rules in addition to firewall-local configuration?<BR>
<CENTER>(Only applicable for TargetType == 'FirewallAPI')<BR>
<INPUT TYPE=\"hidden\" NAME=\"targetInfo[includePanorama]\" value=\"0\">
";
if (isset($_POST['targetInfo']['includePanorama']))
	{
		if ($selectedTargetInfo['includePanorama'] == "1")
		{print "	<INPUT TYPE=\"checkbox\" class=\"BigCheckbox\" NAME=\"targetInfo[includePanorama]\" VALUE=\"1\" checked>";}
		else
		{print "	<INPUT TYPE=\"checkbox\" class=\"BigCheckbox\" NAME=\"targetInfo[includePanorama]\" VALUE=\"1\">";}
	}
else
{print "	<INPUT TYPE=\"checkbox\" class=\"BigCheckbox\" NAME=\"targetInfo[includePanorama]\" VALUE=\"1\">";}



print "
</CENTER>
</TD>
</TR>
</TABLE><BR>
<TABLE>
<TR>
<TD>
	Target Filename/Address:   ";

if (isset($_POST['targetInfo']['target']))
	{
		print "	<INPUT ID=\"target\" TYPE=\"TEXT\" VALUE=\"{$selectedTargetInfo['target']}\" NAME=\"targetInfo[target]\">";
	}
else
	{
		print "	<INPUT ID=\"target\" TYPE=\"TEXT\" VALUE=\"Enter IP or FQDN of Target\" NAME=\"targetInfo[target]\">";
	}

	print "	<BR>
	If TargetType == *API provide target address (IP, FQDN, or SN@PanoramaIP/FQDN)<BR>
	IP/FQDN alone will make changes on specified target.<BR>
	SerialNumber@PanoramaIP/FQDN will proxy the API call through Panorama to the specified Firewall.<BR>
	If TargetType == *XML provide target XML file (relative or full path acceptable)
</TD>
";


print "<TD>
	Output Filename:   ";
if (isset($_POST['targetInfo']['output']))
{print "	<INPUT ID=\"output\" type=\"TEXT\" value=\"{$selectedTargetInfo['output']}\" name=\"targetInfo[output]\">";}
else
{print "	<INPUT ID=\"output\" type=\"TEXT\" value=\"Output.XML\" name=\"targetInfo[output]\">";}

print "
	<BR>
	<CENTER>(Only Applicable when Target is an XML file)</CENTER>
</TD>
</TR>
</TABLE><BR>
Target Location: ";
if (isset($_POST['targetInfo']['location']))
{print "<INPUT ID=\"location\" type=\"TEXT\" value=\"{$selectedTargetInfo['location']}\" NAME=\"targetInfo[location]\">";}
else
{print "<INPUT ID=\"location\" type=\"TEXT\" value=\"any\" NAME=\"targetInfo[location]\">";}

print "<BR>
Enter the Device Group(s) or vSys(s) to be modified. (Can also be a coma separated list - 'DeviceGroup1, DeviceGroup2')<BR>
NOTE: This builder defaults to \"Any\"<BR>If removed from the command the PAN Configurator will default to 'shared' for Panorama targets and vSYS1 for Firewall targets.<BR><BR>
<CENTER><input type=\"submit\" value=\"Submit TargetInfo\"></CENTER>
</DIV>
<BR>
<HR STYLE=\"clear: left\">
";
?>