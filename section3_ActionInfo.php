<?php

if (isset($_POST['Actions']))
	{$selectedActions = $_POST['Actions'];}
if (isset($_POST['Arguments']))
	{$selectedArguments = $_POST['Arguments'];}



print "

<DIV ID=\"selectAction\" class=\"sectionActionInfo\" STYLE=\"clear: left\"> 

<CENTER><H3>Select the desired action(s):</H3></CENTER>

<select multiple=\"yes\" name=\"Actions[]\" size=20>";
foreach($availableActions as $action )
{
if (isset($_POST['Actions'])) // Check if user has submitted actions to be performed. If so list their choices while re-building the multi-select box.
	{
		if (in_array($action['name'], $selectedActions))
		{
			print "\n	<OPTION value=\"{$action['name']}\" SELECTED=\"selected\">{$action['name']}</OPTION>";
		}
		else
		{
			print "\n	<OPTION value=\"{$action['name']}\">{$action['name']}</OPTION>";
		}
	}
	else //if the user has not selected any actions build the multi-select box so they may do so.
	{
	print "\n	<OPTION value=\"{$action['name']}\">{$action['name']}</OPTION>";
	}
}
print "
</select>
<BR><BR><CENTER><input type=\"submit\" value=\"Select Action(s)\"></CENTER>
</DIV>
";

if (isset($_POST['Actions']))
{
	// If the user has selected actions build the arguments definition section to define the arguments for the action(s).
	print "
	<DIV ID=\"selectArguments\" class=\"sectionActionInfo\">

		<CENTER><H3>Set the arguments for selected actions:</H3></CENTER>

		<TABLE>
		<!--StartHeaderRow-->
		<TR CLASS=\"VerticalPadded\">
			<TH CLASS=\"Argument\">
				Action
			</TH>
			<TD>
				<TABLE WIDTH=100%>
					<TR>
						<TD CLASS=\"ArgumentLabel\">
							Argument (Argument Type)
						</TD>
						<TD CLASS=\"Argument\">
							Desired Value
						</TD>
						<TD CLASS=\"ArgumentDefault\">
							Default Values
						</TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
		<!--EndHeaderRow-->
	";
	foreach($selectedActions AS $key=>$selectedAction) // $selectedAction = NAME of action
	{
		$selectedAction = strtolower($selectedAction);
		$action = $availableActions[$selectedAction]; // $action = ARRAY with full details of $selectedAction
	print 
	"			
		<!--StartAction-->
		<TR CLASS=\"VerticalPadded\">
		<TH CLASS=\"Argument\">
			$selectedAction
		</TH>
		<!--StartArguments-->
		<TD>
			<TABLE WIDTH=100%>
	";
	if (isset($action['args'])) // check if the action has arguments, and if so present them to the user to be filled in.
		{
		foreach($action['args'] as $argName => &$arg)
			{
			$argType = $arg['type'];
			if (isset($arg['choices']))
			{
				$argType = $argType . " [Limited to List]";
			}
			print"<!--StartArgument-->
				<TR>
					<TD CLASS=\"ArgumentLabel\">
						$argName ($argType)
					</TD>
					<TD CLASS=\"Argument\">";
			switch ($arg['type'])
				{
				case "bool":
					print "<!--ArgType: Boolean--><input type=\"hidden\" name=\"Arguments[$selectedAction][$argName]\" value=\"False\">";
					if (isset($_POST['Arguments'][$selectedAction][$argName]))
					{
						if ($selectedArguments[$selectedAction][$argName] == 'False')
						{
						print "<!--ArgType: Boolean--><input type=\"checkbox\" class=\"BigCheckbox\" name=\"Arguments[$selectedAction][$argName]\" value=\"True\">";
						}	
						elseif ($selectedArguments[$selectedAction][$argName] == 'True')
						{
						print "<!--ArgType: Boolean--><input type=\"checkbox\" class=\"BigCheckbox\" name=\"Arguments[$selectedAction][$argName]\" value=\"True\" checked>";
						}
					}
					else
					{	
						if ($arg['default'] == "yes")
						{
							print "<!--ArgType: Boolean--><input type=\"checkbox\" class=\"BigCheckbox\" name=\"Arguments[$selectedAction][$argName]\" value=\"True\" checked>";
						}
						else
						{
							print "<!--ArgType: Boolean--><input type=\"checkbox\" class=\"BigCheckbox\" name=\"Arguments[$selectedAction][$argName]\" value=\"True\">";
						}
					}
					break;
				case "string":
					print "<!--ArgType: String";
					if (isset($arg['choices']))
					{
						print "-WithChoices-->\n					";
					foreach($arg['choices'] as $choice)
						{
							if (isset($_POST['Arguments'][$selectedAction][$argName]) && $selectedArguments[$selectedAction][$argName] == $choice)
							{
								print "		<input type=\"radio\" name=\"Arguments[$selectedAction][$argName]\" value=\"$choice\" checked=\"checked\">$choice\n					";
							}
							else
							{
								if ($choice == $arg['default'])
								{
									print "		<input type=\"radio\" name=\"Arguments[$selectedAction][$argName]\" value=\"$choice\" checked=\"checked\">$choice\n					";
								}
								else
								{
									print "		<input type=\"radio\" name=\"Arguments[$selectedAction][$argName]\" value=\"$choice\">$choice\n					";
								}
							}
						}
					}
					else
					{
						print "-WithoutChoices-->";
						if (isset($_POST['Arguments']) && isset($selectedArguments[$selectedAction][$argName]))
						{
						print "<input type=\"text\" name=Arguments[$selectedAction][$argName] value=\"{$selectedArguments[$selectedAction][$argName]}\">\n					";
						}
						else
						{
						print "<input type=\"text\" name=Arguments[$selectedAction][$argName]>\n					";
						}
					}
					break;
					case "pipeSeparatedList":
					print "<!--ArgType: pipeSeparatedList";
					if (isset($arg['choices']))
					{
						print "-WithChoices-->\n					";
					foreach($arg['choices'] as $choice)
						{
							if (isset($_POST['Arguments'][$selectedAction][$argName]) && $selectedArguments[$selectedAction][$argName] == $choice)
							{
								print "		<input type=\"radio\" name=\"Arguments[$selectedAction][$argName]\" value=\"$choice\" checked=\"checked\">$choice\n					";
							}
							else
							{
								if ($choice == $arg['default'])
								{
									print "		<input type=\"radio\" name=\"Arguments[$selectedAction][$argName]\" value=\"$choice\" checked=\"checked\">$choice\n					";
								}
								else
								{
									print "		<input type=\"radio\" name=\"Arguments[$selectedAction][$argName]\" value=\"$choice\">$choice\n					";
								}
							}
						}
					}
					else
					{
						print "-WithoutChoices-->";
						if (isset($_POST['Arguments']) && isset($selectedArguments[$selectedAction][$argName]))
						{
						print "
						<input type=\"text\" name=Arguments[$selectedAction][$argName] value=\"{$selectedArguments[$selectedAction][$argName]}\">\n					";
						}
						else
						{
						print "
						<input type=\"text\" name=Arguments[$selectedAction][$argName]>\n					";
						}
					break;
					}
				}
			if (isset($arg['default']))
				{
					print"</TD>
					<TD CLASS=\"ArgumentDefault\">
						{$arg['default']}
					";
					
					
					
				}
			
			
			
			
			
			
			
			print"</TD>
				</TR>
				<!--EndArgument-->
			";
			}	
		}
	else
		//[$selectedAction] value =\"$selectedAction\">
		{
		print "<!--StartArgumentless-->
				<TR>
					<TD>
						No arguments for this action
						<INPUT type=\"hidden\" name=Arguments[$selectedAction][] value=\"noArgs\">
					</TD>
				</TR>
				<!--EndArgumentless-->
			";
		}
	print "</TABLE>
		</TD>\n";
	print "		<!--EndArguments-->
</TR>
<!--EndAction-->\n";
	}
print "
</TABLE>
<BR>

<CENTER><input type=\"submit\" value=\"Submit Argument Values\"></CENTER>
</DIV><BR>
";
}
print "<HR STYLE=\"clear: left\">";
?>