<?php

include 'Functions.php';
include "$pathToPan_Configurator/utils/common/actions.php";
include "$pathToPan_Configurator/lib/misc-classes/RQuery.php";

ksort(RuleCallContext::$supportedActions);
ksort(RQuery::$defaultFilters['rule']);
$availableActions = RuleCallContext::$supportedActions;
$availableFilters = RQuery::$defaultFilters['rule'];

$availableTargetTypes = array();
$availableTargetTypes[] = 'PanoramaAPI';
$availableTargetTypes[] = 'PanoramaXML';
$availableTargetTypes[] = 'FirewallAPI';
$availableTargetTypes[] = 'FirewallXML';

$availableRuleTypes = array();
$availableRuleTypes[] = 'Security';
$availableRuleTypes[] = 'NAT';
$availableRuleTypes[] = 'All';
$availableRuleTypes[] = 'Decryption';
$availableRuleTypes[] = 'PBF';
$availableRuleTypes[] = 'QOS';
$availableRuleTypes[] = 'DOS';
$availableRuleTypes[] = 'AppOverride';

include 'section1_Preamble.php';
print "\n\n<!--End Section 1 - Preamble-->\n\n";

if (isset($_POST))
{
	Print "
	<!--StartDebug-->\n\n
		<DIV CLASS=\"invisible\">";
		print_r($_POST);
	Print "
		</DIV>\n\n
	<!--EndDebug-->
	";
}

if (isset($_POST['Arguments']))
{
	print "\n\n<!--Start Section 5 - Command Output-->\n\n";
	include 'section5_CommandOutput.php';
	print "\n\n<!--End Section 5 - Command Output-->\n\n";
}	
if (isset($_POST['Arguments']))
{
	print "\n\n<!--Start Section 4 - FilterInfo-->\n\n";
	include 'section4_FilterInfo.php';
	print "\n\n<!--End Section 4 - FilterInfo-->\n\n";
}

if (isset($_POST['targetInfo']))
{
	print "\n\n<!--Start Section 3 - ActionInfo-->\n\n";
	include 'section3_ActionInfo.php';
	print "\n\n<!--End Section 3 - ActionInfo-->\n\n";
}

print "\n\n<!--Start Section 2 - targetInfo-->\n\n";
include 'section2_TargetInfo.php';
print "\n\n<!--End Section 2 - targetInfo-->\n\n";





	

print "

</FORM>
</BODY>
</HTML>
";

?>
