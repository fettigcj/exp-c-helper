<?php
if (isset($_POST['FilterTargets']))
{$selectedFilterTargets = $_POST['FilterTargets'];}
if (isset($_POST['Filters']))
{$selectedFilters = $_POST['Filters'];}
if (isset($_POST['FilterStrings']))
{$selectedFilterStrings = $_POST['FilterStrings'];}

// $NoCriteria = array("is.allow", "is.deny", "is.negative", "is.any", "is.negated", "is.set", "at.end", "at.start", )

/*
Print "
	<!--StartDebug-->\n\n
		<DIV CLASS=\"invisible\">";

print "\n<BR><BR>AllFilters:";
	$testval = $availableFilters['action']['operators'];
	foreach ($testval as $criterianame => $criteriadetails) {
		print "$criterianame \n";
	}
	//print_r($availableFilters);
	print "<BR>";

Print "
		</DIV>\n\n
	<!--EndDebug-->
	";

*/

print "


<DIV ID=\"selectFilterTargets\" class=\"sectionFilterInfo\" STYLE=\"Clear: Left\">
<H4>Select Desired Filter Targets:</H4><BR>\n\n

<select multiple=\"yes\" name=\"FilterTargets[]\" size=\"20\">

";

foreach($availableFilters as $filtername => $filter )
{
	if (isset($_POST['FilterTargets'])) // Check if user has submitted filters to be performed. If so list their choices while re-building the multi-select box.
	{
		if (in_array($filtername, $selectedFilterTargets))
		{
			print "\n	<OPTION value=\"$filtername\" SELECTED=\"selected\">$filtername</OPTION>";
		}
		else
		{
			print "\n	<OPTION value=\"$filtername\">$filtername</OPTION>";
		}
	}
	else //if the user has not selected any filters build the multi-select box so they may do so.
	{
		print "\n	<OPTION value=\"$filtername\">$filtername</OPTION>";
	}
}
print "
</select>
<CENTER><input type=\"submit\" value=\"Select Filter Targets\"></CENTER>
</DIV>";

if (isset($_POST['FilterTargets'])) // If the user has selected filters build the filterArguments definition section to define the arguments for the filter
{
	print "

	<DIV ID=\"selectFilterType\" CLASS=\"sectionFilterInfo\">
	<H4>Select Desired filter Types</H4>\n\n


	";

	// <SELECT MULTIPLE=\"yes\" name=\"Filters[$filterTarget][]\" size=\"20\">

	foreach ($selectedFilterTargets as $filterTarget) 
	{
		$target = $availableFilters[$filterTarget]['operators'];
		if (isset($target['>,<,=,!']))
			{
				$target['GreaterThan'] = array();
				$target['GreaterThan'] = $target['>,<,=,!'];
				$target['LessThan'] = array();
				$target['LessThan'] = $target['>,<,=,!'];
				$target['EqualTo'] = array();
				$target['EqualTo'] = $target['>,<,=,!'];
				$target['NotEqualTo'] = array();
				$target['NotEqualTo'] = $target['>,<,=,!'];
				unset($target['>,<,=,!']);
				//print "<!--StartDebugTarget-->";
				//print_r($target);
				//print "<!--EndDebugTarget-->";
			}
		$count = count($target);
		print "
		<TABLE WIDTH=100%>
		<TR>
		<TD WIDTH=33%>
			$filterTarget
		</TD>
		<TD>
			<SELECT MULTIPLE=\"yes\" name=\"Filters[$filterTarget][]\" size=\"$count\">";
			foreach ($target as $criteriatype => $criteriadetails)
			{	
				//$criteriatype = str_replace('>,<,=,!', 'MathOps(GT,LT,EQ,NEQ)', $criteriatype);
				if ($count == '1')
				{print "<OPTION VALUE=\"$criteriatype\" SELECTED=\"selected\">$criteriatype</OPTION>";}
				else
				{
					if (isset($_POST['Filters'][$filterTarget]))
						{
							$parent = $_POST['Filters'][$filterTarget];
							if (in_array($criteriatype, $parent) || $count == '1')
							{print "<OPTION VALUE=\"$criteriatype\" SELECTED=\"selected\">$criteriatype</OPTION>";}
							else
							{print "<OPTION VALUE=\"$criteriatype\">$criteriatype</OPTION>";}
						}
					else
						{print "<OPTION VALUE=\"$criteriatype\">$criteriatype</OPTION>";}
				}
			}
		print "
		</TD>
		</TR>
		</TABLE>

		";
	}
	print "<CENTER><input type=\"submit\" value=\"Select Filter Types\"></CENTER>

	</DIV>

	";
}

if (isset($_POST['Filters']))
{
	print "
	<DIV ID=\"selectFilterArguments\" class=\"sectionFilterInfo\">
	<CENTER><H4>Set the arguments for selected filters:</H4><BR>
	NOTE: Not every filter requires an input.<BR>
	'is.something'/'at.something' filters for example simply check for the 'something'<BR>
	In some cases you may need to leave a field blank in the below.</CENTER>
	<TABLE>
	";

	foreach ($_POST['Filters'] as $filter => $criterias)
	{
		foreach ($criterias as $key => $criteria)
		{
			print "<TR><TD>$filter {$criteria}</TD><TD> ";
			if ((strpos($criteria, 'is.') !== False) || (strpos($criteria, 'at.') !== False))
				{
					print "'is.' and 'at.' filters do not receive criteria.
					<INPUT TYPE=\"hidden\" name=FilterStrings[$filter][$criteria] value=\"IsOrAt-NoInput\">\n
					</TD></TR>\n";
				}
			else
			{
				if (isset($_POST['FilterStrings'][$filter][$criteria]))
					{
						print "<INPUT TYPE=\"text\" name=FilterStrings[$filter][$criteria] value=\"{$selectedFilterStrings[$filter][$criteria]}\">\n</TD></TR>\n";
					}
				else
					{
						print "<INPUT TYPE=\"text\" name=FilterStrings[$filter][$criteria]>\n</TD></TR>\n";
					}
			}
		}
	}
	print "</TABLE><BR>
	<CENTER><input type=\"submit\" value=\"(Re)-Build Command\"></CENTER>
	</DIV>";
}

print "
<HR STYLE=\"clear: left\">
";
?>