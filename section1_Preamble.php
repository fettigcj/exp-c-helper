<!DOCTYPE html>
<HTML>
<HEAD>
	<TITLE>Pan-Configurator Helper</TITLE>
	<LINK rel="stylesheet" type="text/css" href="stylesheet.css">
</HEAD>
<BODY>
<DIV CLASS="sectionPreamble">
<CENTER>
<H2>
	Please carefully select the desired options below.<BR> 
	This tool will assist in building a syntactically correct command.<BR>
	Ensuring the validity/desirability of what that command does is up to you.
</H2>
<TABLE WIDTH=50%>
	<TR>
	<TD WIDTH=20% ALIGN=CENTER>
		<H3><A HREF=Help.html>Help - General Info</A></H3>
	</TD>	
	<TD WIDTH=20% ALIGN=CENTER>
		<H3><A HREF=ListActions.php>Help - List Actions</A></H3>
	</TD>
	<TD WIDTH=20% ALIGN=CENTER>
		<H3><A HREF=ListFilters.php>Help - List Filters</A></H3>
	</TD>
	<TD WIDTH=20% ALIGN=CENTER>
		<H3><A HREF=ExpeditionConverter2020_08_12.zip>Download Expedition Converter</A></H3>
	</TR>
	<TR><TD COLSPAN=4><CENTER><H2><A HREF=".">Start Over</A></H2></CENTER></TD></TR>
</TABLE>
</CENTER>
</DIV>
<FORM action="" method="post">