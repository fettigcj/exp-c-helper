<?php
function ActionStringBuilder($ActionArray)
{
	$firstAction = True;
	$string = "";
	foreach ($ActionArray as $action => $arguments)
	{
		if ($firstAction == True)
			{
				$string .= $action . ":";
			}
		else
			{
				$string .= "/" . $action . ":";
			}
		$firstArgument = True;
		foreach ($arguments as $argument => $argumentValue) 
			{
				if ($firstArgument == True)
				{
					$string .= $argumentValue;
					$firstArgument = False;
				}
				else
				{
					$string .= "," . $argumentValue;
				}
				$firstArgument = False;
			}
		$firstAction = False;
	}

	return str_replace(":noArgs","",$string);
}

function FilterStringBuilder($FilterArray)
{
	$first = True;
	$string = "'filter=(";
	foreach ($FilterArray as $filter => $filters)
	{
		foreach ($filters as $filtername => $filtertext)
		{
			if ($first == True)
			{
				$string .= "($filter $filtername $filtertext)";
				$first = False;
			}
			else
			{
				$string .= " and ($filter $filtername $filtertext)";	
			}
			
		}
	}
	$string .= ")'";
	$string = str_replace("LessThan", "<", $string);
	$string = str_replace("GreaterThan", ">", $string);
	$string = str_replace("EqualTo", "=", $string);
	$string = str_replace("NotEqualTo", "!", $string);

	return str_replace(" IsOrAt-NoInput)", ")", $string);
}

function OutStringBuilder($targetInfo)
{
	if (strpos($targetInfo['targetType'], 'API') !== False)
		{return "";}
	else
		{return " out={$targetInfo['output']} ";}
	// leading and trailing spaces are important due to how '$outstring' is jammed in the middle of $CommandString
}

print "
<CENTER>
<DIV CLASS=\"sectionCommandOutput\">
";
$filterString = "";
if (isset($_POST['targetInfo']))
	{
		$target = "";
		// $selectedTargetInfo = $_POST['targetInfo'];
		if ((strpos($_POST['targetInfo']['targetType'], 'API') !== False))
			$target = "api://";
		$target .= $_POST['targetInfo']['target'];
		$ruleType = $_POST['targetInfo']['ruleType'];
		$outstring = OutStringBuilder($_POST['targetInfo']);
		$location = $_POST['targetInfo']['location'];
		$displayStats = $_POST['targetInfo']['displayStats'];
		$includePanorama = $_POST['targetInfo']['includePanorama'];
	}
else
	{print "Something went wrong. Target info missing. Command Output cannot be built yet. Please fill in more info, then try again.";}
if (isset($_POST['Arguments']))
	{
		$actions = ActionStringBuilder($_POST['Arguments']);
	}
else
	{print "Something went wrong. Action Argument info missing. Try again.";}
if (isset($_POST['FilterStrings']))
{
	$filterString = FilterStringBuilder($_POST['FilterStrings']);	
}
else
	{$filter = "";}


$CommandString = "php rules-edit.php in=" . $target . $outstring . " ruletype=" . $ruleType . " location=" . $location . " actions=" . $actions . " " . $filterString;

if ($displayStats == True)
{
	$CommandString .= " Stats";
}
if ($includePanorama == True)
{
	$CommandString .= " loadPanoramaPushedConfig";
}

print "If necessary you may manually update the below filter layout. For instance you may want to exchange 'and' for 'or' as necessary. You may also find it helpful to re-arrange the parenthetical groupings to produce a more exacting search filter. In such cases each filter can be used more than once. Such filter strings are outside the scope of this tool.<BR><BR>

<H3>$CommandString</H3><BR><BR>

Note: If you go back and edit a lower section you should re-build the command string to ensure the above reflects all changes. Removal of actions may not remove corresponding action arguments until the command is re-built.<BR><BR>
<CENTER><INPUT TYPE=\"submit\" VALUE=\"Rebuild Command\"></CENTER>
</DIV>
</CENTER>
";
print "
<HR STYLE=\"clear: left\">
";


?>