<?php

$pathToPan_Configurator = (__DIR__ . "/Expedition-Converter");

function list_to_string(&$array, $separator = ',')
    {
        $ret = '';
        $first = true;

        foreach($array as &$el)
        {
            if( $first )
            {
                $first = false;
                if( is_string($el) )
                    $ret .= $el;
                else
                    $ret .= $el->name();
            }
            else
            {
                if( is_string($el) )
                    $ret .= $separator.$el;
                else
                    $ret .= $separator.$el->name();
            }

        }

        return $ret;

    }

//Vestigial function to find supported input for rules-edit.php. No longer used, potentially useful for historic purposes.
function findSupportedArgs($pathToPan_Configurator){
        $fp = fopen("$pathToPan_Configurator/utils/rules-edit.php","r");
        $out = fopen('PAN-C.Helper.SupportedArguments.php',"w");
        fputs($out, '<?php'."\n");
        fputs($out, '$supportedArguments = Array();'."\n");
        while ($rec = fgets($fp))
        {
                // Find lines that contain "$supportedArguments['"
                if (strpos($rec, 'supportedArguments[\''))
                {
                        fputs($out, $rec."\n");
                }
        }
        fputs($out, '?>'."\n");
        fclose($fp);
        fclose($out);
        }

?>
