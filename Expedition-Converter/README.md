Expedition-Converter
================

**Homepage** : download latest sources on [GitHub](https://spring.paloaltonetworks.com/swaschkut/Expedition-Converter). Windows package with PHP binaries here: [dev.zip](https://spring.paloaltonetworks.com/swaschkut/Expedition-Converter-Windows-tool/raw/master/dev.zip)

**Installation**:

Please follow the Installation document.

MAC-OS:
run: 'sh mac_set_path_variable.sh'

Windows: please use DOCKER - check for more information available below

**Usage**: pa_migration-parser vendor=[VENDOR] file=[vendor firewall config file] out=[PAN-OS converter firewall config file] [print | debug | in=[PAN-OS base config file]]


extended usage of Expedition-Converter
================

In addition to the migration part, Expedition-Converter bring in util scripts for general PAN-OS changes.
pa_address-edit, pa_service-edit, pa_tag-edit, pa_rule-edit to mention only some of the available once.


And for misc script support there is a own Github Enterprise repo available here:
https://spring.paloaltonetworks.com/swaschkut/misc_EXP-C

In this repo you find for example a script for Panorama managed device License overview Export to Excel file.


PAN-PHP-FRAMEWORK
================

PAN-PHP-FRAMEWORK is a PHP library aimed at making PANOS config changes easy (and XML free ;), maintainable and allowing complex scenarios like rule merging, unused object tracking, conversion of checkpoint exclusion groups, massive rule editing, AppID conversion … to name the ones I do on a regular basis and which are not offered by our GUI. It will work seamlessly on local config file or API.

**Homepage** : download latest sources on [GitHub](https://spring.paloaltonetworks.com/swaschkut/Expedition-Converter). Windows package with PHP binaries here: [dev.zip](https://spring.paloaltonetworks.com/swaschkut/Expedition-Converter-Windows-tool/raw/master/dev.zip)

**Requirements** : PHP 7.1 with curl module

**Usage**: include the file lib/panos_php_framework.php in your own script to load the necessary classes.

File tree:
* **/lib/** contains library files source code
* **/utils/** contains ready to run scripts, more information in [utils/readme.txt](/utils)
* **/doc/index.html**  has all classes documentations
* **/example-xxx.php** are examples about using this library

Docker build
============

* **MacOS** : [run on MacOS terminal]
	```bash
	cd [Expedition-Converter Root folder]
	docker build -t expedition-converter .
	docker run -v ${PWD}:/expedition-converter -it expedition-converter
	```

* **WINDOWS** : [run on Windows terminal]
	```bash
	cd [Expedition-Converter Root folder]
	docker build -t expedition-converter .
	docker run -v %CD%:/expedition-converter -it expedition-converter
	```

* **UPDATE Expedition-Converter** : [this must be done inside the Docker Container]

	```bash
	cd /tools/Expedition-Converter
	git -c user.name=test -c user.email=test@test.com stash
	git clean -f
	git -c http.sslVerify=false pull origin master
	```
