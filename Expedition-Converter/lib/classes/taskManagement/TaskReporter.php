<?php


interface TaskReporter
{
    public function start();

    /***
     * Returns true if the task has been cancelled
     */
    public function isCancelled();

    /***
     * Reports this task as started
     * @return mixed
     */
    public function setStarted();

    /***
     * Updates the task status to flag to be relaunched in a given period of time
     *
     * @param STRING $timeUnit
     * @param INT $timeValue
     * @param STRING $submessage
     * @return mixed
     */
    public function setToRelaunch(STRING $timeUnit, INT $timeValue, STRING $submessage);

    /***
     * @param STRING|NULL $submessage
     * @param STRING|NULL $resultCode
     * @return mixed
     */
    public function setCompleted(STRING $submessage = null, STRING $resultCode = null);

    public function setCompletedSilent(STRING $subMessage = null, STRING $resultCode = null);

    public function setFailed(STRING $subMessage = null, STRING $resultCode = null);

    public function setFailedSilent(STRING $subMessage = null, STRING $resultCode = null);

    public function increaseFailed();

    public function increaseCompleted();

    public function updateTaskStatus(STRING $percentage, BOOL $correct, STRING $subMessage = null, STRING $resultCode = null);

    /*
    public function notifyCompletedTaskJob():bool;

    public function notifyFailedTaskJob():bool;
*/
}