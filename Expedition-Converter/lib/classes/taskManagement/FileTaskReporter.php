<?php


class FileTaskReporter implements TaskReporter
{

    public function start()
    {
        // TODO: Implement start() method.
        echo "The conversion start\n";
    }

    /**
     * @inheritDoc
     */
    public function isCancelled()
    {
        // TODO: Implement isCancelled() method.
        echo "The conversion is cancelled\n";
        return FALSE;
    }

    /**
     * @inheritDoc
     */
    public function setStarted()
    {
        // TODO: Implement setStarted() method.
        $this->start();
    }

    /**
     * @inheritDoc
     */
    public function setToRelaunch(STRING $timeUnit, INT $timeValue, STRING $submessage)
    {
        // TODO: Implement setToRelaunch() method.
        echo "Oj, oj. We will have to rerun this execution. Didn't work now\n";
    }

    /**
     * @inheritDoc
     */
    public function setCompleted(STRING $submessage = null, STRING $resultCode = null)
    {
        // TODO: Implement setCompleted() method.
        echo "We are DONE\n";
    }

    public function setCompletedSilent(STRING $subMessage = null, STRING $resultCode = null)
    {
        // TODO: Implement setCompletedSilent() method.
        $this->setCompleted();
    }

    public function setFailed(STRING $subMessage = null, STRING $resultCode = null)
    {
        // TODO: Implement setFailed() method.
        echo "FAILED\n";
    }

    public function setFailedSilent(STRING $subMessage = null, STRING $resultCode = null)
    {
        // TODO: Implement setFailedSilent() method.
        $this->setFailed();
    }

    public function increaseFailed()
    {
        // TODO: Implement increaseFailed() method.
        echo "This part failed\n";
    }

    public function increaseCompleted()
    {
        // TODO: Implement increaseCompleted() method.
        echo "This part completed\n";
    }

    public function updateTaskStatus(STRING $percentage, bool $correct, STRING $subMessage = null, STRING $resultCode = null)
    {
        // TODO: Implement updateTaskStatus() method.
    }

    /*
    public function notifyCompletedTaskJob(): bool
    {
        // TODO: Implement notifyCompletedTaskJob() method.
    }

    public function notifyFailedTaskJob(): bool
    {
        // TODO: Implement notifyFailedTaskJob() method.
    }
    */
}