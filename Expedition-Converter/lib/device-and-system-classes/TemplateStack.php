<?php
/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */

class TemplateStack
{
    use ReferenceableObject;
    use PathableName;
    use PanSubHelperTrait;

    /** @var PanoramaConf */
    public $owner;

    /** @var  array */
    public $templates = array();

    protected $FirewallsSerials = array();

    /**
     * Template constructor.
     * @param string $name
     * @param PanoramaConf $owner
     */
    public function __construct($name, $owner)
    {
        $this->name = $name;
        $this->owner = $owner;
        $this->deviceConfiguration = new PANConf(null, null, $this);
    }

    public function load_from_domxml(DOMElement $xml)
    {
        $this->xmlroot = $xml;

        $this->name = DH::findAttribute('name', $xml);
        if( $this->name === FALSE )
            derr("templatestack name not found\n", $xml);

        #print "template-stack: ".$this->name."\n";
        $tmp = DH::findFirstElement('templates', $xml);

        if( $tmp !== FALSE )
        {
            foreach( $tmp->childNodes as $node )
            {
                if( $node->nodeType != XML_ELEMENT_NODE ) continue;

                $ldv = $node->textContent;
                $this->templates[] = $ldv;
                //print "Template '{$ldv}' found\n";
                //Todo: add reference to Template
            }
            #print_r( $this->templates );
        }

        $this->FirewallsSerials = $this->owner->managedFirewallsStore->get_serial_from_xml($xml);
        foreach( $this->FirewallsSerials as $serial )
        {
            $managedFirewall = $this->owner->managedFirewallsStore->find($serial);
            if( $managedFirewall !== null )
                $managedFirewall->addTemplateStack($this->name);
        }
    }

    public function name()
    {
        return $this->name;
    }

    public function isTemplateStack()
    {
        return TRUE;
    }

}

