<?php
/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */

class Template
{
    use ReferenceableObject;
    use PathableName;
    use PanSubHelperTrait;

    /** @var PanoramaConf */
    public $owner;

    /** @var  PANConf */
    public $deviceConfiguration;

    protected $FirewallsSerials = array();

    /**
     * Template constructor.
     * @param string $name
     * @param PanoramaConf $owner
     */
    public function __construct($name, $owner)
    {
        $this->name = $name;
        $this->owner = $owner;
        $this->deviceConfiguration = new PANConf(null, null, $this);
    }

    public function load_from_domxml(DOMElement $xml)
    {
        $this->xmlroot = $xml;

        $this->name = DH::findAttribute('name', $xml);
        if( $this->name === FALSE )
            derr("template name not found\n", $xml);

        $tmp = DH::findFirstElementOrCreate('config', $xml);

        $this->deviceConfiguration->load_from_domxml($tmp);

        $this->FirewallsSerials = $this->owner->managedFirewallsStore->get_serial_from_xml($xml);
        foreach( $this->FirewallsSerials as $serial )
        {
            $managedFirewall = $this->owner->managedFirewallsStore->find($serial);
            if( $managedFirewall !== null )
                $managedFirewall->addTemplate($this->name);
        }
    }

    public function name()
    {
        return $this->name;
    }

    public function &getXPath()
    {
        $str = "/config/devices/entry[@name='localhost.localdomain']/template/entry[@name='" . $this->name . "']";

        return $str;
    }

    public function isTemplate()
    {
        return TRUE;
    }

}

