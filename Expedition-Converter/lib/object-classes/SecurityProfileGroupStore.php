<?php

/**
 * Class SecurityProfileGroup
 * @property SecurityProfileGroupStore[] $o
 * @property VirtualSystem|DeviceGroup|PanoramaConf|PANConf $owner
 * @method SecurityProfileGroupStore[] getAll()
 */
class SecurityProfileGroupStore extends ObjStore
{
    /** @var null|SecurityProfileGroupStore */
    protected $parentCentralStore = null;

    public static $childn = 'SecurityProfileGroupStore';

    private $secprof_array = array('virus', 'spyware', 'vulnerability', 'file-blocking', 'wildfire-analysis', 'url-filtering');

    /** @var DOMElement */
    public $securityProfileRoot;

    /** @var DOMElement */
    public $securityProfileGroupRoot;

    public function __construct($owner)
    {
        $this->classn = &self::$childn;

        $this->owner = $owner;
        $this->o = array();

        if( isset($owner->parentDeviceGroup) && $owner->parentDeviceGroup !== null )
            $this->parentCentralStore = $owner->parentDeviceGroup->SecurityProfileGroupStore;
        else
            $this->findParentCentralStore();

    }


    public function load_securityprofile_groups_from_domxml($xml)
    {
        $this->securityProfileGroupRoot = $xml;

        $duplicatesRemoval = array();

        $tmp_group_array = array();

        foreach( $this->securityProfileGroupRoot->childNodes as $node )
        {
            if( $node->nodeType != XML_ELEMENT_NODE )
                continue;
            $name = DH::findAttribute('name', $node);

            //create SecurityProfileGroup
            $tmp_group_array[$name]['name'] = $name;


            foreach( $this->secprof_array as $secprof_type )
            {
                $tmp_type = DH::findFirstElement($secprof_type, $node);

                if( $tmp_type != FALSE )
                {
                    $tmp_type = DH::findFirstElement('member', $tmp_type);

                    //find SecurityProfile of type XYZ and add it to SecurityProfileGroup
                    if( $tmp_type != null )
                        $tmp_group_array[$name][$secprof_type] = $tmp_type->nodeValue;
                }
            }
        }

        /*
        if( count( $tmp_group_array) > 0  )
        {
            print $this->owner->name()."\n";
            print "SecProfGroup:\n";
            print_r($tmp_group_array);
            print "######################################################\n";
        }
*/
    }

    /**
     * @param $name
     * @param null $ref
     * @param bool $nested
     * @return null|SecurityProfileGroupStore
     */
    public function find($name, $ref = null, $nested = TRUE)
    {
        $f = $this->findByName($name, $ref);

        if( $f !== null )
            return $f;

        if( $nested && $this->parentCentralStore !== null )
            return $this->parentCentralStore->find($name, $ref, $nested);

        return null;
    }

    public function removeAllSecurityProfileGroups()
    {
        $this->removeAll();
        $this->rewriteXML();
    }

    /**
     * add a SecurityProfileGroup to this store. Use at your own risk.
     * @param SecurityProfileGroupStore $Obj
     * @param bool
     * @return bool
     */
    public function addSecurityProfileGroup(SecurityProfileGroupStore $Obj, $rewriteXML = TRUE)
    {
        $ret = $this->add($Obj);
        if( $ret && $rewriteXML )
        {
            if( $this->xmlroot === null )
                $this->xmlroot = DH::findFirstElementOrCreate('profiles', $this->owner->xmlroot);

            $this->xmlroot->appendChild($Obj->xmlroot);
        }
        return $ret;
    }

    /**
     * @param string $base
     * @param string $suffix
     * @param integer|string $startCount
     * @return string
     */
    public function findAvailableSecurityProfileGroupName($base, $suffix, $startCount = '')
    {
        $maxl = 31;
        $basel = strlen($base);
        $suffixl = strlen($suffix);
        $inc = $startCount;
        $basePlusSuffixL = $basel + $suffixl;

        while( TRUE )
        {
            $incl = strlen(strval($inc));

            if( $basePlusSuffixL + $incl > $maxl )
            {
                $newname = substr($base, 0, $basel - $suffixl - $incl) . $suffix . $inc;
            }
            else
                $newname = $base . $suffix . $inc;

            if( $this->find($newname) === null )
                return $newname;

            if( $startCount == '' )
                $startCount = 0;
            $inc++;
        }
    }


    /**
     * return tags in this store
     * @return SecurityProfileGroupStore[]
     */
    public function tags()
    {
        return $this->o;
    }

    function createSecurityProfile($name, $ref = null)
    {
        if( $this->find($name, null, FALSE) !== null )
            derr('SecurityProfileGroup named "' . $name . '" already exists, cannot create');

        if( $this->xmlroot === null )
        {
            if( $this->owner->isDeviceGroup() || $this->owner->isVirtualSystem() )
                $this->xmlroot = DH::findFirstElementOrCreate('tag', $this->owner->xmlroot);
            else
                $this->xmlroot = DH::findFirstElementOrCreate('tag', $this->owner->sharedroot);
        }

        $newSecurityProfileGroup = new SecurityProfileGroupStore($name, $this);
        $newSecurityProfileGroup->owner = null;

        $newSecurityProfileRoot = DH::importXmlStringOrDie($this->owner->xmlroot->ownerDocument, SecurityProfileGroupStore::$templatexml);
        $newSecurityProfileRoot->setAttribute('name', $name);
        $newSecurityProfileGroup->load_from_domxml($newSecurityProfileRoot);

        if( $ref !== null )
            $newSecurityProfileGroup->addReference($ref);

        $this->addSecurityProfileGroup($newSecurityProfileGroup);

        return $newSecurityProfileGroup;
    }

    function findOrCreate($name, $ref = null, $nested = TRUE)
    {
        $f = $this->find($name, $ref, $nested);

        if( $f !== null )
            return $f;

        return $this->createSecurityProfileGroup($name, $ref);
    }

    function API_createSecurityProfileGroup($name, $ref = null)
    {
        $newSecurityProfileGroup = $this->createSecurityProfileGroup($name, $ref);

        if( !$newSecurityProfileGroup->isTmp() )
        {
            $xpath = $this->getXPath();
            $con = findConnectorOrDie($this);
            $element = $newSecurityProfileGroup->getXmlText_inline();
            $con->sendSetRequest($xpath, $element);
        }

        return $newSecurityProfileGroup;
    }


    /**
     * @param SecurityProfileGroupStore $tag
     *
     * @return bool  True if Zone was found and removed. False if not found.
     */
    public function removeSecurityProfileGroup(SecurityProfileGroupStore $tag)
    {
        $ret = $this->remove($tag);

        if( $ret && !$tag->isTmp() && $this->xmlroot !== null )
        {
            $this->xmlroot->removeChild($tag->xmlroot);
        }

        return $ret;
    }

    /**
     * @param SecurityProfileGroupStore $securityProfileGroup
     * @return bool
     */
    public function API_removeSecurityProfileGroup(SecurityProfileGroupStore $securityProfileGroup)
    {
        $xpath = null;

        if( !$securityProfileGroup->isTmp() )
            $xpath = $securityProfileGroup->getXPath();

        $ret = $this->removeSecurityProfile($securityProfileGroup);

        if( $ret && !$securityProfileGroup->isTmp() )
        {
            $con = findConnectorOrDie($this);
            $con->sendDeleteRequest($xpath);
        }

        return $ret;
    }

    public function &getXPath()
    {
        $str = '';

        if( $this->owner->isDeviceGroup() || $this->owner->isVirtualSystem() )
            $str = $this->owner->getXPath();
        elseif( $this->owner->isPanorama() || $this->owner->isFirewall() )
            $str = '/config/shared';
        else
            derr('unsupported');

        $str = $str . '/profiles';

        return $str;
    }


    private function &getBaseXPath()
    {
        if( $this->owner->isPanorama() || $this->owner->isFirewall() )
        {
            $str = "/config/shared";
        }
        else
            $str = $this->owner->getXPath();


        return $str;
    }

    public function &getSecurityProfileGroupStoreXPath()
    {
        $path = $this->getBaseXPath() . '/profiles';
        return $path;
    }

    public function rewriteXML()
    {
        if( count($this->o) > 0 )
        {
            if( $this->xmlroot === null )
                return;

            $this->xmlroot->parentNode->removeChild($this->xmlroot);
            $this->xmlroot = null;
        }

        if( $this->xmlroot === null )
        {
            if( count($this->o) > 0 )
                DH::findFirstElementOrCreate('profiles', $this->owner->xmlroot);
        }

        DH::clearDomNodeChilds($this->xmlroot);
        foreach( $this->o as $o )
        {
            if( !$o->isTmp() )
                $this->xmlroot->appendChild($o->xmlroot);
        }
    }


    /**
     *
     * @ignore
     */
    protected function findParentCentralStore()
    {
        $this->parentCentralStore = null;

        $cur = $this->owner;
        while( isset($cur->owner) && $cur->owner !== null )
        {
            $ref = $cur->owner;
            if( isset($ref->SecurityProfileGroupStore) &&
                $ref->SecurityProfileGroupStore !== null )
            {
                $this->parentCentralStore = $ref->SecurityProfileGroupStore;
                //print $this->toString()." : found a parent central store: ".$parentCentralStore->toString()."\n";
                return;
            }
            $cur = $ref;
        }

    }

}

