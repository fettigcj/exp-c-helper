<?php


class SecurityProfileStore extends ObjStore
{
    use PathableName;
    use XmlConvertible;

    /** @var VirtualSystem|DeviceGroup|PanoramaConf|PANConf|null */
    public $owner;
    public $name = 'temporaryname';

    protected $type = '**needsomethinghere**';

    protected $fastMemToIndex = null;
    protected $fastNameToIndex = null;


    /** @var SecurityProfileURL[] */
    public $_all = array();

    /** @var SecurityProfileURL[] */
    public $_SecurityProfiles = array();

    /** @var SecurityProfileGroup[] */
    protected $_SecurityProfileGroups = array();

    /** @var null|SecurityProfileStore */
    protected $parentCentralStore = null;

    public static $childn = 'SecurityProfile';

    private $secprof_array = array('virus', 'spyware', 'vulnerability', 'file-blocking', 'wildfire-analysis', 'url-filtering', 'custom-url-category', 'predefined-url');

    /** @var DOMElement */
    public $securityProfileRoot;

    /** @var DOMElement */
    public $securityProfileGroupRoot;

    /*
    public $URLSecProf=null;
    public $AntiVirusSecProf=null;
    public $AntiSpywareSecProf=null;
    public $FileBlockingSecProf=null;
    public $WildFireSecProf=null;
    public $DataFilteringSecProf=null;
    public $DoSProtectionSecProf=null;
     */

    static private $storeNameByType = array(

        'SecurityProfileURL' => array('name' => 'URL', 'varName' => 'urlSecProf', 'xpathRoot' => 'url-filtering'),
        'SecurityProfileAntiVirus' => array('name' => 'Virus', 'varName' => 'avSecProf', 'xpathRoot' => 'virus'),
        'SecurityProfileAntiSpyware' => array('name' => 'AntiSpyware', 'varName' => 'asSecProf', 'xpathRoot' => 'spyware'),
        'SecurityProfileVulnerability' => array('name' => 'Vulnerability', 'varName' => 'fbSecProf', 'xpathRoot' => 'vulnerability'),
        'SecurityProfileFileBlocking' => array('name' => 'FileBlocking', 'varName' => 'fbSecProf', 'xpathRoot' => 'file-blocking'),
        'SecurityProfileWildFire' => array('name' => 'Wildfire', 'varName' => 'wfSecProf', 'xpathRoot' => 'wildfire-analysis'),
        'SecurityProfileDataFiltering' => array('name' => 'Data-Filtering', 'varName' => 'dfSecProf', 'xpathRoot' => 'XYZ'),
        'SecurityProfileDoSProtection' => array('name' => 'DoSProtection', 'varName' => 'dosSecProf', 'xpathRoot' => 'XYZ'),

        'CustomSecurityProfileURL' => array('name' => 'customURL', 'varName' => 'customUrlSecProf', 'xpathRoot' => 'custom-url-category'),
        'PredefinedSecurityProfileURL' => array('name' => 'predefinedURL', 'varName' => 'predefinedUrlSecProf', 'xpathRoot' => 'predefined-url-category'),

    );


    public function name()
    {
        return $this->name;
    }

    public function __construct($owner, $profileType)
    {
        $this->classn = &self::$childn;

        $this->owner = $owner;
        $this->o = array();

        if( isset($owner->parentDeviceGroup) && $owner->parentDeviceGroup !== null )
            $this->parentCentralStore = $owner->parentDeviceGroup->securityProfileStore;
        else
            $this->findParentCentralStore();

        $this->_SecurityProfiles = array();

        $allowedTypes = array_keys(self::$storeNameByType);
        if( !in_array($profileType, $allowedTypes) )
            derr("Error : type '$profileType' is not a valid one");

        $this->type = $profileType;

        $this->name = self::$storeNameByType[$this->type]['name'];

    }

    public $predefinedStore_appid_version = null;

    /** @var null|SecurityProfileStore */
    public static $predefinedStore = null;

    /**
     * @return SecurityProfileStore|null
     */
    public static function getPredefinedStore()
    {
        if( self::$predefinedStore !== null )
            return self::$predefinedStore;


        self::$predefinedStore = new SecurityProfileStore(null, "PredefinedSecurityProfileURL");
        self::$predefinedStore->setName('predefined URL');
        self::$predefinedStore->load_from_predefinedfile();

        return self::$predefinedStore;
    }


    public function load_from_domxml(DOMElement $xml)
    {
        $this->securityProfileRoot = $xml;

        $duplicatesRemoval = array();


        $duplicatesRemoval = array();
        $nameIndex = array();

        if( $xml !== null )
        {
            $this->xmlroot = $xml;

            foreach( $xml->childNodes as $node )
            {
                if( $node->nodeType != XML_ELEMENT_NODE )
                    continue;
                if( $node->tagName != 'entry' )
                {
                    mwarning("A SecyrityProfile entry with tag '{$node->tagName}' was found and ignored");
                    continue;
                }
                /** @var SecurityProfileURL|CustomSecurityProfileURL $nr */
                $nr = new $this->type($this);
                $nr->load_from_domxml($node);
                if( PH::$enableXmlDuplicatesDeletion )
                {
                    if( isset($nameIndex[$nr->name()]) )
                    {
                        mwarning("SecProf named '{$nr->name()}' is present twice on the config and was cleaned by PAN-PHP-FRAMEWORK");
                        $duplicatesRemoval[] = $node;
                        continue;
                    }
                }

                $nameIndex[$nr->name()] = TRUE;
                $this->_SecurityProfiles[] = $nr;
                #$this->_SecurityProfiles[$this->name] = $nr;
                $this->_all[] = $nr;
                #$this->_all[$this->name] = $nr;
                $this->o[] = $nr;
            }
        }


        /*
                $tmp_array = array();
                foreach( $this->secprof_array as $secprof_type )
                {
                    $tmp_array[$secprof_type] = array();

                    $tmp_type = DH::findFirstElementOrCreate($secprof_type, $xml);


                    foreach( $tmp_type->childNodes as $node )
                    {
                        if( $node->nodeType != XML_ELEMENT_NODE )
                            continue;
                        $typeName = DH::findAttribute('name', $node);
                        $tmp_array[$secprof_type][$typeName] = array();
                        //Todo: 20190114 - add more information

                        if( $secprof_type == 'virus' )
                        {
                            $url = new SecurityProfileAntiVirus( $typeName, $this );
                            $url->load_from_domxml( $node );
        */
        /*
        $tmp_decoder = DH::findFirstElement('decoder', $node);
        if( $tmp_decoder !== FALSE )
        {
            foreach( $tmp_decoder->childNodes as $tmp_entry )
            {
                if( $tmp_entry->nodeType != XML_ELEMENT_NODE )
                    continue;


                $appName = DH::findAttribute('name', $tmp_entry);
                if( $appName === FALSE )
                    derr("secprof name not found\n");


                $tmp_array[$secprof_type][$typeName][$appName] = array();

                $action = DH::findFirstElement('action', $tmp_entry);
                if( $action !== FALSE )
                    $tmp_array[$secprof_type][$typeName][$appName]['action'] = $action->textContent;

                $action_wildfire = DH::findFirstElement('wildfire-action', $tmp_entry);
                if( $action_wildfire !== FALSE )
                    $tmp_array[$secprof_type][$typeName][$appName]['wildfire-action'] = $action_wildfire->textContent;
            }
        }
        */
        /*
                        }
                        elseif( $secprof_type == 'vulnerability' || $secprof_type == 'spyware' || $secprof_type == 'file-blocking' || $secprof_type == 'wildfire-analysis')
                        {
                            $tmp_rule = DH::findFirstElement('rules', $node);
                            if( $tmp_rule !== FALSE )
                            {
                                $tmp_array[$secprof_type][$typeName]['rules'] = array();
                                foreach( $tmp_rule->childNodes as $tmp_entry1 )
                                {
                                    if( $tmp_entry1->nodeType != XML_ELEMENT_NODE )
                                        continue;

                                    $vb_severity = DH::findAttribute('name', $tmp_entry1);
                                    if( $vb_severity === FALSE )
                                        derr("VB severity name not found\n");

                                    $severity = DH::findFirstElement('severity', $tmp_entry1);
                                    if( $severity !== FALSE )
                                    {
                                        if( $severity->nodeType != XML_ELEMENT_NODE )
                                            continue;

                                        $tmp_array[$secprof_type][$typeName]['rules'][$vb_severity]['severity'] = array();
                                        foreach( $severity->childNodes as $member )
                                        {
                                            if( $member->nodeType != XML_ELEMENT_NODE )
                                                continue;

                                            $tmp_array[$secprof_type][$typeName]['rules'][$vb_severity]['severity'][$member->textContent] = $member->textContent;
                                        }
                                    }

                                    $severity = DH::findFirstElement('file-type', $tmp_entry1);
                                    if( $severity !== FALSE )
                                    {
                                        if( $severity->nodeType != XML_ELEMENT_NODE )
                                            continue;

                                        $tmp_array[$secprof_type][$typeName]['rules'][$vb_severity]['file-type'] = array();
                                        foreach( $severity->childNodes as $member )
                                        {
                                            if( $member->nodeType != XML_ELEMENT_NODE )
                                                continue;

                                            $tmp_array[$secprof_type][$typeName]['rules'][$vb_severity]['file-type'][$member->textContent] = $member->textContent;
                                        }
                                    }

                                    $action = DH::findFirstElement('action', $tmp_entry1);
                                    if( $action !== FALSE )
                                    {
                                        if( $action->nodeType != XML_ELEMENT_NODE )
                                            continue;

                                        $tmp_action = DH::firstChildElement( $action );
                                        if( $tmp_action !== FALSE )
                                            $tmp_array[$secprof_type][$typeName]['rules'][$vb_severity]['action'] = $tmp_action->nodeName;

                                        if( $secprof_type == 'file-blocking' )
                                            $tmp_array[$secprof_type][$typeName]['rules'][$vb_severity]['action'] = $action->textContent;
                                    }

                                    $packet_capture = DH::findFirstElement('packet-capture', $tmp_entry1);
                                    if( $packet_capture !== FALSE )
                                    {
                                        if( $packet_capture->nodeType != XML_ELEMENT_NODE )
                                            continue;

                                        $tmp_array[$secprof_type][$typeName]['rules'][$vb_severity]['packet-capture'] = $packet_capture->textContent;
                                    }

                                    $direction = DH::findFirstElement('direction', $tmp_entry1);
                                    if( $direction !== FALSE )
                                    {
                                        if( $direction->nodeType != XML_ELEMENT_NODE )
                                            continue;

                                        $tmp_array[$secprof_type][$typeName]['rules'][$vb_severity]['direction'] = $direction->textContent;
                                    }

                                    $analysis = DH::findFirstElement('analysis', $tmp_entry1);
                                    if( $analysis !== FALSE )
                                    {
                                        if( $analysis->nodeType != XML_ELEMENT_NODE )
                                            continue;

                                        $tmp_array[$secprof_type][$typeName]['rules'][$vb_severity]['analysis'] = $analysis->textContent;
                                    }
                                }
                            }

                            $tmp_threat_exception = DH::findFirstElement('threat-exception', $node);
                            if( $tmp_threat_exception !== FALSE )
                            {
                                $tmp_array[$secprof_type][$typeName]['threat-exception'] = array();
                                foreach( $tmp_threat_exception->childNodes as $tmp_entry1 )
                                {
                                    if( $tmp_entry1->nodeType != XML_ELEMENT_NODE )
                                        continue;

                                    $tmp_name = DH::findAttribute('name', $tmp_entry1);
                                    if( $tmp_name === FALSE )
                                        derr("VB severity name not found\n");

                                    $action = DH::findFirstElement('action', $tmp_entry1);
                                    if( $action !== FALSE )
                                    {
                                        if( $action->nodeType != XML_ELEMENT_NODE )
                                            continue;

                                        $tmp_action = DH::firstChildElement( $action );
                                        $tmp_array[$secprof_type][$typeName]['threat-exception'][$tmp_name]['action'] = $tmp_action->nodeName;
                                    }
                                }
                            }

                        }
                        elseif( $secprof_type == 'url-filtering' )
                        {
                            $url = new SecurityProfileURL( $typeName, $this );
                            $url->load_from_domxml( $node );
        */
        /*
        $tmp_url_prof_array = array('allow','alert', 'block', 'continue', 'override');

        //predefined URL category
        //$tmp_array[$secprof_type][$typeName]['allow']['URL category'] = all predefined URL category

        foreach( $tmp_url_prof_array as $url_type )
        {
            $tmp_decoder = DH::findFirstElement( $url_type, $node);
            if( $tmp_decoder !== FALSE )
            {
                foreach( $tmp_decoder->childNodes as $tmp_entry )
                {
                    if( $tmp_entry->nodeType != XML_ELEMENT_NODE )
                        continue;

                    $tmp_array[$secprof_type][$typeName][$url_type][] = $tmp_entry->textContent;
                    if( $url_type !== 'allow' )
                    {
                        //unset( $tmp_array[$secprof_type][$typeName]['allow']['URL category'] );
                    }
                }
            }
        }
        */
        /*
                        }
                        elseif( $secprof_type == 'custom-url-category' )
                        {
                            $customurl = new CustomProfileURL( $typeName, $this );
                            $customurl->load_from_domxml( $node );
                        }
                        else
                        {
                            print "\nsecprof_type: ".$secprof_type."\n";
                        }
                        //url-filtering
                        //custom-url-category
                    }
                }
        */
        //Todo SVEN
        /*
                $tmp_continue = false;
                foreach( $tmp_array as $key => $sec_array )
                {
                    if( count( $sec_array ) > 0 )
                        $tmp_continue = true;
                }
                if( $tmp_continue  )
                {

                    print "\n######################################################\n";
                    print "######################################################\n";
                    print $this->owner->name()."\n";

                    foreach( $tmp_array as $key => $sec_array )
                    {
                        if( count( $sec_array ) > 0 )
                        {
                            #if( $key !== "virus" && $key !== "vulnerability" && $key !== "url-filtering" && $key !== "file-blocking" && $key !== "spyware" && $key !== "wildfire-analysis" )
                            #{
                                print $key.":\n";
                                print_r( $sec_array );
                            #}

                        }
                    }

                    print "\n######################################################\n";

                }
        */

    }

    public function load_from_predefinedfile($filename = null)
    {
        if( $filename === null )
        {
            $filename = dirname(__FILE__) . '/predefined.xml';
        }

        $xmlDoc = new DOMDocument();
        $xmlDoc->load($filename, XML_PARSE_BIG_LINES);

        $cursor = DH::findXPathSingleEntryOrDie('/predefined/pan-url-categories', $xmlDoc);

        $this->load_predefined_url_categories_from_domxml($cursor);

    }

    /**
     * Look for a rule named $name. Return NULL if not found
     * @param string $name
     * @return null|SecurityProfileURL|SecurityProfileAntiVirus|CustomSecurityProfileURL
     */
    public function find($name)
    {
        if( !is_string($name) )
            derr("String was expected for rule name");

        if( isset($this->fastNameToIndex[$name]) )
            return $this->_SecurityProfiles[$this->fastNameToIndex[$name]];

        return null;
    }

    /**
     * @param SecurityProfileURL|SecurityProfileAntiVirus|CustomSecurityProfileURL
     * @return bool
     */
    function inStore($SecurityProfile)
    {
        $serial = spl_object_hash($SecurityProfile);

        if( isset($this->fastMemToIndex[$serial]) )
            return TRUE;

        return FALSE;
    }

    /**
     * Returns an Array with all SecurityProfiles inside this store
     * @param null|string|string[] $withFilter
     * @return CustomProfileURL[]|SecurityProfileURL
     */
    public function &securityProfiles($withFilter = null)
    {
        $query = null;

        if( $withFilter !== null && $withFilter !== '' )
        {
            $queryContext = array();

            if( is_array($withFilter) )
            {
                $filter = &$withFilter['query'];
                $queryContext['nestedQueries'] = &$withFilter;
            }
            else
                $filter = &$withFilter;

            $errMesg = '';
            $query = new RQuery('rule');
            if( $query->parseFromString($filter, $errMsg) === FALSE )
                derr("error while parsing query: {$errMesg}");

            $res = array();

            foreach( $this->o as $securityProfile )
            {
                $queryContext['object'] = $securityProfile;
                if( $query->matchSingleObject($queryContext) )
                    $res[] = $securityProfile;
            }

            return $res;
        }

        $res = $this->o;

        return $res;
    }

    /**
     * Counts the number of SecurityProfiles in this store
     *
     */
    public function count()
    {
        return count($this->_SecurityProfiles);
    }

    public function removeAllSecurityProfiles()
    {
        $this->removeAll();
        $this->rewriteXML();
    }

    /**
     * add a SecurityProfile to this store. Use at your own risk.
     * @param SecurityProfile $Obj
     * @param bool
     * @return bool
     */
    /*
     * public function addSecurityProfile( SecurityProfile $Obj, $rewriteXML = true )
    {
        $ret = $this->add($Obj);
        if( $ret && $rewriteXML )
        {
            if( $this->xmlroot === null )
                $this->xmlroot = DH::findFirstElementOrCreate('profiles', $this->owner->xmlroot);

            $this->xmlroot->appendChild($Obj->xmlroot);
        }
        return $ret;
    }
    */

    /**
     * @param string $base
     * @param string $suffix
     * @param integer|string $startCount
     * @return string
     */
    public function findAvailableSecurityProfileName($base, $suffix, $startCount = '')
    {
        $maxl = 31;
        $basel = strlen($base);
        $suffixl = strlen($suffix);
        $inc = $startCount;
        $basePlusSuffixL = $basel + $suffixl;

        while( TRUE )
        {
            $incl = strlen(strval($inc));

            if( $basePlusSuffixL + $incl > $maxl )
            {
                $newname = substr($base, 0, $basel - $suffixl - $incl) . $suffix . $inc;
            }
            else
                $newname = $base . $suffix . $inc;

            if( $this->find($newname) === null )
                return $newname;

            if( $startCount == '' )
                $startCount = 0;
            $inc++;
        }
    }


    /**
     * return tags in this store
     * @return SecurityProfile[]
     */
    public function tags()
    {
        return $this->o;
    }


    /**
     * @param SecurityProfileURL|CustomSecurityProfileURL $rule
     * @return bool
     */
    public function addSecurityProfile($rule)
    {

        if( !is_object($rule) )
            derr('this function only accepts Rule class objects');

        if( $rule->owner !== null )
            derr('Trying to add a rule that has a owner already !');

        /*if( $rule->owner !== $this )
        {
            $rule->from->findParentCentralStore();
            if( !$rule->isPbfRule() )
                $rule->to->findParentCentralStore();
        }*/

        $ser = spl_object_hash($rule);


        if( !isset($this->fastMemToIndex[$ser]) )
        {
            $rule->owner = $this;

            $this->_SecurityProfiles[] = $rule;
            $this->_all[] = $rule;
            $this->o[] = $rule;

            $index = lastIndex($this->_SecurityProfiles);
            $this->fastMemToIndex[$ser] = $index;
            $this->fastNameToIndex[$rule->name()] = $index;

            if( $this->xmlroot === null )
                $this->createXmlRoot();

            $this->xmlroot->appendChild($rule->xmlroot);

            return TRUE;
        }
        else
            derr('You cannot add a SecurityProfiles that is already here :)');


        return FALSE;

    }


    /**
     * Creates a new SecurityProfileURL in this store. It will be placed at the end of the list.
     * @param string $name name of the new Rule
     * @param bool $inPost create it in post or pre (if applicable)
     * @return SecurityProfileURL
     */
    public function newSecurityProfileURL($name)
    {
        $rule = new SecurityProfileURL($this);

        $xmlElement = DH::importXmlStringOrDie($this->owner->xmlroot->ownerDocument, SecurityProfileURL::$templatexml);
        $rule->load_from_domxml($xmlElement);

        $rule->owner = null;
        $rule->setName($name);

        $this->addSecurityProfile($rule);

        return $rule;
    }

    /**
     * Creates a new CustomSecurityProfileURL in this store. It will be placed at the end of the list.
     * @param string $name name of the new Rule
     * @param bool $inPost create it in post or pre (if applicable)
     * @return CustomSecurityProfileURL
     */
    public function newCustomSecurityProfileURL($name)
    {
        $rule = new CustomSecurityProfileURL($this);

        $xmlElement = DH::importXmlStringOrDie($this->owner->xmlroot->ownerDocument, CustomSecurityProfileURL::$templatexml);
        $rule->load_from_domxml($xmlElement);

        $rule->owner = null;
        $rule->setName($name);

        $this->addSecurityProfile($rule);

        return $rule;
    }

    /**
     * Creates a new PredefinedSecurityProfileURL in this store. It will be placed at the end of the list.
     * @param string $name name of the new Rule
     * @param bool $inPost create it in post or pre (if applicable)
     * @return CustomSecurityProfileURL
     */
    public function newPredefinedSecurityProfileURL($name)
    {
        $rule = new PredefinedSecurityProfileURL($this);

        #$xmlElement = DH::importXmlStringOrDie($this->owner->xmlroot->ownerDocument, PredefinedSecurityProfileURL::$templatexml);
        #$rule->load_from_domxml($xmlElement);

        $rule->owner = null;
        $rule->setName($name);

        #$this->addSecurityProfile($rule);

        return $rule;
    }

    public function load_predefined_url_categories_from_domxml(DOMElement $xml)
    {
        foreach( $xml->childNodes as $appx )
        {
            if( $appx->nodeType != XML_ELEMENT_NODE )
                continue;


            $nodeName1 = $appx->nodeName;
            if( $nodeName1 == "hidden-entries" )
                continue;

            $appName = DH::findAttribute('name', $appx);
            if( $appName === FALSE )
                derr("Predefined URL category name not found\n");

            $app = $this->newPredefinedSecurityProfileURL($appName);
            #$app->type = 'predefined';

            $this->add($app);
        }

        sort($this->o);
    }

    /*
        function createSecurityProfile($name, $ref=null)
        {
            if( $this->find($name, null, false ) !== null )
                derr('SecurityProfile named "'.$name.'" already exists, cannot create');

            if( $this->xmlroot === null )
            {
                if( $this->owner->isDeviceGroup() || $this->owner->isVirtualSystem() )
                    $this->xmlroot = DH::findFirstElementOrCreate('tag', $this->owner->xmlroot);
                else
                    $this->xmlroot = DH::findFirstElementOrCreate('tag', $this->owner->sharedroot);
            }

            $newSecurityProfile = new SecurityProfile($name, $this);
            $newSecurityProfile->owner = null;

            $newSecurityProfileRoot = DH::importXmlStringOrDie($this->owner->xmlroot->ownerDocument, SecurityProfile::$templatexml);
            $newSecurityProfileRoot->setAttribute('name', $name);
            $newSecurityProfile->load_from_domxml($newSecurityProfileRoot);

            if( $ref !== null )
                $newSecurityProfile->addReference($ref);

            $this->addSecurityProfile($newSecurityProfile);

            return $newSecurityProfile;
        }

        function findOrCreate($name, $ref=null, $nested=true)
        {
            $f = $this->find($name, $ref, $nested);

            if( $f !== null )
                return $f;

            return $this->createSecurityProfile($name, $ref);
        }

        function API_createSecurityProfile($name, $ref=null)
        {
            $newSecurityProfile = $this->createSecurityProfile($name, $ref);

            if( !$newSecurityProfile->isTmpSecProf() )
            {
                $xpath = $this->getXPath();
                $con = findConnectorOrDie($this);
                $element = $newSecurityProfile->getXmlText_inline();
                $con->sendSetRequest($xpath, $element);
            }

            return $newSecurityProfile;
        }
    */


    /**
     * @param SecurityProfile $tag
     *
     * @return bool  True if Zone was found and removed. False if not found.
     */
    public function removeSecurityProfile(SecurityProfile $tag)
    {
        $ret = $this->remove($tag);

        if( $ret && !$tag->isTmpSecProf() && $this->xmlroot !== null )
        {
            $this->xmlroot->removeChild($tag->xmlroot);
        }

        return $ret;
    }

    /**
     * @param SecurityProfile $securityProfile
     * @return bool
     */
    public function API_removeSecurityProfile(SecurityProfile $securityProfile)
    {
        $xpath = null;

        if( !$securityProfile->isTmp() )
            $xpath = $securityProfile->getXPath();

        $ret = $this->removeSecurityProfile($securityProfile);

        if( $ret && !$securityProfile->isTmp() )
        {
            $con = findConnectorOrDie($this);
            $con->sendDeleteRequest($xpath);
        }

        return $ret;
    }

    public function &getXPath()
    {
        $str = '';

        if( $this->owner->isDeviceGroup() || $this->owner->isVirtualSystem() )
            $str = $this->owner->getXPath();
        elseif( $this->owner->isPanorama() || $this->owner->isFirewall() )
            $str = '/config/shared';
        else
            derr('unsupported');

        $str = $str . '/profiles';

        return $str;
    }


    private function &getBaseXPath()
    {
        if( $this->owner->isPanorama() || $this->owner->isFirewall() )
        {
            $str = "/config/shared";
        }
        else
            $str = $this->owner->getXPath();


        return $str;
    }

    public function &getSecurityProfileStoreXPath()
    {
        $path = $this->getBaseXPath() . '/profiles';
        return $path;
    }

    public function rewriteXML()
    {
        if( count($this->o) > 0 )
        {
            if( $this->xmlroot === null )
                return;

            $this->xmlroot->parentNode->removeChild($this->xmlroot);
            $this->xmlroot = null;
        }

        if( $this->xmlroot === null )
        {
            if( count($this->o) > 0 )
                DH::findFirstElementOrCreate('profiles', $this->owner->xmlroot);
        }

        DH::clearDomNodeChilds($this->xmlroot);
        foreach( $this->o as $o )
        {
            if( !$o->isTmpSecProf() )
                $this->xmlroot->appendChild($o->xmlroot);
        }
    }


    public function createXmlRoot()
    {
        if( $this->xmlroot === null )
        {
            $SecurityProfileTypeForXml = self::$storeNameByType[$this->type]['xpathRoot'];
            $xml = DH::findFirstElementOrCreate('profiles', $this->owner->xmlroot);

            $this->xmlroot = DH::findFirstElementOrCreate($SecurityProfileTypeForXml, $xml);
        }
    }


    /**
     *
     * @ignore
     */
    protected function findParentCentralStore()
    {
        $this->parentCentralStore = null;

        $cur = $this->owner;
        while( isset($cur->owner) && $cur->owner !== null )
        {
            $ref = $cur->owner;
            if( isset($ref->securityProfileStore) &&
                $ref->securityProfileStore !== null )
            {
                $this->parentCentralStore = $ref->securityProfileStore;
                //print $this->toString()." : found a parent central store: ".$parentCentralStore->toString()."\n";
                return;
            }
            $cur = $ref;
        }

    }

    /*
     * config/device/entry/vsys/entry/profiles:
              <profiles>
                <custom-url-category>
                  <entry name="custom_url">
                    <list>
                      <member>sven.waschkut.de</member>
                    </list>
                  </entry>
                </custom-url-category>

                <data-objects>
                  <entry name="custom_data">
                    <pattern-type>
                      <file-properties/>
                    </pattern-type>
                  </entry>
                </data-objects>
     */

    /*
     * custom vulnerability and spyware are under:
     *  config/device/entry/vyss/entry/threats
          <threats>
            <vulnerability>
              <entry name="41000">
                <signature>
                  <standard>
                    <entry name="custom">
                      <order-free>no</order-free>
                      <scope>protocol-data-unit</scope>
                    </entry>
                  </standard>
                </signature>
                <default-action>
                  <alert/>
                </default-action>
                <threatname>custom_vulnerabiltiy</threatname>
                <severity>medium</severity>
                <direction>both</direction>
                <affected-host>
                  <client>yes</client>
                </affected-host>
              </entry>
            </vulnerability>
            <spyware>
              <entry name="15000">
                <signature>
                  <standard>
                    <entry name="custom">
                      <order-free>no</order-free>
                      <scope>protocol-data-unit</scope>
                    </entry>
                  </standard>
                </signature>
                <default-action>
                  <alert/>
                </default-action>
                <threatname>custom_spyware</threatname>
                <severity>medium</severity>
                <direction>both</direction>
              </entry>
            </spyware>
          </threats>
     */
}