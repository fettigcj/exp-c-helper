<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */

trait PanSubHelperTrait
{

    public function isPanorama()
    {
        FALSE;
    }

    public function isFirewall()
    {
        FALSE;
    }

    public function isVirtualSystem()
    {
        FALSE;
    }

    public function isDeviceGroup()
    {
        FALSE;
    }

    public function isTemplate()
    {
        FALSE;
    }

}
