<?php
/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */


class StaticRoute
{
    use XmlConvertible;
    use PathableName;
    use ReferenceableObject;

    //Todo:
    //set interface
    //set metric
    //set nexthop


    /** @var string */
    protected $_destination;

    protected $_nexthopType = 'none';

    protected $_nexthopIP = null;

    /** @var null|string */
    protected $_nexthopVR = null;

    /** @var VirtualRouter */
    public $owner;

    /** @var null|EthernetInterface|AggregateEthernetInterface|TmpInterface */
    protected $_interface = null;


    /**
     * StaticRoute constructor.
     * @param string $name
     * @param VirtualRouter $owner
     */
    function __construct($name, $owner)
    {
        $this->owner = $owner;
        $this->name = $name;
    }

    /**
     * @param $xml DOMElement
     */
    function load_from_xml($xml)
    {
        $this->xmlroot = $xml;

        $this->name = DH::findAttribute('name', $xml);
        if( $this->name === FALSE )
            derr("static-route name not found\n");

        $dstNode = DH::findFirstElementOrDie('destination', $xml);
        $this->_destination = $dstNode->textContent;

        $ifNode = DH::findFirstElement('interface', $xml);
        if( $ifNode !== FALSE )
        {
            $tmp_interface = $this->owner->owner->owner->network->findInterfaceOrCreateTmp($ifNode->textContent);
            $this->_interface = $tmp_interface;
            $tmp_interface->addReference($this);
        }

        $fhNode = DH::findFirstElement('nexthop', $xml);
        if( $fhNode !== FALSE )
        {
            $fhTypeNode = DH::findFirstElement('ip-address', $fhNode);
            if( $fhTypeNode !== FALSE )
            {
                $this->_nexthopType = 'ip-address';
                $this->_nexthopIP = $fhTypeNode->textContent;
                return;
            }
            $fhTypeNode = DH::findFirstElement('ipv6-address', $fhNode);
            if( $fhTypeNode !== FALSE )
            {
                $this->_nexthopType = 'ipv6-address';
                $this->_nexthopIP = $fhTypeNode->textContent;
                return;
            }
            $fhTypeNode = DH::findFirstElement('next-vr', $fhNode);
            if( $fhTypeNode !== FALSE )
            {
                $this->_nexthopType = 'next-vr';
                $this->_nexthopVR = $fhTypeNode->textContent;
                return;
            }

        }
    }

    function create_staticroute_from_xml($xmlString)
    {
        $xmlElement = DH::importXmlStringOrDie($this->owner->owner->xmlroot->ownerDocument, $xmlString);
        $this->load_from_xml($xmlElement);

        return $this;
    }

    /**
     * @return string
     */
    public function destination()
    {
        return $this->_destination;
    }

    /**
     * @return bool|string
     */
    public function destinationIP4Mapping()
    {
        return cidr::stringToStartEnd($this->_destination);
    }

    public function nexthopIP()
    {
        return $this->_nexthopIP;
    }

    /**
     * @return null|string
     */
    public function nexthopVR()
    {
        return $this->_nexthopVR;
    }

    public function nexthopInterface()
    {
        return $this->_interface;
    }


    /**
     * @return string   'none','ip-address'
     */
    public function nexthopType()
    {
        return $this->_nexthopType;
    }

    public function referencedObjectRenamed($h)
    {
        if( $this->_interface === $h )
        {
            $this->_interface = $h;
            $this->rewriteInterface_XML();

            return;
        }

        mwarning("object is not part of this static route : {$h->toString()}");
    }

    public function rewriteInterface_XML()
    {
        DH::createOrResetElement($this->xmlroot, 'interface', $this->_interface->name());
    }

}