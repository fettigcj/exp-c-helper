<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */


trait SONICWALLzone
{

#print_r( $address );
#print_r( $services );

    function add_zone($v, $zones)
    {
        global $debug;
        global $print;


        foreach( $zones as $key => $zone_entry )
        {
            $orig_service_entry = $zone_entry;

            $zone_entry = explode("\n", $zone_entry);
            foreach( $zone_entry as $key => $zone )
            {
                $zone = trim($zone);
                $zone = $this->truncate_names($this->normalizeNames($zone));
                if( $key == 0 )
                {
                    $tmp_zone = $v->zoneStore->find($zone);
                    if( $tmp_zone == null )
                    {
                        if( $print )
                            print "  - name: " . $zone . "\n";
                        $tmp_zone = $v->zoneStore->newZone($zone, 'layer3');
                    }

                }
            }
        }
    }

}

