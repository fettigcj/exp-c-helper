<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */

trait SONICWALLservicegroup
{

    function add_servicegroup($v, $servicegroup)
    {
        global $debug;
        global $print;

        $padding = "   ";
        $padding_name = substr($padding, 0, -1);

        $servicegroup_fix = array();

        foreach( $servicegroup as $key => $servicegroup_entry )
        {
            $servicegroup_entry = trim($servicegroup_entry);
            $servicegroup_entry = explode("\n", $servicegroup_entry);

            foreach( $servicegroup_entry as $key2 => $servicegroup )
            {
                if( $key2 == 0 )
                {
                    $servicegroup = preg_replace('#^"#m', "", $servicegroup);
                    $servicegroup = explode('"', $servicegroup);
                    if( count($servicegroup) == 1 )
                    {
                        $servicegroup = explode(' ', $servicegroup[0]);
                    }
                    $name = trim($servicegroup[0]);
                    $name = $this->truncate_names($this->normalizeNames($name));

                    $tmp_servicegroup = $v->serviceStore->find($name);
                    if( $tmp_servicegroup == null )
                    {
                        if( $print )
                            print "\n" . $padding_name . "* name: " . $name . "\n";
                        $tmp_servicegroup = $v->serviceStore->newServiceGroup($name);
                    }
                }
                else
                {
                    if( $servicegroup != "" )
                    {
                        $servicegroup = trim($servicegroup);
                        $servicegroup = str_replace("service-object ", "", $servicegroup);
                        $servicegroup = str_replace("service-group ", "", $servicegroup);
                        $servicegroup = str_replace('"', "", $servicegroup);

                        $name = $servicegroup;
                        $name = $this->truncate_names($this->normalizeNames($name));
                        $tmp_service = $v->serviceStore->find($name);
                        if( $tmp_service == null )
                        {
                            $tmp_service = $v->serviceStore->find("TMP_" . $name);
                            if( $tmp_service == null )
                            {
                                if( $print || $debug )
                                    print $padding . "X service object name: '" . $name . "' not found. Automatic try to fix in next step.\n";
                                $servicegroup_fix[$tmp_servicegroup->name()][] = $name;
                            }
                            else
                            {
                                if( $print )
                                    print $padding . "- member name: '" . $name . "'\n";
                                $tmp_servicegroup->addMember($tmp_service);
                            }
                        }
                        else
                        {
                            if( $print )
                                print $padding . "- member name: '" . $name . "'\n";
                            $tmp_servicegroup->addMember($tmp_service);
                        }
                    }
                }
            }
        }

        print PH::boldText("\nFIX servicegroups\n");

        foreach( $servicegroup_fix as $key => $servicegroup_array )
        {
            print "\n" . $padding_name . "* name: " . $key . "\n";
            $tmp_service_group = $v->serviceStore->find($key);
            if( $tmp_service_group != null )
            {
                foreach( $servicegroup_array as $member )
                {
                    $tmp_service = $v->serviceStore->find($member);
                    if( $tmp_service != null )
                    {
                        if( $print )
                            print $padding . "- member name: '" . $member . "'\n";
                        $tmp_service_group->addMember($tmp_service);
                    }
                    else
                    {
                        if( $print || $debug )
                            print $padding . "X service object name: '" . $member . "' still not possible to add.\n";
                        $tmp_service_group->set_node_attribute('error', "address object name: '" . $member . "' can not be added");
                    }
                }
            }
            else
            {
                print $padding_name . "X name: " . $key . " not found\n";
            }
        }

    }

}

