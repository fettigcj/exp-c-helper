<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */


require_once("CPmisc.php");
require_once("CP_objects.php");
require_once("CP_services.php");
require_once("CP_accessrules.php");
require_once("CP_natrules.php");

#require_once ("CP_R80_staticroute.php");


class CP extends PARSER
{
    public $mainfolder = "/tmp/test";
    public $newfolder = "";
    public $folder_path = "";
    public $print = FALSE;

    public $v = null;

    use CPmisc;
    use CP_objects;
    use CP_services;
    use CP_accessrules;
    use CP_natrules;


    use SHAREDNEW;

    function vendor_main($config_filename, $pan, $routetable = "")
    {
        $this->logger->start();

        parent::preCheck();


        //swaschkut - tmp, until class migration is done
        $this->print = TRUE;
        $this->print = FALSE;


        //CP specific
        //------------------------------------------------------------------------
        $path = "";
        $project = "";

        $config_path = $path . $config_filename;
        $filename = $config_filename;
        $filenameParts = pathinfo($config_filename);
        $verificationName = $filenameParts['filename'];

        $this->logger->increaseCompleted();

        $data = $this->clean_config($config_path, $project, $config_filename);

        $this->logger->increaseCompleted();
        $this->logger->setCompletedSilent();

        $this->import_config($data, $pan, $routetable); //This should update the $source
        //------------------------------------------------------------------------


        //todo delete all created files and folders

        #delete_files($mainfolder);
        #$this->delete_directory( $this->mainfolder );
    }

    function clean_config($config_path, $project, $config_filename)
    {

        $this->newfolder = $this->mainfolder . "/test_cp";

        print "folder: " . $this->newfolder . "\n";

        if( file_exists($this->newfolder) )
            $this->delete_directory($this->newfolder);

        if( !file_exists($this->newfolder) )
            mkdir($this->newfolder, 0700, TRUE);

        $configFolder = $config_filename;



        $files1 = scandir($configFolder);
        #print_r( $files1 );


        //Todo: 20200604 other possible way: argument "file=[objects]/[policy]/[rulebase]"
        //what about advanced options??? add more arguments ????

        $rulebases = null;
        $policyName = null;
        $objectName = null;

        foreach( $files1 as $item )
        {
            if( strpos($item, "rulebases") !== FALSE )
            {
                $tmp_check = explode(".", $item);
                if( $tmp_check[0] == "rulebases_5_0" && $tmp_check[1] == "fws" )
                    $rulebases = $configFolder . "/" . $item;
            }
            elseif( strpos($item, "PolicyName") !== FALSE )
            {
                $tmp_check = explode(".", $item);
                if( $tmp_check[0] == "PolicyName" && $tmp_check[1] == "W" )
                    $policyName = $configFolder . "/" . $item;
            }
            elseif( strpos($item, "object") !== FALSE )
            {
                $tmp_check = explode(".", $item);
                if( $tmp_check[0] == "objects_5_0" && $tmp_check[1] == "C" )
                    $objectName = $configFolder . "/" . $item;
            }
            elseif( strpos($item, ".W") !== FALSE )
            {
                $tmp_check = explode(".", $item);
                if( $tmp_check[1] == "W" )
                    $policyName = $configFolder . "/" . $item;
            }
            else
                continue;

        }


        if( $policyName == null )
            $policyName = $rulebases;

        print "POLICY: " . $policyName . "\n";
#$policyName = $configFolder."/PolicyName.W";
#print "POLICY2: ".$policyName."\n";


        print "RULEBASE: " . $rulebases . "\n";
#$rulebases = $configFolder."/rulebases_5_0.fws";
#print "RULEBASE2: ".$rulebases."\n";


        print "OBJECT: " . $objectName . "\n";
#$objectName = $configFolder."/objects_5_0.C";
#print "OBJECT2: ".$objectName."\n";

        print "-------------------------------------------\n";

        if( file_exists($rulebases) )
            $migratecomments = "--merge_AI=" . $rulebases;
        else
            $migratecomments = "";

        $policyName = str_replace(' ', '_', $policyName);


        $script_folder = dirname(__FILE__);
        $ParserPath = $script_folder . "/checkpoint-parser.pl";

        $fwdoc = $this->newfolder . "/conf.fwdoc";


        $Parse = "/usr/bin/perl $ParserPath --rules=$policyName --objects=$objectName $migratecomments> $fwdoc";

        if( file_exists($configFolder) )
            shell_exec($Parse);
        else
        {
            derr("Folder: '" . $configFolder . "' not available\n");
        }


        $someArray = array();
        if( (file_exists($fwdoc)) and (filesize($fwdoc) != 0) )
        {
            print "\n#####################################################\n\n";
            print "FILE: '" . $fwdoc . "' successfully created\n";
            print "\n#####################################################\n\n";


            //load file
            $someJSON = file_get_contents($fwdoc);
            //replace all hidden characters  // especially if they are available in comments
            $someJSON = $this->strip_hidden_chars($someJSON);
            // Convert JSON string to Array
            $someArray = json_decode($someJSON, TRUE);
            //JSON validation
            $this->jsonERROR();
            if( !is_array($someArray) )
                derr("json_decode not working");


            #print_r($someArray);        // Dump all data of the Array
            #print "||".count( $someArray )."\n";

        }

        return $someArray;
    }


//---------------------------------------------
//        Parser Logic starts here
//----------------------------------------------


    function import_config($data, $pan, $routetable)
    {
        global $projectdb;
        global $source;

        global $debug;

        $this->v = $pan->findVirtualSystem('vsys1');


        if( $routetable != "" )
        {
            echo PH::boldText("\nimport dynamic Routing\n");
            $cisco = file_get_contents($routetable);
            #$this->importRoutes( $cisco, $pan, $this->v);
        }


        $padding = "";

        foreach( array_keys($data) as $KEYS )
        {
            #print "|" . $KEYS . "|" . count($data[$KEYS]) . "\n";

            #print_r(array_keys($data[$KEYS]));
            #print_r( $data[$KEYS] );

            //print out all keys, first normal then recursive:
            if( $KEYS == "firewall" )
            {
                /*
                [brand] => CheckPoint
                [type] => FireWall-1 / VPN-1
                [version] => 3.0 - 4.1 - NG R65
                [date] => 2020-5-4
                [identifier] => PolicyName.W_FWS
                [filter] => Array
                    (
                    )

                [comment] => Generated: by
                 */
            }
            elseif( $KEYS == "objects" )
            {
                print "\n\n ADD ADDRESS OBJECTS:\n";
                $this->add_host_objects($KEYS, $data[$KEYS]);
            }
            elseif( $KEYS == "services" )
            {
                print "\n\n ADD SERVICES:\n";
                $this->add_services($KEYS, $data[$KEYS]);


            }
            elseif( $KEYS == "layer7filter" )
            {
                #print_r(array_keys($data[$KEYS]));
                #print_r( $data[$KEYS] );
                /*
                 *
                 *   [SunRPC_yppasswd] => Array
                    (
                        [name] => SunRPC_yppasswd
                        [protocol] => SunRPC
                        [comment] => Sun Yellow Pages protocol (NIS), password server
                    )

                [SunRPC_ypserv] => Array
                    (
                        [name] => SunRPC_ypserv
                        [protocol] => SunRPC
                        [comment] => Sun Yellow Pages directory service (YP) protocol, now known as NIS
                    )

                [SunRPC_ypupdated] => Array
                    (
                        [name] => SunRPC_ypupdated
                        [protocol] => SunRPC
                        [comment] => Sun Yellow Pages protocol (NIS), update service
                    )

                [SunRPC_ypxfrd] => Array
                    (
                        [name] => SunRPC_ypxfrd
                        [protocol] => SunRPC
                        [comment] => Sun Yellow Pages protocol (NIS), transfers NIS maps
                    )

                 */
            }
            elseif( $KEYS == "accessrules" )
            {
                //do nothing, check later
            }
            elseif( $KEYS == "natrules" )
            {
                //do nothing, check later
            }
            else
            {
                derr("NOT supported: " . $KEYS);
            }
        }


        foreach( array_keys($data) as $KEYS )
        {
            #print "|" . $KEYS . "|" . count($data[$KEYS]) . "\n";

            #print_r(array_keys($data[$KEYS]));
            #print_r( $data[$KEYS] );

            //print out all keys, first normal then recursive:
            if( $KEYS == "accessrules" )
            {
                print "\n\n ADD Security Rules:\n";
                $this->add_access_rules($KEYS, $data[$KEYS]);


            }
            elseif( $KEYS == "natrules" )
            {
                #print_r(array_keys($data[$KEYS]));
                #print_r( $data[$KEYS] );

                print "\n\n ADD NAT Rules:\n";
                $this->add_nat_rules($KEYS, $data[$KEYS]);

            }


        }

    }


    //CLEANUP function

    function delete_directory($mainfolder)
    {
        if( is_dir($mainfolder) )
            $dir_handle = opendir($mainfolder);
        if( !$dir_handle )
            return FALSE;

        while( $file = readdir($dir_handle) )
        {
            if( $file != "." && $file != ".." )
            {
                if( !is_dir($mainfolder . "/" . $file) )
                {
                    #print "unlink: ".$dirname.'/'.$file."\n";
                    unlink($mainfolder . "/" . $file);
                }

                else
                    $this->delete_directory($mainfolder . '/' . $file);
            }
        }
        closedir($dir_handle);
        #print "DEL folder: ".$dirname."\n";
        rmdir($mainfolder);
        return TRUE;
    }


}


