<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */


#require_once("CPmisc.php");
require_once("CPnew_object.php");
require_once("CPnew_service.php");
#require_once ("CP_accessrules.php");
#require_once ("CP_natrules.php");


class CPnew extends PARSER
{
    public $mainfolder = "/tmp/test";
    public $newfolder = "";
    public $folder_path = "";
    public $print = FALSE;

    public $v = null;

    #use CPmisc;
    use CPnew_object;
    use CPnew_service;

    #use CP_accessrules;
    #use CP_natrules;


    public $checkArray = TRUE;

    //this array is for OBJECTS FILE
    public $objects_key = array(

        #'anyobj',
        #'superanyobj',
        'serverobj',
        #'translations',
        #'servgen',
        #'log-props',
        #'state-act',
        #'SPIobj',
        #'version',
        #'globals',
        #'setup',
        'network_objects',
        'netobj',
        #'vs_slot_objects',
        'services',
        'servobj',
        'servers',
        #'times',
        #'ldap',
        #'opsec',
        #'graph_objects',
        #'resources',
        #'keys',
        #'encryption',
        #'svn',
        #'qos',
        'properties',
        'props',
        #'ce_properties',
        #'customers',
        #'accounting_schemes',
        'resources_types',
        'resourcesobj',
        #'protocols',
        #'tracks',
        #'HitCountSeverity',
        #'statuses',
        #'securemote',
        #'products',
        #'credentials_manager',
        'communities',
        'communitie',
        #'desktop_profiles',
        #'cp_administrators',
        #'policies_collections',
        #'methods',
        #'trusts',
        #'web_authority_must_rules',
        #'web_authority_allow_rules',
        #'web_authority_effect_rules',
        #'web_authority_URLs',
        #'web_sites',
        #'sofaware_gw_types',
        #'external_exported_domains',
        #'0',
        #'SmartCenterDBFiles',
        'netobjadtr'


    );

    public $policy_key = array(
        '0',
        'AdminInfo',
        'default',
        'globally_enforced',
        'queries',
        'queries_adtr',
        'collection',
        'use_VPN_communities',
        'rule'
    );

    use SHAREDNEW;

    function vendor_main($config_filename, $pan, $routetable = "")
    {
        $this->logger->start();

        parent::preCheck();


        //swaschkut - tmp, until class migration is done
        $this->print = TRUE;
        $this->print = FALSE;


        //CP specific
        //------------------------------------------------------------------------
        $path = "";
        $project = "";

        $config_path = $path . $config_filename;
        $filename = $config_filename;
        $filenameParts = pathinfo($config_filename);
        $verificationName = $filenameParts['filename'];

        $this->logger->increaseCompleted();

        $data = $this->clean_config($config_path, $project, $config_filename);

        $this->logger->increaseCompleted();
        $this->logger->setCompletedSilent();

        $this->import_config($data, $pan, $routetable); //This should update the $source
        //------------------------------------------------------------------------


        //todo delete all created files and folders

        #delete_files($mainfolder);
        #$this->delete_directory( $this->mainfolder );
    }

    function clean_config($config_path, $project, $config_filename)
    {

        $this->newfolder = $this->mainfolder . "/test_cpbeta";

        print "folder: " . $this->newfolder . "\n";

        if( file_exists($this->newfolder) )
            $this->delete_directory($this->newfolder);

        if( !file_exists($this->newfolder) )
            mkdir($this->newfolder, 0700, TRUE);

        $configFolder = $config_filename;


        $files1 = scandir($configFolder);
        #print_r( $files1 );


        //Todo: 20200604 other possible way: argument "file=[objects]/[policy]/[rulebase]"
        //what about advanced options??? add more arguments ????

        $rulebases = null;
        $policyName = null;
        $objectName = null;

        foreach( $files1 as $item )
        {
            if( strpos($item, "rulebases") !== FALSE )
            {
                $tmp_check = explode(".", $item);
                if( $tmp_check[0] == "rulebases_5_0" && $tmp_check[1] == "fws" )
                    $rulebases = $configFolder . "/" . $item;
            }
            elseif( strpos($item, "PolicyName") !== FALSE )
            {
                $tmp_check = explode(".", $item);
                if( $tmp_check[0] == "PolicyName" && $tmp_check[1] == "W" )
                    $policyName = $configFolder . "/" . $item;
            }
            elseif( strpos($item, "object") !== FALSE )
            {
                $tmp_check = explode(".", $item);
                if( $tmp_check[0] == "objects_5_0" && $tmp_check[1] == "C" )
                    $objectName = $configFolder . "/" . $item;
            }
            elseif( strpos($item, ".W") !== FALSE )
            {
                $tmp_check = explode(".", $item);
                if( $tmp_check[1] == "W" )
                    $policyName = $configFolder . "/" . $item;
            }
            else
                continue;

        }


        if( $policyName == null )
            $policyName = $rulebases;

        print "POLICY: " . $policyName . "\n";
#$policyName = $configFolder."/PolicyName.W";
#print "POLICY2: ".$policyName."\n";


        print "RULEBASE: " . $rulebases . "\n";
#$rulebases = $configFolder."/rulebases_5_0.fws";
#print "RULEBASE2: ".$rulebases."\n";


        print "OBJECT: " . $objectName . "\n";
#$objectName = $configFolder."/objects_5_0.C";
#print "OBJECT2: ".$objectName."\n";

        print "-------------------------------------------\n";

        /*
        //load file
        $objectFILE = file_get_contents( $objectName );

        $objectFILE = file($objectName);
        foreach ($objectFILE as $key => $line)
        {
            echo "LINE: ".$key."|".$line."";
        }
        */

        //


        print "\nread FILE: " . $objectName . "\n\n";
        $string = file_get_contents($objectName);

        $filesize = filesize($objectName);


        $count_byte = 1;
        while( $filesize > 1024 )
        {
            $filesize = $filesize / 1024;
            $count_byte++;
        }

        print "FILESIZE: " . number_format(($filesize), 2, '.', '') . " ";


        switch ($count_byte)
        {
            case 0:
                echo "i ist gleich 0";
                break;
            case 1:
                echo "";
                break;
            case 2:
                echo "K";
                break;
            case 3:
                echo "M";
                break;
            case 4:
                echo "G";
                break;
            case 5:
                echo "T";
                break;
        }
        print "Byte\n";


        if( $this->checkArray )
        {
            print "use only the following information:\n";
            print_r($this->objects_key);
        }


        print "\noptimize content\n";

        $array = $this->delimeterSplit($string);


        $end_array = $this->callRecursive($array);


        //Todo: contains only one file // e.g. only OBJECTS find general solution
        return $end_array;
    }


//---------------------------------------------
//        Parser Logic starts here
//----------------------------------------------


    function import_config($data, $pan, $routetable)
    {
        global $projectdb;
        global $source;

        global $debug;

        $this->v = $pan->findVirtualSystem('vsys1');


        if( $routetable != "" )
        {
            echo PH::boldText("\nimport dynamic Routing\n");

            $cisco = file_get_contents($routetable);
            #$this->importRoutes( $cisco, $pan, $this->v);
        }

        echo PH::boldText("\nimport address objects\n");
        $this->addHost($data);

        echo PH::boldText("\nimport service objects\n");
        $this->addService($data);

    }


    //CLEANUP function

    function delete_directory($mainfolder)
    {
        if( is_dir($mainfolder) )
            $dir_handle = opendir($mainfolder);
        if( !$dir_handle )
            return FALSE;

        while( $file = readdir($dir_handle) )
        {
            if( $file != "." && $file != ".." )
            {
                if( !is_dir($mainfolder . "/" . $file) )
                {
                    #print "unlink: ".$dirname.'/'.$file."\n";
                    unlink($mainfolder . "/" . $file);
                }

                else
                    $this->delete_directory($mainfolder . '/' . $file);
            }
        }
        closedir($dir_handle);
        #print "DEL folder: ".$dirname."\n";
        rmdir($mainfolder);
        return TRUE;
    }


    function delimeterSplit($input)
    {
        $str = '';
        $key = '';
        $output = array();
        $counter = array();

        $op = 0;
        $cp = 0;

        $found = FALSE;

        foreach( str_split($input) as $k => $v )
        {
            #print "KEY:".$k."|V:|".$v."|\n";

            if( $k === 0 && $v == '"' )
                $found = TRUE;

            if( $v === '(' && !$found )
            {
                ++$op;
            }
            if( isset($input[$k]) && $input[$k] === ')' && !$found )
            {
                ++$cp;
            }


            if( ($op === 0 && $v !== '(') )
            {
                if( $v == ':' && trim($key) != "" && !$found )
                {
                    $output[] = trim($key);
                    $key = '';
                }
                else
                    $key .= $v;

            }
            if( (($op === 1 && $v !== '(') || ($op === 1 && $found) || $op > 1) && $op !== $cp )
            {
                $str .= $v;
                if( $v == '"' && !$found )
                    $found = TRUE;
                elseif( $v == '"' && $found )
                    $found = FALSE;
            }
            if( $op > 0 && $op === $cp )
            {
                $op = 0;
                $cp = 0;

                $key = trim($key);
                $key = str_replace(":", "", $key);
                if( $key == "" )
                    $output[] = $str;
                else
                {
                    if( isset($output[$key]) )
                    {
                        if( isset($counter[$key]) )
                        {
                            $tmp_key = $counter[$key];
                            $counter[$key]++;
                        }
                        else
                        {
                            $tmp_key = 0;
                            $counter[$key] = $tmp_key;

                            $tmp_array = $output[$key];
                            $output[$key] = "fixit";
                            #unset( $output[ $key] );
                            $output[$key . $tmp_key] = $tmp_array;
                            $counter[$key]++;
                            $tmp_key = $counter[$key];

                        }

                        $output[$key . $tmp_key] = $str;
                    }
                    else
                        $output[$key] = $str;
                }

                $str = '';
                $key = '';
            }


        }

        return $output;
    }


    function callRecursive($array, $depth = -1, $padding = "")
    {
        #global $print;
        #global $objects_key;
        #global $checkArray;

        $depth++;
        $final = array();

        $fixit = FALSE;
        $fixit_value = "";

        foreach( $array as $item => $entry )
        {
            if( $entry == "fixit" )
            {
                $fixit = TRUE;
                $fixit_value = $item;
                #mwarning( $item." - is also available with counter" );
            }

            $array1 = $this->delimeterSplit($entry);

            if( !is_numeric($item) )
            {
                $key_finalPrint = "|" . $depth . "|" . $item;
                $key_final = $item;
                if( $this->print )
                    print $padding . PH::boldText($key_finalPrint) . "\n";
            }

            else
            {
                $key_finalPrint = "|" . $depth . "|";
                $key_final = $item;
                if( $this->print )
                    print $padding . PH::boldText($key_finalPrint) . "\n";
            }
            if( count($array1) > 0 )
            {
                if( $depth == 1 && $this->checkArray )
                {
                    if( in_array($key_final, $this->objects_key) )
                    {

                        if( $fixit )
                        {
                            $tmp_fixit = explode($fixit_value, $key_final);
                            if( $key_final == $fixit_value )
                            {

                            }
                            elseif( $tmp_fixit[0] == "" && is_numeric($tmp_fixit[1]) )
                            {
                                $final[$fixit_value][$tmp_fixit[1]] = callRecursive($array1, $depth++, str_pad($padding, strlen($padding) + 5));
                            }
                        }
                        else
                            $final[$key_final] = $this->callRecursive($array1, $depth++, str_pad($padding, strlen($padding) + 5));
                        $depth--;
                    }
                }
                else
                {
                    if( $fixit )
                    {
                        $tmp_fixit = explode($fixit_value, $key_final);
                        if( $key_final == $fixit_value )
                        {

                        }
                        elseif( $tmp_fixit[0] == "" && is_numeric($tmp_fixit[1]) )
                        {
                            $final[$fixit_value][$tmp_fixit[1]] = callRecursive($array1, $depth++, str_pad($padding, strlen($padding) + 5));
                        }
                    }
                    else
                        $final[$key_final] = $this->callRecursive($array1, $depth++, str_pad($padding, strlen($padding) + 5));
                    $depth--;
                }

                $padding = str_pad($padding, strlen($padding) - 5);
            }
            elseif( trim($entry) != "" )
            {
                if( $this->print )
                    print $padding . $entry . "\n";

                if( $depth == 1 && $this->checkArray )
                {
                    if( in_array($key_final, $this->objects_key) )
                    {
                        if( $this->print )
                            print $key_final . "\n";
                        if( $key_final == $fixit_value )
                            $final[$key_final] = array($entry);
                        else

                            $final[$key_final] = $entry;
                    }
                }
                else
                {
                    if( $key_final == $fixit_value )
                        $final[$key_final] = array($entry);
                    else

                        $final[$key_final] = $entry;
                }
            }
            else
            {
                if( $depth == 1 && $this->checkArray )
                {
                    if( in_array($key_final, $this->objects_key) )
                    {
                        if( $this->print )
                            print $key_final . "\n";
                        if( $key_final == $fixit_value )
                            $final[$key_final] = array($entry);
                        else
                            $final[$key_final] = "";
                    }
                }
                else
                {
                    if( $key_final == $fixit_value )
                        $final[$key_final] = array($entry);
                    else
                        $final[$key_final] = "";
                }
            }
        }

        return $final;
    }

}


