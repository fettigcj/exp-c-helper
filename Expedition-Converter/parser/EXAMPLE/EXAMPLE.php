<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */
class EXAMPLE extends PARSER
{
    public function vendor_main($config_filename, $pan, $routetable = "")
    {
        // This starts logging object (for console output)
        $this->logger->start();
        parent::preCheck();
        global $print;
        $print = TRUE;
        $path = "";
        $project = "";
        $config_path = $path . $config_filename;
        $filename = $config_filename;
        $filenameParts = pathinfo($config_filename);
        $verificationName = $filenameParts['filename'];
        $this->logger->increaseCompleted();
        $data = $this->clean_config($config_path, $project, $config_filename);
        $this->logger->increaseCompleted();
        $this->logger->setCompletedSilent();
        $v = $this->import_config($data, $pan); //This should update the $source
        CONVERTER::validate_interface_names($pan);
        CONVERTER::cleanup_unused_predefined_services($pan, "default");
    }

    function clean_config($config_path, $project, $config_filename)
    {
        $config_file = file($config_path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        $data = array();
        foreach( $config_file as $line => $names_line )
        {
            $data[] = $names_line;
        }
        #file_put_contents("test_output.txt", $data);
        #file_put_contents('test_output.txt', print_r($data, true));
        return $data;
    }

    /**
     * @param array $data
     * @param PANConf $pan
     */
    function import_config($data, $pan)
    {
        // We create/import a base-config first.
        // Use this vsys in the base config that was imported:
        $vsysName = "Example";
        $vsysID = 1;
        $v = $pan->findVSYS_by_displayName($vsysName);
        if( $v === null )
        {
            $v = $pan->findVirtualSystem('vsys' . $vsysID);
            $v->setAlternativeName($vsysName);
        }
        else
        {
            //create new vsys, search for latest ID
            do
            {
                $vsysID++;
                $v = $pan->findVirtualSystem('vsys' . $vsysID);
            } while( $v !== null );
            if( $v === null )
            {
                $v = $pan->createVirtualSystem(intval($vsysID), $vsysName . $vsysID);
                if( $v === null )
                {
                    derr("vsys" . $vsysID . " could not be created ? Exit\n");
                }
            }
        }
        echo PH::boldText("\nObject_network loaded\n");
        #get_object_network($data, $v);
        echo PH::boldText("\nsave_names:\n");
        echo PH::boldText("\nload custome application:\n");
        #load_customer_application($v);
        #get_interfaces($cisco_config_file, $source, $vsys, $template);
        echo PH::boldText("\nget interfaces:\n");
        #get_interfaces($data, $v);
        #get_static_routes($cisco_config_file, $source, $vsys, $template);
        echo PH::boldText("\nget static routes\n");
        #get_static_routes($data, $v);
        echo PH::boldText("\nObjectGroup_network loaded\n");
        #get_objectgroup_network2($data, $v);
        echo PH::boldText("\n SERVICES \n");

        echo PH::boldText("\nServices loaded\n");
        #get_object_service($data, $v);
        echo PH::boldText("\nObject_services loaded\n");
        #get_objectgroup_service($data, $v);
        echo PH::boldText("\nProtocol groups loaded\n");
        #get_protocol_groups($data, $v);
        echo PH::boldText("\nICMPGroups loaded\n");
        #get_icmp_groups($data, $v);
        echo PH::boldText("\nNAT twice 'before':\n");
        #get_twice_nats($data, $v, "before");
        echo PH::boldText("\nNAT objects:'\n");
        #get_objects_nat($data, $v);
        echo PH::boldText("\nNAT twice 'after':\n");
        #get_twice_nats($data, $v, "after");
        print "NAT counter: " . $v->natRules->count() . "\n";
        $userObj = array();
        echo PH::boldText("\nget objectgroup user\n");
        echo PH::boldText("\nget security Policy\n");
        #get_security_policies2($data, $v);
        return $v;
        clean_zone_any($source, $vsys);
    }
}
