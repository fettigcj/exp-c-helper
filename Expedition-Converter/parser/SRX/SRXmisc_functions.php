<?php


trait SRXmisc_functions
{

    function add_junos_services($v)
    {
        global $debug;
        global $print;


        global $projectdb;

        #$exists = $projectdb->query("SELECT id FROM services WHERE source='$source' AND vsys='$vsys' AND name_ext IN ('junos-ftp','junos-xnm-clear-text','junos-sun-rpc-status-udp');");
        #if ($exists->num_rows == 0) {
        $add_srv = array();
        $add_srv[] = array('junos-ftp', 'junos-ftp', '21', 'tcp');
        $add_srv[] = array('junos-tftp', 'junos-tftp', '69', 'udp');
        $add_srv[] = array('junos-rtsp', 'junos-rtsp', '554', 'tcp');
        $add_srv[] = array('junos-netbios-session', 'junos-netbios-session', '139', 'tcp');
        $add_srv[] = array('junos-ssh', 'junos-ssh', '22', 'tcp');
        $add_srv[] = array('junos-telnet', 'junos-telnet', '23', 'tcp');
        $add_srv[] = array('junos-smtp', 'junos-smtp', '25', 'tcp');
        $add_srv[] = array('junos-tacacs', 'junos-tacacs', '49', 'tcp');
        $add_srv[] = array('junos-tacacs-ds', 'junos-tacacs-ds', '65', 'tcp');
        $add_srv[] = array('junos-dhcp-client', 'junos-dhcp-client', '68', 'udp');
        $add_srv[] = array('junos-dhcp-server', 'junos-dhcp-server', '67', 'udp');
        $add_srv[] = array('junos-bootpc', 'junos-bootpc', '68', 'udp');
        $add_srv[] = array('junos-bootps', 'junos-bootps', '67', 'udp');
        $add_srv[] = array('junos-finger', 'junos-finger', '79', 'tcp');
        $add_srv[] = array('junos-http', 'junos-http', '80', 'tcp');
        $add_srv[] = array('junos-https', 'junos-https', '443', 'tcp');
        $add_srv[] = array('junos-pop3', 'junos-pop3', '110', 'tcp');
        $add_srv[] = array('junos-ident', 'junos-ident', '113', 'tcp');
        $add_srv[] = array('junos-nntp', 'junos-nntp', '119', 'tcp');
        $add_srv[] = array('junos-ntp', 'junos-ntp', '123', 'udp');
        $add_srv[] = array('junos-imap', 'junos-imap', '143', 'tcp');
        $add_srv[] = array('junos-imaps', 'junos-imaps', '993', 'tcp');
        $add_srv[] = array('junos-bgp', 'junos-bgp', '179', 'tcp');
        $add_srv[] = array('junos-ldap', 'junos-ldap', '389', 'tcp');
        $add_srv[] = array('junos-snpp', 'junos-snpp', '444', 'tcp');
        $add_srv[] = array('junos-biff', 'junos-biff', '512', 'udp');
        $add_srv[] = array('junos-who', 'junos-who', '513', 'udp');
        $add_srv[] = array('junos-syslog', 'junos-syslog', '514', 'udp');
        $add_srv[] = array('junos-printer', 'junos-printer', '515', 'tcp');
        $add_srv[] = array('junos-rip', 'junos-rip', '520', 'udp');
        $add_srv[] = array('junos-radius', 'junos-radius', '1812', 'udp');
        $add_srv[] = array('junos-radacct', 'junos-radacct', '1813', 'udp');
        $add_srv[] = array('junos-nfsd-tcp', 'junos-nfsd-tcp', '2049', 'tcp');
        $add_srv[] = array('junos-nfsd-udp', 'junos-nfsd-udp', '2049', 'udp');
        $add_srv[] = array('junos-cvspserver', 'junos-cvspserver', '2401', 'tcp');
        $add_srv[] = array('junos-ldp-tcp', 'junos-ldp-tcp', '646', 'tcp');
        $add_srv[] = array('junos-ldp-udp', 'junos-ldp-udp', '646', 'udp');
        $add_srv[] = array('junos-xnm-ssl', 'junos-xnm-ssl', '3220', 'tcp');
        $add_srv[] = array('junos-xnm-clear-text', 'junos-xnm-clear-text', '3221', 'tcp');
        $add_srv[] = array('junos-ike', 'junos-ike', '500', 'udp');
        $add_srv[] = array('junos-aol', 'junos-aol', '5190-5193', 'tcp');
        $add_srv[] = array('junos-chargen', 'junos-chargen', '19', 'udp');
        $add_srv[] = array('junos-dhcp-relay', 'junos-dhcp-relay', '67', 'udp');
        $add_srv[] = array('junos-discard', 'junos-discard', '9', 'udp');
        $add_srv[] = array('junos-dns-udp', 'junos-dns-udp', '53', 'udp');
        $add_srv[] = array('junos-dns-tcp', 'junos-dns-tcp', '53', 'tcp');
        $add_srv[] = array('junos-echo', 'junos-echo', '7', 'udp');
        $add_srv[] = array('junos-gopher', 'junos-gopher', '70', 'tcp');
        $add_srv[] = array('junos-gtp', 'junos-gtp', '2123', 'udp');
        $add_srv[] = array('junos-gnutella', 'junos-gnutella', '6346-6347', 'udp');
        $add_srv[] = array('junos-http-ext', 'junos-http-ext', '7001', 'tcp');
        $add_srv[] = array('junos-internet-locator-service', 'junos-internet-locator-service', '389', 'tcp');
        $add_srv[] = array('junos-ike-nat', 'junos-ike-nat', '4500', 'udp');
        $add_srv[] = array('junos-irc', 'junos-irc', '6660-6669', 'tcp');
        $add_srv[] = array('junos-l2tp', 'junos-l2tp', '1701', 'udp');
        $add_srv[] = array('junos-lpr', 'junos-lpr', '515', 'tcp');
        $add_srv[] = array('junos-mail', 'junos-mail', '25', 'tcp');
        $add_srv[] = array('junos-h323', 'junos-h323', '1720', 'tcp');
        $add_srv[] = array('junos-mgcp-ua', 'junos-mgcp-ua', '2427', 'udp');
        $add_srv[] = array('junos-mgcp-ca', 'junos-mgcp-ca', '2727', 'udp');
        $add_srv[] = array('junos-msn', 'junos-msn', '1863', 'tcp');
        $add_srv[] = array('junos-ms-rpc-tcp', 'junos-ms-rpc-tcp', '135', 'tcp');
        $add_srv[] = array('junos-ms-rpc-udp', 'junos-ms-rpc-udp', '135', 'udp');
        $add_srv[] = array('junos-ms-sql', 'junos-ms-sql', '1433', 'tcp');
        $add_srv[] = array('junos-nbname', 'junos-nbname', '137', 'udp');
        $add_srv[] = array('junos-nbds', 'junos-nbds', '138', 'udp');
        $add_srv[] = array('junos-nfs', 'junos-nfs', '111', 'udp');
        $add_srv[] = array('junos-ns-global', 'junos-ns-global', '15397', 'tcp');
        $add_srv[] = array('junos-ns-global-pro', 'junos-ns-global-pro', '15397', 'tcp');
        $add_srv[] = array('junos-nsm', 'junos-nsm', '69', 'udp');
        $add_srv[] = array('junos-pc-anywhere', 'junos-pc-anywhere', '5632', 'udp');
        $add_srv[] = array('junos-pptp', 'junos-pptp', '1723', 'tcp');
        $add_srv[] = array('junos-realaudio', 'junos-realaudio', '554', 'tcp');
        $add_srv[] = array('junos-sccp', 'junos-sccp', '2000', 'tcp');
        $add_srv[] = array('junos-sip', 'junos-sip', '5060', 'udp');
        $add_srv[] = array('junos-rsh', 'junos-rsh', '514', 'tcp');
        $add_srv[] = array('junos-smb', 'junos-smb', '139', 'tcp');
        $add_srv[] = array('junos-sql-monitor', 'junos-sql-monitor', '1434', 'udp');
        $add_srv[] = array('junos-sqlnet-v1', 'junos-sqlnet-v1', '1525', 'tcp');
        $add_srv[] = array('junos-sqlnet-v2', 'junos-sqlnet-v2', '1521', 'tcp');
        $add_srv[] = array('junos-sun-rpc-tcp', 'junos-sun-rpc-tcp', '111', 'tcp');
        $add_srv[] = array('junos-sun-rpc-udp', 'junos-sun-rpc-udp', '111', 'udp');
        $add_srv[] = array('junos-talk', 'junos-talk', '517', 'udp');
        $add_srv[] = array('junos-ntalk', 'junos-ntalk', '518', 'udp');
        $add_srv[] = array('junos-r2cp', 'junos-r2cp', '28672', 'udp');
        $add_srv[] = array('junos-uucp', 'junos-uucp', '540', 'udp');
        $add_srv[] = array('junos-vdo-live', 'junos-vdo-live', '7000-7010', 'udp');
        $add_srv[] = array('junos-vnc', 'junos-vnc', '5800', 'tcp');
        $add_srv[] = array('junos-wais', 'junos-wais', '210', 'tcp');
        $add_srv[] = array('junos-whois', 'junos-whois', '43', 'tcp');
        $add_srv[] = array('junos-winframe', 'junos-winframe', '1494', 'tcp');
        $add_srv[] = array('junos-x-windows', 'junos-x-windows', '6000-6063', 'tcp');
        $add_srv[] = array('junos-ymsg', 'junos-ymsg', '5050', 'tcp');
        $add_srv[] = array('junos-wxcontrol', 'junos-wxcontrol', '3578', 'tcp');
        $add_srv[] = array('junos-snmp-agentx', 'junos-snmp-agentx', '705', 'tcp');
        $add_srv[] = array('junos-stun', 'junos-stun', '3478-3479', 'udp');
        $add_srv[] = array('junos-tcp-any', 'junos-tcp-any', '1-65535', 'tcp');
        $add_srv[] = array('junos-udp-any', 'junos-udp-any', '1-65535', 'udp');


        /*Todo: SWASCHKUT 20191021 how to fix it?
        $add_srv[] = array( 'junos-persistent-nat','junos-persistent-nat','65535','255');
        $add_srv[] = array( 'junos-icmp-ping','junos-icmp-ping','','');
        $add_srv[] = array( 'junos-icmp-all','junos-icmp-all','','');
        $add_srv[] = array( 'junos-gre','junos-gre','','tcp');

        $add_srv[] = array( 'junos-ping','junos-ping','','');

        $add_srv[] = array( 'junos-smb-session','junos-smb-session','','');
        $add_srv[] = array( 'junos-ms-rpc-wmic','junos-ms-rpc-wmic','','');
        $add_srv[] = array( 'junos-ms-rpc','junos-ms-rpc','','');
        $add_srv[] = array( 'junos-cifs','junos-cifs','','');
        */
        $add_srv[] = array('tmp-junos-persistent-nat', 'tmp-junos-persistent-nat', '65535', 'tcp');
        $add_srv[] = array('tmp-junos-icmp-ping', 'tmp-junos-icmp-ping', '65535', 'tcp');
        $add_srv[] = array('tmp-junos-icmp-all', 'tmp-junos-icmp-all', '65535', 'tcp');
        $add_srv[] = array('tmp-junos-gre', 'tmp-junos-gre', '65535', 'tcp');

        $add_srv[] = array('tmp-junos-ping', 'tmp-junos-ping', '65535', 'tcp');

        $add_srv[] = array('tmp-junos-smb-session', 'tmp-junos-smb-session', '65535', 'tcp');
        $add_srv[] = array('tmp-junos-ms-rpc-wmic', 'tmp-junos-ms-rpc-wmic', '65535', 'tcp');
        $add_srv[] = array('tmp-junos-ms-rpc', 'tmp-junos-ms-rpc', '65535', 'tcp');
        $add_srv[] = array('tmp-junos-ms-rpc-epm', 'tmp-junos-ms-rpc-epm', '65535', 'tcp');
        $add_srv[] = array('tmp-junos-cifs', 'tmp-junos-cifs', '65535', 'tcp');

        $add_srv[] = array('tmp-junos-sun-rpc-rquotad-tcp', 'tmp-junos-sun-rpc-rquotad-tcp', '65535', 'tcp');
        $add_srv[] = array('tmp-junos-sun-rpc-rquotad-udp', 'tmp-junos-sun-rpc-rquotad-udp', '65535', 'udp');

        $add_srv[] = array('tmp-junos-sun-rpc-nlockmgr-tcp', 'tmp-junos-sun-rpc-nlockmgr-tcp', '65535', 'tcp');
        $add_srv[] = array('tmp-junos-sun-rpc-nlockmgr-udp', 'tmp-junos-sun-rpc-nlockmgr-udp', '65535', 'udp');

        $add_srv[] = array('tmp-junos-sun-rpc-portmap-tcp', 'tmp-junos-sun-rpc-portmap-tcp', '65535', 'tcp');//100000
        $add_srv[] = array('tmp-junos-sun-rpc-portmap-udp', 'junos-sun-rpc-portmap-udp', '65535', 'udp');//100000
        $add_srv[] = array('tmp-junos-sun-rpc-nfs-udp', 'tmp-junos-sun-rpc-nfs-udp', '65535', 'udp');//100003
        $add_srv[] = array('tmp-junos-sun-rpc-mountd-tcp', 'tmp-junos-sun-rpc-mountd-tcp', '65535', 'tcp');//100005
        $add_srv[] = array('tmp-junos-sun-rpc-mountd-udp', 'tmp-junos-sun-rpc-mountd-udp', '65535', 'udp');//100005
        $add_srv[] = array('tmp-junos-sun-rpc-ypbind-tcp', 'tmp-junos-sun-rpc-ypbind-tcp', '65535', 'tcp');//100007
        $add_srv[] = array('tmp-junos-sun-rpc-ypbind-udp', 'tmp-junos-sun-rpc-ypbind-udp', '65535', 'udp');//100007
        $add_srv[] = array('tmp-junos-sun-rpc-status-tcp', 'tmp-junos-sun-rpc-status-tcp', '65535', 'tcp');//100024
        $add_srv[] = array('tmp-junos-sun-rpc-status-udp', 'tmp-junos-sun-rpc-status-udp', '65535', 'udp');//100024


        foreach( $add_srv as $service )
        {
            /** @var Service $tmp_service */
            $tmp_service = $v->serviceStore->find($service[1]);
            if( $tmp_service !== FALSE )
            {
                if( $print )
                    print "create service Object: " . $service[1] . ", " . $service[3] . ", " . $service[2] . "\n";

                $tmp_service = $v->serviceStore->newService($service[1], $service[3], $service[2]);
            }
        }

        #}
    }


    /**
     * @param DOMElement $configRoot
     */
    function xml_validation($configRoot)
    {
        foreach( $configRoot->childNodes as $key => $childNode )
        {
            /** @var DOMElement $childNode */
            if( $childNode->nodeType != XML_ELEMENT_NODE )
                continue;

            $nodename = $childNode->nodeName;

            $array_not_in_focus = array(
                "version",
                "groups",
                "apply-groups",
                "system",
                "chassis",
                #"security",
                #"interfaces", //handled in interface part
                "snmp",
                "forwarding-options",
                "event-options",
                "routing-options",
                "protocols",
                "firewall",
                "applications");

            $array_in_focus = array(
                "logical-systems",
                "interfaces",
                "routing-instances",
                "security",
                "applications"

            );

            if( !in_array($nodename, $array_in_focus) )
            {
                mwarning("found nodename: '" . PH::boldText($nodename) . "' which is not in focus for this migration", null, FALSE);
            }
            else
            {
                print "covered: " . $nodename . "\n";
            }


            #print "|".$tmp_name."|\n";

        }
    }
}