<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */

//Todo: SonicOS >= 6.5.0 implementation

//Todo: EXP support
//https://www.sonicwall.com/support/knowledge-base/how-to-get-the-configurations-of-the-firewall-based-on-the-exporting-exp-file/170503330364045/


require_once("lib/pan_php_framework.php");

require_once('SRXinterfaces.php');
require_once('SRXvsys.php');

require_once('SRXaddress.php');
require_once('SRXapplications.php');
require_once('SRXzone.php');
require_once('SRXstaticRoute.php');
require_once('SRXpolicy.php');
require_once('SRXnat.php');

require_once('SRXmisc_functions.php');

require_once('SRXike.php');
require_once('SRXipsec.php');


class SRX extends PARSER
{
    use SRXaddress;
    use SRXapplications;
    use SRXinterfaces;
    use SRXnat;
    use SRXpolicy;
    use SRXstaticRoute;
    use SRXvsys;
    use SRXzone;
    use SRXmisc_functions;
    use SRXike;
    use SRXipsec;

    use SHAREDNEW;

    //Todo: 20190529
    //import routing, schedule, rbl

    //fix NAT issue for DST-nat, especially zone - related to routeing
    //fix interface -> change int name, change zone where int belong to, change vsys where int is member, change all NAT policy where int is used
    //      maybe introduce int references for zone, vsys, NAT policy
    //problem with not allowed object name: address-group -> __select '__select'
    //DST NAT, if translated-destination is group -> check if only one member and use this member

    public $debug = FALSE;
    public $print = FALSE;

    public $LSYS_bool = false;


    #public $string = file_get_contents ($config_filename);


//---------------------------------------------
//        Parser Logic starts here
//----------------------------------------------

    function vendor_main($config_filename, $pan, $routetable = "")
    {
        $this->logger->start();

        parent::preCheck();


        //swaschkut - tmp, until class migration is done
        global $print;
        $print = TRUE;

        //SONICWALL specific
        //------------------------------------------------------------------------
        $natdst_content = array();
        $project = "";
        $path = "";


        $config_path = $path . $config_filename;
        $filename = $config_filename;
        $filenameParts = pathinfo($config_filename);
        $verificationName = $filenameParts['filename'];

        $this->logger->increaseCompleted();

        $data = $this->clean_config($config_path, $project, $config_filename);

        $this->logger->increaseCompleted();
        $this->logger->setCompletedSilent();

        $this->import_config($data, $pan); //This should update the $source
        //------------------------------------------------------------------------
    }


    function clean_config($config_path, $project, $config_filename)
    {
        #CLEAN CONFIG FROM EMPTY LINES AND CTRL+M
        #$data =  implode(PHP_EOL, file($config_path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES));

        $data = file_get_contents($config_path);
        





        if( strpos($data, "rpc-reply xmlns:junos=") === FALSE )
        {
            $tmp_version = $this->find_string_between($data, "<version>", "</version>");
            $tmp_version_arr = explode("-", $tmp_version);
            $tmp_version = $tmp_version_arr[0];
            //"<version>12.3X48-D35.7</version>"

            $tmp_start = "<rpc-reply xmlns:junos=\"http://xml.juniper.net/junos/" . $tmp_version . "/junos\">\n";
            $tmp_end = "\n</rpc-reply>";

            $data = $tmp_start . $data . $tmp_end;
        }



        $doc = new DOMDocument();
        $doc->loadXML($data);


        $root = $doc->documentElement; // Root node
        $configRoot = $root;

        $configRoot = DH::findFirstElement('configuration', $configRoot);
        if( $configRoot === FALSE )
            derr("<configuration> was not found", $root);

        DH::makeElementAsRoot($configRoot, $root);

        return $configRoot;
    }

    public function prepare_address($address, $output_array)
    {
        foreach( $output_array as $tmp_address_entry )
        {
            $tmp_address_entry = $this->strip_hidden_chars($tmp_address_entry);
            $tmp_address_entry = str_replace("address-object ", "", $tmp_address_entry);
            $tmp_address_entry = str_replace("    ", " ", $tmp_address_entry);
            $tmp_address_entry = str_replace(" exit", "", $tmp_address_entry);

            $address[] = $tmp_address_entry;
        }
        return $address;
    }


    public function import_config($configRoot, $pan)
    {
        global $LSYS_bool;

        $root = $configRoot;

        $this->xml_validation($configRoot);


        //todo: import all logical-systems




        $lsystems = DH::findXPath( '/logical-systems', $root );
        #print "counter: ".count($lsystems)."\n";
        if( count($lsystems) > 0 )
        {
            $LSYS_bool = true;
            //import the below info into shared
            $v = $this->create_vsys($pan);
            $this->import_lsys( $v, $root, true );

            //create new vsys per lsys and import

            foreach( $lsystems as $lsys )
            {
                if( $lsys->nodeType != 1 ) continue;


                $name = DH::findFirstElement('name', $lsys);
                print "create LSYS: ".$name->nodeValue."\n";
                $v = $this->create_vsys($pan, $name->nodeValue);

                $this->import_lsys( $v, $lsys );
            }
        }
        else
        {
            //import the below info into vsys1
            $v = $this->create_vsys($pan);
            $this->import_lsys( $v, $root );
        }


        echo PH::boldText("\nVALIDATION - interface name and change into PAN-OS confirm naming convention\n");
        CONVERTER::validate_interface_names($pan);

        #$this->logger->increaseCompleted();

        echo PH::boldText("\nVALIDATION - Region name must not be used as a address / addressgroup object name\n");
        CONVERTER::validation_region_object($pan);
        #$this->logger->increaseCompleted();

        //if security rule count is very high => memory leak problem
        //Todo: where to place custom table for app-migration if needed
        echo PH::boldText("\nVALIDATION - replace tmp services with APP-id if possible\n");
        print "todo\n";
        CONVERTER::AppMigration($pan);
        #$this->logger->increaseCompleted();
    }

    public function import_lsys( $v, $root, $shared = false )
    {
        //IMPORT all which can be also none LSYS info


        print "\nadd_junos_services:\n";
        $this->add_junos_services($v);


        print "\nInterfaces:\n";
        $interfaceRoot = DH::findFirstElement('interfaces', $root);
        if( $interfaceRoot === FALSE )
            mwarning("<interfaces> was not found", $root);
        else
            $this->get_Interfaces_new($interfaceRoot, $v, $shared);


        print "\nstatic Routes:\n";
// - static routes
//    $vr = $configuration->xpath("//configuration/routing-instances/instance");
        $staticrouteRoot = DH::findFirstElement('routing-instances', $root);
        if( $staticrouteRoot === FALSE )
            mwarning("<routing-instances> was not found", $root);
        else
            $this->get_XML_staticRoutes($staticrouteRoot, $v, $shared);


        $securityRoot = DH::findFirstElement('security', $root);
        if( $securityRoot === FALSE )
            derr("<security> was not found", $root);

        print "\nAddress objects:\n";
        $addressbookRoot = DH::findFirstElement('address-book', $securityRoot);
        if( $addressbookRoot !== FALSE )
        {
            $this->get_XML_Zones_Address_All_new2($addressbookRoot, $v);
        }
        else
            mwarning("<address-book> was not found directly under SECURITY", null, FALSE);


        print "\nZone objects: \n";
        $zonesRoot = DH::findFirstElement('zones', $securityRoot);
        if( $zonesRoot === FALSE )
            derr("<zones> was not found", $securityRoot);
        else
            $this->get_XML_Zones($zonesRoot, $v, $shared);


        print "\nService objects:\n";
        $applicationsRoot = DH::findFirstElement('applications', $root);
        if( $applicationsRoot === FALSE )
            mwarning("<applications> was not found", $root);
        else
            $this->get_XML_Applications2($applicationsRoot, $v);

        //security -> ike
        print "\nIKE objects:\n";
        $ikeRoot = DH::findFirstElement('ike', $securityRoot);
        if( $ikeRoot === FALSE )
            mwarning("<ike> was not found", null, FALSE);
        else
            $this->get_XML_IKE($ikeRoot, $v, $shared);

        //security -> ipsec
        print "\nIPSEC objects:\n";
        $ipsecRoot = DH::findFirstElement('ipsec', $securityRoot);
        if( $ipsecRoot === FALSE )
            mwarning("<ipsec> was not found", null, FALSE);
        else
            $this->get_XML_IPSEC($ipsecRoot, $v, $shared);


        print "\npolicies: \n";
        $policiesRoot = DH::findFirstElement('policies', $securityRoot);
        //$address = $configuration->xpath("/configuration/security/policies/global");
        //$address = $configuration->xpath("/configuration/security/policies/policy");
        if( $policiesRoot === FALSE )
            mwarning("<policies> was not found", $securityRoot);
        else
            $this->get_XML_policies2($policiesRoot, $v);

        //Todo: move global rules to TOP

        print "\nnat: \n";
        $natRoot = DH::findFirstElement('nat', $securityRoot);
        if( $natRoot === FALSE )
            mwarning("<nat> was not found", $securityRoot);
        else
        {
            //Todo: static nat rules, recalculate zones, check if settings are correct as orig DNAT with bidir was moved to SNAT with bidir
            //Todo: if source nat rule name is already available, add source destination snat to  dest rule nat, and disable source nat rule
            $this->get_XML_nat2($natRoot, $v);
        }


        //Todo: nat rule move : static, destination, source


        //calculate zones
        //zone recalculation based on NAT rules
        //rename interfaces

    }
}
