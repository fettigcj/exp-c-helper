<?php


trait SRXzone
{
    /**
     * @param DomElement $configRoot
     * @param VirtualSystem $v
     * @return null
     */
    function get_XML_Zones($configRoot, $v, $shared)
    {
        global $debug;
        global $print;
        //$configRoot /configuration/security/zones

/*
        if( $shared )
            $pan = $v;
        else
            */
            $pan = $v->owner;

        /*
        <functional-zone>
            <management>
                <interfaces>
                    <name>reth3.0</name>
                    <host-inbound-traffic>
                        <system-services>
                            <name>ping</name>
                        </system-services>
                    </host-inbound-traffic>
                </interfaces>
            </management>
        </functional-zone>
        <security-zone>
            <name>zonePublicEbiz</name>
            <screen>zonePublicEbiz</screen>
            <interfaces>
                <name>reth1.2700</name>
            </interfaces>
            <interfaces>
                <name>reth2.2709</name>
            </interfaces>
        </security-zone>
         */

        foreach( $configRoot->childNodes as $key => $childNode )
        {
            /** @var DOMElement $childNode */
            if( $childNode->nodeType != XML_ELEMENT_NODE )
                continue;

            $nodeName = $childNode->nodeName;

            if( $nodeName == 'security-zone' )
            {

                foreach( $childNode->childNodes as $key => $child )
                {
                    /** @var DOMElement $childNode */
                    if( $child->nodeType != XML_ELEMENT_NODE )
                        continue;

                    $nodeName = $child->nodeName;

                    if( $nodeName == 'name' )
                    {
                        $zonename = $child->textContent;
                        //create Zone
                        $tmp_zone = $v->zoneStore->find($zonename);
                        if( $tmp_zone == null )
                        {
                            print " - create Zone: " . $zonename . "\n";
                            $tmp_zone = $v->zoneStore->newZone($zonename, 'layer3');
                        }
                    }
                    elseif( $nodeName == 'screen' )
                    {
                        //what todo????
                    }
                    elseif( $nodeName == 'interfaces' )
                    {
                        $interfacename = DH::findFirstElement('name', $child);
                        $interfacename = $interfacename->textContent;
                        //search for interface and add to zone

                        $tmp_interface = $pan->network->ethernetIfStore->findOrCreate($interfacename);
                        /*
                        if( $tmp_interface == null )
                        {
                            $tmp_interface = $pan->network->ethernetIfStore
                        }
                        */

                        print "    - add interface: " . $tmp_interface->name() . "\n";
                        $tmp_zone->attachedInterfaces->addInterface($tmp_interface);

                    }
                    elseif( $nodeName == "address-book" )
                    {
                        $this->get_XML_Zones_Address_All_new2($child, $v);
                    }
                }


            }
            elseif( $nodeName == 'functional-zone' )
            {

            }

        }
    }
}