<?php



trait SRXapplications
{
#function get_XML_Zones_Address_All_new($configuration, $vsys, $source, $template, &$objectsInMemory) {
    /**
     * @param DomElement $configRoot
     * @param VirtualSystem $v
     * @return null
     */
    function get_XML_Applications2($configRoot, $v)
    {
        global $debug;
        global $print;
        //$configRoot /configuration/security/address-book


        foreach( $configRoot->childNodes as $key => $childNode )
        {
            /** @var DOMElement $childNode */
            if( $childNode->nodeType != XML_ELEMENT_NODE )
                continue;

            $nodeName = $childNode->nodeName;

            if( $nodeName == "application" )
            {

                // - service
//$address = $configuration->xpath("/configuration/applications/application");
                /*
                <application>
                    <name>rco</name>
                    <term>
                        <name>rco</name>
                        <protocol>tcp</protocol>
                        <source-port>0-65535</source-port>
                        <destination-port>798-798</destination-port>
                    </term>
                    <term>
                        <name>798-798</name>
                        <protocol>tcp</protocol>
                        <source-port>0-65535</source-port>
                        <destination-port>798-798</destination-port>
                    </term>
                    <term>
                        <name>799-799</name>
                        <protocol>tcp</protocol>
                        <source-port>0-65535</source-port>
                        <destination-port>799-799</destination-port>
                    </term>
                </application>
                 */


                $name = "";
                $protocol = 'tcp';
                $dstports = "65535";
                $tmpservice = null;
                $tmpservice2 = null;

                $tcpudp = false;
                foreach( $childNode->childNodes as $key => $child )
                {
                    /** @var DOMElement $childNode */
                    if( $child->nodeType != XML_ELEMENT_NODE )
                        continue;

                    $nodeName = $child->nodeName;

                    if( $nodeName == 'name' )
                    {
                        $tmpproto = "";

                        $name = $child->textContent;
                        $tcpudpname = $name;
                        #print "addressname: ".$name."\n";

                        $tmpservice = $v->serviceStore->find($name);

                        if( $tmpservice == null )
                        {
                            print "\n - create service object: " . $name . "\n";
                            $tmpservice = $v->serviceStore->newService($name, $protocol, $dstports);
                        }
                        else
                        {
                            mwarning("address object with name: " . $name . " already available\n");
                            continue;
                        }
                    }
                    elseif( $nodeName == 'protocol' )
                    {
                        $protocol = $child->textContent;
                        #print "addressname: ".$name."\n";

                        if( $tmpproto == "" )
                        {
                            if( $protocol == "tcp" || $protocol == "udp" )
                                $tmpservice->setProtocol($protocol);
                            else
                            {
                                $tmpservice2 = $v->serviceStore->find("tmp-" . $tmpservice->name());
                                if( $tmpservice2 == null )
                                {
                                    print "    - change name to: 'tmp-" . $tmpservice->name() . "'\n";
                                    $tmpservice->setName("tmp-" . $tmpservice->name());
                                }
                            }
                        }
                        elseif( $tmpservice->protocol() != $protocol )
                        {
                            $tcpudp = true;

                            //1) rename existing service - add protocol at beginning
                            $tmpservice->setName( strtoupper($tmpservice->protocol())."-" . $tmpservice->name());

                            $tmpname = strtoupper($protocol)."-" . $tcpudpname;
                            $tmpservice2 = $v->serviceStore->find($tmpname );
                            if( $tmpservice2 === null )
                            {
                                print "\n - create service object: " . $tmpname . "\n";
                                $tmpservice2 = $v->serviceStore->newService($tmpname, $protocol, $dstports);

                            }
                            //2) create new object with - protocol_NAME -> set protocol and port

                            // create address group with name
                            //add both members
                            $tmp_servicegroup = $v->serviceStore->find( $tcpudpname );
                            if( $tmp_servicegroup == null )
                            {
                                if( $print )
                                    print "create servicegroup Object: ".$tcpudpname."\n";
                                $tmp_servicegroup = $v->serviceStore->newServiceGroup( $tcpudpname );
                                $tmp_object = $v->serviceStore->find('TCP-'.$tcpudpname);
                                if( $tmp_object !== null )
                                {
                                    $tmp_servicegroup->addMember($tmp_object);
                                }
                                $tmp_object = $v->serviceStore->find('UDP-'.$tcpudpname);
                                if( $tmp_object !== null )
                                {
                                    $tmp_servicegroup->addMember($tmp_object);
                                }
                            }
                        }

                    }
                    elseif( $nodeName == 'source-port' )
                    {
                        $sourceport = $child->textContent;
                        #print "addressname: ".$name."\n";

                        if( $sourceport != "0-65535" && $sourceport != "1-65535" )
                            mwarning("source port must be set");
                    }
                    elseif( $nodeName == 'destination-port' )
                    {
                        $destinationport = $child->textContent;
                        #print "addressname: ".$name."\n";

                        $tmpdstport = $tmpservice->getDestPort();

                        if( !$tcpudp )
                            $tmpservice->setDestPort($tmpdstport);
                        else
                            $tmpservice2->setDestPort($tmpdstport);

                    }
                    elseif( $nodeName == 'description' )
                    {
                        $description = $child->textContent;
                        #print "addressname: ".$name."\n";

                        print "   - set description: " . $description . "\n";
                        if( !$tcpudp )
                            $tmpservice->setDescription($description);
                        else
                            $tmpservice2->setDescription($description);
                    }

                    elseif( $nodeName == 'term' )
                    {
                        /*
                        <term>
                            <name>798-798</name>
                            <protocol>tcp</protocol>
                            <source-port>0-65535</source-port>
                            <destination-port>798-798</destination-port>
                        </term>
                         */
                        foreach( $child->childNodes as $key2 => $child2 )
                        {
                            /** @var DOMElement $childNode */
                            if( $child2->nodeType != XML_ELEMENT_NODE )
                                continue;

                            $nodeName2 = $child2->nodeName;

                            if( $nodeName2 == 'name' )
                            {
                                $name = $child2->textContent;
                                #print "addressname: ".$name."\n";
                            }
                            elseif( $nodeName2 == 'protocol' )
                            {
                                $protocol = $child2->textContent;
                                #print "addressname: ".$name."\n";

                                if( $tmpproto == "" )
                                {
                                    if( $protocol == "tcp" || $protocol == "udp" )
                                    {
                                        $tmpproto = $protocol;
                                        if( $tmpservice->isGroup() )
                                            print "prob: ".$tmpservice->name()."\n";
                                        $tmpservice->setProtocol($protocol);
                                    }

                                    else
                                    {
                                        $tmpservice2 = $v->serviceStore->find("tmp-" . $tmpservice->name());
                                        if( $tmpservice2 == null )
                                        {
                                            print "    - change name to: 'tmp-" . $tmpservice->name() . "'\n";
                                            $tmpservice->setName("tmp-" . $tmpservice->name());
                                        }
                                    }
                                }

                                elseif( $tmpservice->protocol() != $protocol )
                                {
                                    $tcpudp = true;
                                    //1) rename existing service - add protocol at beginning
                                    $tmpservice->setName( strtoupper($tmpservice->protocol())."-" . $tmpservice->name());

                                    $tmpname = strtoupper($protocol)."-" . $tcpudpname;
                                    $tmpservice2 = $v->serviceStore->find($tmpname );
                                    if( $tmpservice2 === null )
                                    {
                                        print "\n - create service object: " . $tmpname . "\n";
                                        $tmpservice2 = $v->serviceStore->newService($tmpname, $protocol, $dstports);
                                    }
                                    //2) create new object with - protocol_NAME -> set protocol and port

                                    // create address group with name
                                    //add both members
                                    $tmp_servicegroup = $v->serviceStore->find( $tcpudpname );
                                    if( $tmp_servicegroup == null )
                                    {
                                        if( $print )
                                            print "create servicegroup Object: ".$tcpudpname."\n";
                                        $tmp_servicegroup = $v->serviceStore->newServiceGroup( $tcpudpname );
                                        $tmp_object = $v->serviceStore->find('TCP-'.$tcpudpname);
                                        if( $tmp_object !== null )
                                        {
                                            $tmp_servicegroup->addMember($tmp_object);
                                        }
                                        $tmp_object = $v->serviceStore->find('UDP-'.$tcpudpname);
                                        if( $tmp_object !== null )
                                        {
                                            $tmp_servicegroup->addMember($tmp_object);
                                        }
                                    }
                                }

                            }
                            elseif( $nodeName2 == 'source-port' )
                            {
                                $sourceport = $child2->textContent;
                                #print "addressname: ".$name."\n";

                                $tmpsrcport = $tmpservice->getSourcePort();


                                if( $sourceport != "0-65535" && $sourceport != "1-65535" )
                                {

                                    if( strpos($tmpsrcport, $sourceport) === FALSE )
                                    {
                                        if( $tmpsrcport == "" )
                                            $port = $sourceport;
                                        else
                                            $port = $tmpsrcport . "," . $sourceport;

                                        print "     - set SrcPort to: " . $port . "\n";
                                        if( !$tcpudp )
                                            $tmpservice->setSourcePort($port);
                                        else
                                            $tmpservice2->setSourcePort($port);
                                    }
                                }

                            }
                            elseif( $nodeName2 == 'destination-port' )
                            {
                                $destinationport = $child2->textContent;
                                #print "addressname: ".$name."\n";
                                if( !$tcpudp )
                                    $tmpdstport = $tmpservice->getDestPort();
                                else
                                    $tmpdstport = $tmpservice2->getDestPort();
                                if( $tmpdstport == "65535" )
                                    $tmpdstport = "";

                                if( strpos($tmpdstport, $destinationport) === FALSE )
                                {
                                    if( $tmpdstport == "" )
                                        $port = $destinationport;
                                    else
                                        $port = $tmpdstport . "," . $destinationport;

                                    print "     - set DestPort to: " . $port . "\n";
                                    if( !$tcpudp )
                                        $tmpservice->setDestPort($port);
                                    else
                                        $tmpservice2->setDestPort($port);
                                }

                            }
                        }

                    }
                    elseif( $nodeName == 'inactivity-timeout' )
                    {
                        mwarning("inactivity-timeout - not implemented yet", $child);
                    }
                    else
                        mwarning("was not found", $child);
                }
            }
            elseif( $nodeName == "application-set" )
            {
                // - ServicesGroups
                //$address = $configuration->xpath("/configuration/applications/application-set");
                /*
                <application-set>
                    <name>appSrvRobotToDevicesGrp</name>
                    <application>
                        <name>junos-icmp-all</name>
                    </application>
                    <application>
                        <name>snmp-161</name>
                    </application>
                    <application>
                        <name>ssh-22</name>
                    </application>
                </application-set>
                 */
                foreach( $childNode->childNodes as $key => $child )
                {
                    /** @var DOMElement $childNode */
                    if( $child->nodeType != XML_ELEMENT_NODE )
                        continue;

                    #$tmpservicegroup = null;

                    $nodeName = $child->nodeName;

                    if( $nodeName == 'name' )
                    {
                        $name = $child->textContent;
                        #print "addressname: ".$name."\n";

                        $tmpservicegroup = $v->serviceStore->find($name);

                        if( $tmpservicegroup == null )
                        {
                            print "\n - create servicegroup object: " . $name . "\n";
                            $tmpservicegroup = $v->serviceStore->newServiceGroup($name);
                        }
                        else
                        {
                            mwarning("service object with name: " . $name . " already available\n");
                            continue;
                        }


                    }
                    elseif( $nodeName == 'application' )
                    {
                        $servicename = DH::findFirstElement('name', $child);
                        if( $servicename === FALSE )
                            derr("<name> was not found", $child);

                        $servicename = $servicename->textContent;
                        #print "addressname: ".$name."\n";

                        $servicename = $this->truncate_names( $this->normalizeNames( $servicename ) );

                        #print "   - find service : ".$servicename."\n";
                        $tmpservice = $v->serviceStore->find($servicename);
                        if( $tmpservice == null )
                        {
                            #print "   - find service : tmp-".$servicename."\n";
                            $tmpservice = $v->serviceStore->find("tmp-" . $servicename);
                        }

                        if( $tmpservice != null )
                        {
                            print "   - add object: " . $tmpservice->name() . "\n";
                            $tmpservicegroup->addMember($tmpservice);
                        }
                        else
                            mwarning("object not found: " . $servicename);
                    }
                }
            }
            else
                mwarning("was not found", $childNode);
        }
    }
}