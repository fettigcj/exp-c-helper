<?php


trait SRXinterfaces
{
#function get_Interfaces_new($configuration, $vsys, $source, $template) {
    function get_Interfaces_new($configRoot, $v, $shared = false)
    {
        global $debug;
        global $print;

        global $LSYS_bool;
        //$configRoot /configuration/interfaces

        /*
        if( $shared )
            $pan = $v;
        else
            */
        $pan = $v->owner;

        foreach( $configRoot->childNodes as $key => $childNode )
        {
            /** @var DOMElement $childNode */
            if( $childNode->nodeType != XML_ELEMENT_NODE )
                continue;

            $interfaceName = "";
            $comment = "";
            $create_subinterface = FALSE;
            $tmp_int_main = null;

            foreach( $childNode->childNodes as $node )
            {
                if( $node->nodeType != XML_ELEMENT_NODE )
                    continue;

                $nodeName = $node->nodeName;

                if( $LSYS_bool && (
                    preg_match("/^ge-/", $interfaceName)
                    || preg_match("/^ae/", $interfaceName)
                    || preg_match("/^fab/", $interfaceName)
                    )
                )
                {
                    continue;
                }


                if( $nodeName == 'name' )
                {
                    $interfaceName = $node->textContent;
                    /** @var EthernetInterface $tmp_int_main */
                    $tmp_int_main = $pan->network->findInterface($interfaceName);
                    if( $tmp_int_main === null )
                    {
                        /*
                        if( preg_match("/^lo/", $interfaceName) )
                        {
                            $tmp_int_main = $pan->network->loopbackIfStore->newLoopbackIf($interfaceName);
                        }
                        else
                            */
                        if( $LSYS_bool && (
                                preg_match("/^ge-/", $interfaceName)
                                || preg_match("/^ae/", $interfaceName)
                                || preg_match("/^fab/", $interfaceName)
                            )
                        )
                        {
                            continue;
                        }
                        else
                        {
                            $tmp_int_main = $pan->network->ethernetIfStore->newEthernetIf($interfaceName, 'layer3');

                            $v->importedInterfaces->addInterface($tmp_int_main);

                            if( $print )
                                print "* create interface with name: " . $interfaceName . "\n";
                        }

                    }




                }
                elseif( $nodeName == "description" )
                {
                    $comment = $node->textContent;
                    if( $print )
                        print "  * description: " . $comment . "\n";
                    $tmp_int_main->description = $comment;
                }
                elseif( $nodeName == "vlan-tagging" )
                {
                    $create_subinterface = TRUE;
                    #print "create subinterfaces\n";
                }
                elseif( $nodeName == "unit" )
                {
                    #print_xmlNode( $node );

                    $tmp_sub = null;
                    foreach( $node->childNodes as $unit )
                    {
                        if( $unit->nodeType != XML_ELEMENT_NODE )
                            continue;

                        $nodeName = $unit->nodeName;

                        if( $nodeName == 'name' )
                        {
                            $tmpunit = $unit->textContent;
                            $tmpame = $interfaceName . "." . $tmpunit;

                            if( $tmpunit != 0 )
                            {
                                $tmp_sub = $pan->network->findInterface($tmpame);
                                if( $tmp_sub === null )
                                {
                                    if( $print )
                                        print "\n  * add subinterface: with name/vlan: " . $tmpame . "\n";
                                    $tmp_sub = $tmp_int_main->addSubInterface($tmpunit);
                                    $create_subinterface = TRUE;


                                    $v->importedInterfaces->addInterface($tmp_sub);
                                }

                            }
                            else
                            {
                                #print "add IP to main interface\n";
                            }

                        }
                        elseif( $nodeName == "description" )
                        {
                            $comment = $unit->textContent;

                            if( is_object( $tmp_sub ) )
                            {
                                if( $print )
                                    print "    * description: " . $comment . "\n";
                                $tmp_sub->description = $comment;
                            }


                        }
                        elseif( $nodeName == "vlan-id" )
                        {
                            $unitTag = $unit->textContent;
                        }
                        elseif( $nodeName == "family" )
                        {
                            $tmp_inet = DH::findFirstElement('inet', $unit);

                            if( is_object( $tmp_inet ) )
                                foreach( $tmp_inet->childNodes as $child_address )
                            {
                                if( $unit->nodeType != XML_ELEMENT_NODE )
                                    continue;

                                $nodeName = $child_address->nodeName;
                                if( $nodeName == "address" )
                                {
                                    $tmp_ip_address = DH::findFirstElement('name', $child_address);
                                    $tmp_ip_address = $tmp_ip_address->textContent;

                                    if( $create_subinterface )
                                    {
                                        $tmp_sub->addIPv4Address($tmp_ip_address);
                                    }
                                    else
                                    {
                                        $tmp_int_main->addIPv4Address($tmp_ip_address);
                                    }
                                    if( $print )
                                        print "    * add IP-adddress: " . $tmp_ip_address . "\n";
                                }
                            }
                        }
                    }
                }
                elseif( $nodeName == "gigether-options" || $nodeName == "redundant-ether-options" || $nodeName == "gratuitous-arp-reply" || $nodeName == "fabric-options" )
                {
                    mwarning("node: '" . $nodeName . "' not covered\n", null, FALSE);
                }
                else
                {
                    mwarning("node: '" . $nodeName . "' not covered\n");
                }
            }
        }
    }

}
