<?php


trait SRXvsys
{
    /**
     * @param PANConf $pan
     * @return VirtualSystem
     */
    function create_vsys($pan, $lsysName = null )
    {
        $vsysName = "SRX";
        $vsysID = 1;

        $v = $pan->findVSYS_by_displayName($vsysName);
        if( $v === null )
        {
            #print "VSYS: ".$vsysID." already available - check displayName ".$vsysName."\n";
            $v = $pan->findVirtualSystem('vsys' . $vsysID);
            $v->setAlternativeName($vsysName);
        }
        else
        {
            //create new vsys, search for latest ID
            do
            {
                $vsysID++;
                $v = $pan->findVirtualSystem('vsys' . $vsysID);
            } while( $v !== null );

            if( $v === null )
            {
                $v = $pan->createVirtualSystem(intval($vsysID), $vsysName . $vsysID);
                if( $lsysName !== null )
                    $v->setAlternativeName($lsysName);

                if( $v === null )
                {
                    derr("vsys" . $vsysID . " could not be created ? Exit\n");
                }
                print "create VSYS: ".$v->name()." - ".$v->alternativeName()."\n";
            }
        }

        return $v;
    }
}