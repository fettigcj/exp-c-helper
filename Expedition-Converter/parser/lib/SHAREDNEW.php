<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */


trait SHAREDNEW
{
    //SVEN

    public $toolname = "PAN-PHP-FRAMEWORK";

    public function strip_hidden_chars($str)
    {
        $chars = array("\r\n", "\n", "\r", "\t", "\0", "\x0B");

        $str = str_replace($chars, "", $str);

        #return preg_replace('/\s+/',' ',$str);
        return $str;
    }

    public function find_string_between($line, $needle1, $needle2 = "--END--")
    {
        $needle_length = strlen($needle1);
        $pos1 = strpos($line, $needle1);

        if( $needle2 !== "--END--" )
            $pos2 = strpos($line, $needle2);
        else
            $pos2 = strlen($line);

        $finding = substr($line, $pos1 + $needle_length, $pos2 - ($pos1 + $needle_length));

        return $finding;
    }

    public function IKE_IPSEC_name_validation($IKE_name, $string = "")
    {
        if( preg_match('[^\d]', $IKE_name) )
        {
            //NO digit allowed at the beginning of a name
            $IKE_name = "X_" . $IKE_name;
            #derr( 'no digit allowed at the beginning of a IKE gateway name' );
        }

        if( preg_match('/[^0-9a-zA-Z_\-]/', $IKE_name) )
        {
            //NO blank allowed in gateway name
            //NO other characters are allowed as seen here
            $IKE_name = preg_replace('/[^0-9a-zA-Z_\-]/', "", $IKE_name);
            if( $string !== "" )
            {
                #print " *** new IKE / IPSEC name: ".$IKE_name." in ".$string." config \n";
                #mwarning( 'Name will be replaced with: '.$name."\n", null, false );

            }
        }

        return $IKE_name;
    }


////////////////


    public function truncate_names($longString)
    {
        global $source;
        $variable = strlen($longString);

        if( $variable < 63 )
        {
            return $longString;
        }
        else
        {
            $separator = '';
            $separatorlength = strlen($separator);
            $maxlength = 63 - $separatorlength;
            $start = $maxlength;
            $trunc = strlen($longString) - $maxlength;
            $salida = substr_replace($longString, $separator, $start, $trunc);

            if( $salida != $longString )
            {
                //Todo: swaschkut - xml attribute adding needed
                #add_log('warning', 'Names Normalization', 'Object Name exceeded >63 chars Original:' . $longString . ' NewName:' . $salida, $source, 'No Action Required');
            }
            return $salida;
        }
    }

    public function normalizeNames($nameToNormalize)
    {
        $nameToNormalize = trim($nameToNormalize);
        //$nameToNormalize = preg_replace('/(.*) (&#x2013;) (.*)/i', '$0 --> $1 - $3', $nameToNormalize);
        //$nameToNormalize = preg_replace("/&#x2013;/", "-", $nameToNormalize);
        $nameToNormalize = preg_replace("/[\/]+/", "_", $nameToNormalize);
        $nameToNormalize = preg_replace("/[,]+/", "_", $nameToNormalize);
        $nameToNormalize = preg_replace("/[^a-zA-Z0-9-_. *]+/", "", $nameToNormalize);
        $nameToNormalize = preg_replace("/[\s]+/", " ", $nameToNormalize);

        $nameToNormalize = preg_replace("/^[-]+/", "", $nameToNormalize);
        $nameToNormalize = preg_replace("/^[_]+/", "", $nameToNormalize);

        $nameToNormalize = preg_replace('/\(|\)/', '', $nameToNormalize);

        return $nameToNormalize;
    }

    public function normalizeComments($nameToNormalize)
    {
        $nameToNormalize = preg_replace("/[^a-zA-Z0-9-_.:\,\s]+/", "", $nameToNormalize);
        return $nameToNormalize;
    }

    public function ip_version($ip)
    {

        $ip = explode("/", $ip);
        $ip = $ip[0];

        if( filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) )
        {
            return "v4";
        }
        elseif( filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) )
        {
            return "v6";
        }
        else
        {
            return "noip";
        }
    }

    public function mask2cidrv4($mask)
    {
        $long = ip2long($mask);
        $base = ip2long('255.255.255.255');
        return 32 - log(($long ^ $base) + 1, 2);
    }

    public function truncate_tags($longString)
    {
        global $source;
        $variable = strlen($longString);

        if( $variable < 127 )
        {
            return $longString;
        }
        else
        {
            $separator = '';
            $separatorlength = strlen($separator);
            $maxlength = 127 - $separatorlength;
            $start = $maxlength;
            $trunc = strlen($longString) - $maxlength;
            $salida = substr_replace($longString, $separator, $start, $trunc);

            if( $salida != $longString )
            {
                #add_log('warning', 'Names Normalization', 'Object Name exceeded >127 chars Original:' . $longString . ' NewName:' . $salida, $source, 'No Action Required', '', '', '');
            }
            return $salida;
        }
    }

    public function truncate_rulenames($longString)
    {

        global $source;

        $variable = strlen($longString);

        $max = getMaxLengthRuleName($source);

        /*$version = calcVersion($source);

        if($version < 8.1){
            $max = 31;
        }elseif($version >= 8.1){
            $max = 63;
        }*/

        if( $variable < $max )
        {
            return $longString;
        }
        else
        {
            $separator = '';
            $separatorlength = strlen($separator);
            $maxlength = $max - $separatorlength;
            $start = $maxlength;
            $trunc = strlen($longString) - $maxlength;
            $salida = substr_replace($longString, $separator, $start, $trunc);

            if( $salida != $longString )
            {
                add_log('warning', 'Names Normalization', 'Object Name exceeded > ' . $max . ' chars Original:' . $longString . ' NewName:' . $salida, $source, 'No Action Required');
            }
            return $salida;
        }
    }

    public function truncate_zone_names_cisco($longString, $version = "6")
    {
        global $projectname;
        global $source;
        global $projectdb;
        $projectdb = selectDatabase($projectname);

        $variable = strlen($longString);

        if( $variable < 14 )
        {
            //return $longString;
        }
        else
        {
            $maxChars = 15;
            $salida = substr_replace($longString, '', $maxChars / 2, $variable - $maxChars);
            if( $salida != $longString )
            {
                add_log('warning', 'Names Normalization', 'Zone Name exceeded >15 chars Original:[' . $longString . '] NewName:[' . $salida . ']', $source, 'No Action Required');
                $projectdb->query("UPDATE security_rules_from SET name='$salida' WHERE name='$longString';");
                $projectdb->query("UPDATE security_rules_to SET name='$salida' WHERE name='$longString';");
                $projectdb->query("UPDATE nat_rules_from SET name='$salida' WHERE name='$longString';");
                $projectdb->query("UPDATE nat_rules SET op_zone_to='$salida' WHERE source='$source' AND op_zone_to='$longString';");
                $projectdb->query("UPDATE routes_static SET zone='$salida' WHERE source='$source' AND zone='$longString';");
            }
            //return $salida;
        }
    }

    public static function default_regions()
    {
        $tmp_regions = array();


        $tmp_regions['0.0.0.0-0.255.255.255'] = 'Reserved(0.0.0.0-0.255.255.255)';
        $tmp_regions['10.0.0.0-10.255.255.255'] = 'Reserved(10.0.0.0-10.255.255.255)';
        $tmp_regions['100.64.0.0-100.127.255.255'] = 'Reserved(100.64.0.0-100.127.255.255)';
        $tmp_regions['127.0.0.0-127.255.255.255'] = 'Reserved(127.0.0.0-127.255.255.255)';
        $tmp_regions['169.254.0.0-169.254.255.255'] = 'Reserved(169.254.0.0-169.254.255.255)';
        $tmp_regions['172.16.0.0-172.31.255.255'] = 'Reserved(172.16.0.0-172.31.255.255)';
        $tmp_regions['192.0.0.0-192.0.0.255'] = 'Reserved(192.0.0.0-192.0.0.255)';
        $tmp_regions['192.0.2.0-192.0.2.255'] = 'Reserved(192.0.2.0-192.0.2.255)';
        $tmp_regions['192.168.0.0-192.168.255.255'] = 'Reserved(192.168.0.0-192.168.255.255)';
        $tmp_regions['192.88.99.0-192.88.99.255'] = 'Reserved(192.88.99.0-192.88.99.255)';
        $tmp_regions['198.18.0.0-198.19.255.255'] = 'Reserved(198.18.0.0-198.19.255.255)';
        $tmp_regions['198.51.100.0-198.51.100.255'] = 'Reserved(198.51.100.0-198.51.100.255)';
        $tmp_regions['203.0.113.0-203.0.113.255'] = 'Reserved(203.0.113.0-203.0.113.255)';
        $tmp_regions['224.0.0.0-224.255.255.255'] = 'Reserved(224.0.0.0-224.255.255.255)';
        $tmp_regions['225.0.0.0-225.255.255.255'] = 'Reserved(225.0.0.0-225.255.255.255)';
        $tmp_regions['226.0.0.0-226.255.255.255'] = 'Reserved(226.0.0.0-226.255.255.255)';
        $tmp_regions['227.0.0.0-227.255.255.255'] = 'Reserved(227.0.0.0-227.255.255.255)';
        $tmp_regions['228.0.0.0-228.255.255.255'] = 'Reserved(228.0.0.0-228.255.255.255)';
        $tmp_regions['229.0.0.0-229.255.255.255'] = 'Reserved(229.0.0.0-229.255.255.255)';
        $tmp_regions['230.0.0.0-230.255.255.255'] = 'Reserved(230.0.0.0-230.255.255.255)';
        $tmp_regions['231.0.0.0-231.255.255.255'] = 'Reserved(231.0.0.0-231.255.255.255)';
        $tmp_regions['232.0.0.0-232.255.255.255'] = 'Reserved(232.0.0.0-232.255.255.255)';
        $tmp_regions['233.0.0.0-233.255.255.255'] = 'Reserved(233.0.0.0-233.255.255.255)';
        $tmp_regions['234.0.0.0-234.255.255.255'] = 'Reserved(234.0.0.0-234.255.255.255)';
        $tmp_regions['235.0.0.0-235.255.255.255'] = 'Reserved(235.0.0.0-235.255.255.255)';
        $tmp_regions['236.0.0.0-236.255.255.255'] = 'Reserved(236.0.0.0-236.255.255.255)';
        $tmp_regions['237.0.0.0-237.255.255.255'] = 'Reserved(237.0.0.0-237.255.255.255)';
        $tmp_regions['238.0.0.0-238.255.255.255'] = 'Reserved(238.0.0.0-238.255.255.255)';
        $tmp_regions['239.0.0.0-239.255.255.255'] = 'Reserved(239.0.0.0-239.255.255.255)';
        $tmp_regions['240.0.0.0-240.255.255.255'] = 'Reserved(240.0.0.0-240.255.255.255)';
        $tmp_regions['241.0.0.0-241.255.255.255'] = 'Reserved(241.0.0.0-241.255.255.255)';
        $tmp_regions['242.0.0.0-242.255.255.255'] = 'Reserved(242.0.0.0-242.255.255.255)';
        $tmp_regions['243.0.0.0-243.255.255.255'] = 'Reserved(243.0.0.0-243.255.255.255)';
        $tmp_regions['244.0.0.0-244.255.255.255'] = 'Reserved(244.0.0.0-244.255.255.255)';
        $tmp_regions['245.0.0.0-245.255.255.255'] = 'Reserved(245.0.0.0-245.255.255.255)';
        $tmp_regions['246.0.0.0-246.255.255.255'] = 'Reserved(246.0.0.0-246.255.255.255)';
        $tmp_regions['247.0.0.0-247.255.255.255'] = 'Reserved(247.0.0.0-247.255.255.255)';
        $tmp_regions['248.0.0.0-248.255.255.255'] = 'Reserved(248.0.0.0-248.255.255.255)';
        $tmp_regions['249.0.0.0-249.255.255.255'] = 'Reserved(249.0.0.0-249.255.255.255)';
        $tmp_regions['250.0.0.0-250.255.255.255'] = 'Reserved(250.0.0.0-250.255.255.255)';
        $tmp_regions['251.0.0.0-251.255.255.255'] = 'Reserved(251.0.0.0-251.255.255.255)';
        $tmp_regions['252.0.0.0-252.255.255.255'] = 'Reserved(252.0.0.0-252.255.255.255)';
        $tmp_regions['253.0.0.0-253.255.255.255'] = 'Reserved(253.0.0.0-253.255.255.255)';
        $tmp_regions['254.0.0.0-254.255.255.255'] = 'Reserved(254.0.0.0-254.255.255.255)';
        $tmp_regions['255.0.0.0-255.255.255.255'] = 'Reserved(255.0.0.0-255.255.255.255)';
        $tmp_regions['A1'] = 'Anonymous Proxy';
        $tmp_regions['A2'] = 'Satellite Provider';
        $tmp_regions['AD'] = 'Andorra';
        $tmp_regions['AE'] = 'United Arab Emirates';
        $tmp_regions['AF'] = 'Afghanistan';
        $tmp_regions['AG'] = 'Antigua And Barbuda';
        $tmp_regions['AI'] = 'Anguilla';
        $tmp_regions['AL'] = 'Albania';
        $tmp_regions['AM'] = 'Armenia';
        $tmp_regions['AN'] = 'Netherlands Antilles';
        $tmp_regions['AO'] = 'Angola';
        $tmp_regions['AP'] = 'Asia Pacific Region';
        $tmp_regions['AQ'] = 'Antarctica';
        $tmp_regions['AR'] = 'Argentina';
        $tmp_regions['AS'] = 'American Samoa';
        $tmp_regions['AT'] = 'Austria';
        $tmp_regions['AU'] = 'Australia';
        $tmp_regions['AW'] = 'Aruba';
        $tmp_regions['AX'] = 'Land Islands';
        $tmp_regions['AZ'] = 'Azerbaijan';
        $tmp_regions['BA'] = 'Bosnia And Herzegovina';
        $tmp_regions['BB'] = 'Barbados';
        $tmp_regions['BD'] = 'Bangladesh';
        $tmp_regions['BE'] = 'Belgium';
        $tmp_regions['BF'] = 'Burkina Faso';
        $tmp_regions['BG'] = 'Bulgaria';
        $tmp_regions['BH'] = 'Bahrain';
        $tmp_regions['BI'] = 'Burundi';
        $tmp_regions['BJ'] = 'Benin';
        $tmp_regions['BL'] = 'Saint BarthLemy';
        $tmp_regions['BM'] = 'Bermuda';
        $tmp_regions['BN'] = 'Brunei Darussalam';
        $tmp_regions['BO'] = 'Bolivia Plurinational State Of';
        $tmp_regions['BQ'] = 'Bonaire Saint Eustatius And Saba';
        $tmp_regions['BR'] = 'Brazil';
        $tmp_regions['BS'] = 'Bahamas';
        $tmp_regions['BT'] = 'Bhutan';
        $tmp_regions['BV'] = 'Bouvet Island';
        $tmp_regions['BW'] = 'Botswana';
        $tmp_regions['BY'] = 'Belarus';
        $tmp_regions['BZ'] = 'Belize';
        $tmp_regions['CA'] = 'Canada';
        $tmp_regions['CC'] = 'Cocos Islands';
        $tmp_regions['CD'] = 'Congo The Democratic Republic Of The';
        $tmp_regions['CF'] = 'Central African Republic';
        $tmp_regions['CG'] = 'Congo';
        $tmp_regions['CH'] = 'Switzerland';
        $tmp_regions['CI'] = 'CTe D Ivoire';
        $tmp_regions['CK'] = 'Cook Islands';
        $tmp_regions['CL'] = 'Chile';
        $tmp_regions['CM'] = 'Cameroon';
        $tmp_regions['CN'] = 'China';
        $tmp_regions['CO'] = 'Colombia';
        $tmp_regions['CR'] = 'Costa Rica';
        $tmp_regions['CU'] = 'Cuba';
        $tmp_regions['CV'] = 'Cape Verde';
        $tmp_regions['CW'] = 'CuraAo';
        $tmp_regions['CX'] = 'Christmas Island';
        $tmp_regions['CY'] = 'Cyprus';
        $tmp_regions['CZ'] = 'Czech Republic';
        $tmp_regions['DE'] = 'Germany';
        $tmp_regions['DJ'] = 'Djibouti';
        $tmp_regions['DK'] = 'Denmark';
        $tmp_regions['DM'] = 'Dominica';
        $tmp_regions['DO'] = 'Dominican Republic';
        $tmp_regions['DZ'] = 'Algeria';
        $tmp_regions['EC'] = 'Ecuador';
        $tmp_regions['EE'] = 'Estonia';
        $tmp_regions['EG'] = 'Egypt';
        $tmp_regions['EH'] = 'Western Sahara';
        $tmp_regions['ER'] = 'Eritrea';
        $tmp_regions['ES'] = 'Spain';
        $tmp_regions['ET'] = 'Ethiopia';
        $tmp_regions['EU'] = 'European Union';
        $tmp_regions['FI'] = 'Finland';
        $tmp_regions['FJ'] = 'Fiji';
        $tmp_regions['FK'] = 'Falkland Islands (Malvinas)';
        $tmp_regions['FM'] = 'Micronesia Federated States Of';
        $tmp_regions['FO'] = 'Faroe Islands';
        $tmp_regions['FR'] = 'France';
        $tmp_regions['GA'] = 'Gabon';
        $tmp_regions['GB'] = 'United Kingdom';
        $tmp_regions['GD'] = 'Grenada';
        $tmp_regions['GE'] = 'Georgia';
        $tmp_regions['GF'] = 'French Guiana';
        $tmp_regions['GG'] = 'Guernsey';
        $tmp_regions['GH'] = 'Ghana';
        $tmp_regions['GI'] = 'Gibraltar';
        $tmp_regions['GL'] = 'Greenland';
        $tmp_regions['GM'] = 'Gambia';
        $tmp_regions['GN'] = 'Guinea';
        $tmp_regions['GP'] = 'Guadeloupe';
        $tmp_regions['GQ'] = 'Equatorial Guinea';
        $tmp_regions['GR'] = 'Greece';
        $tmp_regions['GS'] = 'South Georgia And The South Sandwich Islands';
        $tmp_regions['GT'] = 'Guatemala';
        $tmp_regions['GU'] = 'Guam';
        $tmp_regions['GW'] = 'Guinea-Bissau';
        $tmp_regions['GY'] = 'Guyana';
        $tmp_regions['HK'] = 'Hong Kong';
        $tmp_regions['HN'] = 'Honduras';
        $tmp_regions['HR'] = 'Croatia';
        $tmp_regions['HT'] = 'Haiti';
        $tmp_regions['HU'] = 'Hungary';
        $tmp_regions['ID'] = 'Indonesia';
        $tmp_regions['IE'] = 'Ireland';
        $tmp_regions['IL'] = 'Israel';
        $tmp_regions['IM'] = 'Isle Of Man';
        $tmp_regions['IN'] = 'India';
        $tmp_regions['IO'] = 'British Indian Ocean Territory';
        $tmp_regions['IQ'] = 'Iraq';
        $tmp_regions['IR'] = 'Iran Islamic Republic Of';
        $tmp_regions['IS'] = 'Iceland';
        $tmp_regions['IT'] = 'Italy';
        $tmp_regions['JE'] = 'Jersey';
        $tmp_regions['JM'] = 'Jamaica';
        $tmp_regions['JO'] = 'Jordan';
        $tmp_regions['JP'] = 'Japan';
        $tmp_regions['KE'] = 'Kenya';
        $tmp_regions['KG'] = 'Kyrgyzstan';
        $tmp_regions['KH'] = 'Cambodia';
        $tmp_regions['KI'] = 'Kiribati';
        $tmp_regions['KM'] = 'Comoros';
        $tmp_regions['KN'] = 'Saint Kitts And Nevis';
        $tmp_regions['KP'] = 'Korea Democratic Peoples Republic Of';
        $tmp_regions['KR'] = 'Korea Republic Of';
        $tmp_regions['KW'] = 'Kuwait';
        $tmp_regions['KY'] = 'Cayman Islands';
        $tmp_regions['KZ'] = 'Kazakhstan';
        $tmp_regions['LA'] = 'Lao Peoples Democratic Republic';
        $tmp_regions['LB'] = 'Lebanon';
        $tmp_regions['LC'] = 'Saint Lucia';
        $tmp_regions['LI'] = 'Liechtenstein';
        $tmp_regions['LK'] = 'Sri Lanka';
        $tmp_regions['LR'] = 'Liberia';
        $tmp_regions['LS'] = 'Lesotho';
        $tmp_regions['LT'] = 'Lithuania';
        $tmp_regions['LU'] = 'Luxembourg';
        $tmp_regions['LV'] = 'Latvia';
        $tmp_regions['LY'] = 'Libyan Arab Jamahiriya';
        $tmp_regions['MA'] = 'Morocco';
        $tmp_regions['MC'] = 'Monaco';
        $tmp_regions['MD'] = 'Moldova Republic Of';
        $tmp_regions['ME'] = 'Montenegro';
        $tmp_regions['MF'] = 'Saint Martin (French Part)';
        $tmp_regions['MG'] = 'Madagascar';
        $tmp_regions['MH'] = 'Marshall Islands';
        $tmp_regions['MK'] = 'Macedonia The Former Yugoslav Republic Of';
        $tmp_regions['ML'] = 'Mali';
        $tmp_regions['MM'] = 'Myanmar';
        $tmp_regions['MN'] = 'Mongolia';
        $tmp_regions['MO'] = 'Macao';
        $tmp_regions['MP'] = 'Northern Mariana Islands';
        $tmp_regions['MQ'] = 'Martinique';
        $tmp_regions['MR'] = 'Mauritania';
        $tmp_regions['MS'] = 'Montserrat';
        $tmp_regions['MT'] = 'Malta';
        $tmp_regions['MU'] = 'Mauritius';
        $tmp_regions['MV'] = 'Maldives';
        $tmp_regions['MW'] = 'Malawi';
        $tmp_regions['MX'] = 'Mexico';
        $tmp_regions['MY'] = 'Malaysia';
        $tmp_regions['MZ'] = 'Mozambique';
        $tmp_regions['NA'] = 'Namibia';
        $tmp_regions['NC'] = 'New Caledonia';
        $tmp_regions['NE'] = 'Niger';
        $tmp_regions['NF'] = 'Norfolk Island';
        $tmp_regions['NG'] = 'Nigeria';
        $tmp_regions['NI'] = 'Nicaragua';
        $tmp_regions['NL'] = 'Netherlands';
        $tmp_regions['NO'] = 'Norway';
        $tmp_regions['NP'] = 'Nepal';
        $tmp_regions['NR'] = 'Nauru';
        $tmp_regions['NU'] = 'Niue';
        $tmp_regions['NZ'] = 'New Zealand';
        $tmp_regions['OM'] = 'Oman';
        $tmp_regions['PA'] = 'Panama';
        $tmp_regions['PE'] = 'Peru';
        $tmp_regions['PF'] = 'French Polynesia';
        $tmp_regions['PG'] = 'Papua New Guinea';
        $tmp_regions['PH'] = 'Philippines';
        $tmp_regions['PK'] = 'Pakistan';
        $tmp_regions['PL'] = 'Poland';
        $tmp_regions['PM'] = 'Saint Pierre And Miquelon';
        $tmp_regions['PN'] = 'Pitcairn';
        $tmp_regions['PR'] = 'Puerto Rico';
        $tmp_regions['PS'] = 'Palestinian Territory Occupied';
        $tmp_regions['PT'] = 'Portugal';
        $tmp_regions['PW'] = 'Palau';
        $tmp_regions['PY'] = 'Paraguay';
        $tmp_regions['QA'] = 'Qatar';
        $tmp_regions['RE'] = 'RUnion';
        $tmp_regions['RO'] = 'Romania';
        $tmp_regions['RS'] = 'Serbia';
        $tmp_regions['RU'] = 'Russian Federation';
        $tmp_regions['RW'] = 'Rwanda';
        $tmp_regions['SA'] = 'Saudi Arabia';
        $tmp_regions['SB'] = 'Solomon Islands';
        $tmp_regions['SC'] = 'Seychelles';
        $tmp_regions['SD'] = 'Sudan';
        $tmp_regions['SE'] = 'Sweden';
        $tmp_regions['SG'] = 'Singapore';
        $tmp_regions['SH'] = 'Saint Helena Ascension And Tristan Da Cunha';
        $tmp_regions['SI'] = 'Slovenia';
        $tmp_regions['SJ'] = 'Svalbard And Jan Mayen';
        $tmp_regions['SK'] = 'Slovakia';
        $tmp_regions['SL'] = 'Sierra Leone';
        $tmp_regions['SM'] = 'San Marino';
        $tmp_regions['SN'] = 'Senegal';
        $tmp_regions['SO'] = 'Somalia';
        $tmp_regions['SR'] = 'Suriname';
        $tmp_regions['SS'] = 'South Sudan';
        $tmp_regions['ST'] = 'Sao Tome And Principe';
        $tmp_regions['SV'] = 'El Salvador';
        $tmp_regions['SX'] = 'Sint Maarten (Dutch Part)';
        $tmp_regions['SY'] = 'Syrian Arab Republic';
        $tmp_regions['SZ'] = 'Swaziland';
        $tmp_regions['TC'] = 'Turks And Caicos Islands';
        $tmp_regions['TD'] = 'Chad';
        $tmp_regions['TF'] = 'French Southern Territories';
        $tmp_regions['TG'] = 'Togo';
        $tmp_regions['TH'] = 'Thailand';
        $tmp_regions['TJ'] = 'Tajikistan';
        $tmp_regions['TK'] = 'Tokelau';
        $tmp_regions['TL'] = 'Timor-Leste';
        $tmp_regions['TM'] = 'Turkmenistan';
        $tmp_regions['TN'] = 'Tunisia';
        $tmp_regions['TO'] = 'Tonga';
        $tmp_regions['TR'] = 'Turkey';
        $tmp_regions['TT'] = 'Trinidad And Tobago';
        $tmp_regions['TV'] = 'Tuvalu';
        $tmp_regions['TW'] = 'Taiwan ROC';
        $tmp_regions['TZ'] = 'Tanzania United Republic Of';
        $tmp_regions['UA'] = 'Ukraine';
        $tmp_regions['UG'] = 'Uganda';
        $tmp_regions['UM'] = 'United States Minor Outlying Islands';
        $tmp_regions['US'] = 'United States';
        $tmp_regions['UY'] = 'Uruguay';
        $tmp_regions['UZ'] = 'Uzbekistan';
        $tmp_regions['VA'] = 'Holy See (Vatican City State)';
        $tmp_regions['VC'] = 'Saint Vincent And The Grenadines';
        $tmp_regions['VE'] = 'Venezuela Bolivarian Republic Of';
        $tmp_regions['VG'] = 'Virgin Islands British';
        $tmp_regions['VI'] = 'Virgin Islands U.S.';
        $tmp_regions['VN'] = 'Viet Nam';
        $tmp_regions['VU'] = 'Vanuatu';
        $tmp_regions['WF'] = 'Wallis And Futuna';
        $tmp_regions['WS'] = 'Samoa';
        $tmp_regions['YE'] = 'Yemen';
        $tmp_regions['YT'] = 'Mayotte';
        $tmp_regions['ZA'] = 'South Africa';
        $tmp_regions['ZM'] = 'Zambia';
        $tmp_regions['ZW'] = 'Zimbabwe';


        return $tmp_regions;
    }

    public function convertWildcards(STRING $wildcard, STRING $type)
    {
        # @type cidr or netmask

        $netmask_explode = explode(".", $wildcard);
        if( isset($netmask_explode[3]) && $netmask_explode[3] > "200" )
        {
            $netmask_array = array(255, 254, 252, 248, 240, 224, 192, 128);

            $final_array = array();
            foreach( $netmask_explode as $key => $position )
            {
                if( $position == 255 )
                {
                    $position = 0;
                }
                else
                {
                    if( $position < 127 )
                        $position = 255 - $position;

                    if( !in_array($position, $netmask_array) )
                    {
                        if( in_array($position + 1, $netmask_array) )
                        {
                            $position = $position + 1;
                            #print "mask is now: |".$position."|\n";
                        }
                        else
                            derr("wildcard mask: " . $position . " WRONG");
                    }
                }

                $final_array[$key] = $position;
            }

            $comma_separated = implode(".", $final_array);

            switch ($type)
            {
                case "netmask":
                    return $comma_separated;
                    break;
                case "cidr":
                    return CIDR::netmask2cidr($comma_separated);
                    break;
            }
        }

        switch ($wildcard)
        {

            case "0.255.255.255":
                switch ($type)
                {
                    case "netmask":
                        return "255.0.0.0";
                        break;
                    case "cidr":
                        return "8";
                        break;
                }
                break;
            case "0.127.255.255":
                switch ($type)
                {
                    case "netmask":
                        return "255.128.0.0";
                        break;
                    case "cidr":
                        return "9";
                        break;
                }
                break;
            case "0.63.255.255":
                switch ($type)
                {
                    case "netmask":
                        return "255.192.0.0";
                        break;
                    case "cidr":
                        return "10";
                        break;
                }
                break;
            case "0.31.255.255":
                switch ($type)
                {
                    case "netmask":
                        return "255.224.0.0";
                        break;
                    case "cidr":
                        return "11";
                        break;
                }
                break;
            case "0.15.255.255":
                switch ($type)
                {
                    case "netmask":
                        return "255.240.0.0";
                        break;
                    case "cidr":
                        return "12";
                        break;
                }
                break;
            case "0.7.255.255":
                switch ($type)
                {
                    case "netmask":
                        return "255.248.0.0";
                        break;
                    case "cidr":
                        return "13";
                        break;
                }
                break;
            case "0.3.255.255":
                switch ($type)
                {
                    case "netmask":
                        return "255.252.0.0";
                        break;
                    case "cidr":
                        return "14";
                        break;
                }
                break;
            case "0.1.255.255":
                switch ($type)
                {
                    case "netmask":
                        return "255.254.0.0";
                        break;
                    case "cidr":
                        return "15";
                        break;
                }
                break;
            case "0.0.255.255":
                switch ($type)
                {
                    case "netmask":
                        return "255.255.0.0";
                        break;
                    case "cidr":
                        return "16";
                        break;
                }
                break;
            case "0.0.127.255":
                switch ($type)
                {
                    case "netmask":
                        return "255.255.128.0";
                        break;
                    case "cidr":
                        return "17";
                        break;
                }
                break;
            case "0.0.63.255":
                switch ($type)
                {
                    case "netmask":
                        return "255.255.192.0";
                        break;
                    case "cidr":
                        return "18";
                        break;
                }
                break;
            case "0.0.31.255":
                switch ($type)
                {
                    case "netmask":
                        return "255.255.224.0";
                        break;
                    case "cidr":
                        return "19";
                        break;
                }
                break;
            case "0.0.15.255":
                switch ($type)
                {
                    case "netmask":
                        return "255.255.240.0";
                        break;
                    case "cidr":
                        return "20";
                        break;
                }
                break;
            case "0.0.7.255":
                switch ($type)
                {
                    case "netmask":
                        return "255.255.248.0";
                        break;
                    case "cidr":
                        return "21";
                        break;
                }
                break;
            case "0.0.3.255":
                switch ($type)
                {
                    case "netmask":
                        return "255.255.252.0";
                        break;
                    case "cidr":
                        return "22";
                        break;
                }
                break;
            case "0.0.1.255":
                switch ($type)
                {
                    case "netmask":
                        return "255.255.254.0";
                        break;
                    case "cidr":
                        return "23";
                        break;
                }
                break;
            case "0.0.0.255":
                switch ($type)
                {
                    case "netmask":
                        return "255.255.255.0";
                        break;
                    case "cidr":
                        return "24";
                        break;
                }
                break;
            case "0.0.0.127":
                switch ($type)
                {
                    case "netmask":
                        return "255.255.255.128";
                        break;
                    case "cidr":
                        return "25";
                        break;
                }
                break;
            case "0.0.0.63":
                switch ($type)
                {
                    case "netmask":
                        return "255.255.255.192";
                        break;
                    case "cidr":
                        return "26";
                        break;
                }
                break;
            case "0.0.0.31":
                switch ($type)
                {
                    case "netmask":
                        return "255.255.255.224";
                        break;
                    case "cidr":
                        return "27";
                        break;
                }
                break;
            case "0.0.0.15":
                switch ($type)
                {
                    case "netmask":
                        return "255.255.255.240";
                        break;
                    case "cidr":
                        return "28";
                        break;
                }
                break;
            case "0.0.0.7":
                switch ($type)
                {
                    case "netmask":
                        return "255.255.255.248";
                        break;
                    case "cidr":
                        return "29";
                        break;
                }
                break;
            case "0.0.0.3":
                switch ($type)
                {
                    case "netmask":
                        return "255.255.255.252";
                        break;
                    case "cidr":
                        return "30";
                        break;
                }
                break;
            case "0.0.0.1":
                switch ($type)
                {
                    case "netmask":
                        return "255.255.255.254";
                        break;
                    case "cidr":
                        return "31";
                        break;
                }
                break;
            default:
                return FALSE;

        }


    }

    public function checkNetmask($ip)
    {
        if( !ip2long($ip) )
        {
            return FALSE;
        }
        elseif( strlen(decbin(ip2long($ip))) != 32 && ip2long($ip) != 0 )
        {
            return FALSE;
        }
        elseif( preg_match('/01/', decbin(ip2long($ip))) || !preg_match('/0/', decbin(ip2long($ip))) )
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }


# Match IPv4  cidr_match("1.2.3.4", "0.0.0.0/0"): true
    public function cidr_match($ip, $cidr)
    {
        $list = explode('/', $cidr);
        $subnet = $list[0];
        $mask = $list[1];
        if( (ip2long($ip) & ~((1 << (32 - $mask)) - 1)) == ip2long($subnet) )
        {
            return TRUE;
        }

        return FALSE;
    }

    function netMatchObjects2Ways($sub, $ref, &$way)
    {
        if( isset($sub->cidr) && !strcmp($sub->cidr, '') == 0 )
        {
            $sub_local = "$sub->value/$sub->cidr";
        }
        else
        {
            $sub_local = "$sub->value";
        }
        if( isset($ref->cidr) && !strcmp($ref->cidr, '') == 0 )
        {
            $ref_local = "$ref->value/$ref->cidr";
        }
        else
        {
            $ref_local = "$ref->value";
        }

        $result = netMatch2Ways($sub_local, $ref_local, $way);
        if( $way == AisinB ) return $sub;
        elseif( $way == BisinA ) return $ref;
        else
        {
            $start = $result['start'];
            $end = $result['end'];
            $member = new MemberObject('', '', "$start-$end", '32');
            return null;
        }
    }

    //copied from Expedition lib-objects.php
    function isAinB_Zones(array $groupA, array $groupB, array &$foundZones = null)
    {
        //$groupA/B are arrays of zoneObjects -> name is needed
        //Todo: deep validation of $groupA/B info is needed

        foreach( $groupA as $key => $item )
        {
            $groupA[$key] = $item->name();
        }

        foreach( $groupB as $key => $item )
        {
            $groupB[$key] = $item->name();
        }

        #$isFromCovered = $this->isAinB_Zones($security_rule->from->getAll(), $getNatData->from->getAll(), $zonesFrom);
        //Todo: why is $zonesFrom needed?
        #$isFromCovered = $getNatData->from->includesContainer( $security_rule->from );


        if( in_array("any", $groupB) || in_array("Any", $groupB) )
        {
            if( in_array("any", $groupA) || in_array("Any", $groupA) )
            {
                $foundZones = $groupA;
                return 1;
            }
            else
            {
                $foundZones = $groupA;
                return 2;
            }
        }
        elseif( in_array("any", $groupA) || in_array("Any", $groupA) )
        {
            $foundZones = $groupB;
            return 2;
        }
        else
        {
            $foundZones = array_intersect($groupA, $groupB);
        }

        if( count($foundZones) == 0 )
        {
            return 0;
        }
        if( count($foundZones) == count($groupA) )
        {
            return 1;
        }
        return 2;
    }

    /**
     * This method checks whether the members in groupA are in the groupB.
     * @param array(MemberObject) $groupA
     * @param array(MemberObject) $groupB
     * @param String $debugText
     * @return int
     */
    function isAinB($groupA, $groupB, &$foundMembers = null, $debugText = null)
    {
        //Todo: same from above isAinBZone -> get array membmer names


        if( isset($debugText) )
        {
            echo "$debugText\n";
        }
        $foundMembers = array();

        $allChildrenFound = TRUE;
        $someChildrenFound = FALSE;

        foreach( $groupA as $childMember )
        {
//        echo "Child:";  print_r($childMember);
            if( is_array($childMember) )
            {
                print_r(debug_backtrace());
            }
            if( $this->ip_version($childMember->value()) != 'v4' )
            {
                $allChildrenFound = FALSE;
            }
            else
            {
                foreach( $groupB as $parentMember )
                {
                    $result = -1;

                    if( $this->ip_version($parentMember->value()) != 'v4' )
                    {
                        $newMatch = -1;
                    }
                    else
                    {
//                    echo "Parent:";  print_r($parentMember);
                        $newMatch = $this->netMatchObjects2Ways($childMember, $parentMember, $result);
//            echo "Result: $result\n\n";
                    }

                    if( $result == 1 )
                    { //if one of the members in the groupA is not in groupB, then A is not in B
//                $isChildPartiallyFound = 1;
                        $someChildrenFound = TRUE;
                        $foundMembers[] = $childMember;
                        break;
                    }
                    elseif( $result == 2 )
                    {
                        $allChildrenFound = FALSE;
                        $someChildrenFound = TRUE;
                        $foundMembers[] = $newMatch;
                    }
                    else
                    {
                        $allChildrenFound = FALSE;
                    }
                }
            }
        }

        if( $allChildrenFound )
        {
//        echo "Final result 1\n";
            return 1;
        }
        if( $someChildrenFound )
        {
//        echo "Final result 2\n";
            return 2;
        }
//    echo "Final result 0\n";
        return 0;
    }

    function isAinB_service($groupA, $groupB, &$foundServices = null, $debugText = null)
    {
        #$isSrvCovered = $this->isAinB_service($security_rule->services->getAll(), $getNatData->service, $servicesMatched);

        if( isset($debugText) )
        {
            echo "$debugText\n";
        }
        foreach( $groupA as $childMember )
        {
            $isChildFound = 0;
            $isChildPartiallyFound = 0;
            foreach( $groupB as $parentMember )
            {
                $result = -1;
                $newMatch = serviceMatchObjects2Ways($childMember, $parentMember, $result);
                if( $result == 1 )
                { //if one of the members in the groupA is not in groupB, then A is not in B
                    $isChildFound = 1;
                    $foundServices[] = $newMatch;
                    break;
                }
                elseif( $result == 2 )
                {
                    $isChildPartiallyFound = 1;
                    $foundServices[] = $newMatch;
                }
            }
            if( $isChildFound == 0 && $isChildPartiallyFound == 0 )
            {
//            echo "Service Not Matched\n";
                return 0;
            }
            elseif( $isChildFound == 0 && $isChildPartiallyFound == 1 )
            {
//            echo "Service Partial GroupA Match\n";
                return 2;
            }
        }
//    echo "Service Full GroupA Match\n";
        return 1;
    }


    public function MainAddHost($name, $IPvalue, $type = "ip-netmask", $description = null)
    {
        global $print;

        $location = $this->v;

        $name = $this->truncate_names($this->normalizeNames($name));
        $value = $IPvalue;
        $value = str_replace('"', "", $value);

        $tmp_address = $location->addressStore->find($name);
        if( $tmp_address == null )
        {
            #if( $this->print )
            if( $print )
                print " * create address object: " . $name . " | type: " . $type . " | value: " . $value . "\n";
            $tmp_address = $location->addressStore->newAddress($name, $type, $value);
        }
        else
        {
            mwarning("address object: " . $name . " already available; with value: " . $tmp_address->value() . "\n");
        }

        if( $description !== null )
            $tmp_address->setDescription($description);

        return $tmp_address;
    }

    public function MainAddAddressGroup($name, $members, $description, &$missingMembers = array())
    {
        global $print;

        $location = $this->v;

        $name = $this->truncate_names($this->normalizeNames($name));

        $tmp_addressgroup = $location->addressStore->find($name);
        if( $tmp_addressgroup === null )
        {
            #if( $this->print )
            if( $print )
                print "\n * create addressgroup: " . $name . "\n";
            $tmp_addressgroup = $location->addressStore->newAddressGroup($name);

            if( $description !== null )
                $tmp_addressgroup->setDescription($description);
        }

        foreach( $members as $member )
        {
            $member = $this->truncate_names($this->normalizeNames($member));
            $tmp_address = $location->addressStore->find($member);

            if( $tmp_address !== null )
            {
                /*
                if( !$tmp_addressgroup->hasObjectRecursive($tmp_address) && $tmp_addressgroup != $tmp_address )
                {
                    #if( $this->print )
                    if( $print )
                        print "    * add address object: " . $tmp_address->name() . " to addressgroup: " . $tmp_addressgroup->name() . "\n";

                    $tmp_addressgroup->addMember($tmp_address);
                }
                else
                {*/
                #if( $this->print )
                if( $print )
                    print "    * address object: " . $tmp_address->name() . " already a member or submember of addressgroup: " . $tmp_addressgroup->name() . "\n";

                #if( $this->print )
                if( $print )
                    print "    * add address object: " . $tmp_address->name() . " also as it is added already\n";
                $tmp_addressgroup->addMember($tmp_address);
                #}
            }
            else
            {
                if( $print )
                    print "    X address object: " . $member . " not found - can not be added to addressgroup: " . $tmp_addressgroup->name() . "\n";

                $missingMembers[$tmp_addressgroup->name()][] = $member;
                #mwarning( "address object: ".$member." not found" );
            }
        }

        return $tmp_addressgroup;
    }

    public function MainAddService($name, $protocol, $dport, $description = '', $sport = null)
    {
        global $print;

        $location = $this->v;

        $name = $this->truncate_names($this->normalizeNames($name));

        $tmp_service = $location->serviceStore->find($name);
        if( $tmp_service == null )
        {
            #if( $this->print )
            if( $print )
                print " * create service: " . $name . " | protocol: " . $protocol . " | port: " . $dport . "\n";
            $tmp_service = $location->serviceStore->newService($name, $protocol, $dport, $description, $sport);
        }
        else
        {
            mwarning($protocol . " service: " . $name . " already available\n");
        }

        return $tmp_service;
    }

    public function MainAddServiceGroup($name, $members, $description, &$missingMembers, $find_tmp = FALSE)
    {
        global $print;

        $location = $this->v;

        $name = $this->truncate_names($this->normalizeNames($name));

        $tmp_servicegroup = $location->serviceStore->find($name);
        if( $tmp_servicegroup === null )
        {
            #if( $this->print )
            if( $print )
                print "\n * create servicegroup: " . $name . "\n";
            $tmp_servicegroup = $location->serviceStore->newServiceGroup($name);

            if( $description !== null )
            {
                #$tmp_servicegroup->setDescription($description);
                #mwarning( "PAN-OS servicegroup do not support description" );
            }

        }

        foreach( $members as $member )
        {
            $member = $this->truncate_names($this->normalizeNames($member));
            $tmp_service = $location->serviceStore->find($member);

            if( $tmp_service !== null )
            {
                /*
                if( $print )
                    print "    * add service object: " . $tmp_service->name() . " to servicegroup: " . $tmp_servicegroup->name() . "\n";

                $tmp_servicegroup->addMember($tmp_service);
                */

                if( !$tmp_servicegroup->hasObjectRecursive($tmp_service) && $tmp_servicegroup != $tmp_service )
                {
                    #if( $this->print )
                    if( $print )
                        print "    * add service object: " . $tmp_service->name() . " to servicegroup: " . $tmp_servicegroup->name() . "\n";

                    $tmp_servicegroup->addMember($tmp_service);
                }
                else
                {
                    #if( $this->print )
                    if( $print )
                        print "    * service object: " . $tmp_service->name() . " already a member or submember of servicegroup: " . $tmp_servicegroup->name() . "\n";

                    #if( $this->print )
                    if( $print )
                        print "    * add service object: " . $tmp_service->name() . " also as it is added already\n";
                    $tmp_servicegroup->addMember($tmp_service);
                }


            }
            else
            {
                if( $find_tmp )
                {
                    $tmp_service = $location->serviceStore->find("tmp-" . $member);

                    if( $tmp_service !== null )
                    {
                        ///*
                        if( $print )
                            print "    * add service object: " . $tmp_service->name() . " to servicegroup: " . $tmp_servicegroup->name() . "\n";

                        $tmp_servicegroup->addMember($tmp_service);
                    }
                }

                if( $tmp_service === null )
                {
                    if( $print )
                    {
                        if( $find_tmp )
                            $member_name = "tmp-" . $member;
                        else
                            $member_name = $member;

                        print "    X service object: " . $member_name . " not found - can not be added to servicegroup: " . $tmp_servicegroup->name() . "\n";
                    }


                    $missingMembers[$tmp_servicegroup->name()][] = $member;
                    #mwarning( "service object: ".$member." not found", null,false );
                }

            }
        }

        return $tmp_servicegroup;
    }

    public function json_validate($string)
    {
        if( is_string($string) )
        {
            @json_decode($string);
            return (json_last_error() === JSON_ERROR_NONE);
        }
        return FALSE;
    }
}

