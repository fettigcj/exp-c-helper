<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */

require_once("utils/lib/UTIL.php");

class CONVERTER extends UTIL
{
    public $debug = FALSE;
    public $print = FALSE;
    public $configFile = null;

    public $reduceXML = FALSE;

    public $vendor = null;

    private $myParserClass = '';

    public $appsToMigrate = array();
    public $routetable = "";

    function __construct()
    {
    }

    private function vendor_require()
    {
        //DEFINE VENDOR specific RECUIRE
        if( $this->vendor == "ciscoasa" )
        {
            require_once("parser/CISCO/CISCOASA.php");
            $this->myParserClass = "CISCOASA";
        }
        elseif( $this->vendor == "ciscoisr" )
        {
            require_once("parser/CISCOISR/CISCOISR.php");
            $this->myParserClass = "CISCOISR";
        }
        elseif( $this->vendor == "ciscoswitch" )
        {
            require_once("parser/CISCOSWITCH/CISCOSWITCH.php");
            $this->myParserClass = "CISCOSWITCH";
        }
        elseif( $this->vendor == "netscreen" )
        {
            require_once("parser/SCREENOS/SCREENOS.php");
            $this->myParserClass = "SCREENOS";
        }
        elseif( $this->vendor == "fortinet" )
        {
            require_once("parser/FORTINET/FORTINET.php");
            $this->myParserClass = "FORTINET";
        }

        elseif( $this->vendor == "pfsense" )
        {
            require_once("parser/PFSENSE/PFSENSE.php");
            $this->myParserClass = "PFSENSE";
        }
        elseif( $this->vendor == "sonicwall" )
        {
            require_once("parser/SONICWALL/SONICWALL.php");
            $this->myParserClass = "SONICWALL";
        }
        elseif( $this->vendor == "sophos" )
        {
            require_once("parser/SOPHOS/SOPHOS.php");
            $this->myParserClass = "SOPHOS";
        }
        elseif( $this->vendor == "srx" )
        {
            require_once("parser/SRX/SRX.php");
            $this->myParserClass = "SRX";
        }
        elseif( $this->vendor == "cp-r80" )
        {
            require_once("parser/CP_R80/CP_R80.php");
            $this->myParserClass = "CP_R80";
        }
        elseif( $this->vendor == "cp" )
        {
            require_once("parser/CP/CP.php");
            $this->myParserClass = "CP";
        }
        elseif( $this->vendor == "cp-beta" )
        {
            require_once("parser/CP/develop/CPnew.php");
            $this->myParserClass = "CPnew";
        }
        else
            derr("VENDOR: " . $this->vendor . " is not supported yet");
    }

    public function initial()
    {
        #PH::processCliArgs();


        if( isset(PH::$args['file']) )
        {
            $this->configFile = PH::$args['file'];
        }

        if( isset(PH::$args['in']) )
        {
            $this->configInput = PH::$args['in'];
            $this->configInput = PH::processIOMethod($this->configInput, TRUE);
        }
        else
            $this->configInput['filename'] = __DIR__ . "/../VM300-Baseline.xml";


        if( isset(PH::$args['out']) )
        {
            $this->configOutput = PH::$args['out'];
        }

        if( isset(PH::$args['routetable']) )
        {
            $this->routetable = PH::$args['routetable'];
        }

        if( isset(PH::$args['debug']) )
        {
            $this->debug = TRUE;
        }

        if( isset(PH::$args['print']) )
        {
            $this->print = TRUE;
        }

        if( isset(PH::$args['reducexml']) )
        {
            $this->reduceXML = TRUE;
        }

        if( isset(PH::$args['expedition']) )
        {
            $this->expedition = PH::$args['expedition'];
            $tmp_expedition = explode(",", $this->expedition);

            if( isset($tmp_expedition[0]) && isset($tmp_expedition[1]) && isset($tmp_expedition[2]) && isset($tmp_expedition[3]) )
            {
                $this->expedition_db_ip = $tmp_expedition[0];
                $this->expedition_db_user = $tmp_expedition[1];
                $this->expedition_db_pw = $tmp_expedition[2];
                $this->taskId = $tmp_expedition[3];
            }
            else
            {
                $this->display_error_usage_exit('"expedition" argument has an invalid value. This argument can be only used directly from Expedition Tool');
            }
            unset($tmp_expedition);
        }
    }


    private function global_start()
    {
        $this->utilLogger();


        $this->log->info("start PARSER: " . $this->vendor . " | " . implode(", ", PH::$args));

        $this->xmlDoc = new DOMDocument();
        if( !$this->xmlDoc->load($this->configInput['filename']) )
            derr("error while reading xml config file");


        $this->determineConfigType();

        if( $this->configType != 'panos' )
            derr("Migration Parser is only working with a PAN-OS Firewall Base configuration");

        $this->load_config();

        //get time/mem for migration part
        UTIL::loadStart();
        print "\n\n";
    }


    private function global_end($lineReturn = TRUE, $indentingXml = 0, $indentingXmlIncreament = 1)
    {
        print "\n\n";
        /////////////////////////////
        //get time/mem for migration part
        $this->loadEnd();
        echo "OK! ($this->loadElapsedTime seconds, $this->loadUsedMem memory) used for migration\n";

        ##############################################

        print "\n\n\n";

        $this->save_our_work(FALSE, TRUE, $lineReturn, $indentingXml, $indentingXmlIncreament);

        print "########################################################\n\n";
        print "MIGRATION configuration exported\n\n";
        print "########################################################\n\n";


        $this->log->info("END PARSER: " . $this->vendor);
    }


    public function main($_vendor)
    {
        $this->vendor = $_vendor;

        if( empty($this->configInput) )
            derr("variable: 'IN=' is not set!");
        elseif( empty($this->configOutput) )
            derr("variable: 'OUT=' is not set!");
        elseif( empty($this->configFile) )
            derr("variable: 'FILE=' is not set!");


        self::global_start();

        self::vendor_require();
        //will be replace with the below one
        #vendor_main( $this->configFile, $this->pan );

        //Todo DIDAC
        $myParserObject = new $this->myParserClass($this->taskId, $this->expedition, $this->expedition_db_ip, $this->expedition_db_user, $this->expedition_db_pw);
        $myParserObject->vendor_main($this->configFile, $this->pan, $this->routetable);


        if( $this->reduceXML )
            self::global_end(FALSE, -1, 0);
        else
            self::global_end();
    }


    static public function cleanup_unused_predefined_services($pan, $tag)
    {
        foreach( $pan->getVirtualSystems() as $v )
        {
            foreach( $v->serviceStore->all() as $tmp_service )
            {
                if( $tmp_service->tags->hasTag($tag) && $tmp_service->objectIsUnusedRecursive() )
                {
                    $v->serviceStore->remove($tmp_service, TRUE);
                }
            }
        }

    }

    static public function validate_interface_names($pan)
    {
        global $debug;
        global $print;

        $padding = "   ";
        $padding_name = substr($padding, 0, -1);

        #$pan = $v->owner;

        $tmp_interfaces = $pan->network->getAllInterfaces();

        $counter = 1;
        $tmp_int_name = array();
        foreach( $tmp_interfaces as $tmp_interface )
        {
            #if( $tmp_interface->type !== "tmp" && get_class( $tmp_interface ) == "EthernetInterface" )
            if( $tmp_interface->type !== "tmp" )
            {

                $int_name = $tmp_interface->name();
                if( get_class($tmp_interface) == "EthernetInterface" )
                {
                    if( strpos($int_name, "ethernet") === FALSE && strpos($int_name, "ae") === FALSE && strpos($int_name, "tunnel") === FALSE )
                    {
                        if( strpos($int_name, ".") === FALSE )
                        {
                            do
                            {
                                $new_name = "ethernet1/" . $counter;

                                $counter++;

                                $tmp_int = $pan->network->findInterface($new_name);
                                $tmp_int_name[$int_name] = $new_name;
                            } while( $tmp_int !== null );

                        }
                        else
                        {
                            $tmp_tag = explode(".", $int_name);
                            $new_name = $tmp_int_name[$tmp_tag[0]] . "." . $tmp_tag[1];
                        }

                        $addlog = "Interface: '" . $int_name . "' renamed to " . $new_name;
                        print $padding . "X " . $addlog . "\n";
                        $tmp_interface->display_references();
                        $tmp_interface->setName($new_name);

                        //todo: add description
                        #$tmp_interface->_description .= " renamed from '".$int_name."'";
                        //add migration log

                        $tmp_interface->set_node_attribute('warning', $addlog);


                    }

                }
                elseif( get_class($tmp_interface) == "TunnelInterface" )
                {
                    $tunnelcounter = 1;

                    #if( strpos( $int_name, "." ) === false ){
                    do
                    {
                        $new_name = "tunnel." . $tunnelcounter;

                        $tunnelcounter++;

                        $tmp_int = $pan->network->findInterface($new_name);
                        $tmp_int_name[$int_name] = $new_name;
                    } while( $tmp_int !== null );

                    /*}
                    else
                    {
                        $tmp_tag = explode( ".", $int_name);
                        $new_name = $tmp_int_name[ $tmp_tag[0] ].".". $tmp_tag[1];
                    }
                    */

                    $addlog = "Interface: '" . $int_name . "' renamed to " . $new_name;
                    print $padding . "X " . $addlog . "\n";
                    $tmp_interface->setName($new_name);
                    $tmp_interface->set_node_attribute('warning', $addlog);
                }
                else
                {
                    print " - migration for interface class: " . get_class($tmp_interface) . " not implemented yet! for interface: ".$int_name."\n";
                }

                //Todo: replace from routing
                /*
                                elseif( strpos( $int_name, "ethernet" ) !== false )
                                {
                                    //Todo: detailed check needed
                                    print "Interface: ".$int_name." not renamed!\n";
                                }
                                elseif( strpos( $int_name, "ae" ) !== false  )
                                {
                                    //Todo: detailed check needed
                                    print "Interface: ".$int_name." not renamed!\n";
                                }
                                elseif( strpos( $int_name, "tunnel" ) !== false  )
                                {
                                    //Todo: detailed check needed
                                    print "Interface: ".$int_name." not renamed!\n";
                                }*/

            }
            else
            {
                mwarning("interface: " . $tmp_interface->name() . " is of type: " . $tmp_interface->type . " and not renamed", null, FALSE);
            }
        }
    }

    /**
     * @param PANConf $pan
     */
    //Todo: bring in correct virtual router
    static public function calculate_zones($pan, $mode)
    {
        $vsyss = $pan->virtualSystems;

        $tmp_virtualRouters = $pan->network->virtualRouterStore->virtualRouters();

        foreach( $tmp_virtualRouters as $virtualRouter )
        {
            foreach( $virtualRouter->findConcernedVsys() as $virtualSystem )
            {
                foreach( $vsyss as $v )
                {
                    if( $v->name() === $virtualSystem->name() )
                    {
                        $vrouter = $virtualRouter->name();

                        foreach( $v->securityRules->rules() as $rule )
                        {
                            print "check SecRule: " . $rule->name() . "\n";
                            $rule->zoneCalculation('from', $mode, $vrouter);
                            $rule->zoneCalculation('to', $mode, $vrouter);
                        }

                        foreach( $v->natRules->rules() as $rule )
                        {
                            print "check NATRule: " . $rule->name() . "\n";
                            $rule->zoneCalculation('from', $mode, $vrouter);
                            $rule->zoneCalculation('to', $mode, $vrouter);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param PANConf $pan
     */
    static public function validation_nat_bidir($pan)
    {
        $vsyss = $pan->virtualSystems;

        foreach( $vsyss as $v )
        {
            foreach( $v->natRules->rules() as $rule )
            {
                $hasgroup = FALSE;
                if( $rule->isBiDirectional() )
                {
                    print "NATRule: " . $rule->name() . " - bidirnat check\n";

                    //Todo: another validation is needed, if addressgroup has only one address member, then replace addressgroup with member
                    $sources = $rule->source->all();
                    if( count($sources) > 1 )
                    {
                        foreach( $sources as $source )
                            if( $source->isGroup() )
                                $hasgroup = TRUE;
                    }
                    elseif( count($sources) == 1 )
                    {
                        foreach( $sources as $source )
                        {
                            if( $source->isGroup() )
                            {
                                $tmp_members = $source->members();
                                if( count($tmp_members) == 1 && $tmp_members[0]->isAddress() )
                                {
                                    $rule->source->setAny();
                                    print "      - replace addressgroup: " . $source->name() . " with their single address object member: " . $tmp_members[0]->name() . "\n";
                                    $rule->source->addObject($tmp_members[0]);
                                }
                                else
                                    $hasgroup = TRUE;
                            }
                        }
                    }
                }

                if( $hasgroup )
                {
                    print "      - disabled bidir NAT\n";
                    $rule->setBiDirectional(FALSE);
                }

                if( $rule->isBiDirectional() && !$rule->sourceNatTypeIs_None() && $rule->destinationNatIsEnabled() )
                {
                    print "TODO:      - disabled bidir NAT - TMP\n";
                    #$rule->setBiDirectional( false );

                    print "TODO:      - create new rule and and swap information\n";
                }
            }
        }
    }

    /**
     * @param PANConf $pan
     */
    static public function validation_region_object($pan)
    {
        $vsyss = $pan->virtualSystems;

        foreach( $vsyss as $v )
        {
            $addresses = $v->addressStore->all();
            foreach( $addresses as $address )
            {
                $oldName = $address->name();
                if( strlen($oldName) == 2 )
                {
                    $tmp_default_region = SHAREDNEW::default_regions();
                    if( isset($tmp_default_region[$oldName]) )
                    {
                        $tmp_name_postfix = "_noRegion";
                        $newName = $oldName . $tmp_name_postfix;
                        $address->setName($newName);
                        $msg = "Address Region Validation - rename object: '" . $oldName . "' to: '" . $newName . "'";
                        print $msg . "\n";
                        $address->set_node_attribute('warning', $msg);
                    }
                }
            }
        }
    }

    /**
     * @param PANConf $pan
     */
    static public function AppMigration($pan)
    {
        global $appsToMigrate;

        $appsToMigrate = array();
        $appsToMigrate['PPTP-Data-GRE'] = array('toApp' => array('ipsec-esp'));
        $appsToMigrate['IPSec-ESP'] = array('toApp' => array('ipsec-esp'));
        $appsToMigrate['IPSec-AH'] = array('toApp' => array('ipsec-ah'));
        $appsToMigrate['____app-icmp'] = array('toApp' => array('icmp', 'ping', 'traceroute'));
        $appsToMigrate['tmp-icmp'] = array('toApp' => array('icmp', 'ping', 'traceroute'));


        $appsToMigrate['tmp-icmp_8_0'] = array('toApp' => array('icmp_echo'));
        $appsToMigrate['tmp-icmp_3_*'] = array('toApp' => array('icmp_unreachable'));
        $appsToMigrate['tmp-icmp_*_*'] = array('toApp' => array('icmp', 'ping', 'traceroute'));
        $appsToMigrate['tmp-icmp_0_0'] = array('toApp' => array('icmp_echo-reply'));

        //Todo: one Fortinet example
        $appsToMigrate['tmp-icmp'] = array('toApp' => array('icmp', 'ping', 'traceroute'));

        $appsToMigrate['tmp-gtraceroute'] = array('toApp' => array('icmp', 'ping', 'traceroute'));
        $appsToMigrate['tmp-GS_gtraceroute'] = array('toApp' => array('icmp', 'ping', 'traceroute'));
        $appsToMigrate['tmp-ICMP_ANY'] = array('toApp' => array('icmp', 'ping', 'traceroute'));
        $appsToMigrate['tmp-PING'] = array('toApp' => array('icmp', 'ping', 'traceroute'));
        $appsToMigrate['tmp-TRACEROUTE'] = array('toApp' => array('icmp', 'ping', 'traceroute'));

        //JUNOS
        //////////////////////////////////////////////////////////////////////////////////////
//- tmp service 'tmp-junos-persistent-nat' found from ServiceStore
        $appsToMigrate['tmp-junos-icmp-ping'] = array('toApp' => array('icmp', 'ping', 'traceroute'));
        $appsToMigrate['tmp-junos-icmp-all'] = array('toApp' => array('icmp', 'ping', 'traceroute'));
        $appsToMigrate['tmp-junos-gre'] = array('toApp' => array('gre'));
        $appsToMigrate['tmp-junos-ping'] = array('toApp' => array('icmp', 'ping', 'traceroute'));

        $appsToMigrate['tmp-junos-ping'] = array('toApp' => array('icmp', 'ping', 'traceroute'));

        $appsToMigrate['tmp-junos-ms-rpc-wmic'] = array('toApp' => array('msrpc'));
        $appsToMigrate['tmp-junos-ms-rpc'] = array('toApp' => array('msrpc'));
        $appsToMigrate['tmp-junos-ms-rpc-epm'] = array('toApp' => array('msrpc'));
        $appsToMigrate['tmp-junos-cifs'] = array('toApp' => array('msrpc'));
        $appsToMigrate['tmp-junos-sun-rpc-rquotad-tcp'] = array('toApp' => array('rpc'));
        $appsToMigrate['tmp-junos-sun-rpc-rquotad-udp'] = array('toApp' => array('rpc'));
        $appsToMigrate['tmp-junos-sun-rpc-nlockmgr-tcp'] = array('toApp' => array('rpc'));
        $appsToMigrate['tmp-junos-sun-rpc-nlockmgr-udp'] = array('toApp' => array('rpc'));
        $appsToMigrate['tmp-junos-sun-rpc-portmap-tcp'] = array('toApp' => array('rpc'));
        $appsToMigrate['tmp-junos-sun-rpc-nfs-udp'] = array('toApp' => array('rpc'));
        $appsToMigrate['tmp-junos-sun-rpc-mountd-tcp'] = array('toApp' => array('rpc'));
        $appsToMigrate['tmp-junos-sun-rpc-mountd-udp'] = array('toApp' => array('rpc'));
        $appsToMigrate['tmp-junos-sun-rpc-ypbind-udp'] = array('toApp' => array('rpc'));
        $appsToMigrate['tmp-junos-sun-rpc-ypbind-tcp'] = array('toApp' => array('rpc'));
        $appsToMigrate['tmp-junos-sun-rpc-status-tcp'] = array('toApp' => array('rpc'));
        $appsToMigrate['tmp-junos-sun-rpc-status-udp'] = array('toApp' => array('rpc'));
        //////////////////////////////////////////////////////////////////////////////////////



        $vsyss = $pan->virtualSystems;

        foreach( $vsyss as $v )
        {
            echo "\n************* SVC TO APP MIGRATION NOW **************\n\n";

            echo "\n *** PHASE 1 :LOOKING FOR SERVICES TO TRANSLATE AND BUILDING LIST OF RULES USING THEM\n\n";

            CONVERTER::AppMigrationPhase1($v, $pan);


            echo "\n***********************************************\n";
            echo " *** PHASE 2 :LOOKING FOR RULES WITH appsToBeAdded\n\n";

            CONVERTER::AppMigrationPhase2($v, $pan);

            echo "\n***********************************************\n";
            echo " *** PHASE 3 : ELIMINATE ANY REFERENCE TO THESE SERVICES \n\n";
            CONVERTER::AppMigrationPhase3($v, $pan);
        }

    }

    /**
     * @param VirtualSystem $vsys1
     * @param PANConf $fw
     */
    public static function AppMigrationPhase1($vsys1, $fw)
    {
        global $appsToMigrate;

        $listTmpServices = array_merge($vsys1->serviceStore->serviceTmpObjects(), $fw->serviceStore->serviceTmpObjects(), $vsys1->serviceStore->all('name regex /^tmp-/'));
        foreach( $listTmpServices as $tmpService )
        {
            echo "- tmp service '{$tmpService->name()}' found from ServiceStore\n";

            // is it part of the translation table ?
            if( !isset($appsToMigrate[$tmpService->name()]) )
            {
                //echo "\n\n **** THIS SERVICE '{$tmpService->name()}' IS NOT IN THE TABLE PLEASE REVIEW IT ****\n\n";
                continue;
            }

            $tmpService->migrated = TRUE;

            $foundAction = FALSE;
            $appMigrateRecord = $appsToMigrate[$tmpService->name()];

            if( isset($appMigrateRecord['toApp']) )
            {
                $foundAction = TRUE;

                $rules = $vsys1->securityRules->rules();

                // go after all security rules and see if they are using this object
                foreach( $rules as $rule )
                {
                    if( $rule->services->hasObjectRecursive($tmpService) )
                    {
                        echo "  - Rule '{$rule->name()}' is using it\n";
                        foreach( $appMigrateRecord['toApp'] as $app )
                        {
                            echo "     - adding app '$app'' to the list of apps to add\n";
                            $rule->appsToAdd[$app] = TRUE;
                        }
                    }
                }
            }
            if( !$foundAction )
            {
                echo "\n\n **** THIS SERVICE '{$tmpService->name()}' HAS NO ASSOCIATED ACTION PLEASE FIX ****\n\n";
                exit(1);
            }

            echo "\n";
        }
    }

    /**
     * @param VirtualSystem $vsys1
     * @param PANConf $fw
     */
    public static function AppMigrationPhase2($vsys1, $fw)
    {
        $rules = $vsys1->securityRules->rules();

        foreach( $rules as $rule )
        {
            if( !isset($rule->appsToAdd) )
                continue;

            echo "- rule '{$rule->name()}' will be cloned and added the following " . count($rule->appsToAdd) . " apps : ";

            foreach( $rule->appsToAdd as $app => $value )
            {
                echo $app . ', ';
            }

            echo "\n";

            // find a name for the cloned rule
            $newRuleName = $vsys1->securityRules->findAvailableName($rule->name(), '-app');
            echo "   - cloned rule will be named '$newRuleName'\n";

            // clone said rule
            $newRule = $vsys1->securityRules->cloneRule($rule, $newRuleName);

            $rule->appConvertedRule = $newRule;

            // move it after the original one
            $vsys1->securityRules->moveRuleBefore($newRule, $rule);

            // add applications to that rule
            foreach( $rule->appsToAdd as $app => $value )
            {
                $findAppObject = $vsys1->appStore->findOrCreate($app);
                $newRule->apps->addApp($findAppObject);
                echo "   - added app '{$app}'\n";
            }

            // make rule use app-default
            $newRule->services->setApplicationDefault();

            echo "\n";
        }
    }

    /**
     * @param VirtualSystem $vsys1
     * @param PANConf $fw
     */
    public static function AppMigrationPhase3($vsys1, $fw)
    {
        $listTmpServices = array_merge($vsys1->serviceStore->serviceTmpObjects(), $fw->serviceStore->serviceTmpObjects(), $vsys1->serviceStore->all('name regex /^tmp-/'));
        foreach( $listTmpServices as $tmpService )
        {
            if( !isset($tmpService->migrated) )
                continue;

            echo " - taking care of '{$tmpService->name()}'\n";
            $references = $tmpService->getReferences();

            foreach( $references as $ref )
            {
                $class = get_class($ref);
                echo "   - reference with class '$class' found\n";

                if( $class == 'ServiceRuleContainer' )
                {
                    /** @var ServiceRuleContainer $ref */
                    if( $ref->count() == 1 )
                    {
                        echo "     - it was the last object, we will remove rule '{$ref->owner->name()}' and rename 'app' one with original name\n";
                        $rule = $ref->owner;
                        $rule->owner->remove($rule, TRUE);
                        if( isset($rule->appConvertedRule) )
                            $rule->appConvertedRule->setName($rule->name());
                    }
                    else
                    {
                        echo "     - service '{$tmpService->name()}' to be removed from rule '{$ref->owner->name()}'\n";
                        $ref->remove($tmpService);
                    }
                }
                elseif( $class == 'ServiceGroup' )
                {
                    /** @var ServiceGroup $ref */
                    echo "     - service '{$tmpService->name()}' to be removed from Group '{$ref->name()}'\n";
                    if( !$ref->removeMember($tmpService) )
                    {
                        echo " **** ERROR : removal of service group failed !!! **** \n";
                        exit(1);
                    }
                }
                else
                {
                    echo "\n\n **** THIS CLASS '$class' IS NOT SUPPORTED PLEASE IMPLEMENT USE CASE ****\n\n";
                    exit(1);
                }
            }

            echo "\n";
        }
    }
}
