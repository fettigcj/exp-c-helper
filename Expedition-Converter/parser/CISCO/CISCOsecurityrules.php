<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */


trait CISCOsecurityrules
{

    /**
     * @param array $data
     * @param VirtualSystem $v
     */
#function get_security_policies2(STRING $devicegroup, ARRAY $cisco_config_file, STRING $source, STRING $vsys, MemoryObjectsHandlerCisco &$inMemoryObjects=null):array
    function get_security_policies2($data, $v): array
    {
        global $projectdb;

        global $debug;
        global $print;


        #$AccessGroups = array();
        #$AccessGroups['global'] = new SecurityGroup('global');
        $AccessGroups['global'] = array();
        $thecolor = 1;
        $addTag = array();
        $allTags = array();

        /*
            $loadAllTags=$projectdb->query("SELECT * FROM tag WHERE source='$source' AND vsys='$vsys';");
            if ($loadAllTags->num_rows>0){
                while($loadAllTagsData=$loadAllTags->fetch_assoc()){
                    $name=$loadAllTagsData['name'];
                    $allTags[$name]=$name;
                }
            }

            $maxTagsResults=$projectdb->query("SELECT max(id) as max FROM tag");
            if($maxTagsResults->num_rows == 0){
                $tagID = 1;
            }
            else{
                $data = $maxTagsResults->fetch_assoc();
                $tagID = $data['max']+1;
            }
        */


        //Create the AccessGroups and the Tags for the AccessGroup
        foreach( $data as $line => $names_line )
        {
            if( preg_match("/^access-group /i", $names_line) )
            {
                $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

                #$group = new SecurityGroup($netObj[1]);
                $group = array();
                $group['position'] = 1;
                /*
                            $tmp_rule = $v->securityRules->find($netObj[1]);
                            if( $tmp_rule == null )
                            {
                                if( $print )
                                    print "create rule: '".$netObj[1]."'\n";
                                $tmp_rule = $v->securityRules->newSecurityRule( $netObj[1]);
                                $tmp_rule->setDisabled( true );
                            }
                            else
                            {
                                if( $debug )
                                    #mwarning(  "rule: ".$netObj[1]."  already available\n", null, false);
                                continue;
                            }
                */

                $direction = $netObj[2];
                /*
                $tmp_zone = $v->zoneStore->find($netObj[2]);
                if( $tmp_zone == null )
                    $tmp_zone = $v->zoneStore->newZone($netObj[2], 'layer3');
                */
                switch ($direction)
                {
                    case "in":
                        #$group->addZoneFrom($netObj[4]);
                        $group['addZoneFrom'] = $netObj[4];
                        #$tmp_rule->from->addZone($tmp_zone);
                        break;
                    case "out":
                        #$group->addZoneTo($netObj[4]);
                        $group['addZoneTo'] = $netObj[4];
                        #$tmp_rule->to->addZone($tmp_zone);
                        break;
                    default:
                        break;
                }

                $AccessGroups[$netObj[1]] = $group;
                #$AccessGroups[$netObj[1]] = $tmp_rule;

                #Add Tag
                $color = "color" . $thecolor;
                $tagname = $this->truncate_tags($netObj[1]);
                /*
                $query = "SELECT * FROM tag WHERE source='$source' AND vsys='$vsys' AND BINARY name ='$tagname'";
                $result = $projectdb->query($query);
                if($result->num_rows == 0) {
                    $addTag[]="('$tagID','$tagname','$source','$vsys','$color')";
                    $tagID++;
                }
                if ($thecolor == 16) {
                    $thecolor = 1;
                } else {
                    $thecolor++;
                }
                */
                $tagname = $this->truncate_tags($this->normalizeNames($tagname));
                if( $tagname != "" )
                {
                    $tmp_tag = $v->tagStore->find($tagname);

                    if( $tmp_tag == null )
                    {
                        $color = "color" . $thecolor;
                        #$tag_id = $projectdb->insert_id;
                        $tmp_tag = $v->tagStore->createTag($tagname);
                        if( $print )
                            print"    Tag create: " . $tmp_tag->name() . "\n";
                        $tmp_tag->setColor($color);

                        if( $thecolor == 16 )
                            $thecolor = 1;
                        else
                            $thecolor++;
                    }


                    #$tmp_rule->tags->addTag($tmp_tag);
                }

            }
            if( preg_match("/crypto map /", $names_line) )
            {
                $split = explode(" ", $names_line);
                if( ($split[4] == "match") and ($split[5] == "address") )
                {
                    #$group = new SecurityGroup(trim($split[6]));
                    $group = array();
                    $group['position'] = 1;
                    $AccessGroups[trim($split[6])] = $group;
                    /*
                    $tmp_rule = $v->securityRules->isRuleNameAvailable( trim($split[6]) );
                    if( $tmp_rule != FALSE )
                    {
                        if( $print )
                            print "create rule: '".trim($split[6])."'\n";
                        $tmp_rule = $v->securityRules->newSecurityRule( trim($split[6]) );
                        $tmp_rule->setDisabled( true );
                    }
                    else
                    {
                        if( $debug )
                            #mwarning(  "rule: ".$netObj[1]."  already available\n", null, false);
                        continue;
                    }

                    $AccessGroups[trim($split[6])] = $tmp_rule;
                       */

                    #Add Tag
                    $color = "color" . $thecolor;
                    $tagname = $this->truncate_tags(trim($split[6]));
                    /*
                    $query = "SELECT * FROM tag WHERE source='$source' AND vsys='$vsys' AND BINARY name ='$tagname'";
                    $result = $projectdb->query($query);
                    if($result->num_rows == 0) {
                        $addTag[]="('$tagID','$tagname','$source','$vsys','$color')";
                        $tagID++;
                    }
                    if ($thecolor == 16) {
                        $thecolor = 1;
                    } else {
                        $thecolor++;
                    }
                    */
                    $tagname = $this->truncate_tags($this->normalizeNames($tagname));
                    if( $tagname != "" )
                    {
                        $tmp_tag = $v->tagStore->find($tagname);

                        if( $tmp_tag == null )
                        {
                            $color = "color" . $thecolor;
                            #$tag_id = $projectdb->insert_id;
                            $tmp_tag = $v->tagStore->createTag($tagname);
                            $tmp_tag->setColor($color);

                            if( $thecolor == 16 )
                                $thecolor = 1;
                            else
                                $thecolor++;
                        }

                        if( $print )
                            print"    Tag add: " . $tmp_tag->name() . "\n";
                        #$tmp_rule->tags->addTag($tmp_tag);
                    }

                }
            }
        }

        /*
        if (count($addTag)>0){
            $unique=array_unique($addTag);
            $projectdb->query("INSERT INTO tag (id, name,source,vsys,color) VALUES ".implode(",",$unique).";");
            unset($unique);unset($addTag);
        }


        $inMemoryObjects->reloadTags($projectdb, $source, $vsys);
    */
        /* @var $group SecurityGroup
         * @var $groupName string
         */

        foreach( $AccessGroups as $groupName => &$group )
        {
            #$tagObj = $v->tagStore->find( $groupName );
            $tagObj = $v->tagStore->findOrCreate($groupName);
            #$tagObj = $inMemoryObjects->getTag($source, $vsys, $groupName);
            if( $tagObj != null )
                $group['setTag'] = $tagObj;
            else
            {
                mwarning("TAG: " . $groupName . " not found");
            }

        }


        /*
        # Second Round to read the ACCESS-LISTS
        #Get Last lid from Profiles
        $getlastlid = $projectdb->query("SELECT max(id) as max FROM security_rules;");
        $getLID1 = $getlastlid->fetch_assoc();
        $lid = intval($getLID1['max']) + 1;
        $getlastlid = $projectdb->query("SELECT max(position) as max FROM security_rules;");
        $getLID1 = $getlastlid->fetch_assoc();
        $position = intval($getLID1['max']) + 1;
    */

        /*
        //TODO: Remove the load of application-default?
        //$getApplicationdefault = $projectdb->query("SELECT id FROM shared_services WHERE name='application-default' AND source='$source';");
        $getApplicationdefault = $projectdb->query("SELECT id FROM services WHERE name='application-default' AND source='$source' AND vsys = 'shared';");
        if ($getApplicationdefault->num_rows == 1) {
            $getApplicationdefaultData = $getApplicationdefault->fetch_assoc();
            $application_default = $getApplicationdefaultData['id'];
        }
        else {
            add_default_services($source);
            //$getApplicationdefault = $projectdb->query("SELECT id FROM shared_services WHERE name='application-default' AND source='$source';");
            $getApplicationdefault = $projectdb->query("SELECT id FROM services WHERE name='application-default' AND source='$source' AND vsys = 'shared';");
    //            if ($getApplicationdefault->num_rows == 1) {
            $getApplicationdefaultData = $getApplicationdefault->fetch_assoc();
            $application_default = $getApplicationdefaultData['id'];
    //            }
        }*/


        //LOAD required global services
        #$any = new MemberObject('any','any','0.0.0.0','0');
        #$allTcp = $inMemoryObjects->getServiceReference($devicegroup, $source, $vsys, "all_TCP_ports");
        #$allUdp = $inMemoryObjects->getServiceReference($devicegroup, $source, $vsys, "all_UDP_ports");

        $Protocol_array = array("tcp", "udp");
        foreach( $Protocol_array as $Protocol )
        {
            $tmp_service = $v->serviceStore->find($Protocol . "-All");
            if( $tmp_service === null )
            {
                if( $print )
                    print "  * create service: " . $Protocol . "-All\n";
                $tmp_service = $v->serviceStore->newService($Protocol . "-All", $Protocol, "1-65535");
            }

            if( $Protocol == "tcp" )
                $allTcp = $tmp_service;
            else
                $allUdp = $tmp_service;
        }


        $newRuleComment = '';
        $checkFirepower = 0;
        $isFirePower = 0;


        $tmp_counter = 1;
        foreach( $data as $line => $names_line )
        {
            $skip_acl = FALSE;
            $IPv6 = FALSE;
            $names_line = trim($names_line);
            if( preg_match("/^access-list /i", $names_line) || preg_match("/^ipv6 access-list /i", $names_line) )
            {
//                echo "$names_line".PHP_EOL;

                if( preg_match("/^ipv6 access-list /i", $names_line) )
                {
                    $IPv6 = TRUE;
                    $names_line = str_replace("ipv6 access-list", "access-list", $names_line);
                }

                preg_match_all('/"(?:\\\\.|[^\\\\"])*"|\S+/', $names_line, $netObj);
                $netObj = $netObj[0];


                $tmp_rule = $v->securityRules->find($netObj[1]);
                if( $tmp_rule != null )
                {
                    mwarning("rule: " . $netObj[1] . " available\n", null, FALSE);
                }


                if( isset($AccessGroups[$netObj[1]]) )
                {


                    #print "rulename: ".$AccessGroups[$netObj[1]]->name()."\n";

                    array_shift($netObj); //Remove the "access-list" string
                    $groupName = array_shift($netObj); //


                    #print "groupname: |".$groupName."|\n";


                    /*
                    $newRule = new SecurityRuleCisco();
                    $newRule->setRuleString($names_line);
                    $newRule->setPosition($AccessGroups[$groupName]->getLastRulePosition()+1);
                    $newRule->setName( substr($groupName, 0, 15) . "_" . $newRule->position);
    */
                    $name = substr($groupName, 0, 30) . "_" . $AccessGroups[$groupName]['position'];

                    $tmp_rule = $v->securityRules->find($name);
                    if( $tmp_rule == null )
                    {
                        if( $print )
                            print "\n* create securityRule: " . $name . "\n";

                        $tmp_rule = $v->securityRules->newSecurityRule($name);
                        $tmp_protocol = "";
                        $service_source = "";
                        #$AccessGroups[$groupName]['position']++;
                    }


                    //COMMENTS
                    $remark = $netObj[0];
                    if( $remark == "remark" )
                    {
                        array_shift($netObj);
                        $newRuleComment .= addslashes(" " . implode(" ", $netObj));
                        $newRuleComment = $this->truncate_names($this->normalizeNames($newRuleComment));
                        continue;
                    }
                    else
                    {
                        //Todo: if remark then no rule
                        $AccessGroups[$groupName]['position']++;
                    }

                    //RULE NUMBER
                    $lineNumber = $netObj[0];
                    switch ($lineNumber)
                    {
                        case "line":
                            array_shift($netObj);
                            array_shift($netObj);
                            break;
                    }

                    $extended = $netObj[0];
                    switch ($extended)
                    {
                        case "advanced":
                            if( $checkFirepower == 0 )
                            {
                                $checkFirepower = 1;
                                $isFirePower = 1;
                                #$newRule->setIsFirepower(1);
                                $tmp_tag = $v->tagStore->findOrCreate("isFirepower");
                                if( $print )
                                    print "  * add Tag: isFirepower to rule: " . $tmp_rule->name();
                                $tmp_rule->tags->addTag($tmp_tag);
                            }
                        case "standard":
                        case "extended":
                            array_shift($netObj);
                            $tmp_tag = $v->tagStore->findOrCreate($groupName);
                            if( $print )
                                print "    * add Tag: " . $groupName . "\n";
                            $tmp_rule->tags->addTag($tmp_tag);
                            break;
                        case "ethertype":
                            $addlog = 'INFO - Reading Security Policies - The following ACL was not imported: "' . $names_line . '" - Level 2 rules are not supported - rules - security rules';
                            $skip_acl = TRUE;
                            break;
                    }

                    if( $skip_acl )
                    {
                        continue;
                    }

                    //ACTION
                    $action = $netObj[0];
                    switch ($action)
                    {
                        case 'permit':
                            #$newRule->setAction("allow");
                            if( $print )
                                print "    * set action allow\n";
                            $tmp_rule->setAction("allow");
                            array_shift($netObj);
                            break;
                        case "deny":
                            #$newRule->setAction("deny");
                            $tmp_rule->setAction("deny");
                            if( $print )
                                print "    * set action deny\n";
                            array_shift($netObj);
                            break;

                        default:
                            break;
                    }

                    //continue;

                    //PROTOCOL
                    $protocolObject = array_shift($netObj); //
                    switch ($protocolObject)
                    {
                        case 'object-group':
                            $value = array_shift($netObj);
                            $value = $this->truncate_names($this->normalizeNames($value));
                            #mwarning(  "object-group value: ".$value."\n", null, false);
                            #continue;


                            $tmp_service = $v->serviceStore->find($value);
                            if( !is_null($tmp_service) )
                            {
                                if( $print )
                                    print "    * add service: " . $tmp_service->name() . "\n";
                                $tmp_rule->services->add($tmp_service);
                                if( $value == "domain" )
                                    print "448\n";
                            }
                            /*
                            $object = $inMemoryObjects->getProtocolGroupReference($devicegroup, $source, $vsys, $value);
                            if($object['type'] == 'cisco_protocol_groups'){ //We received a set of protocols, such as ICMP, TCP
                                $newRule->setProtocol($object['value']);
                            }
                            elseif($object['type'] == 'services_groups_id'){  //The protocol is actually a set of services
                                $newRule->setProtocol(array());
                                $newRule->setService($object['value']);
                            }
                            */
                            break;

                        case 'object':
                            $value = array_shift($netObj);
                            $value = $this->truncate_names($this->normalizeNames($value));
                            #mwarning(  "object value: ".$value."\n", null, false);
                            #continue;
                            $tmp_service = $v->serviceStore->find($value);
                            if( !is_null($tmp_service) )
                            {
                                if( $print )
                                    print "    * add service: " . $tmp_service->name() . "\n";
                                if( $value == "domain" )
                                    print "471\n";
                                $tmp_rule->services->add($tmp_service);
                            }

                            /*
                            $object = $inMemoryObjects->getServiceReference($devicegroup, $source, $vsys, $value);
                            $newRule->setProtocol(array());
                            $newRule->setService($object);
                            //echo "OBJECT-------------------------------\n";
                            //print_r($object);
                            */
                            break;

                        case 'ip':
                            //Todo: swaschkut 20191011 how to fix protocol IP in general
                            $tmp_protocol = "ip";
                            #$tmp_protocol = "tcp";
                            #$newRule->setProtocol(["ip"]);
                            #$newRule->setService([$any]);
                            break;

                        case 'tcp':
                        case 'udp':
                            #$newRule->setProtocol([$protocolObject]);
                            $tmp_protocol = $protocolObject;
                            break;

                        case 'icmp':

                            $app = 'icmp';
                            $findAppObject = $v->appStore->findOrCreate($app);
                            $tmp_rule->apps->addApp($findAppObject);
                            echo "   - added app '{$app}'\n";
                            break;

                        case 'ah':
                            /*
                            $newRule->setService([$any]);
                            $newRule->setSourcePort([$any]);
                            $newRule->setProtocol([$protocolObject]);
                            $appObj = $inMemoryObjects->getDefaultApplication('ipsec-ah');
                            $newRule->setApplications([$appObj]);
                            */
                            $app = 'ipsec-ah';
                            $findAppObject = $v->appStore->findOrCreate($app);
                            $tmp_rule->apps->addApp($findAppObject);
                            echo "   - added app '{$app}'\n";
                            break;

                        case 'icmp6':
                            /*
                                                    $newRule->setService([$any]);
                                                    $newRule->setSourcePort([$any]);
                                                    $newRule->setProtocol([$protocolObject]);
                                                    $appObj = $inMemoryObjects->getDefaultApplication('ipv6-icmp');
                                                    $newRule->setApplications([$appObj]);
                            */
                            $app = 'ipv6-icmp';
                            $findAppObject = $v->appStore->findOrCreate($app);
                            $tmp_rule->apps->addApp($findAppObject);
                            echo "   - added app '{$app}'\n";
                            break;

                        case "gre":
                            /*
                            $newRule->setService([$any]);
                            $newRule->setSourcePort([$any]);
                            $newRule->setProtocol([$protocolObject]);
                            $appObj = $inMemoryObjects->getDefaultApplication('gre');
                            $newRule->setApplications([$appObj]);
                            */
                            $app = 'gre';
                            $findAppObject = $v->appStore->findOrCreate($app);
                            $tmp_rule->apps->addApp($findAppObject);
                            echo "   - added app '{$app}'\n";
                            break;

                        case 'esp';
                            /*
                                $newRule->setService([$any]);
                                $newRule->setSourcePort([$any]);
                                $newRule->setProtocol([$protocolObject]);
                                $appObj = $inMemoryObjects->getDefaultApplication('ipsec-esp');
                                $newRule->setApplications([$appObj]);
                            */
                            $app = 'ipsec-esp';
                            $findAppObject = $v->appStore->findOrCreate($app);
                            $tmp_rule->apps->addApp($findAppObject);
                            echo "   - added app '{$app}'\n";
                            break;

                        case 'igrp':
                            /*
                            $newRule->setService([$any]);
                            $newRule->setSourcePort([$any]);
                            $newRule->setProtocol([$protocolObject]);
                            $appObj = $inMemoryObjects->getDefaultApplication('igp');
                            $newRule->setApplications([$appObj]);
                            */
                            $app = 'igp';
                            $findAppObject = $v->appStore->findOrCreate($app);
                            $tmp_rule->apps->addApp($findAppObject);
                            echo "   - added app '{$app}'\n";
                            break;

                        case 'ipinip':
                            /*
                            $newRule->setService([$any]);
                            $newRule->setSourcePort([$any]);
                            $newRule->setProtocol([$protocolObject]);
                            $appObj = $inMemoryObjects->getDefaultApplication('ip-in-ip');
                            $newRule->setApplications([$appObj]);
                            */
                            $app = 'ip-in-ip';
                            $findAppObject = $v->appStore->findOrCreate($app);
                            $tmp_rule->apps->addApp($findAppObject);
                            echo "   - added app '{$app}'\n";
                            break;

                        case 'nos':
                            /*
                            $newRule->setService([$any]);
                            $newRule->setSourcePort([$any]);
                            $newRule->setProtocol([$protocolObject]);
                            $appObj = $inMemoryObjects->getDefaultApplication('ipip');
                            $newRule->setApplications([$appObj]);
                            */
                            $app = 'ipip';
                            $findAppObject = $v->appStore->findOrCreate($app);
                            $tmp_rule->apps->addApp($findAppObject);
                            echo "   - added app '{$app}'\n";
                            break;

                        case 'pcp':
                            /*
                            $newRule->setService([$any]);
                            $newRule->setSourcePort([$any]);
                            $newRule->setProtocol([$protocolObject]);
                            $appObj = $inMemoryObjects->getDefaultApplication('ipcomp');
                            $newRule->setApplications([$appObj]);
                            */
                            $app = 'ipcomp';
                            $findAppObject = $v->appStore->findOrCreate($app);
                            $tmp_rule->apps->addApp($findAppObject);
                            echo "   - added app '{$app}'\n";
                            break;


                        default:  //This should be for ICMP/PIM/IPX


                            //Todo: SVEN add APP_ID????
                            #$newRule->setService([$any]);
//                        $newRule->setSourcePort([$any]);
                            #$newRule->setProtocol([$protocolObject]);
                            break;
                    }

//                    $localPosition = $AccessGroups[$groupName]->getLastRulePosition();
//                    $newRule->setPosition($localPosition+1);

                    //Todo: SWASCHKUT check if correctly placed
                    #$AccessGroups[$groupName]['position']++;


                    //OTHER FIELDS INCLUDING: Source, Destination, SourcePort, DestinationPort, User, Log
                    # Adding experimental support for FirePower
                    $rule_object_set = array();
                    while( count($netObj) > 0 )
                    {
                        $currentField = array_shift($netObj);

                        #print "currentfield: ".$currentField."\n";

                        if( $currentField != "webtype" && $currentField != "ethertype" )
                        {
                            #$newRule->setComment($newRuleComment);

                            if( $newRuleComment != "" )
                            {
                                if( $print )
                                    print "    * description: " . $newRuleComment . "\n";
                                $tmp_rule->setDescription($newRuleComment);
                                $newRuleComment = '';
                            }

                            switch ($currentField)
                            {

                                case "ifc":
                                    $value = array_shift($netObj);
                                    mwarning("ifc not implemented yet: value: " . $value, null, FALSE);
                                    /*
                                    $missingFields = $newRule->getMissingFields();
                                    if(in_array('zoneFrom', $missingFields)){
                                        $newRule->addZoneFrom($value);
                                    }
                                    elseif(in_array('zoneTo', $missingFields)){
                                        $newRule->addZoneTo($value);
                                    }
                                    */
                                    break;
                                case "rule-id":
                                    $value = array_shift($netObj);
                                    mwarning('rule-id not implemented yet; value:' . $value, null, FALSE);

                                    #$newRule->setFirepowerId($value);
                                    break;
                                case "any":
                                case "any4":
                                case "any6":
                                    /*
                                    $missingFields = $newRule->getMissingFields();
                                    if(in_array('source', $missingFields)){
                                        $newRule->setSource($any);
                                    }
                                    elseif(in_array('destination', $missingFields)){
                                        $newRule->setDestination($any);
                                    }
                                    elseif(in_array('service', $missingFields)){
                                        $newRule->setService($any);
                                    }
    //                                $newRule->fillAddress($any);
                                    */

                                    if( !isset($rule_object_set['source']) )
                                    {
                                        print "    * source set to ANY\n";
                                        $tmp_rule->source->setAny();
                                        $rule_object_set['source'] = "set";
                                    }
                                    elseif( !isset($rule_object_set['destination']) )
                                    {
                                        print "    * destination set to ANY\n";
                                        $tmp_rule->destination->setAny();
                                        $rule_object_set['destination'] = "set";
                                    }
                                    elseif( !isset($rule_object_set['service']) )
                                    {
                                        print "    * service set to ANY\n";
                                        $tmp_rule->services->setAny();
                                        $rule_object_set['service'] = "set";
                                    }
                                    break;

                                case "object":
                                    $value = array_shift($netObj);
                                    $value = CISCOASA::removeEnclosingQuotes($value);
                                    $value = $this->truncate_names($this->normalizeNames($value));
                                    #mwarning( "object found; value: ".$value , null, false);

                                    $ipversion = $this->ip_version($value);
                                    if( $ipversion == 'v4' )
                                    {
                                        $tmp_prefix = "H-";
                                        $netmask = "/32";
                                    }
                                    elseif( $ipversion == 'v6' )
                                    {
                                        $tmp_prefix = "H-";
                                        $netmask = "/128";
                                    }
                                    else
                                    {
                                        $tmp_prefix = "";
                                        $netmask = "";
                                        #mwarning( "no v4 or v6 ipversion found value:". $value );
                                    }

                                    $tmp_address = $v->addressStore->find($tmp_prefix . $value);
                                    if( $tmp_address === null )
                                    {
                                        if( $print )
                                            print "     * create address object: " . $tmp_prefix . $value . " , value: " . $value . $netmask . "\n";
                                        $tmp_address = $v->addressStore->newAddress($tmp_prefix . $value, 'ip-netmask', $value . $netmask);
                                    }


                                    if( !isset($rule_object_set['source']) )
                                    {
                                        if( $tmp_address !== null )
                                        {
                                            if( $print )
                                                print "    * add source - address object: " . $tmp_prefix . $value . " , value: " . $value . $netmask . "\n";
                                            $tmp_rule->source->addObject($tmp_address);
                                            $rule_object_set['source'] = "set";
                                        }
                                    }
                                    elseif( !isset($rule_object_set['destination']) )
                                    {
                                        if( $tmp_address !== null )
                                        {
                                            if( $print )
                                                print "    * add destination - address object: " . $tmp_prefix . $value . " , value: " . $value . $netmask . "\n";
                                            $tmp_rule->destination->addObject($tmp_address);
                                            $rule_object_set['destination'] = "set";
                                        }
                                    }
                                    else
                                    {
                                        mwarning("Problem 1959: object value in a wrong position? $names_line\n");
                                    }

                                    /*
                                    $missingFields = $newRule->getMissingFields();
                                    //If we have filled in the Source, Destination and Service, we then missed the SourcePort in prior loads. We need to correct it :(
                                    if($object = $inMemoryObjects->getAddressReference($devicegroup, $source, $vsys, $value)){
                                        if(in_array('source', $missingFields)){
                                            $newRule->setSource($object);
                                        }
                                        elseif(in_array('destination', $missingFields)){
                                            $newRule->setDestination($object);
                                        }
                                        else{
                                            $newRule->addLog('warning',
                                                'Reading Security Policies',
                                                'Security RuleID [_RuleLid_] is using an object-group ['.$value.'] but both Source and Destination are filled. ['.$names_line.']',
                                                'Review the security rule for precaution');
                                            echo "Problem object:address:fields_full => This object is not recognized. [$names_line]\n";
                                        }
                                    }
                                    elseif($objects = $inMemoryObjects->getServiceReference($devicegroup, $source, $vsys, $value, $newRule->protocol)){
                                        if(in_array('destination', $missingFields)){
                                            $newRule->setSourcePort($objects);
                                        }
                                        elseif(in_array('service', $missingFields)){
                                            $newRule->setService($objects);
                                        }
                                        else{
                                            $newRule->addLog('warning',
                                                'Reading Security Policies',
                                                'Security RuleID [_RuleLid_] is using an object-group ['.$value.'] but both SourcePort and Service are filled. ['.$names_line.']',
                                                'Review the security rule for precaution');
                                            echo "Problem object:service:fields_full => This object is not recognized [$names_line]\n";
                                        }
                                    }
                                    else{
                                        $newRule->addLog('warning',
                                            'Reading Security Policies',
                                            'Security RuleID [_RuleLid_] is using an object ['.$value.'] but this Object is unknown. ['.$names_line.']',
                                            'Review the security rule for precaution');
                                        echo "Problem object:unknown_object => This object is not recognized [$names_line]\n";
                                    }
                                    */

//                                if(!in_array('source', $missingFields) && !in_array('destination', $missingFields) && !in_array('service', $missingFields)) {
//                                    $object = $inMemoryObjects->getServiceReference($devicegroup, $source, $vsys, $value, $newRule->protocol);
//                                    $newRule->correctSourcePort($devicegroup, $source, $vsys, $inMemoryObjects, $object);
//                                }
//                                else{
//                                    if(in_array('source', $missingFields)){
//                                        $object = $inMemoryObjects->getAddressReference($devicegroup, $source, $vsys, $value);
//                                        $newRule->setSource($object);
//                                    }
//                                    elseif(in_array('destination', $missingFields)){
//                                        $object = $inMemoryObjects->getAddressReference($devicegroup, $source, $vsys, $value);
//                                        $newRule->setDestination($object);
//                                    }
//                                    elseif(in_array('service', $missingFields)){
//                                        $object = $inMemoryObjects->getServiceReference($devicegroup, $source, $vsys, $value, $newRule->protocol);
//                                        $newRule->setService($object);
//                                    }
//                                    else{
//                                        echo "Problem 1894: This object is not recognized";
//                                    }
//                                }

                                    break;

                                case "object-group":
                                    $value = array_shift($netObj);
                                    $value = CISCOASA::removeEnclosingQuotes($value);
                                    $value = $this->truncate_names($this->normalizeNames($value));
                                    #mwarning( "object-group found; value: ".$value , null, false);


                                    #if( !in_array('source', $rule_object_set))
                                    if( !isset($rule_object_set['source']) )
                                    {

                                        $tmp_address = $v->addressStore->find($value);
                                        if( $tmp_address === null )
                                        {
                                            mwarning("addressgroup: " . $value . " not found", null, FALSE);
                                        }
                                        if( $tmp_address !== null )
                                        {
                                            if( $print )
                                                print "    * add source - addressgroup object: " . $value . "\n";
                                            $tmp_rule->source->addObject($tmp_address);
                                            $rule_object_set['source'] = "set";
                                        }
                                    }
                                    #elseif( !in_array('destination', $rule_object_set))
                                    elseif( !isset($rule_object_set['destination']) )
                                    {
                                        $tmp_address = $v->addressStore->find($value);
                                        if( $tmp_address === null )
                                        {

                                            $tmp_service = $v->serviceStore->find($value);
                                            if( $tmp_service !== null )
                                            {
                                                print "addressgroup not found - now it is service source, but how to implement\n" . $names_line . "\n";
                                                print "which port information is needed?\n";
                                                foreach( $tmp_service->members() as $member )
                                                {
                                                    if( $member->isService() )
                                                        print "member: " . $member->name() . " - sourceport: |" . $member->getSourcePort() . "| dstport: |" . $member->getDestPort() . "|\n";
                                                    else
                                                    {
                                                        foreach( $tmp_service->members() as $member )
                                                        {
                                                            if( $member->isService() )
                                                                print "member: " . $member->name() . " - sourceport: |" . $member->getSourcePort() . "| dstport: |" . $member->getDestPort() . "|\n";
                                                            else
                                                            {
                                                                mwarning("this is not a solution fix it finaly");
                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                            else
                                            {
                                                mwarning("servicegroup: " . $value . " not found", null, FALSE);
                                            }

                                        }
                                        if( $tmp_address !== null )
                                        {
                                            if( $print )
                                                print "    * add destination - addressgroup object: " . $value . "\n";
                                            $tmp_rule->destination->addObject($tmp_address);
                                            $rule_object_set['destination'] = "set";
                                        }
                                    }
                                    elseif( !isset($rule_object_set['service']) )
                                    {
                                        if( $value == "domain" )
                                            print "896\n";
                                        $tmp_service = $v->serviceStore->find($value);
                                        if( $tmp_service === null )
                                        {
                                            mwarning("servicegroup: " . $value . " not found", null, FALSE);
                                        }
                                        if( $tmp_service !== null )
                                        {
                                            if( $print )
                                                print "    * add service - servicegroup object: " . $value . "\n";
                                            $tmp_rule->services->add($tmp_service);
                                            if( $value == "domain" )
                                                print "911\n";

                                            $rule_object_set['service'] = "set";
                                        }
                                    }
                                    else
                                    {
                                        mwarning("Problem 1960: object-group value in a wrong position? $names_line\n");
                                    }

                                    /*
                                    $missingFields = $newRule->getMissingFields();

                                    //Let's check it is a service or an address
                                    if($object = $inMemoryObjects->getAddressGroupReference($devicegroup, $source, $vsys, $value)){
                                        if(in_array('source', $missingFields)){
                                            $newRule->setSource($object);
                                        }
                                        elseif(in_array('destination', $missingFields)){
                                            $newRule->setDestination($object);
                                        }
                                        else{
                                            $newRule->addLog('warning',
                                                'Reading Security Policies',
                                                'Security RuleID [_RuleLid_] is using an object-group ['.$value.'] but both Source and Destination are filled. ['.$names_line.']',
                                                'Review the security rule for precaution');
                                            echo "Problem object-group:address:fields_full => This object is not recognized [$names_line]\n";
                                        }
                                    }
                                    elseif($objects = $inMemoryObjects->getServiceGroupReference($devicegroup, $source, $vsys, $value)){

                                        if(in_array('destination', $missingFields)){
                                            $newRule->setSourcePort($objects);
                                        }
                                        elseif(in_array('service', $missingFields)){
                                            $newRule->setService($objects);
                                        }
                                        else{
                                            $newRule->addLog('warning',
                                                'Reading Security Policies',
                                                'Security RuleID [_RuleLid_] is using an object-group ['.$value.'] but both SourcePort and Service are filled. ['.$names_line.']',
                                                'Review the security rule for precaution');
                                            echo "Problem object-group:service:fields_full => This object is not recognized [$names_line]\n";
                                        }
                                    }
                                    else{
                                        $newRule->addLog('warning',
                                            'Reading Security Policies',
                                            'Security RuleID [_RuleLid_] is using an object-group ['.$value.'] but this Object is unknown. ['.$names_line.']',
                                            'Review the security rule for precaution');
                                        echo "Problem object-group:unknwon_object => This object is not recognized [$names_line]\n";
                                    }
                                    */

                                    break;

                                case "host":
                                    $value = array_shift($netObj);
                                    $value = $this->truncate_names($this->normalizeNames($value));
                                    #mwarning( "host found; value: ".$value , null, false);

                                    #$missingFields = $newRule->getMissingFields();
                                    #if( !in_array('source', $rule_object_set))

                                    $ipversion = $this->ip_version($value);
                                    if( $ipversion == 'v4' )
                                    {
                                        #$object = $inMemoryObjects->getIPAddressReference($devicegroup, $source, $vsys, $value, 32);
                                        #$newRule->setSource($object);
                                        $netmask = "32";
                                        $tmp_prefix = "H-";
                                    }
                                    elseif( $ipversion == 'v6' )
                                    {
                                        $netmask = "128";
                                        $tmp_prefix = "H-";
                                    }
                                    else
                                    {
                                        $tmp_prefix = "";
                                        $netmask = "";
                                        #mwarning( "no v4 or v6 ipversion found for host" );
                                    }

                                    $tmp_address = $v->addressStore->find($tmp_prefix . $value);
                                    if( $tmp_address === null )
                                    {
                                        if( $tmp_prefix == "" && $netmask == "" )
                                            mwarning("something wrong with host object", null, FALSE);
                                        else
                                        {
                                            if( $print )
                                                print "    * create address object: H-" . $value . " , value: " . $value . "/" . $netmask . "\n";
                                            $tmp_address = $v->addressStore->newAddress("H-" . $value, 'ip-netmask', $value . "/" . $netmask);
                                        }

                                    }

                                    if( !isset($rule_object_set['source']) )
                                    {

                                        if( $tmp_address !== null )
                                        {
                                            if( $print )
                                                print "    * add source - address object: H-" . $value . " , value: " . $value . "/" . $netmask . "\n";
                                            $tmp_rule->source->addObject($tmp_address);
                                            $rule_object_set['source'] = "set";
                                        }

                                        /*
                                        print "search IP:".$value."\n";
                                        $tmp_address1 = $v->addressStore->all( 'value string.regex /'.$value.'/' );

                                        if( !empty( $tmp_address1 ) && count( $tmp_address1 )  == 1 && $tmp_address1[0]->getNetworkValue() == $value )
                                        {
                                            print_r( $tmp_address1 );
                                            #print "mip existing ip name ".$tmp_address1[0]->name()." value: ".$tmp_address1[0]->value()." add to rule\n";
                                            $tmp_address1 = $tmp_address1[0];
                                            $tmp_rule->source->addObject($tmp_address1);
                                        }
                                        else
                                        {
                                            if( $print )
                                                print "  * create address object: H-".$value." , value: ".$value."/32\n";
                                            $tmp_address = $v->addressStore->newAddress( "H-".$value, 'ip-netmask', $value."/32" );
                                            $tmp_rule->source->addObject($tmp_address);
                                        }
                                        */

                                        /*
                                        if ($ipversion == 'ipv6') {
                                            $object = $inMemoryObjects->getIPAddressReference($devicegroup, $source, $vsys, $value, 128);
                                            $newRule->setSource($object);
                                        } else {
                                            $object = $inMemoryObjects->getAddressReference($devicegroup, $source, $vsys, $value, 32);
                                            $newRule->setSource($object);
                                        }
                                        */

                                    }
                                    elseif( !isset($rule_object_set['destination']) )
                                    {
                                        if( $tmp_address !== null )
                                        {
                                            if( $print )
                                                print "    * add destination - address object: H-" . $value . " , value: " . $value . "/" . $netmask . "\n";
                                            $tmp_rule->destination->addObject($tmp_address);
                                            $rule_object_set['destination'] = "set";
                                        }


                                        /*
                                        $ipversion = $this->ip_version($value);
                                        if ($ipversion == 'v4') {
                                            #$object = $inMemoryObjects->getIPAddressReference($devicegroup, $source, $vsys, $value, 32);
                                            #$newRule->setDestination($object);

                                            print "search IP:".$value."\n";
                                            $tmp_address1 = $v->addressStore->all( 'value string.regex /'.$value.'/' );

                                            print_r( $tmp_address1 );

                                            if( !empty( $tmp_address1 ) && count( $tmp_address1 )  == 1 && $tmp_address1[0]->getNetworkValue() == $value )
                                            {
                                                print_r( $tmp_address1 );
                                                #print "mip existing ip name ".$tmp_address1[0]->name()." value: ".$tmp_address1[0]->value()." add to rule\n";
                                                $tmp_address1 = $tmp_address1[0];
                                                $tmp_rule->destination->addObject($tmp_address1);
                                            }
                                            else
                                            {
                                                if( $print )
                                                    print "  * create address object: H-".$value." , value: ".$value."/32\n";
                                                $tmp_address = $v->addressStore->newAddress( "H-".$value, 'ip-netmask', $value."/32" );
                                                $tmp_rule->source->addObject($tmp_address);
                                            }
                                        }
                                        */
                                        /*
                                        if ($ipversion == 'ipv6') {
                                            $object = $inMemoryObjects->getIPAddressReference($devicegroup, $source, $vsys, $value, 128);
                                            $newRule->setDestination($object);
                                        } else {
                                            $object = $inMemoryObjects->getAddressReference($devicegroup, $source, $vsys, $value, 32);
                                            $newRule->setDestination($object);
                                        }
                                        */

                                    }
                                    else
                                    {
                                        echo "Problem 1959: Host value in a wrong position? $names_line\n";
                                    }

                                    break;

                                case "interface":
                                    $value = array_shift($netObj);
                                    $value = $this->truncate_names($this->normalizeNames($value));
                                    #mwarning( "interface found; value: ".$value , null, false);
                                    #print "LINE: ".$names_line."\n";

                                    $tmp_zone = $v->zoneStore->find($value);

                                    if( !isset($rule_object_set['source']) )
                                    {
                                        if( $print )
                                            print "    * add source ANY and set from: " . $value . "\n";
                                        $tmp_rule->source->setAny();
                                        if( $tmp_zone !== null )
                                            $tmp_rule->from->addZone($tmp_zone);
                                        $rule_object_set['source'] = "set";
                                    }
                                    elseif( !isset($rule_object_set['destination']) )
                                    {
                                        if( $print )
                                            print "    * add destination ANY and set to: " . $value . "\n";
                                        $tmp_rule->destination->setAny();

                                        if( $tmp_zone !== null )
                                            $tmp_rule->to->addZone($tmp_zone);
                                        $rule_object_set['destination'] = "set";
                                    }
                                    /*
                                    $missingFields = $newRule->getMissingFields();
                                    if(in_array('source', $missingFields)){
                                        $newRule->setZoneFrom([$value]);
                                        $newRule->setSource($any);
                                    }
                                    elseif(in_array('destination', $missingFields)){
                                        $newRule->setZoneTo([$value]);
                                        $newRule->setDestination($any);
                                    }
                                    */
                                    break;
                                case "inactive":
                                    #$newRule->setDisabled();
                                    $tmp_rule->setDisabled(TRUE);
                                    break;

                                case "user-group":
                                case "user":
                                    $value = array_shift($netObj);
                                    $value = CISCOASA::removeEnclosingQuotes($value);
                                    $value = CISCOASA::removeDoubleBackSlash($value);
                                    $value = $this->truncate_names($this->normalizeNames($value));
                                    mwarning("user found: " . $value . "\n", null, FALSE);
                                    #$newRule->setUser([$value]);
                                    break;

                                case "object-group-user":
                                    $value = array_shift($netObj);
                                    $value = $this->truncate_names($this->normalizeNames($value));
                                    mwarning("user found: " . $value . "\n", null, FALSE);
                                    #$object = $inMemoryObjects->getUsers($devicegroup, $source, $vsys, $value);
                                    #$newRule->setUser($object);
                                    break;

                                // Cases for SERVICES
                                case "neq":
                                    #$missingFields = $newRule->getMissingFields();
                                    $value = array_shift($netObj);
                                    $value = $this->truncate_names($this->normalizeNames($value));
                                    #$object = $inMemoryObjects->getServiceNEQ($devicegroup, $source, $vsys, $value, $newRule->protocol);
                                    mwarning("neq found; value: " . $value, null, FALSE);

                                    /*
                                     * if(in_array('destination', $missingFields)){
                                        $newRule->setSourcePort($object);
                                    }
                                    else {
                                        $newRule->setService($object);
                                    }
                                    */
                                    break;

                                case "eq":
                                    #$missingFields = $newRule->getMissingFields();
                                    $value = array_shift($netObj);
                                    $value = $this->truncate_names($this->normalizeNames($value));

                                    #$object = $inMemoryObjects->getServiceEQ($devicegroup, $source, $vsys, $value, $newRule->protocol);
                                    #mwarning( "eq found; value: ".$value , null, false);
                                    /*
                                    if(in_array('destination', $missingFields)){
                                        $newRule->setSourcePort($object);
                                    }
                                    else {
                                        $newRule->setService($object);
                                    }
                                    */

                                    if( !isset($rule_object_set['destination']) )
                                    {
                                        print "1WARNING sven\n";
                                        print "LINE: " . $names_line . "\n";

                                        mwarning("eq found but service source must be set, value: " . $value, null, FALSE);
                                        $service_source = $tmp_protocol . "/" . $value;
                                        if( $tmp_protocol == "ip" )
                                        {
                                            //Todo: further tasks needed to migrate
                                            mwarning("protocol IP found but TCP is used: " . $service_source);
                                            $tmp_protocol = "tcp";
                                        }

                                    }
                                    else
                                    {

                                        if( $service_source != "" )
                                            mwarning("service source need to be set: " . $service_source);

                                        print "find value: " . $value . "\n";
                                        $tmp_service = $v->serviceStore->find($value);
                                        if( $tmp_service === null )
                                        {
                                            if( $tmp_protocol == "" || $tmp_protocol == "ip" )
                                            {
                                                //Todo: SVEN 20200203
                                                //if IP then create tcp and udp, plus a service group where both are in
                                                $tmp_protocol = "tcp";
                                            }


                                            $tmp_service = $v->serviceStore->find($tmp_protocol . "-" . $value);
                                            if( $tmp_service === null )
                                            {
                                                if( $print )
                                                    print "     * create service: " . $tmp_protocol . "-" . $value . "\n";
                                                $tmp_service = $v->serviceStore->newService($tmp_protocol . "-" . $value, $tmp_protocol, $value);
                                            }
                                        }
                                        if( $tmp_service !== null )
                                        {
                                            /*
                                            if( $tmp_service->isGroup() )
                                            {
                                                $tmp_service_grp = $v->serviceStore->find( $tmp_protocol."-".$value );
                                                #mwarning( "servicgroup found and protocol is: ".$tmp_protocol );
                                                if( $tmp_service_grp !== null )
                                                    $tmp_service = $tmp_service_grp;
                                                else
                                                {
                                                    if( $print )
                                                        print "     * create service: " . $tmp_protocol."-".$value . "\n";
                                                    $tmp_service = $v->serviceStore->newService($tmp_protocol."-".$value, $tmp_protocol, $value);
                                                }
                                            }*/
                                            if( !$tmp_service->isGroup() && $tmp_protocol != "" && $tmp_service->protocol() != $tmp_protocol )
                                            {
                                                if( !is_numeric($value) )
                                                {
                                                    $tmp_service2 = $v->serviceStore->find($value);
                                                    if( $tmp_service2 !== null )
                                                    {
                                                        $value = $tmp_service2->getDestPort();
                                                    }
                                                }
                                                $tmp_service = $v->serviceStore->find($tmp_protocol . "-" . $value);
                                                #mwarning( "servicgroup found and protocol is: ".$tmp_protocol );
                                                if( $tmp_service === null )
                                                {
                                                    if( $print )
                                                        print "     * create service: " . $tmp_protocol . "-" . $value . "\n";
                                                    $tmp_service = $v->serviceStore->newService($tmp_protocol . "-" . $value, $tmp_protocol, $value);
                                                }
                                            }
                                            if( $print )
                                                print "    * add service - service object: " . $tmp_service->name() . "\n";
                                            $tmp_rule->services->add($tmp_service);
                                            if( $value == "domain" )
                                                print "1254\n";

                                            $rule_object_set['service'] = "set";
                                        }
                                    }

                                    break;

                                case "lt":
                                    #$missingFields = $newRule->getMissingFields();
                                    $value = array_shift($netObj);
                                    $value = $this->truncate_names($this->normalizeNames($value));
                                    #$object = $inMemoryObjects->getServiceLT($devicegroup, $source, $vsys, $value, $newRule->protocol);

                                    mwarning("lt found; value: " . $value, null, FALSE);
                                    /*
                                    if(in_array('destination', $missingFields)){
                                        $newRule->setSourcePort($object);
                                    }
                                    else {
                                        $newRule->setService($object);
                                    }
                                    */

                                    if( !isset($rule_object_set['destination']) )
                                    {
                                        print "2WARNING sven\n";
                                        print "LINE: " . $names_line . "\n";

                                        mwarning("service source must be set");

                                    }
                                    elseif( !isset($rule_object_set['service']) )
                                    {
                                        //$tmp_protocol
                                        if( $tmp_protocol == "ip" )
                                        {
                                            //Todo: further tasks needed to migrate
                                            $tmp_protocol = "tcp";
                                        }

                                        $port = "1-" . (intval($value) - 1);
                                        if( $tmp_protocol != "" )
                                        {
                                            $tmp_service = $v->serviceStore->find($tmp_protocol . "-" . $port);
                                            if( $tmp_service === null )
                                            {
                                                if( $print )
                                                    print "   * create service " . $tmp_protocol . "-" . $port . "\n";
                                                $tmp_service = $v->serviceStore->newService($tmp_protocol . "-" . $port, $tmp_protocol, $port);
                                            }
                                            if( $print )
                                                print "   * add service: " . $tmp_service->name() . "\n";
                                            $tmp_rule->services->add($tmp_service);
                                            if( $value == "domain" )
                                                print "1309\n";
                                        }
                                    }
                                    break;

                                case "gt":
                                    #$missingFields = $newRule->getMissingFields();
                                    $value = array_shift($netObj);
                                    $value = $this->truncate_names($this->normalizeNames($value));
                                    #$object = $inMemoryObjects->getServiceGT($devicegroup, $source, $vsys, $value, $newRule->protocol);
                                    #mwarning( "gt found; value: ".$value , null, false);
                                    /*
                                    if(in_array('destination', $missingFields)){
                                        $newRule->setSourcePort($object);
                                    }
                                    else {
                                        $newRule->setService($object);
                                    }
                                    */

                                    if( !isset($rule_object_set['destination']) )
                                    {
                                        print "3WARNING sven\n";
                                        print "LINE: " . $names_line . "\n";

                                        mwarning("service source must be set");

                                    }
                                    elseif( !isset($rule_object_set['service']) )
                                    {
                                        //$tmp_protocol
                                        if( $tmp_protocol == "ip" )
                                        {
                                            //Todo: further tasks needed to migrate
                                            $tmp_protocol = "tcp";
                                        }


                                        $port = (intval($value) + 1) . "-65535";
                                        if( $tmp_protocol != "" )
                                        {
                                            $tmp_service = $v->serviceStore->find($tmp_protocol . "-" . $port);
                                            if( $tmp_service === null )
                                            {
                                                if( $print )
                                                    print "   * create service " . $tmp_protocol . "-" . $port . "\n";
                                                $tmp_service = $v->serviceStore->newService($tmp_protocol . "-" . $port, $tmp_protocol, $port);
                                            }
                                            if( $print )
                                                print "   * add service: " . $tmp_service->name() . "\n";
                                            $tmp_rule->services->add($tmp_service);
                                            if( $value == "domain" )
                                                print "1360\n";
                                        }
                                    }


                                    break;

                                case "range":
                                    #$missingFields = $newRule->getMissingFields();
                                    $valueStart = array_shift($netObj);
                                    $valueEnd = array_shift($netObj);


                                    if( !is_numeric($valueStart) )
                                    {
                                        $tmp_service = $v->serviceStore->find($valueStart);
                                        if( $tmp_service !== null )
                                        {
                                            $valueStart = $tmp_service->getDestPort();
                                        }
                                    }
                                    if( !is_numeric($valueEnd) )
                                    {
                                        $tmp_service = $v->serviceStore->find($valueEnd);
                                        if( $tmp_service !== null )
                                        {
                                            $valueEnd = $tmp_service->getDestPort();
                                        }
                                    }

                                    //Todo if $valuestart or $valueEnd nnot numeric =>


                                    $value = $valueStart . "-" . $valueEnd;
                                    $value = $this->truncate_names($this->normalizeNames($value));
                                    #$object = $inMemoryObjects->getServiceRangeReference($devicegroup, $source, $vsys, $valueStart, $valueEnd, $newRule->protocol);


                                    if( !isset($rule_object_set['destination']) )
                                    {
                                        mwarning("service source: range found; value: " . $value, null, FALSE);
                                        print "service source must be set now\n";
                                        if( $tmp_protocol == "ip" )
                                        {
                                            //Todo: further tasks needed to migrate
                                            $tmp_protocol = "tcp";
                                        }
                                        $service_source = $tmp_protocol . "/" . $value;
                                    }
                                    elseif( !isset($rule_object_set['service']) )
                                    {
                                        if( $service_source != "" )
                                            mwarning("service source need to be set: " . $service_source);

                                        if( $tmp_protocol == "ip" )
                                        {
                                            //Todo: further tasks needed to migrate
                                            $tmp_protocol = "tcp";
                                        }


                                        if( $tmp_protocol != "" )
                                        {
                                            $tmp_service = $v->serviceStore->find($tmp_protocol . "-" . $value);
                                            if( $tmp_service === null )
                                            {
                                                if( $print )
                                                    print "   * create service " . $tmp_protocol . "-" . $value . "\n";
                                                $tmp_service = $v->serviceStore->newService($tmp_protocol . "-" . $value, $tmp_protocol, $value);
                                            }
                                            if( $print )
                                                print "   * add service: " . $tmp_service->name() . "\n";
                                            $tmp_rule->services->add($tmp_service);
                                            if( $value == "domain" )
                                                print "1414\n";
                                        }
                                        else
                                        {
                                            #print "LINE: ".$names_line."\n";
                                            #mwarning( "could be object-group TCPUDP - but where to find?" );

                                            $tmp_service_group = $v->serviceStore->find("TCPUDP-" . $value);
                                            if( $tmp_service_group === null )
                                            {
                                                //create service group
                                                if( $print )
                                                    print "   * create servicegroup: 'TCPUDP-" . $value . "'\n";
                                                $tmp_service_group = $v->serviceStore->newServiceGroup("TCPUDP-" . $value);

                                                $tmp_service = $v->serviceStore->find("tcp-" . $value);
                                                if( $tmp_service == null )
                                                {
                                                    if( $print )
                                                        print "   * create service: 'tcp-" . $value . "'\n";
                                                    $tmp_service = $v->serviceStore->newService('tcp-' . $value, 'tcp', $value);
                                                }
                                                if( $tmp_service !== null )
                                                {
                                                    if( $print )
                                                        print "   * add tcp service: " . $tmp_service->name() . "'\n";
                                                    $tmp_service_group->addMember($tmp_service);
                                                }

                                                $tmp_service = $v->serviceStore->find("udp-" . $value);
                                                if( $tmp_service == null )
                                                {
                                                    if( $print )
                                                        print "   * create service: 'udp-" . $value . "'\n";
                                                    $tmp_service = $v->serviceStore->newService('udp-' . $value, 'udp', $value);
                                                }
                                                if( $tmp_service !== null )
                                                {
                                                    if( $print )
                                                        print "   * add udp service: " . $tmp_service->name() . "'\n";
                                                    $tmp_service_group->addMember($tmp_service);
                                                }
                                            }
                                            if( $tmp_service_group !== null )
                                            {
                                                if( $print )
                                                    print "   * add service: " . $tmp_service_group->name() . "\n";
                                                $tmp_rule->services->add($tmp_service_group);
                                                if( $value == "domain" )
                                                    print "1462\n";
                                            }
                                        }


                                    }


                                    /*
                                    if(in_array('destination', $missingFields)){
                                        $newRule->setSourcePort($object);
                                    }
                                    else {
                                        $newRule->setService($object);
                                    }
                                    */
                                    break;


                                case "echo":
                                case "echo-reply":
                                    /*
                                    if(in_array('icmp',$newRule->protocol)){
                                        $object = $inMemoryObjects->getDefaultApplication('ping');
                                        if(!is_null($object)) {
                                            $newRule->addApplication($object);
                                        }
                                    }
                                    */
                                    $app = 'ping';
                                    $findAppObject = $v->appStore->findOrCreate($app);
                                    $tmp_rule->apps->addApp($findAppObject);
                                    echo "   - app add: '{$app}'\n";
                                    #mwarning( "echo / echo-reply found; value: " , null, false);
                                    break;
                                case "source-quench":
                                    //This is a type of icmp
                                    break;

                                case "traceroute":
                                    /*
                                    if(in_array('icmp',$newRule->protocol)){
                                        $object = $inMemoryObjects->getDefaultApplication('traceroute');
                                        if(!is_null($object)) {
                                            $newRule->addApplication($object);
                                        }
                                    }
                                    */
                                    $app = 'icmp';
                                    $findAppObject = $v->appStore->findOrCreate($app);
                                    $tmp_rule->apps->addApp($findAppObject);
                                    echo "   - app add: '{$app}'\n";
                                    $app = 'ping';
                                    $findAppObject = $v->appStore->findOrCreate($app);
                                    $tmp_rule->apps->addApp($findAppObject);
                                    echo "   - app add: '{$app}'\n";
                                    $app = 'traceroute';
                                    $findAppObject = $v->appStore->findOrCreate($app);
                                    $tmp_rule->apps->addApp($findAppObject);
                                    echo "   - app add: '{$app}'\n";
                                    #mwarning( "traceroute found; value: " , null, false);
                                    break;

                                case "unreachable":
                                    /*
                                    if(in_array('icmp',$newRule->protocol)){
                                        $object = getApplicationSnippet($projectdb, 'icmp_destination_unreachable', $source, $vsys);
    //                                    $object =new MemberObject('custom_application','applications','icmp_destination_unreachable','snippet');
                                        $newRule->addApplication($object);
                                    }*/
                                    mwarning("unreachable found; value: ", null, FALSE);
                                    break;

                                case "log":
                                case "disable":
                                case "warnings":
                                case "default":
                                case "debugging":
                                case "time-exceeded":
                                case "notifications":
                                case "critical":
//                            case "unreachable":
                                    break;

                                case "interval":
                                    $interval = array_shift($netObj);
                                    break;

                                case "time-range":
                                    $starting_date = array_shift($netObj);

                                    $addlog = "time-range field with value ['.$starting_date.'] is used. This field has not been migrated. Check whether this security rule needs to be activated: " . $names_line;
                                    $tmp_rule->set_node_attribute('warning', $addlog);
                                    //Todo: swaschkut 20191011
                                    //set rule to disable because of time-range found?
                                    //$tmp_rule->setDisabled( true );

                                    /*
                                    $newRule->addLog('warning',
                                        'Reading Security Policies',
                                        'Security RuleID [_RuleLid_] is using a time-range field with value ['.$starting_date.']. This field has not been migrated',
                                        'Check whether this security rule needs to be activated');
                                    */
                                    break;

                                /*
                                 *                          //Todo: SWASCHKUT implementation needed for:
                                 * example:                 access-list DMZ_NAME_access_in extended permit tcp 1.2.11.0 255.255.255.0 object-group DM_INLINE_NETWORK_588 eq 2345
                                 */
                                default:
                                    //Check if it is an IP netmask
                                    $currentIP = explode("/", $currentField);
                                    if( isset($currentIP[1]) )
                                        $ipv6Mask = $currentIP[1];

                                    $versionCheck = $this->ip_version($currentIP[0]);

                                    print "IPversion: " . $versionCheck . " for: " . $currentIP[0] . "\n";

                                    if( $versionCheck == "v4" )
                                    {
                                        $nextField = isset($netObj[0]) ? $netObj[0] : '';


                                        $cidr = CIDR::netmask2cidr($nextField);
                                        //$cidr = $this->mask2cidrv4($nextField);


                                        if( $cidr == "32" )
                                        {
                                            $NameComplete = "H-$currentField";
                                        }
                                        else
                                        {
                                            $NameComplete = "N-$currentField-$cidr";
                                        }

                                        #$addressObj = $objectsInMemory->getAddressReference($fileName, $source, $vsys, $obj2, $hostCidr);
                                        #$addMember2[] = "('$groupLid','$NameComplete','$source','$vsys','$addressObj->location','$addressObj->name')";
                                        $tmp_address = $v->addressStore->find($NameComplete);
                                        if( $tmp_address === null )
                                        {
                                            if( $print )
                                                print "  * create address object: " . $NameComplete . " | value: " . $currentField . ", CIDR: " . $cidr . "\n";
                                            $tmp_address = $v->addressStore->newAddress($NameComplete, 'ip-netmask', $currentField . "/" . $cidr);
                                        }


                                        /*
                                        $object = $inMemoryObjects->getAddressReference($devicegroup, $source, $vsys, $currentField, $cidr);
    //                                        echo "$names_line\n";
    //                                        echo "CurrentField = $currentField\n";
    //                                        print_r($object);
                                        $newRule->fillAddress($object);
                                        */
                                        array_shift($netObj);
                                    }
                                    elseif( $versionCheck == "v6" )
                                    {
                                        #mwarning( "IPv6 not implemented for default case", null, false );
                                        #print "value: ".$currentField."\n";
                                        #print "v6MASK: ".$ipv6Mask."\n";

                                        /*$memberObject = $inMemoryObjects->getIPv6AddressReference($devicegroup,$source, $vsys, $currentField, $ipv6Mask);
                                        $newRule->fillAddress($memberObject);
                                        */

                                        $value = $currentField;
                                        $name = str_replace(":", "_", $value);
                                        $name = str_replace("/", "m", $name);
                                        $tmp_address = $v->addressStore->find($name);
                                        if( $tmp_address === null )
                                        {
                                            if( $print )
                                                print "     * create address object: " . $name . " , value: " . $value . "\n";
                                            $tmp_address = $v->addressStore->newAddress($name, 'ip-netmask', $value);
                                        }

                                        #array_shift($netObj);
                                    }
                                    else
                                    {
                                        //Check if it is a known label
                                        $nextField = isset($netObj[0]) ? $netObj[0] : '';
                                        $cidr = $this->convertWildcards($nextField, 'cidr');
                                        if( $cidr == FALSE )
                                        {
                                            if( $this->checkNetmask($nextField) )
                                            {
                                                $cidr = $this->mask2cidrv4($nextField);
                                                array_shift($netObj);
                                            }
                                        }
                                        #mwarning( "1nov4 / nov6 which IPversion not implemented for default case: ".$currentField."|".$cidr, null, false );

                                        $tmp_prefix = "";
                                        $value = $currentField;
                                        $netmask = $cidr;


                                        $tmp_name = $tmp_prefix . $value . "_" . $netmask;
                                        $tmp_address = $v->addressStore->find($value);
                                        if( $tmp_address === null )
                                        {
                                            if( $print )
                                                print "     * create address object: " . $tmp_name . " , value: " . $value . "/" . $netmask . "\n";
                                            $tmp_address = $v->addressStore->newAddress($tmp_prefix . $value, 'ip-netmask', $value . "/" . $netmask);
                                        }
                                        else
                                        {
                                            //check if netmask is 0
                                            if( $tmp_address->isAddress() && $tmp_address->getNetworkMask() == 32 )
                                            {
                                                if( $netmask !== null )
                                                {
                                                    $value = $value . "/" . $netmask;
                                                    $tmp_prefix = "";
                                                }

                                                $tmp_value = $tmp_address->value();
                                                $tmp_value1 = explode("/", $tmp_value);
                                                $tmp_value = $tmp_value1[0];

                                                $tmp_name = $tmp_address->name() . "_" . $netmask;
                                                $tmp_address = $v->addressStore->find($tmp_name);
                                                if( $tmp_address === null )
                                                {
                                                    if( $print )
                                                        print "  * create address object: " . $tmp_name . " change value to: " . $tmp_value . "/" . $netmask . "\n";
                                                    $value = $tmp_value;
                                                    $tmp_address = $v->addressStore->newAddress($tmp_name, 'ip-netmask', $tmp_value . "/" . $netmask);
                                                }
                                            }
                                        }


                                        /*
                                        $missingFields = $newRule->getMissingFields();
                                        if(in_array('source', $missingFields)) {
                                            $object = $inMemoryObjects->getAddressReference($devicegroup, $source, $vsys, $currentField, $cidr);
                                            $newRule->setSource($object);
                                        }
                                        elseif(in_array('destination', $missingFields)) {
                                            $object = $inMemoryObjects->getAddressReference($devicegroup, $source, $vsys, $currentField, $cidr);
                                            $newRule->setDestination($object);
                                        }
                                        else{
                                            echo "Problem 2115: Field $currentField not recognized in [$names_line]\n";
                                        }
                                        */
                                    }

                                    if( $tmp_address !== null )
                                    {
                                        if( !isset($rule_object_set['source']) )
                                        {
                                            if( $tmp_address !== null )
                                            {
                                                if( $print )
                                                    print "    * add source - address object: " . $tmp_address->name() . " , value: " . $tmp_address->value() . "\n";
                                                $tmp_rule->source->addObject($tmp_address);
                                                $rule_object_set['source'] = "set";


                                            }
                                        }
                                        elseif( !isset($rule_object_set['destination']) )
                                        {
                                            if( $tmp_address !== null )
                                            {
                                                if( $print )
                                                {
                                                    if( $tmp_address->isAddress() )
                                                        print "    * add destination - address object: " . $tmp_address->name() . " , value: " . $tmp_address->value() . "\n";
                                                    else
                                                        print "    * add destination - address object: " . $tmp_address->name() . "\n";

                                                }

                                                $tmp_rule->destination->addObject($tmp_address);
                                                $rule_object_set['destination'] = "set";
                                            }
                                        }
                                        else
                                        {
                                            mwarning("Problem 2115: Field '" . $currentField . "' not recognized in [" . $names_line . "]", null, FALSE);
                                        }
                                    }
                                    else
                                    {
                                        mwarning("no address object found!");
                                    }


                                    break;
                            }
                        }
                    }


                    if( $tmp_rule->services->isAny() )
                    {
                        #mwarning( "further validation needed if protocol [IP/tcp/udp] and set correct service" );
                        #print "tmp_protocol: ".$tmp_protocol."\n";

                        //Adding AllPorts for security rules with TCP and UDP protocols that do not specify a port
                        if( $tmp_protocol == "ip" )
                        {
                            print "    * service set to ALL-TCP ports\n";
                            $tmp_rule->services->add($allTcp);
                            print "    * service set to ALL-UDP ports\n";
                            $tmp_rule->services->add($allUdp);
                        }
                        else
                        {
                            if( $tmp_protocol == "tcp" )
                            {
                                print "    * service set to ALL-TCP ports\n";
                                $tmp_rule->services->add($allTcp);
                            }
                            elseif( $tmp_protocol == "udp" )
                            {
                                print "    * service set to ALL-UDP ports\n";
                                $tmp_rule->services->add($allUdp);
                            }
                            else
                            {
                                print "protocol is: |" . $tmp_protocol . "|\n";
                            }
                        }

                    }

                    //Todo: Sven Waschkut 20200203
                    continue;

                    //In case a Security Rule belongs to an Access Group that was not found (not declared), create the group here
                    if( !isset($AccessGroups[$groupName]) )
                    {
                        $AccessGroups[$groupName] = new SecurityGroup($groupName, FALSE);
                    }

                    //Final check before adding this Rule into the Group (memory space still)
                    if( $newRule->isValid() )
                    {
                        //Complementing the rule with information coming from the Access Group it belongs to
                        $from = $AccessGroups[$groupName]->getZoneFrom();
                        $to = $AccessGroups[$groupName]->getZoneTo();
                        if( count($from) > 0 )
                        {
                            $newRule->setZoneFrom($from);
                        }
                        if( count($to) > 0 )
                        {
                            $newRule->setZoneTo($to);
                        }

                        $newRule->addTag($AccessGroups[$groupName]->getTag());

                        if( in_array('icmp', $newRule->protocol) && $newRule->application == null )
                        {
                            $object = $inMemoryObjects->getDefaultApplication('icmp');
                            if( !is_null($object) )
                            {
                                $newRule->addApplication($object);
                            }
                        }

                        if( in_array('sctp', $newRule->protocol) && $newRule->application == null )
                        {
                            $object = $inMemoryObjects->getDefaultApplication('sctp');
                            if( !is_null($object) )
                            {
                                $newRule->addApplication($object);
                            }
                        }

                        //Adding AllPorts for security rules with TCP and UDP protocols that do not specify a port
                        if( !isset($newRule->service) )
                        {
                            if( isset($newRule->protocol['ip']) )
                            {
                                $newRule->setService(array_merge($allTcp, $allUdp));
                            }
                            else
                            {
                                if( isset($newRule->protocol['tcp']) )
                                {
                                    $newRule->addService($allTcp[0]);
                                }
                                if( isset($newRule->protocol['udp']) )
                                {
                                    $newRule->addService($allUdp[0]);
                                }
                            }
                        }

                        //Check if the rule needs to be split into Two due to Services that should be transformed into Apps
                        $tcpudpProtocols = array();     //TCP and UDP protocols
                        $networkProtocols = $newRule->protocol;  //Other protocols
                        //echo "-----------------\n";
                        //echo "Newrules->service: \n";
                        //print_r($newRule->service);
                        //echo "Newrules->protocol: \n";
                        //print_r($newRule->protocol);

                        if( isset($networkProtocols['tcp']) )
                        {
                            $tcpudpProtocols[] = 'tcp';
                            unset($networkProtocols['tcp']);
                        }
                        if( isset($networkProtocols['udp']) )
                        {
                            $tcpudpProtocols[] = 'udp';
                            unset($networkProtocols['udp']);
                        }
                        if( isset($networkProtocols['ip']) )
                        {
                            $tcpudpProtocols[] = 'tcp';
                            $tcpudpProtocols[] = 'udp';
                            unset($networkProtocols['ip']);
                        }

                        //If we have network protocols and TCP/UDP protocols, we need to split the rule
                        //print_r($networkProtocols);
                        //print_r($tcpudpProtocols);

                        if( count($networkProtocols) > 0 && count($tcpudpProtocols) > 0 )
                        {
                            $networkProtocolRule = clone $newRule;

                            //Preparing and adding the TCP/UDP rule
                            $newRule->setApplications(array());
                            $newRule->setProtocol($tcpudpProtocols);


                            //Check that the services are from the Protocol that has been specified
                            //Initially the Service may have both UDP and TCP ports
                            //It only one of the two protocols is in this service, then we need to find out which ports we should keep
                            if( count($tcpudpProtocols) == 1 )
                            {
                                $protocol = array_pop($tcpudpProtocols);
                                $services = $newRule->service;

                                $explodedServices = explodeGroups2Services($services, $projectdb, $source, $vsys);
                                $validServices = array();
                                /* @var $service MemberObject */
                                foreach( $explodedServices as $service )
                                {

                                    //echo "Service cidr: " .$service->cidr. "\n";
                                    //echo "Protocol: " .$protocol. "\n";
                                    //print_r($service);
                                    if( $service->cidr == $protocol )
                                    {
                                        $validServices[] = $service;
                                    }
                                }
                                if( count($validServices) == count($explodedServices) )
                                {
                                    $newRule->setService($services);
                                }
                                else
                                {
                                    $newRule->setService($validServices);
                                }
                            }

                            //Verify if the source port has been defined
                            if( count($newRule->sourcePort) > 0 && $newRule->sourcePort[0]->name != 'any' )
                            {
                                $sourcePorts = array();
                                /* @var $sourcePort MemberObject */
                                foreach( $newRule->sourcePort as $sourcePort )
                                {
                                    $sourcePorts[] = $sourcePort->cidr . "/" . $sourcePort->value;
                                }
                                $newRule->addLog(
                                    'warning',
                                    'Reading Security Policies',
                                    'This rule had the following source ports:' . implode(', ', $sourcePorts),
                                    'Recommended to identify involved Applications and verify they are included in the rule');
                            }

                            $AccessGroups[$groupName]->addRule($newRule);

                            //Preparing and adding the NetworkProtocol rule
                            $networkProtocolRule->setProtocol($networkProtocols);
                            $networkProtocolRule->setService(array($any));
                            $AccessGroups[$groupName]->addRule($networkProtocolRule);
                        }
                        elseif( count($networkProtocols) > 0 )
                        { //Preparing and adding the NetworkProtocol rule
                            $newRule->setProtocol($networkProtocols);
                            $newRule->setService(array($any));
                            $AccessGroups[$groupName]->addRule($newRule);
                        }
                        else
                        { //Preparing and Adding the TPC/UDP Rule to the Group
                            $newRule->setApplications(array());
                            if( count($tcpudpProtocols) == 1 )
                            {
                                $protocol = array_pop($tcpudpProtocols);
                                $services = $newRule->service;
                                $explodedServices = $inMemoryObjects->explodeGroup2Services($services, $source, $vsys);
                                $validServices = array();
                                /* @var $service MemberObject */
                                foreach( $explodedServices as $service )
                                {
                                    if( $service->cidr == $protocol )
                                    {
                                        $validServices[] = $service;
                                    }
                                }
                                if( count($validServices) == count($explodedServices) )
                                {
                                    $newRule->setService($services);
                                }
                                else
                                {
                                    $newRule->setService($validServices);
                                }
                            }

                            //Verify if the source port has been defined
                            if( count($newRule->sourcePort) > 0 && $newRule->sourcePort[0]->name != 'any' )
                            {
                                $sourcePorts = array();
                                /* @var $sourcePort MemberObject */
                                foreach( $newRule->sourcePort as $sourcePort )
                                {
                                    $sourcePorts[] = $sourcePort->cidr . "/" . $sourcePort->value;
                                }
                                $newRule->addLog(
                                    'warning',
                                    'Reading Security Policies',
                                    'This rule had the following source ports: ' . implode(', ', $sourcePorts),
                                    'Recommended to identify involved Applications and verify they are included in the rule');
                            }

                            $AccessGroups[$groupName]->addRule($newRule);
                        }
                    }
                }
            }
        }


        //Todo: swaschkut 20191010 reorganisation of the sec rules:
        // get last entry of access-groups and move all sec rules with tag from access-group to top,
        //continue with the next "last" access-group

        //Todo: Sven Waschkut remove it
        $empty_array = array();
        return $empty_array;

//    print_r($AccessGroups);

        //Move the global access-group to the last entry
        // We have defined 'global' as the first element at the beginning of the function

        $globalGroup = array_shift($AccessGroups);
        $AccessGroups['global'] = $globalGroup;

        // Calculate the rule IDs
        $query = "SELECT max(id) as max FROM security_rules";
        $result = $projectdb->query($query);
        if( $result->num_rows > 0 )
        {
            $data = $result->fetch_assoc();
            $initiGlobalPosition = is_null($data['max']) ? 0 : $data['max'];
        }
        else
        {
            $initiGlobalPosition = 0;
        }

        /* @var $accessGroup SecurityGroup */
        foreach( $AccessGroups as $key => $accessGroup )
        {
            if( $accessGroup->getUsed() == TRUE )
            {
                $AccessGroups[$key]->setInitialID($initiGlobalPosition);
                $initiGlobalPosition += $accessGroup->getLastRulePosition();
            }
        }

        //Insert new Addresses and Services
        $inMemoryObjects->insertNewAddresses($projectdb);
        $inMemoryObjects->insertNewServices($projectdb);

        //        $inMemoryObjects->removeUnusedLabels($projectdb, $source, $vsys);

        removeUnusedLabels($projectdb, $source, $vsys);

        //Generate Security Rules
        $sec_rules = array();
        $sec_rules_srv = array();
        $sec_rules_src = array();
        $sec_rules_dst = array();
        $sec_rules_from = array();
        $sec_rules_to = array();
        $sec_rules_usr = array();
        $sec_rules_tags = array();
        $sec_rules_app = array();


        //print_r($AccessGroups);

        /* @var $accessGroup SecurityGroup
         * @var $rule SecurityRuleCisco
         * @var $srv MemberObject
         * @var $src MemberObject
         * @var $dst MemberObject
         * @var $from String
         * @var $to String
         * @var $user String
         * @var $app MemberObject
         */
        $firePowerTrack = array();

        foreach( $AccessGroups as $key => $accessGroup )
        {
            if( $accessGroup->getUsed() )
            {
                foreach( $accessGroup->getRules() as $rule )
                {
                    //( source, vsys, rule_lid, table_name, member_lid)
//                    print_r($rule);

                    if( $isFirePower == 1 )
                    {
                        $firePowerId = $rule->getFirepowerId();
                        if( !isset($firePowerTrack[$firePowerId]) )
                        {
                            $firePowerTrack[$firePowerId] = $firePowerId;
                            $rule_lid = $rule->globalPosition;
                            // (id, position, name, name_ext, description, action, disabled, vsys, source)
                            $sec_rules[] = "('$rule->globalPosition', '$rule->globalPosition', '$rule->name','$rule->name', '$rule->comment','$rule->action', $rule->disabled, '$vsys', '$source')";
                        }
                        if( isset($rule->service) )
                        {
                            foreach( $rule->service as $srv )
                            {
                                if( $srv->location != 'any' )
                                {
                                    $sec_rules_srv[] = "('$source', '$vsys','$rule_lid','$srv->location','$srv->name')";
                                }
                            }
                        }

                        //( source, vsys, rule_lid, table_name, member_lid)
                        $src = $rule->source;
                        if( $src->location != 'any' )
                        {
                            $sec_rules_src[] = "('$source', '$vsys','$rule_lid','$src->location','$src->name')";
                        }

                        //( source, vsys, rule_lid, table_name, member_lid)
                        $dst = $rule->destination;
                        if( $dst->location != 'any' )
                        {
                            $sec_rules_dst[] = "('$source', '$vsys','$rule_lid','$dst->location','$dst->name')";
                        }

                        //(source, vsys, rule_lid, name)
                        if( isset($rule->zoneFrom) )
                        {
                            foreach( $rule->zoneFrom as $from )
                            {
                                $sec_rules_from[] = "('$source', '$vsys','$rule_lid','$from')";
                            }
                        }

                        //(source, vsys, rule_lid, name)
                        if( isset($rule->zoneTo) )
                        {
                            foreach( $rule->zoneTo as $to )
                            {
                                $sec_rules_to[] = "('$source', '$vsys','$rule_lid','$to')";
                            }
                        }

                        //(source, vsys, rule_lid, name)
                        if( isset($rule->user) )
                        {
                            foreach( $rule->user as $user )
                            {
                                $sec_rules_usr[] = "('$source', '$vsys','$rule_lid','$user')";
                            }
                        }

                        //(source, vsys, rule_lid, table_name, member_lid)
                        if( isset($rule->application) )
                        {
                            foreach( $rule->application as $app )
                            {
                                $sec_rules_app[] = "('$source', '$vsys','$rule_lid','$app->location','$app->name')";
                            }
                        }

                        //(source, vsys, member_lid, rule_lid, table_name, member_lid)
                        if( isset($rule->tag) )
                        {
                            foreach( $rule->tag as $tag )
                            {
                                $sec_rules_tags[] = "('$source', '$vsys','$rule_lid','$tag->location','$tag->name')";
                            }
                        }

                        //Adding all the warning messages
                        if( isset($rule->logs) )
                        {
                            foreach( $rule->logs as $log )
                            {
                                //Replace _RuleLid_ by its Rulelid
                                $log['message'] = str_replace('_RuleLid_', $rule_lid, $log['message']);
                                add_log2($log['logType'], $log['task'], $log['message'], $source, $log['action'], 'rules', $rule_lid, 'security_rules');
                            }
                        }


                    }
                    else
                    {
                        $rule_lid = $rule->globalPosition;
                        // (id, position, name, name_ext, description, action, disabled, vsys, source)
                        $sec_rules[] = "('$rule->globalPosition', '$rule->globalPosition', '$rule->name','$rule->name', '$rule->comment','$rule->action', $rule->disabled, '$vsys', '$source')";

                        if( isset($rule->service) )
                        {
                            foreach( $rule->service as $srv )
                            {
                                if( $srv->location != 'any' )
                                {
                                    $sec_rules_srv[] = "('$source', '$vsys','$rule_lid','$srv->location','$srv->name')";
                                }
                            }
                        }

                        //( source, vsys, rule_lid, table_name, member_lid)
                        $src = $rule->source;
                        if( $src->location != 'any' )
                        {
                            $sec_rules_src[] = "('$source', '$vsys','$rule_lid','$src->location','$src->name')";
                        }

                        //( source, vsys, rule_lid, table_name, member_lid)
                        $dst = $rule->destination;
                        if( $dst->location != 'any' )
                        {
                            $sec_rules_dst[] = "('$source', '$vsys','$rule_lid','$dst->location','$dst->name')";
                        }

                        //(source, vsys, rule_lid, name)
                        if( isset($rule->zoneFrom) )
                        {
                            foreach( $rule->zoneFrom as $from )
                            {
                                $sec_rules_from[] = "('$source', '$vsys','$rule_lid','$from')";
                            }
                        }

                        //(source, vsys, rule_lid, name)
                        if( isset($rule->zoneTo) )
                        {
                            foreach( $rule->zoneTo as $to )
                            {
                                $sec_rules_to[] = "('$source', '$vsys','$rule_lid','$to')";
                            }
                        }

                        //(source, vsys, rule_lid, name)
                        if( isset($rule->user) )
                        {
                            foreach( $rule->user as $user )
                            {
                                $sec_rules_usr[] = "('$source', '$vsys','$rule_lid','$user')";
                            }
                        }

                        //(source, vsys, rule_lid, table_name, member_lid)
                        if( isset($rule->application) )
                        {
                            foreach( $rule->application as $app )
                            {
                                $sec_rules_app[] = "('$source', '$vsys','$rule_lid','$app->location','$app->name')";
                            }
                        }

                        //(source, vsys, member_lid, rule_lid, table_name, member_lid)
                        if( isset($rule->tag) )
                        {
                            foreach( $rule->tag as $tag )
                            {
                                $sec_rules_tags[] = "('$source', '$vsys','$rule_lid','$tag->location','$tag->name')";
                            }
                        }

                        //Adding all the warning messages
                        if( isset($rule->logs) )
                        {
                            foreach( $rule->logs as $log )
                            {
                                //Replace _RuleLid_ by its Rulelid
                                $log['message'] = str_replace('_RuleLid_', $rule_lid, $log['message']);
                                add_log2($log['logType'], $log['task'], $log['message'], $source, $log['action'], 'rules', $rule_lid, 'security_rules');
                            }
                        }
                    }

                }
            }
        }


        if( count($sec_rules) > 0 )
        {
            $query = "INSERT INTO security_rules (id, position, name, name_ext, description, action, disabled, vsys, source) VALUES " . implode(",", $sec_rules) . ";";
            $projectdb->query($query);
            unset($sec_rules);
        }

        if( count($sec_rules_srv) > 0 )
        {
            $unique = array_unique($sec_rules_srv);
            $query = "INSERT INTO security_rules_srv (source, vsys, rule_lid, table_name, member_lid) VALUES " . implode(",", $unique) . ";";
            $projectdb->query($query);
            unset($sec_rules_srv);
            unset($unique);
        }

        if( count($sec_rules_src) > 0 )
        {
            $unique = array_unique($sec_rules_src);
            $query = "INSERT INTO security_rules_src (source, vsys, rule_lid, table_name, member_lid) VALUES " . implode(",", $unique) . ";";
            $projectdb->query($query);
            unset($sec_rules_src);
            unset($unique);
        }

        if( count($sec_rules_dst) > 0 )
        {
            $unique = array_unique($sec_rules_dst);
            $query = "INSERT INTO security_rules_dst (source, vsys, rule_lid, table_name, member_lid) VALUES " . implode(",", $unique) . ";";
            $projectdb->query($query);
            unset($sec_rules_dst);
            unset($unique);
        }

        if( count($sec_rules_from) > 0 )
        {
            $unique = array_unique($sec_rules_from);
            $query = "INSERT INTO security_rules_from (source, vsys, rule_lid, name) VALUES " . implode(",", $unique) . ";";
            $projectdb->query($query);
            unset($sec_rules_from);
            unset($unique);
        }

        if( count($sec_rules_to) > 0 )
        {
            $unique = array_unique($sec_rules_to);
            $query = "INSERT INTO security_rules_to (source, vsys, rule_lid, name) VALUES " . implode(",", $unique) . ";";
            $projectdb->query($query);
            unset($sec_rules_to);
            unset($unique);
        }

        if( count($sec_rules_usr) > 0 )
        {
            $unique = array_unique($sec_rules_usr);
            $query = "INSERT INTO security_rules_usr (source, vsys, rule_lid, name) VALUES " . implode(",", $unique) . ";";
            $projectdb->query($query);
            unset($sec_rules_usr);
            unset($unique);
        }

        if( count($sec_rules_app) > 0 )
        {
            $unique = array_unique($sec_rules_app);
            $query = "INSERT INTO security_rules_app (source, vsys, rule_lid, table_name, member_lid) VALUES " . implode(",", $unique) . ";";
            $projectdb->query($query);
            unset($sec_rules_app);
            unset($unique);
        }

        if( count($sec_rules_tags) > 0 )
        {
            $unique = array_unique($sec_rules_tags);
            $query = "INSERT INTO security_rules_tag (source, vsys, rule_lid, table_name, member_lid) VALUES " . implode(",", $unique) . ";";
            $projectdb->query($query);
            unset($sec_rules_tags);
            unset($unique);
        }

        if( $isFirePower == 1 )
        {
            # Fix Rule 9998 Remove the services and add app teredo
            $get9998 = $projectdb->query("SELECT id FROM security_rules WHERE description LIKE '%rule-id 9998%' AND source='$source' LIMIT 1;");
            if( $get9998->num_rows == 1 )
            {
                $get9998data = $get9998->fetch_assoc();
                $get9998Id = $get9998data['id'];
                $projectdb->query("DELETE from security_rules_srv WHERE rule_lid='$get9998Id';");
                $getTeredo = $projectdb->query("SELECT id FROM default_applications WHERE name = 'teredo';");
                if( $getTeredo->num_rows == 1 )
                {
                    $getTeredoData = $getTeredo->fetch_assoc();
                    $getTeredoId = $getTeredoData['id'];
                    $projectdb->query("INSERT INTO security_rules_app (rule_lid,member_lid,table_name) VALUES ('$get9998Id','$getTeredoId','default_applications');");
                }
            }
        }

        return $AccessGroups;
    }


    function get_security_policies($cisco_config_file, $source, $vsys)
    {
        global $projectdb;
        $AccessGroups = array();
        $comments = "";
        $x = 1;
        $thecolor = 1;
#First Round to get ACCESS-GROUPS ONLY ACCESS-LISTS ASSIGNED TO INTERFACE
        foreach( $cisco_config_file as $line => $names_line )
        {
            if( preg_match("/^access-group /i", $names_line) )
            {
                $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                array_push($AccessGroups, $netObj[1]);
                #Add Tag
                $color = "color" . $thecolor;
                $tagname = $this->truncate_tags($netObj[1]);
                $query = "SELECT * FROM tag WHERE source='$source' AND vsys='$vsys' AND BINARY name ='$tagname'";
                //echo "$query\n";
                $result = $projectdb->query($query);
                if( $result->num_rows == 0 )
                {
                    $projectdb->query("INSERT INTO tag (name,source,vsys,color) VALUES ('$tagname','$source','$vsys','$color');");
                }
                if( $thecolor == 16 )
                {
                    $thecolor = 1;
                }
                else
                {
                    $thecolor++;
                }
            }
            if( preg_match("/crypto map /", $names_line) )
            {
                $split = explode(" ", $names_line);
                if( ($split[4] == "match") and ($split[5] == "address") )
                {
                    array_push($AccessGroups, trim($split[6]));
                    #Add Tag
                    $color = "color" . $thecolor;
                    $tagname = $this->truncate_tags(trim($split[6]));
                    $query = "SELECT * FROM tag WHERE source='$source' AND vsys='$vsys' AND BINARY name ='$tagname'";
                    //echo "$query\n";
                    $result = $projectdb->query($query);
                    if( $result->num_rows == 0 )
                    {
                        $projectdb->query("INSERT INTO tag (name,source,vsys,color) VALUES ('$tagname','$source','$vsys','$color');");
                    }
                    if( $thecolor == 16 )
                    {
                        $thecolor = 1;
                    }
                    else
                    {
                        $thecolor++;
                    }
                }

            }

        }

        $manualServiceToApp = array('ah' => 'ipsec-ah', 'icmp6' => 'ipv6-icmp', 'esp' => 'ipsec-esp', 'igrp' => 'igp',
            'ipinip' => 'ip-in-ip', 'nos' => 'ipip', 'pcp' => 'ipcomp');

        if( empty($AccessGroups) )
        {
            add_log('error', 'Reading Security Rules', 'No Access-groups found', $source, 'No Security Rules Added. Assign first the ACLs with access-groups.');
        }
        else
        {
            # Second Round to read the ACCESS-LISTS
            $comments = "";
            #Get Last lid from Profiles
            $getlastlid = $projectdb->query("SELECT max(id) as max FROM security_rules;");
            $getLID1 = $getlastlid->fetch_assoc();
            $lid = intval($getLID1['max']) + 1;
            $getlastlid = $projectdb->query("SELECT max(position) as max FROM security_rules;");
            $getLID1 = $getlastlid->fetch_assoc();
            $position = intval($getLID1['max']) + 1;

            $rule_application = array();
            $rule_service = array();
            $rule = array();
            $rule_source = array();
            $rule_destination = array();
            $rule_from = array();
            $rule_to = array();
            $rule_tag = array();
            $oldtag = "";
            $rule_mapping = array();
            $comment = array();
            //$getApplicationdefault = $projectdb->query("SELECT id FROM shared_services WHERE name='application-default' AND source='$source';");
            $getApplicationdefault = $projectdb->query("SELECT id FROM services WHERE name='application-default' AND source='$source' AND vsys = 'shared';");
            if( $getApplicationdefault->num_rows == 1 )
            {
                $getApplicationdefaultData = $getApplicationdefault->fetch_assoc();
                $application_default = $getApplicationdefaultData['id'];
            }
            else
            {
                add_default_services($source);
                //$getApplicationdefault = $projectdb->query("SELECT id FROM shared_services WHERE name='application-default' AND source='$source';");
                $getApplicationdefault = $projectdb->query("SELECT id FROM services WHERE name='application-default' AND source='$source' AND vsys = 'shared';");
//            if ($getApplicationdefault->num_rows == 1) {
                $getApplicationdefaultData = $getApplicationdefault->fetch_assoc();
                $application_default = $getApplicationdefaultData['id'];
//            }
            }

            $done_with_globalComments = TRUE;
            foreach( $cisco_config_file as $line => $names_line )
            {
                $names_line = trim($names_line);
                if( preg_match("/^access-list /i", $names_line) )
                {
                    $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                    if( (in_array($netObj[1], $AccessGroups)) or ($netObj[1] == "global") )
                    {
                        #Clean Vars
                        $source_type = "";
                        $src = "";
                        $destination_type = "";
                        $dst = "";
                        $port_type = "";
                        $port = "";
                        $rule_disabled = "0";
                        $negate_source = 0;
                        $negate_destination = 0;
                        $action = "";
                        $isRule = 0;

                        $startRule = 2;
                        $isProtocol = FALSE;
                        $CiscoProtocol = "none";
                        $isServiceObject = 0;
                        $protocol_tcp_udp = 0;

                        $accesslistName = $netObj[1];

                        if( preg_match("/\bline\b/", $names_line) )
                        {
                            $startRule = $startRule + 2;
                        }
                        if( preg_match("/\bextended\b/", $names_line) )
                        {
                            $startRule = $startRule + 1;
                        }

                        if( preg_match("/\bremark\b/i", $names_line, $dd) )
                        {
                            if( $done_with_globalComments == TRUE )
                            {
                                $done_with_globalComments = FALSE;
                                $comment = array();
                            }

                            $dirty_comments = preg_split("/ remark /", $names_line);
                            $thisComment = addslashes(rtrim($dirty_comments[1]));
                            $thisComment = str_replace("\n", '', $thisComment); // remove new lines
                            $thisComment = str_replace("\r", '', $thisComment);
                            $comment[] = $thisComment;
                            continue;

                        }
                        else
                        {
                            $done_with_globalComments = TRUE;
                        }

                        if( preg_match("/\bwebtype\b/", $names_line) )
                        {
                            #Dont work with webtype rules
                            continue;
                        }

                        #Check if is Disabled
                        $count = count($netObj);
                        $last = $count - 1;
                        if( $netObj[$last] == "inactive" )
                        {
                            $rule_disabled = "1";
                        }
                        else
                        {
                            $rule_disabled = "0";
                        }

                        #Check the protocol
                        $proto = $netObj[$startRule + 1];
                        $isProtocol = FALSE;
                        if( is_numeric($proto) )
                        {
                            $getApp = $projectdb->query("SELECT id,name FROM default_applications WHERE default_protocol='$proto';");
                            if( $getApp->num_rows == 1 )
                            {
                                $getAppData = $getApp->fetch_assoc();
                                $application_name = $getAppData['name'];
                                $application_id = $getAppData['id'];
                                add_log2('warning', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using the Protocol number [' . $proto . '].', $source, 'App-id [' . $application_name . '] has been added to that Rule', 'rules', $lid, 'security_rules');
                                $rule_application[] = "('$source','$vsys','$lid','default_applications','$application_id')";
                                //$rule_service[] = "('$source','$vsys','$lid','shared_services','$application_default')";
                                $rule_service[] = "('$source','$vsys','$lid','services','$application_default')";
                                $isProtocol = TRUE;
                            }
                            else
                            {
                                add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Unknown Protocol number [' . $proto . ']. Fix it before to finish', $source, 'Check it manually', 'rules', $lid, 'security_rules');
                                $isProtocol = TRUE;
                            }
                        }
                        else
                        {
                            if( ($proto == "object") or ($proto == "object-group") )
                            {
                                # Do nothing here
                                $isProtocol = FALSE;
                            }
                            elseif( ($proto == "ip") or ($proto == "tcp") or ($proto == "udp") )
                            {
                                #Do Nothing - Continue reading
                                $isProtocol = TRUE;
                            }
                            else
                            {
                                $getApp = $projectdb->query("SELECT id FROM default_applications WHERE name='$proto';");
                                if( $getApp->num_rows == 1 )
                                {
                                    $getAppData = $getApp->fetch_assoc();
                                    $application_id = $getAppData['id'];
                                    add_log2('warning', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using the Protocol Name [' . $proto . '].', $source, 'App-id [' . $proto . '] has been added to that Rule', 'rules', $lid, 'security_rules');
                                    $rule_application[] = "('$source','$vsys','$lid','default_applications','$application_id')";
                                    //$rule_service[] = "('$source','$vsys','$lid','shared_services','$application_default')";
                                    $rule_service[] = "('$source','$vsys','$lid','services','$application_default')";
                                    $isProtocol = TRUE;
                                }
                                else
                                {
                                    if( isset($manualServiceToApp[$proto]) )
                                    {
                                        $getApp = $projectdb->query("SELECT id FROM default_applications WHERE name='" . $manualServiceToApp[$proto] . "';");
                                        if( $getApp->num_rows == 1 )
                                        {
                                            $getAppData = $getApp->fetch_assoc();
                                            $application_id = $getAppData['id'];
                                            add_log2('warning', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using the Protocol Name [' . $proto . '].', $source, 'App-id [' . $manualServiceToApp[$proto] . '] has been added to that Rule', 'rules', $lid, 'security_rules');
                                            $rule_application[] = "('$source','$vsys','$lid','default_applications','$application_id')";
                                            //$rule_service[] = "('$source','$vsys','$lid','shared_services','$application_default')";
                                            $rule_service[] = "('$source','$vsys','$lid','services','$application_default')";
                                            $isProtocol = TRUE;
                                        }
                                        else
                                            add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Unknown Protocol number [' . $proto . '] which triggered a SQL error, manual fix is required and report to developers welcome', $source, 'Check it manually', 'rules', $lid, 'security_rules');
                                    }
                                    else
                                    {
                                        add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Unknown Protocol number [' . $proto . ']. Fix it before to finish', $source, 'Check it manually', 'rules', $lid, 'security_rules');
                                        $isProtocol = TRUE;
                                    }
                                }
                            }
                        }

                        if( (($netObj[$startRule + 1] == "tcp") or ($netObj[$startRule + 1] == "udp") or ($isProtocol)) and (($netObj[$startRule] == "permit") or ($netObj[$startRule] == "deny")) )
                        {
                            $isRule = 1;
                            $action = $netObj[$startRule];
                            if( $action == "permit" )
                            {
                                $action = "allow";
                            }
                            $ruleProtocol = $netObj[$startRule + 1];
                            if( ($netObj[$startRule + 2] == "any") or ($netObj[$startRule + 2] == "any4") or ($netObj[$startRule + 2] == "any6") )
                            {
                                $next = $startRule + 3;
                            }
                            elseif( $netObj[$startRule + 2] == "object-group" )
                            {
                                $SourceGroup = $netObj[$startRule + 3];
                                $getSourceGroup = $projectdb->query("SELECT id FROM address_groups_id WHERE source='$source' AND BINARY name_ext='$SourceGroup' AND vsys='$vsys';");
                                $next = $startRule + 4;
                                if( $getSourceGroup->num_rows > 0 )
                                {
                                    $getSourceGroupData = $getSourceGroup->fetch_assoc();
                                    $newlid = $getSourceGroupData['id'];
                                    $rule_source[] = "('$source','$vsys','$lid','address_groups_id','$newlid')";
                                }
                                else
                                {
                                    add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Object-group [' . $SourceGroup . '] that is not defined in my Database. Fix it before to finish', $source, 'Check it manually', 'rules', $lid, 'security_rules');
                                }
                            }
                            elseif( $netObj[$startRule + 2] == "host" )
                            {
                                $SourceHost = $netObj[$startRule + 3];
                                $next = $startRule + 4;
                                $hostCidr = "32";
                                if( validateIpAddress($SourceHost, "v4") )
                                {
                                    $getSource = $projectdb->query("SELECT id FROM address WHERE source='$source' AND ipaddress='$SourceHost' AND cidr='$hostCidr' AND vsys='$vsys' AND vtype='';");
                                    if( $getSource->num_rows == 0 )
                                    {
                                        $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values('H-$SourceHost','ip-netmask','H-$SourceHost','0','$source','0','$SourceHost','$hostCidr','1','$vsys');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                    else
                                    {
                                        $getSourceData = $getSource->fetch_assoc();
                                        $newlid = $getSourceData['id'];
                                        $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                }
                                else
                                {
                                    # Is a name
                                    $getSource = $projectdb->query("SELECT id,cidr,ipaddress FROM address WHERE source='$source' AND BINARY name_ext='$SourceHost' AND vsys='$vsys' AND vtype='';");
                                    if( $getSource->num_rows == 0 )
                                    {
                                        add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Address [' . $SourceHost . '] that is not defined in my Database. Fix it before to finish', $source, 'Adding to the DB [ip:1.1.1.1]. Add IP Address', 'rules', $lid, 'security_rules');
                                        $name_int = $this->truncate_names($this->normalizeNames($SourceHost));
                                        $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values('$name_int','ip-netmask','$SourceHost','1','$source','0','1.1.1.1','$hostCidr','1','$vsys');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                    else
                                    {
                                        $getSourceData = $getSource->fetch_assoc();
                                        $newlid = $getSourceData['id'];
                                        $cidr = $getSourceData['cidr'];
                                        $ipaddress = $getSourceData['ipaddress'];
                                        $ipversion = $this->ip_version($ipaddress);
                                        if( $cidr == $hostCidr )
                                        {
                                            $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                        }
                                        else
                                        {
                                            $getCheck = $projectdb->query("SELECT id FROM address WHERE source='$source' AND vsys='$vsys' AND ipaddress='$ipaddress' AND cidr='$hostCidr' AND vtype='';");
                                            if( $getCheck->num_rows == 0 )
                                            {
                                                $myname = "$SourceHost-$hostCidr";
                                                $name_int = $this->truncate_names($this->normalizeNames($myname));
                                                $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,$ipversion,vsys) values('$name_int','ip-netmask','$myname','1','$source','1','$ipaddress','$hostCidr','1','$vsys');");
                                                $newlid = $projectdb->insert_id;
                                                $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                            }
                                            else
                                            {
                                                $data = $getCheck->fetch_assoc();
                                                $newlid = $data['id'];
                                                $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                            }
                                        }
                                    }
                                }
                            }
                            elseif( $netObj[$startRule + 2] == "object" )
                            {
                                $SourceHost = $netObj[$startRule + 3];
                                $next = $startRule + 4;

                                # Is a name
                                $getSource = $projectdb->query("SELECT id FROM address WHERE source='$source' AND BINARY name_ext='$SourceHost' AND vsys='$vsys' AND vtype='object';");
                                if( $getSource->num_rows == 0 )
                                {
                                    add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Address [' . $SourceHost . '] that is not defined in my Database. Fix it before to finish', $source, 'Adding to the DB [ip:1.1.1.1]. Add IP Address', 'rules', $lid, 'security_rules');
                                    $name_int = $this->truncate_names($this->normalizeNames($SourceHost));
                                    $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys,vtype) values('$name_int','ip-netmask','$SourceHost','1','$source','0','1.1.1.1','$hostCidr','1','$vsys','object');");
                                    $newlid = $projectdb->insert_id;
                                    $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                }
                                else
                                {
                                    $getSourceData = $getSource->fetch_assoc();
                                    $newlid = $getSourceData['id'];
                                    $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                }
                            }
                            elseif( $netObj[$startRule + 2] == "interface" )
                            {
                                $next = $startRule + 4;
                            }
                            elseif( $this->checkNetmask($netObj[$startRule + 3]) )
                            {
                                $next = $startRule + 4;
                                if( validateIpAddress($netObj[$startRule + 2], "v4") )
                                {
                                    $src = $netObj[$startRule + 2];
                                    $SourceNetmask = $netObj[$startRule + 3];
                                    $SourceCidr = $this->mask2cidrv4($SourceNetmask);
                                    #Check if exists an object or create it
                                    $getSource = $projectdb->query("SELECT id FROM address WHERE source='$source' AND ipaddress='$src' AND cidr='$SourceCidr' AND vsys='$vsys' AND vtype='';");
                                    if( $getSource->num_rows == 0 )
                                    {
                                        if( $SourceCidr == 32 )
                                        {
                                            $NameComplete = "H-$src";
                                        }
                                        else
                                        {
                                            $NameComplete = "N-$src-$SourceCidr";
                                        }
                                        $getDup = $projectdb->query("SELECT id FROM address WHERE source='$source' AND BINARY name_ext='$NameComplete' AND vsys='$vsys' AND vtype='';");
                                        if( $getDup->num_rows == 0 )
                                        {
                                            $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values('$NameComplete','ip-netmask','$NameComplete','1','$source','0','$src','$SourceCidr','1','$vsys');");
                                            $newlid = $projectdb->insert_id;
                                            $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                        }
                                        else
                                        {
                                            $getSourceData = $getSource->fetch_assoc();
                                            $newlid = $getSourceData['id'];
                                            $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                        }
                                    }
                                    else
                                    {
                                        $getSourceData = $getSource->fetch_assoc();
                                        $newlid = $getSourceData['id'];
                                        $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                }
                                else
                                {
                                    $src = $netObj[$startRule + 2];
                                    $SourceNetmask = $netObj[$startRule + 3];
                                    $SourceCidr = $this->mask2cidrv4($SourceNetmask);
                                    $getSource = $projectdb->query("SELECT id,cidr,ipaddress FROM address WHERE source='$source' AND BINARY name_ext='$src' AND vsys='$vsys' AND vtype='';");
                                    if( $getSource->num_rows == 0 )
                                    {
                                        add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Address [' . $src . '] that is not defined in my Database. Fix it before to finish', $source, 'Adding to the DB [ip:1.1.1.1]. Add IP Address', 'rules', $lid, 'security_rules');
                                        $name_int = $this->truncate_names($this->normalizeNames($src));
                                        $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values('$name_int','ip-netmask','$src','1','$source','1','1.1.1.1','$hostCidr','1','$vsys');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                    else
                                    {
                                        $getSourceData = $getSource->fetch_assoc();
                                        $cidr = $getSourceData['cidr'];
                                        $ipaddress = $getSourceData['ipaddress'];
                                        $ipversion = $this->ip_version($ipaddress);
                                        if( $cidr == $SourceCidr )
                                        {
                                            $newlid = $getSourceData['id'];
                                            $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                        }
                                        else
                                        {
                                            $getCheck = $projectdb->query("SELECT id FROM address WHERE source='$source' AND BINARY name_ext='$src-$SourceCidr' AND vtype='';");
                                            if( $getCheck->num_rows == 0 )
                                            {
                                                $myname = "$src-$SourceCidr";
                                                $name_int = $this->truncate_names($this->normalizeNames($myname));
                                                $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values('$name_int','ip-netmask','$myname','1','$source','0','$ipaddress','$SourceCidr','1','$vsys');");
                                                $newlid = $projectdb->insert_id;
                                                $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                            }
                                            else
                                            {
                                                $data = $getCheck->fetch_assoc();
                                                $newlid = $data['id'];
                                                $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        elseif( $netObj[$startRule] == "remark" )
                        {
                            #Nothing to do
                        }
                        elseif( $netObj[$startRule + 1] == "object" )
                        {
                            #access-list dmz extended permit object udp_8030 object metintsugpar0191_1 object-group F22SGS6001_F22SGS6002
                            #Really? services ??
                            $service = $netObj[$startRule + 2];
                            $getSRV = $projectdb->query("SELECT id FROM services WHERE source='$source' AND BINARY name_ext='$service' ANd vsys='$vsys';");
                            if( $getSRV->num_rows == 1 )
                            {
                                $myData = $getSRV->fetch_assoc();
                                $newlid = $myData['id'];
                                $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                            }
                            else
                            {
                                add_log2('error', 'Reading Security Policies', 'Rule not covered: ' . $names_line, $source, 'Security RuleID [' . $lid . ']', 'rules', $lid, 'security_rules');
                            }

                            $isRule = 1;
                            $action = $netObj[$startRule];
                            if( $action == "permit" )
                            {
                                $action = "allow";
                            }
                            $ruleProtocol = $netObj[$startRule + 1];
                            if( ($netObj[$startRule + 3] == "any") or ($netObj[$startRule + 3] == "any4") or ($netObj[$startRule + 3] == "any6") )
                            {
                                $next = $startRule + 4;
                            }
                            elseif( $netObj[$startRule + 3] == "object-group" )
                            {
                                $SourceGroup = $netObj[$startRule + 4];
                                $getSourceGroup = $projectdb->query("SELECT id FROM address_groups_id WHERE source='$source' AND BINARY name_ext='$SourceGroup' AND vsys='$vsys';");
                                $next = $startRule + 5;
                                if( $getSourceGroup->num_rows > 0 )
                                {
                                    $getSourceGroupData = $getSourceGroup->fetch_assoc();
                                    $newlid = $getSourceGroupData['id'];
                                    $rule_source[] = "('$source','$vsys','$lid','address_groups_id','$newlid')";
                                }
                                else
                                {
                                    add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Object-group [' . $SourceGroup . '] that is not defined in my Database. Fix it before to finish', $source, 'Check it manually', 'rules', $lid, 'security_rules');
                                }
                            }
                            elseif( $netObj[$startRule + 3] == "host" )
                            {
                                $SourceHost = $netObj[$startRule + 4];
                                $next = $startRule + 5;
                                $hostCidr = "32";
                                if( validateIpAddress($SourceHost, "v4") )
                                {
                                    $getSource = $projectdb->query("SELECT id FROM address WHERE source='$source' AND ipaddress='$SourceHost' AND cidr='$hostCidr' AND vsys='$vsys' AND vtype='';");
                                    if( $getSource->num_rows == 0 )
                                    {
                                        $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values('H-$SourceHost','ip-netmask','H-$SourceHost','1','$source','0','$SourceHost','$hostCidr','1','$vsys');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                    else
                                    {
                                        $getSourceData = $getSource->fetch_assoc();
                                        $newlid = $getSourceData['id'];
                                        $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                }
                                else
                                {
                                    # Is a name
                                    $getSource = $projectdb->query("SELECT id,cidr,ipaddress FROM address WHERE source='$source' AND BINARY name_ext='$SourceHost' AND vsys='$vsys' AND vtype='';");
                                    if( $getSource->num_rows == 0 )
                                    {
                                        add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Address [' . $SourceHost . '] that is not defined in my Database. Fix it before to finish', $source, 'Adding to the DB [ip:1.1.1.1]. Add IP Address', 'rules', $lid, 'security_rules');
                                        $name_int = $this->truncate_names($this->normalizeNames($SourceHost));
                                        $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values('$name_int','ip-netmask','$SourceHost','1','$source','0','1.1.1.1','$hostCidr','1','$vsys');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                    else
                                    {
                                        $getSourceData = $getSource->fetch_assoc();
                                        $newlid = $getSourceData['id'];
                                        $cidr = $getSourceData['cidr'];
                                        $ipaddress = $getSourceData['ipaddress'];
                                        $ipversion = $this->ip_version($ipaddress);
                                        if( $cidr == $hostCidr )
                                        {
                                            $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                        }
                                        else
                                        {
                                            $getCheck = $projectdb->query("SELECT id FROM address WHERE source='$source' AND ipaddress='$ipaddress' AND cidr='$hostCidr' AND vsys='$vsys' AND vtype='';");
                                            if( $getCheck->num_rows == 0 )
                                            {
                                                $myname = "$SourceHost-$hostCidr";
                                                $name_int = $this->truncate_names($this->normalizeNames($myname));
                                                $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,$ipversion,vsys) values('$name_int','ip-netmask','$myname','1','$source','1','$ipaddress','$hostCidr','1','$vsys');");
                                                $newlid = $projectdb->insert_id;
                                                $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                            }
                                            else
                                            {
                                                $data = $getCheck->fetch_assoc();
                                                $newlid = $data['id'];
                                                $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                            }
                                        }
                                    }
                                }
                            }
                            elseif( $netObj[$startRule + 3] == "object" )
                            {
                                $SourceHost = $netObj[$startRule + 4];
                                $next = $startRule + 5;

                                # Is a name
                                $getSource = $projectdb->query("SELECT id FROM address WHERE source='$source' AND BINARY name_ext='$SourceHost' AND vsys='$vsys' AND vtype='object';");
                                if( $getSource->num_rows == 0 )
                                {
                                    add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Address [' . $SourceHost . '] that is not defined in my Database. Fix it before to finish', $source, 'Adding to the DB [ip:1.1.1.1]. Add IP Address', 'rules', $lid, 'security_rules');
                                    $name_int = $this->truncate_names($this->normalizeNames($SourceHost));
                                    $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys,vtype) values('$name_int','ip-netmask','$SourceHost','1','$source','0','1.1.1.1','$hostCidr','1','$vsys','object');");
                                    $newlid = $projectdb->insert_id;
                                    $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                }
                                else
                                {
                                    $getSourceData = $getSource->fetch_assoc();
                                    $newlid = $getSourceData['id'];
                                    $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                }
                            }
                            elseif( $netObj[$startRule + 3] == "interface" )
                            {
                                $next = $startRule + 5;
                            }
                            elseif( $this->checkNetmask($netObj[$startRule + 4]) )
                            {
                                $next = $startRule + 5;
                                if( validateIpAddress($netObj[$startRule + 3], "v4") )
                                {
                                    $src = $netObj[$startRule + 3];
                                    $SourceNetmask = $netObj[$startRule + 4];
                                    $SourceCidr = $this->mask2cidrv4($SourceNetmask);
                                    #Check if exists an object or create it
                                    $getSource = $projectdb->query("SELECT id FROM address WHERE source='$source' AND ipaddress='$src' AND cidr='$SourceCidr' AND vsys='$vsys' AND vtype='';");
                                    if( $getSource->num_rows == 0 )
                                    {
                                        if( $SourceCidr == 32 )
                                        {
                                            $NameComplete = "H-$src";
                                        }
                                        else
                                        {
                                            $NameComplete = "N-$src-$SourceCidr";
                                        }
                                        $getDup = $projectdb->query("SELECT id FROM address WHERE source='$source' AND BINARY name_ext='$NameComplete' AND vsys='$vsys' AND vtype='';");
                                        if( $getDup->num_rows == 0 )
                                        {
                                            $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values('$NameComplete','ip-netmask','$NameComplete','1','$source','0','$src','$SourceCidr','1','$vsys');");
                                            $newlid = $projectdb->insert_id;
                                            $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                        }
                                        else
                                        {
                                            $getSourceData = $getSource->fetch_assoc();
                                            $newlid = $getSourceData['id'];
                                            $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                        }
                                    }
                                    else
                                    {
                                        $getSourceData = $getSource->fetch_assoc();
                                        $newlid = $getSourceData['id'];
                                        $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                }
                                else
                                {
                                    $src = $netObj[$startRule + 3];
                                    $SourceNetmask = $netObj[$startRule + 4];
                                    $SourceCidr = $this->mask2cidrv4($SourceNetmask);
                                    $getSource = $projectdb->query("SELECT id,cidr,ipaddress FROM address WHERE source='$source' AND BINARY name_ext='$src' AND vsys='$vsys' AND vtype='';");
                                    if( $getSource->num_rows == 0 )
                                    {
                                        add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Address [' . $src . '] that is not defined in my Database. Fix it before to finish', $source, 'Adding to the DB [ip:1.1.1.1]. Add IP Address', 'rules', $lid, 'security_rules');
                                        $name_int = $this->truncate_names($this->normalizeNames($src));
                                        $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values('$name_int','ip-netmask','$src','1','$source','1','1.1.1.1','$hostCidr','1','$vsys');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                    else
                                    {
                                        $getSourceData = $getSource->fetch_assoc();
                                        $cidr = $getSourceData['cidr'];
                                        $ipaddress = $getSourceData['ipaddress'];
                                        $ipversion = $this->ip_version($ipaddress);
                                        if( $cidr == $SourceCidr )
                                        {
                                            $newlid = $getSourceData['id'];
                                            $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                        }
                                        else
                                        {
                                            $getCheck = $projectdb->query("SELECT id FROM address WHERE source='$source' AND BINARY name_ext='$src-$SourceCidr' AND vsys='$vsys' AND vtype='';");
                                            if( $getCheck->num_rows == 0 )
                                            {
                                                $myname = "$src-$SourceCidr";
                                                $name_int = $this->truncate_names($this->normalizeNames($myname));
                                                $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values('$name_int','ip-netmask','$myname','1','$source','0','$ipaddress','$SourceCidr','1','$vsys');");
                                                $newlid = $projectdb->insert_id;
                                                $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                            }
                                            else
                                            {
                                                $data = $getCheck->fetch_assoc();
                                                $newlid = $data['id'];
                                                $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        elseif( $netObj[$startRule + 1] == "object-group" )
                        {
                            $isRule = 1;
                            $action = $netObj[$startRule];
                            if( $action == "permit" )
                            {
                                $action = "allow";
                            }
                            #Protocol Group or ServiceObjectGroup
                            $protocol_group = $netObj[$startRule + 2];
                            $getProtocol = $projectdb->query("SELECT member FROM cisco_protocol_groups WHERE source='$source' AND BINARY name='$protocol_group' AND vsys='$vsys';");
                            if( $getProtocol->num_rows > 0 )
                            {
                                # Check if its only tcp-udp
                                $istcpudp = $projectdb->query("SELECT member FROM cisco_protocol_groups WHERE source='$source' AND BINARY name='$protocol_group' AND member!='tcp' AND member!='udp';");
                                if( $istcpudp->num_rows == 0 )
                                {
                                    $protocol_tcp_udp = 1;
                                }
                                else
                                {
                                    $protocol_tcp_udp = 0;
                                    while( $myProto = $istcpudp->fetch_assoc() )
                                    {
                                        $member = $myProto['member'];
                                        if( is_numeric($member) )
                                        {
                                            $getApp = $projectdb->query("SELECT id,name FROM default_applications WHERE default_protocol='$member';");
                                            if( $getApp->num_rows == 1 )
                                            {
                                                $getAppData = $getApp->fetch_assoc();
                                                $application_name = $getAppData['name'];
                                                $application_id = $getAppData['id'];
                                                add_log2('warning', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using the Protocol number [' . $member . '].', $source, 'App-id [' . $application_name . '] has been added to that Rule', 'rules', $lid, 'security_rules');
                                                $rule_application[] = "('$source','$vsys','$lid','default_applications','$application_id')";
                                                //$rule_service[] = "('$source','$vsys','$lid','shared_services','$application_default')";
                                                $rule_service[] = "('$source','$vsys','$lid','services','$application_default')";
                                            }
                                            else
                                            {
                                                add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Unknown Protocol number [' . $member . ']. Fix it before to finish', $source, 'Check it manually', 'rules', $lid, 'security_rules');
                                            }
                                        }
                                        else
                                        {
                                            $getApp = $projectdb->query("SELECT id FROM default_applications WHERE name='$member';");
                                            if( $getApp->num_rows == 1 )
                                            {
                                                $getAppData = $getApp->fetch_assoc();
                                                $application_id = $getAppData['id'];
                                                add_log2('warning', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using the Protocol Name [' . $member . '].', $source, 'App-id [' . $member . '] has been added to that Rule', 'rules', $lid, 'security_rules');
                                                $rule_application[] = "('$source','$vsys','$lid','default_applications','$application_id')";
                                                //$rule_service[] = "('$source','$vsys','$lid','shared_services','$application_default')";
                                                $rule_service[] = "('$source','$vsys','$lid','services','$application_default')";
                                            }
                                            else
                                            {
                                                if( isset($manualServiceToApp[$member]) )
                                                {
                                                    $getApp = $projectdb->query("SELECT id FROM default_applications WHERE name='" . $manualServiceToApp[$member] . "';");
                                                    if( $getApp->num_rows == 1 )
                                                    {
                                                        $getAppData = $getApp->fetch_assoc();
                                                        $application_id = $getAppData['id'];
                                                        add_log2('warning', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using the Protocol Name [' . $member . '].', $source, 'App-id [' . $manualServiceToApp[$member] . '] has been added to that Rule', 'rules', $lid, 'security_rules');
                                                        $rule_application[] = "('$source','$vsys','$lid','default_applications','$application_id')";
                                                        $rule_service[] = "('$source','$vsys','$lid','shared_services','$application_default')";
                                                    }
                                                    else
                                                        add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Unknown Protocol number [' . $member . '] which triggered a SQL error, manual fix is required and report to developpers welcome', 'rules', $lid, 'security_rules');
                                                }
                                                else
                                                {
                                                    add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Unknown Protocol number [' . $member . ']. Fix it before to finish', $member, 'Check it manually', 'rules', $lid, 'security_rules');
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                #Will Check if its an Service-Object
                                $getServiceObject = $projectdb->query("SELECT id FROM services_groups_id WHERE source='$source' AND BINARY name_ext='$protocol_group' AND vsys='$vsys'");
                                if( $getServiceObject->num_rows == 1 )
                                {
                                    $getServiceObjectData = $getServiceObject->fetch_assoc();
                                    $newlid = $getServiceObjectData['id'];
                                    $rule_service[] = "('$source','$vsys','$lid','services_groups_id','$newlid')";
                                }
                                /*
                    $getServiceObject=$projectdb->query("SELECT id FROM services_groups_id WHERE source='$source' AND name='$protocol_group' AND vsys='$vsys'");
                    if ($getServiceObject->num_rows==0){
                    #FIX If found a object-group referencing a service-group add all the members to the srv
                    $getSGid=$projectdb->query("SELECT id FROM services_groups_id WHERE source='$source' AND name='$protocol_group' AND vsys='$vsys'");
                    if (mysql_num_rows($getSGid)==1){
                    $mySG=mysql_fetch_assoc($getSGid);
                    $mylid=$mySG['id'];
                    $getM=$projectdb->query("SELECT member_lid,table_name FROM services_groups WHERE source='$source' AND lid='$mylid';");
                    while ($mynewData=mysql_fetch_assoc($getM)){
                    $M_member_lid=$mynewData['member_lid'];
                    $M_table_name=$mynewData['table_name'];
                    $getDup=$projectdb->query("SELECT id FROM security_rules_srv WHERE source='$source' AND rule_lid='$lid' AND member_lid='$M_member_lid' AND table_name='$M_table_name';");
                    if ($getDup->num_rows==0){
                    $projectdb->query("INSERT INTO security_rules_srv (rule_lid,member_lid,table_name,project,vsys) values('$lid','$M_member_lid','$M_table_name','$projectname','vsys1');");
                    }
                    }
                    }
                    else {
                    #Protocol GROUP doesnt exist in the database But it can be a service group id
                    add_log('4','Phase 5: Reading Security Rules','Protocol-Group ['.$protocol_group.'] Is not in the Database. Rule ['.$lid.']',$projectname,'Please review the object-group protocol definition');
                    }
                    }
                    else {
                    #Is Service Object ADD Services and at the end add the apps
                    $isServiceObject=1;
                    $getServices=$projectdb->query("SELECT service_lid FROM cisco_service_objects WHERE source='$source' AND name='$protocol_group' and service_lid!='';");
                    if (mysql_num_rows($getServices)>0){
                    while ($data=mysql_fetch_assoc($getServices)){
                    $service_lid=$data['service_lid'];
                    $projectdb->query("INSERT INTO security_rules_srv (rule_lid,member_lid,table_name,project,vsys) values('$lid','$service_lid','services','$projectname','vsys1');");
                    }
                    }
                    }
                   */
                            }

                            #Delete 1 element and reorder to mantain the same index
                            unset($netObj[$startRule + 1]);
                            $myTMP = array_values($netObj);
                            $netObj = $myTMP;

                            if( ($netObj[$startRule + 2] == "any") or ($netObj[$startRule + 2] == "any4") or ($netObj[$startRule + 2] == "any6") )
                            {
                                $next = $startRule + 3;
                            }
                            elseif( $netObj[$startRule + 2] == "object-group" )
                            {
                                $SourceGroup = $netObj[$startRule + 3];
                                $getSourceGroup = $projectdb->query("SELECT id FROM address_groups_id WHERE source='$source' AND BINARY name_ext='$SourceGroup' AND vsys='$vsys';");
                                $next = $startRule + 4;
                                if( $getSourceGroup->num_rows > 0 )
                                {
                                    $getSourceGroupData = $getSourceGroup->fetch_assoc();
                                    $newlid = $getSourceGroupData['id'];
                                    $rule_source[] = "('$source','$vsys','$lid','address_groups_id','$newlid')";
                                }
                                else
                                {
                                    add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Object-Group [' . $SourceGroup . '] that is not in my DB.', $source, 'Add manually and Assign to the Rule', 'rules', $lid, 'security_rules');
                                }
                            }
                            elseif( $netObj[$startRule + 2] == "host" )
                            {
                                $SourceHost = $netObj[$startRule + 3];
                                $next = $startRule + 4;
                                $hostCidr = "32";
                                if( validateIpAddress($SourceHost, "v4") )
                                {
                                    $getSource = $projectdb->query("SELECT id FROM address WHERE source='$source' AND ipaddress='$SourceHost' AND cidr='$hostCidr' AND vsys='$vsys' AND vtype='';");
                                    if( $getSource->num_rows == 0 )
                                    {
                                        $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values('H-$SourceHost','ip-netmask','H-$SourceHost','1','$source','0','$SourceHost','$hostCidr','1','$vsys');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                    else
                                    {
                                        $getSourceData = $getSource->fetch_assoc();
                                        $newlid = $getSourceData['id'];
                                        $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                }
                                else
                                {
                                    # Is a name
                                    $getSource = $projectdb->query("SELECT id,cidr,ipaddress FROM address WHERE source='$source' AND BINARY name_ext='$SourceHost' AND vsys='$vsys' AND vtype='';");
                                    if( $getSource->num_rows == 0 )
                                    {
                                        add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Address [' . $SourceHost . '] that is not defined in my Database. Fix it before to finish', $source, 'Adding to the DB [ip:1.1.1.1]. Add IP Address', 'rules', $lid, 'security_rules');
                                        $name_int = $this->truncate_names($this->normalizeNames($SourceHost));
                                        $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values('$name_int','ip-netmask','$SourceHost','1','$source','0','1.1.1.1','$hostCidr','1','$vsys');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                    else
                                    {
                                        $getSourceData = $getSource->fetch_assoc();
                                        $newlid = $getSourceData['id'];
                                        $cidr = $getSourceData['cidr'];
                                        $ipaddress = $getSourceData['ipaddress'];
                                        $ipversion = $this->ip_version($ipaddress);
                                        if( $cidr == $hostCidr )
                                        {
                                            $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                        }
                                        else
                                        {
                                            $getCheck = $projectdb->query("SELECT id FROM address WHERE source='$source' AND ipaddress='$ipaddress' AND cidr='$hostCidr' AND vsys='$vsys' AND vtype='';");
                                            if( $getCheck->num_rows == 0 )
                                            {
                                                $myname = "$SourceHost-$hostCidr";
                                                $name_int = $this->truncate_names($this->normalizeNames($myname));
                                                $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,$ipversion,vsys) values('$name_int','ip-netmask','$myname','1','$source','1','$ipaddress','$hostCidr','1','$vsys');");
                                                $newlid = $projectdb->insert_id;
                                                $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                            }
                                            else
                                            {
                                                $data = $getCheck->fetch_assoc();
                                                $newlid = $data['id'];
                                                $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                            }
                                        }
                                    }
                                }
                            }
                            elseif( $netObj[$startRule + 2] == "interface" )
                            {
                                $next = $startRule + 4;
                            }
                            elseif( $this->checkNetmask($netObj[$startRule + 3]) )
                            {
                                $next = $startRule + 4;
                                if( validateIpAddress($netObj[$startRule + 2], "v4") )
                                {
                                    $src = $netObj[$startRule + 2];
                                    $SourceNetmask = $netObj[$startRule + 3];
                                    $SourceCidr = $this->mask2cidrv4($SourceNetmask);
                                    #Check if exists an object or create it
                                    $getSource = $projectdb->query("SELECT id FROM address WHERE source='$source' AND ipaddress='$src' AND cidr='$SourceCidr' AND vsys='$vsys' AND vtype='';");
                                    if( $getSource->num_rows == 0 )
                                    {
                                        if( $SourceCidr == 32 )
                                        {
                                            $NameComplete = "H-$src";
                                        }
                                        else
                                        {
                                            $NameComplete = "N-$src-$SourceCidr";
                                        }
                                        $getDup = $projectdb->query("SELECT id FROM address WHERE source='$source' AND BINARY name_ext='$NameComplete' AND vsys='$vsys' AND vtype='';");
                                        if( $getDup->num_rows == 0 )
                                        {
                                            $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values('$NameComplete','ip-netmask','$NameComplete','1','$source','0','$src','$SourceCidr','1','$vsys');");
                                            $newlid = $projectdb->insert_id;
                                            $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                        }
                                        else
                                        {
                                            $getSourceData = $getSource->fetch_assoc();
                                            $newlid = $getSourceData['id'];
                                            $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                        }
                                    }
                                    else
                                    {
                                        $getSourceData = $getSource->fetch_assoc();
                                        $newlid = $getSourceData['id'];
                                        $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                }
                                else
                                {
                                    $src = $netObj[$startRule + 2];
                                    $SourceNetmask = $netObj[$startRule + 3];
                                    $SourceCidr = $this->mask2cidrv4($SourceNetmask);
                                    $getSource = $projectdb->query("SELECT id,cidr,ipaddress FROM address WHERE source='$source' AND BINARY name_ext='$src' AND vsys='$vsys' AND vtype='';");
                                    if( $getSource->num_rows == 0 )
                                    {
                                        add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Address [' . $src . '] that is not defined in my Database. Fix it before to finish', $source, 'Adding to the DB [ip:1.1.1.1]. Add IP Address', 'rules', $lid, 'security_rules');
                                        $name_int = $this->truncate_names($this->normalizeNames($src));
                                        $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values('$name_int','ip-netmask','$src','1','$source','1','1.1.1.1','$hostCidr','1','$vsys');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                    else
                                    {
                                        $getSourceData = $getSource->fetch_assoc();
                                        $cidr = $getSourceData['cidr'];
                                        $ipaddress = $getSourceData['ipaddress'];
                                        $ipversion = $this->ip_version($ipaddress);
                                        if( $cidr == $SourceCidr )
                                        {
                                            $newlid = $getSourceData['id'];
                                            $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                        }
                                        else
                                        {
                                            $getCheck = $projectdb->query("SELECT id FROM address WHERE source='$source' AND BINARY name_ext='$src-$SourceCidr' AND vsys='$vsys' AND vtype='';");
                                            if( $getCheck->num_rows == 0 )
                                            {
                                                $myname = "$src-$SourceCidr";
                                                $name_int = $this->truncate_names($this->normalizeNames($myname));
                                                $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values ('$name_int','ip-netmask','$myname','1','$source','0','$ipaddress','$SourceCidr','1','$vsys');");
                                                $newlid = $projectdb->insert_id;
                                                $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                            }
                                            else
                                            {
                                                $data = $getCheck->fetch_assoc();
                                                $newlid = $data['id'];
                                                $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                            }
                                        }
                                    }
                                }
                            }
                            elseif( $netObj[$startRule + 2] == "object" )
                            {
                                $SourceHost = $netObj[$startRule + 3];
                                $next = $startRule + 4;
                                $getAddress = $projectdb->query("SELECT id FROM address WHERE source='$source' AND BINARY name_ext='$SourceHost' AND vsys='$vsys' AND vtype='object';");
                                if( $getAddress->num_rows == 1 )
                                {
                                    $getData = $getAddress->fetch_assoc();
                                    $newlid = $getData['id'];
                                    $rule_source[] = "('$source','$vsys','$lid','address','$newlid')";
                                }
                            }
                        }

                        if( $next != "" )
                        {
                            if( ($netObj[$next] == "lt") or ($netObj[$next] == "gt") or ($netObj[$next] == "eq") or ($netObj[$next] == "neq") or ($netObj[$next] == "range") or ($netObj[$next] == "object-group") )
                            {
                                if( $netObj[$next] == "range" )
                                {
                                    $next = $next + 3;
                                }
                                elseif( $netObj[$next] == "object-group" )
                                {
                                    $SourceGroup = $netObj[$next + 1];
                                    $isSG = $projectdb->query("SELECT id FROM services_groups_id WHERE source='$source' AND BINARY name_ext='$SourceGroup' AND vsys='$vsys';");
                                    if( $isSG->num_rows == 0 )
                                    {
                                        # Do nothing by now
                                    }
                                    else
                                    {
                                        $next = $next + 2;
                                    }
                                }
                                else
                                {
                                    $next = $next + 2;
                                }
                            }
                            # Destination
                            $Destination = $netObj[$next];
                            if( ($Destination == "any") or ($Destination == "any4") or ($Destination == "any6") )
                            {
                                $next_service = $next + 1;
                            }
                            elseif( $Destination == "object-group" )
                            {
                                $SourceGroup = $netObj[$next + 1];
                                $getSourceGroup = $projectdb->query("SELECT id FROM address_groups_id WHERE source='$source' AND BINARY name_ext='$SourceGroup' AND vsys='$vsys';");
                                if( $getSourceGroup->num_rows > 0 )
                                {
                                    $getSourceGroupData = $getSourceGroup->fetch_assoc();
                                    $newlid = $getSourceGroupData['id'];
                                    $rule_destination[] = "('$source','$vsys','$lid','address_groups_id','$newlid')";
                                }
                                else
                                {
                                    add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Object-Group [' . $SourceGroup . '] that is not in my DB.', $source, 'Add manually and Assign to the Rule', 'rules', $lid, 'security_rules');
                                }
                                $next_service = $next + 2;
                            }
                            elseif( $Destination == "host" )
                            {
                                $SourceHost = $netObj[$next + 1];
                                $hostCidr = "32";
                                if( validateIpAddress($SourceHost, "v4") )
                                {
                                    $getSource = $projectdb->query("SELECT id FROM address WHERE source='$source' AND ipaddress='$SourceHost' AND cidr='$hostCidr' AND vsys='$vsys' AND vtype='';");
                                    if( $getSource->num_rows == 0 )
                                    {
                                        $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values('H-$SourceHost','ip-netmask','H-$SourceHost','1','$source','0','$SourceHost','$hostCidr','1','$vsys');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_destination[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                    else
                                    {
                                        $getSourceData = $getSource->fetch_assoc();
                                        $newlid = $getSourceData['id'];
                                        $rule_destination[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                }
                                else
                                {
                                    # Is a name
                                    $getSource = $projectdb->query("SELECT id,cidr,ipaddress FROM address WHERE source='$source' AND BINARY name_ext='$SourceHost' AND vsys='$vsys' AND vtype='';");
                                    if( $getSource->num_rows == 0 )
                                    {
                                        add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Address [' . $SourceHost . '] that is not defined in my Database. Fix it before to finish', $source, 'Adding to the DB [ip:1.1.1.1]. Add IP Address', 'rules', $lid, 'security_rules');
                                        $name_int = $this->truncate_names($this->normalizeNames($SourceHost));
                                        $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values('$name_int','ip-netmask','$SourceHost','1','$source','0','1.1.1.1','$hostCidr','1','$vsys');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_destination[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                    else
                                    {
                                        $getSourceData = $getSource->fetch_assoc();
                                        $newlid = $getSourceData['id'];
                                        $cidr = $getSourceData['cidr'];
                                        $ipaddress = $getSourceData['ipaddress'];
                                        $ipversion = $this->ip_version($ipaddress);
                                        if( $cidr == $hostCidr )
                                        {
                                            $rule_destination[] = "('$source','$vsys','$lid','address','$newlid')";
                                        }
                                        else
                                        {
                                            $getCheck = $projectdb->query("SELECT id FROM address WHERE source='$source' AND ipaddress='$ipaddress' AND cidr='$hostCidr' AND vsys='$vsys' AND vtype='';");
                                            if( $getCheck->num_rows == 0 )
                                            {
                                                $myname = "$SourceHost-$hostCidr";
                                                $name_int = $this->truncate_names($this->normalizeNames($myname));
                                                $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,$ipversion,vsys) values('$name_int','ip-netmask','$myname','1','$source','1','$ipaddress','$hostCidr','1','$vsys');");
                                                $newlid = $projectdb->insert_id;
                                                $rule_destination[] = "('$source','$vsys','$lid','address','$newlid')";
                                            }
                                            else
                                            {
                                                $data = $getCheck->fetch_assoc();
                                                $newlid = $data['id'];
                                                $rule_destination[] = "('$source','$vsys','$lid','address','$newlid')";
                                            }
                                        }
                                    }
                                }
                                $next_service = $next + 2;
                            }
                            elseif( $Destination == "interface" )
                            {
                                $next_service = $next + 2;
                            }
                            elseif( $Destination == "object" )
                            {
                                $SourceHost = $netObj[$next + 1];
                                $getAddress = $projectdb->query("SELECT id FROM address WHERE source='$source' AND BINARY name_ext='$SourceHost' AND vsys='$vsys' AND vtype='object';");
                                if( $getAddress->num_rows == 1 )
                                {
                                    $getData = $getAddress->fetch_assoc();
                                    $newlid = $getData['id'];
                                    $rule_destination[] = "('$source','$vsys','$lid','address','$newlid')";
                                }
                                $next_service = $next + 2;
                            }
                            elseif( $this->checkNetmask($netObj[$next + 1]) )
                            {
                                if( validateIpAddress($netObj[$next], "v4") )
                                {
                                    $src = $netObj[$next];
                                    $SourceNetmask = $netObj[$next + 1];
                                    $SourceCidr = $this->mask2cidrv4($SourceNetmask);
                                    #Check if exists an object or create it
                                    $getSource = $projectdb->query("SELECT id FROM address WHERE source='$source' AND ipaddress='$src' AND cidr='$SourceCidr' AND vsys='$vsys' AND vtype='';");
                                    if( $getSource->num_rows == 0 )
                                    {
                                        if( $SourceCidr == 32 )
                                        {
                                            $NameComplete = "H-$src";
                                        }
                                        else
                                        {
                                            $NameComplete = "N-$src-$SourceCidr";
                                        }
                                        $getDup = $projectdb->query("SELECT id FROM address WHERE source='$source' AND BINARY name_ext='$NameComplete' AND vsys='$vsys' AND vtype='';");
                                        if( $getDup->num_rows == 0 )
                                        {
                                            $name_int = $this->truncate_names($this->normalizeNames($NameComplete));
                                            $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values('$name_int','ip-netmask','$NameComplete','1','$source','0','$src','$SourceCidr','1','$vsys');");
                                            $newlid = $projectdb->insert_id;
                                            $rule_destination[] = "('$source','$vsys','$lid','address','$newlid')";
                                        }
                                        else
                                        {
                                            $getSourceData = $getSource->fetch_assoc();
                                            $newlid = $getSourceData['id'];
                                            $rule_destination[] = "('$source','$vsys','$lid','address','$newlid')";
                                        }
                                    }
                                    else
                                    {
                                        $getSourceData = $getSource->fetch_assoc();
                                        $newlid = $getSourceData['id'];
                                        $rule_destination[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                }
                                else
                                {
                                    $src = $netObj[$next];
                                    $SourceNetmask = $netObj[$next + 1];
                                    $SourceCidr = $this->mask2cidrv4($SourceNetmask);
                                    $getSource = $projectdb->query("SELECT id,cidr,ipaddress FROM address WHERE source='$source' AND BINARY name_ext='$src' AND vsys='$vsys' AND vtype='';");
                                    if( $getSource->num_rows == 0 )
                                    {
                                        add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using an Address [' . $src . '] that is not defined in my Database. Fix it before to finish', $source, 'Adding to the DB [ip:1.1.1.1]. Add IP Address', 'rules', $lid, 'security_rules');
                                        $name_int = $this->truncate_names($this->normalizeNames($src));
                                        $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys) values ('$name_int','ip-netmask','$src','1','$source','1','1.1.1.1','$hostCidr','1','$vsys');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_destination[] = "('$source','$vsys','$lid','address','$newlid')";
                                    }
                                    else
                                    {
                                        $getSourceData = $getSource->fetch_assoc();
                                        $cidr = $getSourceData['cidr'];
                                        $ipaddress = $getSourceData['ipaddress'];
                                        $ipversion = $this->ip_version($ipaddress);
                                        if( $cidr == $SourceCidr )
                                        {
                                            $newlid = $getSourceData['id'];
                                            $rule_destination[] = "('$source','$vsys','$lid','address','$newlid')";
                                        }
                                        else
                                        {
                                            $getCheck = $projectdb->query("SELECT id FROM address WHERE source='$source' AND BINARY name_ext='$src-$SourceCidr' AND vsys='$vsys' AND vtype='';");
                                            if( $getCheck->num_rows == 0 )
                                            {
                                                $myname = "$src-$SourceCidr";
                                                $name_int = $this->truncate_names($this->normalizeNames($myname));
                                                $projectdb->query("INSERT INTO address (type,name_ext,name,checkit,source,used,ipaddress,cidr,v4,vsys) values ('ip-netmask','$myname','$name_int','1','$source','0','$ipaddress','$SourceCidr','1','$vsys');");
                                                $newlid = $projectdb->insert_id;
                                                $rule_destination[] = "('$source','$vsys','$lid','address','$newlid')";
                                            }
                                            else
                                            {
                                                $data = $getCheck->fetch_assoc();
                                                $newlid = $data['id'];
                                                $rule_destination[] = "('$source','$vsys','$lid','address','$newlid')";
                                            }
                                        }
                                    }
                                }
                                $next_service = $next + 2;
                            }
                        }

                        #Add Services
                        if( isset($netObj[$next_service]) )
                        {
                            if( $netObj[$next_service] == "log" )
                            {

                            }
                            elseif( $netObj[$next_service] == "eq" )
                            {
                                $ServiceName = $netObj[$next_service + 1];
                                if( is_numeric($ServiceName) )
                                {
                                    #port number
                                    if( $protocol_tcp_udp == 1 )
                                    {
                                        $ruleProtocol = "tcp";
                                        $getSRV = $projectdb->query("SELECT id FROM services WHERE source='$source' AND dport='$ServiceName' AND protocol='$ruleProtocol' AND vsys='$vsys';");
                                        if( $getSRV->num_rows > 0 )
                                        {
                                            $data = $getSRV->fetch_assoc();
                                            $newlid = $data['id'];
                                            $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                        }
                                        else
                                        {
                                            $myname = "$ruleProtocol-$ServiceName";
                                            $name_int = $this->truncate_names($this->normalizeNames($myname));
                                            $projectdb->query("INSERT INTO services (name,vsys,name_ext,checkit,source,used,dport,protocol) values('$name_int','$vsys','$ruleProtocol-$ServiceName','1','$source','1','$ServiceName','$ruleProtocol');");
                                            $newlid = $projectdb->insert_id;
                                            $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                        }
                                        $ruleProtocol = "udp";
                                        $getSRV = $projectdb->query("SELECT id FROM services WHERE source='$source' AND dport='$ServiceName' AND protocol='$ruleProtocol' AND vsys='$vsys';");
                                        if( $getSRV->num_rows > 0 )
                                        {
                                            $data = $getSRV->fetch_assoc();
                                            $newlid = $data['id'];
                                            $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                        }
                                        else
                                        {
                                            $myname = "$ruleProtocol-$ServiceName";
                                            $name_int = $this->truncate_names($this->normalizeNames($myname));
                                            $projectdb->query("INSERT INTO services (name,vsys,name_ext,checkit,source,used,dport,protocol) values('$name_int','$vsys','$ruleProtocol-$ServiceName','1','$source','1','$ServiceName','$ruleProtocol');");
                                            $newlid = $projectdb->insert_id;
                                            $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                        }
                                    }
                                    else
                                    {
                                        $getSRV = $projectdb->query("SELECT id FROM services WHERE source='$source' AND dport='$ServiceName' AND protocol='$ruleProtocol' AND vsys='$vsys';");
                                        if( $getSRV->num_rows > 0 )
                                        {
                                            $data = $getSRV->fetch_assoc();
                                            $newlid = $data['id'];
                                            $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                        }
                                        else
                                        {
                                            $myname = "$ruleProtocol-$ServiceName";
                                            $name_int = $this->truncate_names($this->normalizeNames($myname));
                                            $projectdb->query("INSERT INTO services (name,vsys,name_ext,checkit,source,used,dport,protocol) values('$name_int','$vsys','$ruleProtocol-$ServiceName','1','$source','1','$ServiceName','$ruleProtocol');");
                                            $newlid = $projectdb->insert_id;
                                            $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                        }
                                    }
                                }
                                else
                                {
                                    #Port Name
                                    if( $protocol_tcp_udp == 1 )
                                    {
                                        $ruleProtocol = "tcp";
                                        $searchname = $projectdb->query("SELECT id,dport FROM services WHERE source='$source' AND BINARY name_ext='$ServiceName' LIMIT 1;");
                                        $cisconame = $searchname->fetch_assoc();
                                        $port_final = $cisconame['dport'];
                                        if( $port_final == "" )
                                        {
                                            add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using a Service Name [' . $ServiceName . '] and Protocol [' . $ruleProtocol . '] that is not defined in my Database. Fix it before to finish', $source, 'Adding to the DB [dport:65000].', 'rules', $lid, 'security_rules');
                                            $port_final = "65000";
                                        }
                                        else
                                        {
                                            $getSRV = $projectdb->query("SELECT id FROM services WHERE source='$source' AND dport='$port_final' AND protocol='$ruleProtocol';");
                                            if( $getSRV->num_rows > 0 )
                                            {
                                                $data = $getSRV->fetch_assoc();
                                                $newlid = $data['id'];
                                                $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                            }
                                            else
                                            {
                                                #create it
                                                $myname = "$ruleProtocol-$port_final";
                                                $name_int = $this->truncate_names($this->normalizeNames($myname));
                                                $projectdb->query("INSERT INTO services (name,vsys,name_ext,checkit,source,used,protocol,dport) values('$name_int','$vsys','$ruleProtocol-$port_final','1','$source','1','$ruleProtocol','$port_final');");
                                                $newlid = $projectdb->insert_id;
                                                $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                            }
                                        }
                                        $ruleProtocol = "udp";
                                        $searchname = $projectdb->query("SELECT id,dport FROM services WHERE source='$source' AND BINARY name_ext='$ServiceName' LIMIT 1;");
                                        $cisconame = $searchname->fetch_assoc();
                                        $port_final = $cisconame['dport'];
                                        if( $port_final == "" )
                                        {
                                            add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using a Service Name [' . $ServiceName . '] and Protocol [' . $ruleProtocol . '] that is not defined in my Database. Fix it before to finish', $source, 'Adding to the DB [dport:65000].', 'rules', $lid, 'security_rules');
                                            $port_final = "65000";
                                        }
                                        else
                                        {
                                            $getSRV = $projectdb->query("SELECT id FROM services WHERE source='$source' AND dport='$port_final' AND protocol='$ruleProtocol' AND vsys='$vsys';");
                                            if( $getSRV->num_rows > 0 )
                                            {
                                                $data = $getSRV->fetch_assoc();
                                                $newlid = $data['id'];
                                                $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                            }
                                            else
                                            {
                                                $myname = "$ruleProtocol-$port_final";
                                                $name_int = $this->truncate_names($this->normalizeNames($myname));
                                                $projectdb->query("INSERT INTO services (name,vsys,name_ext,checkit,source,used,protocol,dport) values('$name_int','$vsys','$ruleProtocol-$port_final','1','$source','1','$ruleProtocol','$port_final');");
                                                $newlid = $projectdb->insert_id;
                                                $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        $searchname = $projectdb->query("SELECT id,dport FROM services WHERE source='$source' AND BINARY name_ext='$ServiceName' LIMIT 1;");
                                        $cisconame = $searchname->fetch_assoc();
                                        $port_final = $cisconame['dport'];
                                        if( $port_final == "" )
                                        {
                                            add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using a Service Name [' . $ServiceName . '] and Protocol [' . $ruleProtocol . '] that is not defined in my Database. Fix it before to finish', $source, 'Adding to the DB [dport:6500].', 'rules', $lid, 'security_rules');
                                            $port_final = "6500";
                                        }
                                        else
                                        {
                                            $getSRV = $projectdb->query("SELECT id FROM services WHERE source='$source' AND dport='$port_final' AND protocol='$ruleProtocol' AND vsys='$vsys';");
                                            if( $getSRV->num_rows > 0 )
                                            {
                                                $data = $getSRV->fetch_assoc();
                                                $newlid = $data['id'];
                                                $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                            }
                                            else
                                            {
                                                $myname = "$ruleProtocol-$port_final";
                                                $name_int = $this->truncate_names($this->normalizeNames($myname));
                                                $projectdb->query("INSERT INTO services (name,vsys,name_ext,checkit,source,used,protocol,dport) values('$name_int','$vsys','$ruleProtocol-$port_final','1','$source','1','$ruleProtocol','$port_final');");
                                                $newlid = $projectdb->insert_id;
                                                $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                            }
                                        }
                                    }
                                }
                            }
                            elseif( $netObj[$next_service] == "lt" )
                            {
                                $ServiceName = $netObj[$next_service + 1];
                                if( is_numeric($ServiceName) )
                                {
                                    $port_final = $ServiceName;
                                    $srv_port_before = intval($port_final);
                                    $port_final = "1-$srv_port_before";
                                }
                                else
                                {
                                    $searchname = $projectdb->query("SELECT id,dport FROM services WHERE source='$source' AND BINARY name_ext='$ServiceName' LIMIT 1;");
                                    $cisconame = $searchname->fetch_assoc();
                                    $port_final = $cisconame['dport'];
                                    if( $port_final == "" )
                                    {
                                        add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using a Service Name [' . $ServiceName . '] and Protocol [' . $ruleProtocol . '] that is not defined in my Database. Fix it before to finish', $source, 'Adding to the DB [dport:6500].', 'rules', $lid, 'security_rules');
                                        $port_final = "6500";
                                    }
                                    $srv_port_before = intval($port_final);
                                    $port_final = "1-$srv_port_before";
                                }
                                if( $protocol_tcp_udp == 1 )
                                {
                                    $ruleProtocol = "tcp";
                                    $getPort = $projectdb->query("SELECT id FROM services WHERE source='$source' AND dport='$port_final' AND protocol='$ruleProtocol' AND vsys='$vsys';");
                                    if( $getPort->num_rows == 0 )
                                    {
                                        $myname = "LT-$ruleProtocol-$port_final";
                                        $name_int = $this->truncate_names($this->normalizeNames($myname));
                                        $projectdb->query("INSERT INTO services (name,vsys,name_ext,checkit,source,used,protocol,dport) values('$name_int','$vsys','LT-$ruleProtocol-$port_final','1','$source','1','$ruleProtocol','$port_final');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    }
                                    else
                                    {
                                        $data = $getPort->fetch_assoc();
                                        $newlid = $data['id'];
                                        $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    }
                                    $ruleProtocol = "udp";
                                    $getPort = $projectdb->query("SELECT id FROM services WHERE source='$source' AND dport='$port_final' AND protocol='$ruleProtocol' AND vsys='$vsys';");
                                    if( $getPort->num_rows == 0 )
                                    {
                                        $myname = "LT-$ruleProtocol-$port_final";
                                        $name_int = $this->truncate_names($this->normalizeNames($myname));
                                        $projectdb->query("INSERT INTO services (name,vsys,name_ext,checkit,source,used,protocol,dport) values('$name_int','$source','LT-$ruleProtocol-$port_final','1','$source','1','$ruleProtocol','$port_final');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    }
                                    else
                                    {
                                        $data = $getPort->fetch_assoc();
                                        $newlid = $data['id'];
                                        $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    }
                                }
                                else
                                {
                                    $getPort = $projectdb->query("SELECT id FROM services WHERE source='$source' AND protocol='$ruleProtocol' and dport='$port_final' AND vsys='$vsys';");
                                    if( $getPort->num_rows == 0 )
                                    {
                                        $myname = "LT-$ruleProtocol-$port_final";
                                        $name_int = $this->truncate_names($this->normalizeNames($myname));
                                        $projectdb->query("INSERT INTO services (name,vsys,name_ext,checkit,source,used,protocol,dport) values('$name_int','$vsys','LT-$ruleProtocol-$port_final','1','$source','1','$ruleProtocol','$port_final');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    }
                                    else
                                    {
                                        $data = $getPort->fetch_assoc();
                                        $newlid = $data['id'];
                                        $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    }
                                }
                            }
                            elseif( $netObj[$next_service] == "gt" )
                            {
                                $ServiceName = $netObj[$next_service + 1];
                                if( is_numeric($ServiceName) )
                                {
                                    $port_final = $ServiceName;
                                    $srv_port_before = intval($port_final);
                                    $port_final = "$srv_port_before-65535";
                                }
                                else
                                {
                                    $searchname = $projectdb->query("SELECT id,dport FROM services WHERE source='$source' AND BINARY name_ext='$ServiceName' LIMIT 1;");
                                    $cisconame = $searchname->fetch_assoc();
                                    $port_final = $cisconame['dport'];
                                    if( $port_final == "" )
                                    {
                                        add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using a Service port-name [' . $ServiceName . '] and Protocol [' . $ruleProtocol . '] that is not defined in my Database. Fix it before to finish', $source, 'Adding to the DB [dport:1025].', 'rules', $lid, 'security_rules');
                                        $port_final = "1025";
                                    }
                                    $srv_port_before = intval($port_final);
                                    $port_final = "$srv_port_before-65535";
                                }
                                if( $protocol_tcp_udp == 1 )
                                {
                                    $ruleProtocol = "tcp";
                                    $getPort = $projectdb->query("SELECT id FROM services WHERE source='$source' AND dport='$port_final' AND protocol='$ruleProtocol' AND vsys='$vsys';");
                                    if( $getPort->num_rows == 0 )
                                    {
                                        $myname = "GT-$ruleProtocol-$port_final";
                                        $name_int = $this->truncate_names($this->normalizeNames($myname));
                                        $projectdb->query("INSERT INTO services (name,vsys,name_ext,checkit,source,used,protocol,dport) values('$name_int','$vsys','GT-$ruleProtocol-$port_final','1','$source','1','$ruleProtocol','$port_final');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    }
                                    else
                                    {
                                        $data = $getPort->fetch_assoc();
                                        $newlid = $data['id'];
                                        $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    }
                                    $ruleProtocol = "udp";
                                    $getPort = $projectdb->query("SELECT id FROM services WHERE source='$source' AND dport='$port_final' AND protocol='$ruleProtocol' AND vsys='$vsys';");
                                    if( $getPort->num_rows == 0 )
                                    {
                                        $myname = "GT-$ruleProtocol-$port_final";
                                        $name_int = $this->truncate_names($this->normalizeNames($myname));
                                        $projectdb->query("INSERT INTO services (name,vsys,name_ext,checkit,source,used,protocol,dport) values('$name_int','$vsys','GT-$ruleProtocol-$port_final','1','$source','1','$ruleProtocol','$port_final');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    }
                                    else
                                    {
                                        $data = $getPort->fetch_assoc();
                                        $newlid = $data['id'];
                                        $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    }
                                }
                                else
                                {
                                    $getPort = $projectdb->query("SELECT id FROM services WHERE source='$source' AND protocol='$ruleProtocol' and dport='$port_final' AND vsys='$vsys';");
                                    if( $getPort->num_rows == 0 )
                                    {
                                        $myname = "GT-$ruleProtocol-$port_final";
                                        $name_int = $this->truncate_names($this->normalizeNames($myname));
                                        $projectdb->query("INSERT INTO services (name,vsys,name_ext,checkit,source,used,protocol,dport) values('$name_int','$vsys','GT-$ruleProtocol-$port_final','1','$source','1','$ruleProtocol','$port_final');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    }
                                    else
                                    {
                                        $data = $getPort->fetch_assoc();
                                        $newlid = $data['id'];
                                        $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    }
                                }
                            }
                            elseif( $netObj[$next_service] == "neq" )
                            {
                                $ServiceName = $netObj[$next_service + 1];
                                if( is_numeric($ServiceName) )
                                {
                                    $port_final = $ServiceName;
                                    $srv_port_before = intval($port_final) - 1;
                                    $srv_port_after = intval($port_final) + 1;
                                    $port_final = "1-$srv_port_before,$srv_port_after-65535";
                                    $port_final_name = "1-$srv_port_before_$srv_port_after-65535";
                                }
                                else
                                {
                                    $searchname = $projectdb->query("SELECT id,dport FROM services WHERE source='$source' AND BINARY name_ext='$ServiceName' LIMIT 1;");
                                    $cisconame = $searchname->fetch_assoc();
                                    $port_final = $cisconame['dport'];
                                    if( $port_final == "" )
                                    {
                                        add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using a Service Name [' . $ServiceName . '] and Protocol [' . $ruleProtocol . '] that is not defined in my Database. Fix it before to finish', $source, 'Adding to the DB [dport:6500].', 'rules', $lid, 'security_rules');
                                        $port_final = "6500";
                                    }
                                    $srv_port_before = intval($port_final) - 1;
                                    $srv_port_after = intval($port_final) + 1;
                                    $port_final = "1-$srv_port_before,$srv_port_after-65535";
                                    $port_final_name = "1-$srv_port_before_$srv_port_after-65535";
                                }

                                if( $protocol_tcp_udp == 1 )
                                {
                                    $ruleProtocol = "tcp";
                                    $getPort = $projectdb->query("SELECT id FROM services WHERE source='$source' AND protocol='$ruleProtocol' and dport='$port_final' AND vsys='$vsys';");
                                    if( $getPort->num_rows == 0 )
                                    {
                                        $myname = "NO-$ruleProtocol-$port_final_name";
                                        $name_int = $this->truncate_names($this->normalizeNames($myname));
                                        $projectdb->query("INSERT INTO services (name,vsys,name_ext,checkit,source,used,protocol,dport) values('$name_int','$vsys','NO-$ruleProtocol-$port_final_name','1','$source','1','$ruleProtocol','$port_final');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    }
                                    else
                                    {
                                        $data = $getPort->fetch_assoc();
                                        $newlid = $data['id'];
                                        $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    }
                                    $ruleProtocol = "udp";
                                    $getPort = $projectdb->query("SELECT id FROM services WHERE source='$source' AND protocol='$ruleProtocol' and dport='$port_final' AND vsys='$vsys';");
                                    if( $getPort->num_rows == 0 )
                                    {
                                        $myname = "NO-$ruleProtocol-$port_final_name";
                                        $name_int = $this->truncate_names($this->normalizeNames($myname));
                                        $projectdb->query("INSERT INTO services (name,vsys,name_ext,checkit,source,used,protocol,dport) values('$name_int','$vsys','NO-$ruleProtocol-$port_final_name','1','$source','1','$ruleProtocol','$port_final');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    }
                                    else
                                    {
                                        $data = $getPort->fetch_assoc();
                                        $newlid = $data['id'];
                                        $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    }
                                }
                                else
                                {
                                    $getPort = $projectdb->query("SELECT id FROM services WHERE source='$source' AND protocol='$ruleProtocol' and dport='$port_final' AND vsys='$vsys';");
                                    if( $getPort->num_rows == 0 )
                                    {
                                        $myname = "NO-$ruleProtocol-$port_final_name";
                                        $name_int = $this->truncate_names($this->normalizeNames($myname));
                                        $projectdb->query("INSERT INTO services (name,vsys,name_ext,checkit,source,used,protocol,dport) values('$name_int','$vsys','NO-$ruleProtocol-$port_final_name','1','$source','1','$ruleProtocol','$port_final');");
                                        $newlid = $projectdb->insert_id;
                                        $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    }
                                    else
                                    {
                                        $data = $getPort->fetch_assoc();
                                        $newlid = $data['id'];
                                        $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    }
                                }
                            }
                            elseif( $netObj[$next_service] == "range" )
                            {
                                $port_first = $netObj[$next_service + 1];
                                $port_last = rtrim($netObj[$next_service + 2]);

                                if( is_numeric($port_first) )
                                {
                                    $port_first_port = $port_first;
                                }
                                else
                                {
                                    # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                                    $searchname = $projectdb->query("SELECT id,dport FROM services WHERE source='$source' AND BINARY name_ext='$port_first' LIMIT 1;");
                                    $cisconame = $searchname->fetch_assoc();
                                    $port_first_port = $cisconame['dport'];

                                    if( $port_first_port == "" )
                                    {
                                        add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using a Range port-name [' . $port_first . '] and Protocol [' . $ruleProtocol . '] that is not defined in my Database. Fix it before to finish', $source, 'Adding to the DB [dport:6500].', 'rules', $lid, 'security_rules');
                                        $port_first_port = "6500";
                                    }
                                }
                                if( is_numeric($port_last) )
                                {
                                    $port_last_port = $port_last;
                                }
                                else
                                {
                                    # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                                    $searchname = $projectdb->query("SELECT id,dport FROM services WHERE source='$source' AND BINARY name_ext='$port_last' LIMIT 1;");
                                    $cisconame = $searchname->fetch_assoc();
                                    $port_last_port = $cisconame['dport'];

                                    if( $port_last_port == "" )
                                    {
                                        add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using a Range port-name [' . $port_first . '] and Protocol [' . $ruleProtocol . '] that is not defined in my Database. Fix it before to finish', $source, 'Adding to the DB [dport:6500].', 'rules', $lid, 'security_rules');
                                        $port_last_port = "6500";
                                    }
                                }

                                # Check first if they are EQUAL
                                if( $port_first_port == $port_last_port )
                                {
                                    $isRange = "";
                                    $LastPort = "";
                                    $vtype = "";
                                    add_log2('warning', 'Reading Security Policies', 'Security RuleID [' . $lid . ']. Moving Service-Range to Service [' . $names_line . '] ports are the same ', $source, 'No Action Required', 'rules', $lid, 'security_rules');
                                }
                                else
                                {
                                    $isRange = "-range";
                                    $LastPort = "-$port_last_port";
                                    $vtype = "range";
                                }

                                if( $protocol_tcp_udp == 1 )
                                {
                                    $myname = "tcp" . $isRange . "-$port_first_port" . $LastPort;
                                    $name_int = $this->truncate_names($this->normalizeNames($myname));
                                    $search = $projectdb->query("SELECT id FROM services WHERE source='$source' AND name_ext='tcp" . $isRange . "-$port_first_port" . $LastPort . "' ANd vsys='$vsys';");
                                    if( $search->num_rows == 0 )
                                    {
                                        $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$name_int','tcp" . $isRange . "-$port_first_port" . $LastPort . "','tcp','$port_first_port" . $LastPort . "','1','$source','$vsys');");
                                        $newlid = $projectdb->insert_id;
                                    }
                                    else
                                    {
                                        $data = $search->fetch_assoc();
                                        $newlid = $data['id'];
                                    }
                                    $projectdb->query("INSERT INTO security_rules_srv (rule_lid,member_lid,table_name,source,vsys) VALUES ('$lid','$newlid','services','$source','vsys1');");
                                    $newlid = $projectdb->insert_id;
                                    $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                    $search = $projectdb->query("SELECT id FROM services WHERE source='$source' AND name_ext='udp" . $isRange . "-$port_first_port" . $LastPort . "' AND vsys='$vsys';");
                                    if( $search->num_rows == 0 )
                                    {
                                        $myname = "udp" . $isRange . "-$port_first_port" . $LastPort;
                                        $name_int = $this->truncate_names($this->normalizeNames($myname));
                                        $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$name_int','udp" . $isRange . "-$port_first_port" . $LastPort . "','udp','$port_first_port" . $LastPort . "','1','$source','$vsys');");
                                        $newlid = $projectdb->insert_id;
                                    }
                                    else
                                    {
                                        $data = $search->fetch_assoc();
                                        $newlid = $data['id'];
                                    }
                                    $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                }
                                else
                                {
                                    $myname = "$ruleProtocol" . $isRange . "-$port_first_port" . $LastPort;
                                    $name_int = $this->truncate_names($this->normalizeNames($myname));
                                    $search = $projectdb->query("SELECT id FROM services WHERE source='$source' AND name_ext='$ruleProtocol" . $isRange . "-$port_first_port" . $LastPort . "' AND vsys='$vsys';");
                                    if( $search->num_rows == 0 )
                                    {
                                        $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$name_int','$ruleProtocol" . $isRange . "-$port_first_port" . $LastPort . "','$ruleProtocol','$port_first_port" . $LastPort . "','1','$source','$vsys');");
                                        $newlid = $projectdb->insert_id;
                                    }
                                    else
                                    {
                                        $data = $search->fetch_assoc();
                                        $newlid = $data['id'];
                                    }
                                    $rule_service[] = "('$source','$vsys','$lid','services','$newlid')";
                                }
                            }
                            elseif( $netObj[$next_service] == "object-group" )
                            {
                                $ServiceGroupName = $netObj[$next_service + 1];
                                $getSG = $projectdb->query("SELECT id FROM services_groups_id WHERE source='$source' AND BINARY name_ext='$ServiceGroupName' AND vsys='$vsys' LIMIT 1;");
                                if( $getSG->num_rows == 1 )
                                {
                                    $data = $getSG->fetch_assoc();
                                    $newlid = $data['id'];
                                    $rule_service[] = "('$source','$vsys','$lid','services_groups_id','$newlid')";
                                }
                                else
                                {
                                    #Check if its a icmp_group
                                    $getICMPGroup = $projectdb->query("SELECT id FROM cisco_icmp_groups WHERE source='$source' AND BINARY name='$ServiceGroupName' AND vsys='$vsys';");
                                    if( $getICMPGroup->num_rows > 0 )
                                    {
                                        $getICMP = $projectdb->query("SELECT id FROM default_applications WHERE name='icmp';");
                                        $getICMPData = $getICMP->fetch_assoc();
                                        $getICMPid = $getICMPData['id'];
                                        $rule_application[] = "('$source','$vsys','$lid','default_applications','$getICMPid')";
                                        //$rule_service[] = "('$source','$vsys','$lid','shared_services','$application_default')";
                                        $rule_service[] = "('$source','$vsys','$lid','services','$application_default')";
                                        add_log2('warning', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using a ICMP-Protocol Group [' . $ServiceGroupName . '].', $source, 'Replacing by ICMP app-id', 'rules', $lid, 'security_rules');
                                    }
                                    else
                                    {
                                        add_log2('error', 'Reading Security Policies', 'Security RuleID [' . $lid . '] is using a Service Group [' . $ServiceGroupName . '] but is not defined in my DB', $source, 'Create it and add it to the Rule', 'rules', $lid, 'security_rules');
                                    }
                                }
                            }
                        }


                        if( $isRule == 1 )
                        {
                            $next = "";
                            $next_service = "";
                            #Create the RULE
                            $accesslistNamePan = $this->truncate_tags($accesslistName);
                            if( $accesslistNamePan != $oldtag )
                            {
                                $getTag = $projectdb->query("SELECT id FROM tag WHERE name='$accesslistNamePan' AND source='$source' AND vsys='$vsys';");
                                if( $getTag->num_rows == 1 )
                                {
                                    $getTagData = $getTag->fetch_assoc();
                                    $tag_id = $getTagData['id'];
                                    $rule_tag[] = "('$source','$vsys','$lid','tag','$tag_id')";
                                    $oldtag = $accesslistNamePan;
                                }
                            }
                            else
                            {
                                $rule_tag[] = "('$source','$vsys','$lid','tag','$tag_id')";
                            }

                            $rule_name = "Rule $x";
                            $comments = implode(" ", $comment);
                            $rule[] = "('$lid','$position','$rule_name','$comments','$action','$rule_disabled','$vsys','$source','0','$accesslistName')";
                            $rule_mapping[] = "('$names_line','$rule_name','$lid')";
                            //$comment = array(); $comments="";

                            $lid++;
                            $position++;
                            $x++;
                        }
                    }
                }
            }

            #Save the Data into the DB
            if( count($rule) > 0 )
            {
                $projectdb->query("INSERT INTO security_rules (id,position,name,description,action,disabled,vsys,source,checkit,devicegroup) VALUES " . implode(",", $rule) . ";");
                unset($rule);

                if( count($rule_application) > 0 )
                {
                    $unique = array_unique($rule_application);
                    $projectdb->query("INSERT INTO security_rules_app (source,vsys,rule_lid,table_name,member_lid) VALUES " . implode(",", $unique) . ";");
                    unset($rule_application);
                    unset($unique);
                }
                if( count($rule_tag) > 0 )
                {
                    $unique = array_unique($rule_tag);
                    $projectdb->query("INSERT INTO security_rules_tag (source,vsys,rule_lid,table_name,member_lid) VALUES " . implode(",", $unique) . ";");
                    unset($rule_tag);
                    unset($unique);
                }
                if( count($rule_from) > 0 )
                {
                    $unique = array_unique($rule_from);
                    $projectdb->query("INSERT INTO security_rules_from (source,vsys,rule_lid,name) VALUES " . implode(",", $unique) . ";");
                    unset($rule_from);
                    unset($unique);
                }
                if( count($rule_to) > 0 )
                {
                    $unique = array_unique($rule_to);
                    $projectdb->query("INSERT INTO security_rules_to (source,vsys,rule_lid,name) VALUES " . implode(",", $unique) . ";");
                    unset($rule_to);
                    unset($unique);
                }
                if( count($rule_source) > 0 )
                {
                    $projectdb->query("INSERT INTO security_rules_src (source,vsys,rule_lid,table_name,member_lid) VALUES " . implode(",", $rule_source) . ";");
                    unset($rule_source);
                }
                if( count($rule_destination) > 0 )
                {
                    $projectdb->query("INSERT INTO security_rules_dst (source,vsys,rule_lid,table_name,member_lid) VALUES " . implode(",", $rule_destination) . ";");
                    unset($rule_destination);
                }
                if( count($rule_service) > 0 )
                {
                    $unique = array_unique($rule_service);
                    $projectdb->query("INSERT INTO security_rules_srv (source,vsys,rule_lid,table_name,member_lid) VALUES " . implode(",", $unique) . ";");
                    unset($rule_service);
                    unset($unique);
                }
                if( count($rule_mapping) > 0 )
                {
                    $projectdb->query("INSERT INTO cisco_policy_mapping (line,name,rule_lid) VALUES " . implode(",", $rule_mapping) . ";");
                    unset($rule_mapping);
                }
            }
        }
    }

}

