<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */


trait CISCOappid
{
    function load_customer_application($v)
    {

        $custom_appids = array();

        /*                  => IPv4; IPv6
        'echo'              => 0,8;128,129
        'echo-request'      => 8; 128
        'echo-reply'        => 0; 129
        'unreachable'       => 3; 1
        'port-unreachable'  =>
        'time-exceeded'     => 11; 3
        'nd-na'             => -; 136
        'nd-ns'             => -; 135
        */

//Todo: max. customapp-id name is 31 characters
        $custom_appids['icmp_echo-reply'] = array(0, 0);
        $custom_appids['icmp_unreachable'] = array(3, 0);
        $custom_appids['icmp_destination_unreachable'] = array(3, 0);
        $custom_appids['icmp_echo'] = array(8, 0);
        $custom_appids['icmp_source_quench'] = array(4, 0);
        $custom_appids['icmp_timestamp'] = array(13, 0);
        $custom_appids['icmp_info_req'] = array(15, 0);
        $custom_appids['icmp_address_mask'] = array(17, 0);
        $custom_appids['icmp_redirect'] = array(5, 0);
        $custom_appids['icmp_time_exceeded'] = array(11, 0);

        $custom_appids['ipv6-icmp_echo-request'] = array(128, 0);
        $custom_appids['ipv6-icmp_port-unreachable'] = array(1, 4);
        $custom_appids['ipv6-icmp_dest-unreachable'] = array(1, 0);
        $custom_appids['ipv6-icmp_time-exceeded'] = array(3, 0);
        $custom_appids['ipv6-icmp_nd-na'] = array(136, 0);
        $custom_appids['ipv6-icmp_nd-ns'] = array(135, 0);
        $custom_appids['ipv6-icmp_pkt_toobig'] = array(2, 0);
        $custom_appids['ipv6-icmp_param_problem_c1'] = array(4, 1);
        $custom_appids['ipv6-icmp_param_problem_c2'] = array(4, 2);

        $app_string = "<application>\n";
        foreach( $custom_appids as $key => $icmp_type )
        {
            if( strpos($key, "ipv6") !== FALSE )
                $ident_by = 'ident-by-icmp6-type';
            else
                $ident_by = 'ident-by-icmp-type';

            if( isset($icmp_type[1]) )
                $icmp_code = "<code>" . $icmp_type[1] . "</code>";
            else
                $icmp_code = "";

            $tmp_string = "    <entry name=\"" . $key . "\">
      <subcategory>ip-protocol</subcategory>
      <category>networking</category>
      <technology>network-protocol</technology>
      <risk>1</risk>
      <default>
        <" . $ident_by . ">
          <type>" . $icmp_type[0] . "</type>
          " . $icmp_code . "
        </" . $ident_by . ">
      </default>
    </entry>";
            $app_string .= $tmp_string . "\n";

            //todo: find xml root for appstore and add tmp_string
        }
        $app_string .= "</application>";
#print $test_string;

        #$tmp = DH::findFirstElementOrCreate('application', $xml);
        #$v->appStore->load_application_custom_from_domxml($tmp);

        $doc = new DOMDocument();
        $doc->loadXML($app_string);
        $tmp = DH::findFirstElementOrCreate('application', $doc);
        print "load appid\n";
        $v->appStore->load_application_custom_from_domxml($tmp);

        $v->appStore->rewriteXML();


        foreach( $v->appStore->apps() as $app )
        {
            if( $app->type == 'application-custom' )
            {
                print "appname: " . $app->name() . "\n";
            }
        }
    }
}

