<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */

require_once("lib/pan_php_framework.php");

trait CISCOservices
{
    #function get_icmp_groups($cisco_config_file, $source, $vsys) {
    function get_icmp_groups($data, $v)
    {
        global $projectdb;

        global $debug;
        global $print;

        $addName = array();
        $isIcmpGroup = 0;
        $IcmpGroupName = "";
        foreach( $data as $line => $names_line )
        {
            $names_line = trim($names_line);
            if( !preg_match("/^icmp-object/", $names_line) )
            {
                $isIcmpGroup = 0;
            }

            if( preg_match("/^object-group icmp-type/i", $names_line) )
            {
                $isIcmpGroup = 1;
                $names = explode(" ", $names_line);
                $IcmpGroupName = rtrim($names[2]);

                $tmp_servicegroup = $v->serviceStore->find($IcmpGroupName);
                if( $tmp_servicegroup === null )
                {
                    if( $print )
                        print " * create protocol group: " . $IcmpGroupName . "\n";
                    $tmp_servicegroup = $v->serviceStore->newServiceGroup($IcmpGroupName);
                }
            }

            if( $isIcmpGroup == 1 )
            {
                if( preg_match("/\bicmp-object\b/i", $names_line) )
                {
                    $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                    #$addName[] = "('$netObj[1]','$source','$vsys','$IcmpGroupName')";

                    $tmp_service = $v->serviceStore->find($netObj[1]);
                    if( $tmp_service === null )
                    {
                        $tmp_service = $v->serviceStore->find("tmp-" . $netObj[1]);
                        if( $tmp_service !== null )
                        {
                            if( $print )
                                print "   * add service: " . $tmp_service->name() . " to icmp group " . $tmp_servicegroup->name() . "\n";
                            $tmp_servicegroup->addMember($tmp_service);
                        }
                        else
                        {
                            #derr( "object-group protcol - protocol-object: ".$netObj[1]. " not yet available: ".$names_line );
                            if( $print )
                                print "   * create service tmp-" . $netObj[1] . "\n";
                            if( $netObj[1] == "tcp" || $netObj[1] == "udp" )
                                $Protocol = $netObj[1];
                            else
                                $Protocol = 'tcp';

                            $tmp_service = $v->serviceStore->newService("tmp-" . $netObj[1], $Protocol, "1-65535");
                            $tmp_service->set_node_attribute('error', "no service protocol - " . $Protocol . " is used");
                            if( $print )
                                print "   * add service: " . $tmp_service->name() . " to icmp group " . $tmp_servicegroup->name() . "\n";
                            $tmp_servicegroup->addMember($tmp_service);
                        }

                    }
                    else
                    {
                        if( $print )
                            print "   * add service: " . $tmp_service->name() . " to icmp group " . $tmp_servicegroup->name() . "\n";
                        $tmp_servicegroup->addMember($tmp_service);
                    }
                }
            }


        }
        /*
        if (count($addName) > 0) {
            $projectdb->query("INSERT INTO cisco_icmp_groups (member,source,vsys,name) values" . implode(",", $addName) . ";");
            unset($addName);
        }
        */
    }

    #function get_protocol_groups($cisco_config_file, $source, $vsys) {
    function get_protocol_groups($data, $v)
    {
        global $projectdb;

        global $debug;
        global $print;

        $addName = array();
        $isProtocolGroup = 0;
        foreach( $data as $line => $names_line )
        {
            $names_line = trim($names_line);

            if( preg_match("/^object-group protocol/i", $names_line) )
            {
                $isProtocolGroup = 1;
                $names = explode(" ", $names_line);
                $ProtocolGroupName = rtrim($names[2]);

                $tmp_servicegroup = $v->serviceStore->find($ProtocolGroupName);
                if( $tmp_servicegroup === null )
                {
                    if( $print )
                        print " * create protocol group: " . $ProtocolGroupName . "\n";
                    $tmp_servicegroup = $v->serviceStore->newServiceGroup($ProtocolGroupName);
                }
            }

            if( $isProtocolGroup == 1 )
            {
                if( preg_match("/protocol-object/i", $names_line) )
                {
                    $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                    #$addName[] = "('$netObj[1]','$source','$vsys','$ProtocolGroupName')";
                    $tmp_service = $v->serviceStore->find($netObj[1]);
                    if( $tmp_service === null )
                    {
                        $tmp_service = $v->serviceStore->find("tmp-" . $netObj[1]);
                        if( $tmp_service !== null )
                        {
                            if( $print )
                                print "   * add service: " . $tmp_service->name() . " to protocol group " . $tmp_servicegroup->name() . "\n";
                            $tmp_servicegroup->addMember($tmp_service);
                        }
                        else
                        {
                            #derr( "object-group protcol - protocol-object: ".$netObj[1]. " not yet available: ".$names_line );
                            if( $print )
                                print "   * create service tmp-" . $netObj[1] . "\n";
                            if( $netObj[1] == "tcp" || $netObj[1] == "udp" )
                                $Protocol = $netObj[1];
                            else
                                $Protocol = 'tcp';

                            $tmp_service = $v->serviceStore->newService("tmp-" . $netObj[1], $Protocol, "1-65535");
                            $tmp_service->set_node_attribute('error', "no service protocol - tcp is used");
                            if( $print )
                                print "   * add service: " . $tmp_service->name() . " to protocol group " . $tmp_servicegroup->name() . "\n";
                            $tmp_servicegroup->addMember($tmp_service);
                        }

                    }
                    else
                    {
                        if( $print )
                            print "   * add service: " . $tmp_service->name() . " to protocol group " . $tmp_servicegroup->name() . "\n";
                        $tmp_servicegroup->addMember($tmp_service);
                    }

                }
            }


        }
        /*
        if (count($addName) > 0) {
            $projectdb->query("INSERT INTO cisco_protocol_groups (member,source,vsys,name) values" . implode(",", $addName) . ";");
            unset($addName);
        }
        */

        //
    }


    function add_cisco_services($data, $v)
    {
        global $projectdb;
        global $debug;
        global $print;

        $vsys = $v->name();

        $source = "";

        #$exists = $projectdb->query("SELECT id FROM services WHERE source='$source' AND vsys='$vsys' AND name_ext IN ('echo','gopher','pcanywhere-data');");
        #if ($exists->num_rows == 0) {
        $add_srv = array();
        $add_srv[] = array($source, $vsys, 'echo', 'echo', '', '7');
        $add_srv[] = array($source, $vsys, 'discard', 'discard', '', '9');
        $add_srv[] = array($source, $vsys, 'tacacs', 'tacacs', 'tcp', '49');  //changed tacacs-plus
        $add_srv[] = array($source, $vsys, 'tacacs', 'tacacs', 'udp', '49');  //changed tacacs-plus
        $add_srv[] = array($source, $vsys, 'domain', 'domain', 'udp', '53');
        $add_srv[] = array($source, $vsys, 'sunrpc', 'sunrpc', 'tcp', '111'); // changed portmapper
        $add_srv[] = array($source, $vsys, 'sunrpc', 'sunrpc', 'udp', '111'); // changed portmapper
        $add_srv[] = array($source, $vsys, 'pim-auto-rp', 'pim-auto-rp', 'tcp', '496');
        $add_srv[] = array($source, $vsys, 'pim-auto-rp', 'pim-auto-rp', 'udp', '496');
        $add_srv[] = array($source, $vsys, 'talk', 'talk', 'tcp', '517');
        $add_srv[] = array($source, $vsys, 'talk', 'talk', 'udp', '517');
        $add_srv[] = array($source, $vsys, 'kerberos', 'kerberos', 'tcp', '750');
        $add_srv[] = array($source, $vsys, 'kerberos', 'kerberos', 'udp', '750');
        $add_srv[] = array($source, $vsys, 'nfs', 'nfs', 'tcp', '2049');
        $add_srv[] = array($source, $vsys, 'nfs', 'nfs', 'udp', '2049');
        $add_srv[] = array($source, $vsys, 'sip', 'sip', 'tcp', '5060');
        $add_srv[] = array($source, $vsys, 'sip', 'sip', 'udp', '5060');
        //        $add_srv[] = array($source,$vsys,'112','vrrp','112','0');
        //        $add_srv[] = array($source,$vsys,'46','rsvp','46','0');
        //        $add_srv[] = array($source,$vsys,'57','skip','57','0');
        //        $add_srv[] = array($source,$vsys,'97','etherip','97','0');
        //        $add_srv[] = array($source,$vsys,'ah','ipsec-ah','ah','0');
        //        $add_srv[] = array($source,$vsys,'eigrp','eigrp','eigrp','0');
        //        $add_srv[] = array($source,$vsys,'esp','ipsec-esp','esp','0');
        //        $add_srv[] = array($source,$vsys,'gre','gre','gre','0');
        //        $add_srv[] = array($source,$vsys,'icmp','icmp','icmp','0');
        //        $add_srv[] = array($source,$vsys,'icmp6','ipv6-icmp','icmp6','0');
        //        $add_srv[] = array($source,$vsys,'igmp','igmp','igmp','0');
        //        $add_srv[] = array($source,$vsys,'ipinip','ip-in-ip','ipinip','0');
        //        $add_srv[] = array($source,$vsys,'ipsec','ipsec','ipsec','0');
        //        $add_srv[] = array($source,$vsys,'ospf','ospf','ospf','0');
        //        $add_srv[] = array($source,$vsys,'pim','pim','pim','0');
        $add_srv[] = array($source, $vsys, 'daytime', 'daytime', 'tcp', '13');
        $add_srv[] = array($source, $vsys, 'chargen', 'chargen', 'tcp', '19');
        $add_srv[] = array($source, $vsys, 'ftp-data', 'ftp-data', 'tcp', '20');
        $add_srv[] = array($source, $vsys, 'ftp', 'ftp', 'tcp', '21');
        $add_srv[] = array($source, $vsys, 'ssh', 'ssh', 'tcp', '22');
        $add_srv[] = array($source, $vsys, 'telnet', 'telnet', 'tcp', '23');
        $add_srv[] = array($source, $vsys, 'smtp', 'smtp', 'tcp', '25');
        $add_srv[] = array($source, $vsys, 'whois', 'whois', 'tcp', '43');
        $add_srv[] = array($source, $vsys, 'gopher', 'gopher', 'tcp', '70');
        $add_srv[] = array($source, $vsys, 'finger', 'finger', 'tcp', '79');
        $add_srv[] = array($source, $vsys, 'www', 'www', 'tcp', '80');
        $add_srv[] = array($source, $vsys, 'hostname', 'hostname', 'tcp', '101');
        $add_srv[] = array($source, $vsys, 'pop2', 'pop2', 'tcp', '109');
        $add_srv[] = array($source, $vsys, 'pop3', 'pop3', 'tcp', '110');
        $add_srv[] = array($source, $vsys, 'ident', 'ident', 'tcp', '113');
        $add_srv[] = array($source, $vsys, 'nntp', 'nntp', 'tcp', '119');
        $add_srv[] = array($source, $vsys, 'netbios-ssn', 'netbios-ssn', 'tcp', '139');  //changed netbios-ss_tcp
        $add_srv[] = array($source, $vsys, 'imap4', 'imap4', 'tcp', '143');  //changed imap
        $add_srv[] = array($source, $vsys, 'bgp', 'bgp', 'tcp', '179');
        $add_srv[] = array($source, $vsys, 'irc', 'irc', 'tcp', '194');
        $add_srv[] = array($source, $vsys, 'ldap', 'ldap', 'tcp', '389');
        $add_srv[] = array($source, $vsys, 'https', 'https', 'tcp', '443');
        $add_srv[] = array($source, $vsys, 'exec', 'exec', 'tcp', '512');  //changed r-exec
        $add_srv[] = array($source, $vsys, 'login', 'login', 'tcp', '513');
        $add_srv[] = array($source, $vsys, 'cmd', 'cmd', 'tcp', '514');
        $add_srv[] = array($source, $vsys, 'rsh', 'rsh', 'tcp', '514');
        $add_srv[] = array($source, $vsys, 'lpd', 'lpd', 'tcp', '515');
        $add_srv[] = array($source, $vsys, 'uucp', 'uucp', 'tcp', '540');
        $add_srv[] = array($source, $vsys, 'klogin', 'klogin', 'tcp', '543'); //changed eklogin
        $add_srv[] = array($source, $vsys, 'kshell', 'kshell', 'tcp', '544');
        $add_srv[] = array($source, $vsys, 'rtsp', 'rtsp', 'tcp', '554');
        $add_srv[] = array($source, $vsys, 'ldaps', 'ldaps', 'tcp', '636');
        $add_srv[] = array($source, $vsys, 'lotusnotes', 'lotusnotes', 'tcp', '1352');  //changed lotus-notes
        $add_srv[] = array($source, $vsys, 'citrix-ica', 'citrix-ica', 'tcp', '1494');  //changed citrix
        $add_srv[] = array($source, $vsys, 'sqlnet', 'sqlnet', 'tcp', '1521'); //changed oracle
        $add_srv[] = array($source, $vsys, 'h323', 'h323', 'tcp', '1720'); //changed h.323
        $add_srv[] = array($source, $vsys, 'pptp', 'pptp', 'tcp', '1723');
        $add_srv[] = array($source, $vsys, 'ctiqbe', 'ctiqbe', 'tcp', '2748');
        $add_srv[] = array($source, $vsys, 'cifs', 'cifs', 'tcp', '3020');
        $add_srv[] = array($source, $vsys, 'aol', 'aol', 'tcp', '5190');  //changed aim
        $add_srv[] = array($source, $vsys, 'pcanywhere-data', 'pcanywhere-data', 'tcp', '5631');
        $add_srv[] = array($source, $vsys, 'time', 'time', 'udp', '37');
        $add_srv[] = array($source, $vsys, 'nameserver', 'nameserver', 'udp', '42');
        $add_srv[] = array($source, $vsys, 'bootps', 'bootps', 'udp', '67');
        $add_srv[] = array($source, $vsys, 'bootpc', 'bootpc', 'udp', '68');
        $add_srv[] = array($source, $vsys, 'tftp', 'tftp', 'udp', '69');
        $add_srv[] = array($source, $vsys, 'ntp', 'ntp', 'udp', '123');
        $add_srv[] = array($source, $vsys, 'netbios-ns', 'netbios-ns', 'udp', '137');
        $add_srv[] = array($source, $vsys, 'netbios-dgm', 'netbios-dgm', 'udp', '138');  //changed netbios-dg
        $add_srv[] = array($source, $vsys, 'netbios-ss', 'netbios-ss', 'udp', '139');  //changed netbios-ss_udp
        $add_srv[] = array($source, $vsys, 'snmp', 'snmp', 'udp', '161');
        $add_srv[] = array($source, $vsys, 'snmptrap', 'snmptrap', 'udp', '162');  //changed snmp-trap
        $add_srv[] = array($source, $vsys, 'xdmcp', 'xdmcp', 'udp', '177');
        $add_srv[] = array($source, $vsys, 'dnsix', 'dnsix', 'udp', '195');
        $add_srv[] = array($source, $vsys, 'mobile-ip', 'mobile-ip', 'udp', '434');  //changed mobile
        $add_srv[] = array($source, $vsys, 'isakmp', 'isakmp', 'udp', '500');
        $add_srv[] = array($source, $vsys, 'biff', 'biff', 'udp', '512');
        $add_srv[] = array($source, $vsys, 'who', 'who', 'udp', '513');
        $add_srv[] = array($source, $vsys, 'syslog', 'syslog', 'udp', '514');
        $add_srv[] = array($source, $vsys, 'rip', 'rip', 'udp', '520');
        $add_srv[] = array($source, $vsys, 'radius', 'radius', 'udp', '1645');
        $add_srv[] = array($source, $vsys, 'radius-acct', 'radius-acct', 'udp', '1646');
        $add_srv[] = array($source, $vsys, 'secureid-udp', 'secureid-udp', 'udp', '5510');
        $add_srv[] = array($source, $vsys, 'pcanywhere-status', 'pcanywhere-status', 'udp', '5632');

        $add_srv[] = array($source, $vsys, 'snmptrap', 'snmptrap', 'udp', '162');
        //        $add_srv[] = array($source,$vsys,'netbios-ssn','netbios-ssn','tcp','139');


        #$out = implode(",", $add_srv);
        #$projectdb->query("INSERT INTO services (source,vsys,name_ext,name,protocol,dport) VALUES " . $out . ";");

        foreach( $add_srv as $service )
        {
            $tmp_tag = $v->tagStore->findOrCreate("default");

            $tmp_service = $v->serviceStore->find($service[2]);
            if( $tmp_service === null )
            {
                if( $service[4] == "" )
                {
                    $tmp_service = $v->serviceStore->find("tmp-" . $service[2]);
                    if( $tmp_service === null )
                    {
                        if( $print )
                            print " * create service Object: tmp-" . $service[2] . ", tcp, " . $service[5] . "\n";
                        $tmp_service = $v->serviceStore->newService("tmp-" . $service[2], "tcp", $service[5]);
                    }
                    $tmp_service->set_node_attribute('error', "no service protocoll - tcp is used");
                }
                else
                {
                    if( $print )
                        print " * create service Object: " . $service[2] . ", " . $service[4] . ", " . $service[5] . "\n";
                    $tmp_service = $v->serviceStore->newService($service[2], $service[4], $service[5]);
                }
                $tmp_service->tags->addTag($tmp_tag);
            }
            elseif( $tmp_service->protocol() != $service[4] && $tmp_service->getDestPort() != $service[2] )
            {
                print "change service name:" . $tmp_service->name() . "\n";
                $tmp_service->setName($tmp_service->protocol() . "-" . $tmp_service->name());
                print "new service name:" . $tmp_service->name() . "\n";

                $tmp_service2 = $v->serviceStore->find($service[4] . "-" . $service[2]);
                if( $tmp_service2 === null )
                {
                    if( $print )
                        print " * create service Object: " . $service[4] . "-" . $service[2] . ", " . $service[4] . ", " . $service[5] . "\n";
                    $tmp_service2 = $v->serviceStore->newService($service[4] . "-" . $service[2], $service[4], $service[5]);

                }

                if( $print )
                {
                    print " * create addressgroup: " . $service[2] . "\n";
                    print "  * add service: " . $tmp_service->name() . "\n";
                    print "  * add service: " . $tmp_service2->name() . "\n";
                }
                $tmp_servicegroup = $v->serviceStore->newServiceGroup($service[2]);

                $tmp_servicegroup->tags->addTag($tmp_tag);
                $tmp_service->tags->addTag($tmp_tag);
                $tmp_service2->tags->addTag($tmp_tag);

                $tmp_servicegroup->addMember($tmp_service);
                $tmp_servicegroup->addMember($tmp_service2);
            }
        }

        unset($add_srv);
        #}
    }


    #function get_object_service($cisco_config_file, $source, $vsys) {
    function get_object_service($data, $v)
    {
        global $projectdb;
        global $debug;
        global $print;

        $vsys = $v->name();
        $ObjectServiceName = "";
        $ObjectServiceNamePan = "";

        $isObjectService = 0;
        $addService = array();

        $source = "";

        foreach( $data as $line => $names_line )
        {
            $addlog = "";
            $names_line = trim($names_line);
            if( $isObjectService == 1 )
            {

                $found = FALSE;
                $tmp_service = $v->serviceStore->find($ObjectServiceNamePan);


                if( preg_match("/^service protocol/i", $names_line) )
                {
                    $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                    $srv_protocol = $netObj[2];


                    if( $tmp_service !== null )
                    {
                        if( $tmp_service->isService() && $srv_protocol == $tmp_service->protocol() )
                            $found = TRUE;
                    }

                    if( !$found && $tmp_service === null )
                    {
                        #$addService[] = array($ObjectServiceNamePan,$ObjectServiceName,$srv_protocol,'','','','','1',$source,$vsys);
                        $tmp_service = $v->serviceStore->find("tmp-" . $ObjectServiceNamePan);
                        if( $tmp_service === null )
                        {
                            if( $print )
                                print " * create service tmp-" . $ObjectServiceNamePan . "\n";
                            $tmp_service = $v->serviceStore->newService("tmp-" . $ObjectServiceNamePan, "tcp", "6500");
                            $tmp_service->set_node_attribute('error', 'Service Protocol found [' . $ObjectServiceName . '] and Protocol [' . $srv_protocol . '] - Replace it by the right app-id - tcp 6500 is used');
                        }

                    }
                    else
                        mwarning("object: " . $ObjectServiceNamePan . " already available");


                    /*
                    $search = $projectdb->query("SELECT id FROM services WHERE source='$source' AND BINARY name_ext='$ObjectServiceName' AND protocol='$srv_protocol' AND vsys='$vsys';");

                    if ($search->num_rows == 0) {
                        $addService[] = "('$ObjectServiceNamePan','$ObjectServiceName','$srv_protocol','','','','','1','$source','$vsys')";
                        add_log('warning', 'Reading Services Objects and Groups', 'Service Protocol found [' . $ObjectServiceName . '] and Protocol [' . $srv_protocol . ']', $source, 'Replace it by the right app-id');
                    }
                    */
                }
                elseif( preg_match("/^service icmp/i", $names_line) )
                {
                    $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

                    if( $tmp_service !== null )
                    {
                        if( $tmp_service->isService() )
                            $found = TRUE;
                    }

                    if( !$found && $tmp_service === null )
                    {
                        if( isset($netObj[2]) )
                            $icmptype = $netObj[2];
                        else
                            $icmptype = "";

                        #$addService[] = "('$ObjectServiceNamePan','$ObjectServiceName','icmp','','',1,'$icmptype','1','$source','$vsys')";
                        $tmp_service = $v->serviceStore->find("tmp-" . $ObjectServiceNamePan);
                        if( $tmp_service === null )
                        {
                            if( $print )
                                print " * create service tmp-" . $ObjectServiceNamePan . "\n";
                            $tmp_service = $v->serviceStore->newService("tmp-" . $ObjectServiceNamePan, "tcp", "6500");
                            $tmp_service->set_node_attribute('error', 'ICMP Service found [' . $ObjectServiceName . '] with icmptype: "' . $icmptype . '" - Replace it by the ICMP app-id - tcp 6500 is used');
                        }

                    }
                    else
                        mwarning("object: " . $ObjectServiceNamePan . " already available");
                    /*
                    $search = $projectdb->query("SELECT id FROM services WHERE source='$source' AND BINARY name_ext='$ObjectServiceName' AND vsys='$vsys';");
                    if ($search->num_rows == 0) {
                        $icmptype = $netObj[2];
                        $addService[] = "('$ObjectServiceNamePan','$ObjectServiceName','icmp','','',1,'$icmptype','1','$source','$vsys')";
                        add_log('warning', 'Phase 3: Reading Services Objects and Groups', 'ICMP Service found [' . $ObjectServiceName . ']', $source, 'Replace it by the ICMP app-id');
                    }
                    */
                }
                elseif( preg_match("/^service icmp6/i", $names_line) )
                {
                    $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

                    if( $tmp_service !== null )
                    {
                        if( $tmp_service->isService() )
                            $found = TRUE;
                    }

                    if( !$found && $tmp_service === null )
                    {
                        if( isset($netObj[2]) )
                            $icmptype = $netObj[2];
                        else
                            $icmptype = "";
                        #$addService[] = "('$ObjectServiceNamePan','$ObjectServiceName','icmp','','',1,'$icmptype','1','$source','$vsys')";
                        $tmp_service = $v->serviceStore->find("tmp-" . $ObjectServiceNamePan);
                        if( $tmp_service === null )
                        {
                            if( $print )
                                print " * create service tmp-" . $ObjectServiceNamePan . "\n";
                            $tmp_service = $v->serviceStore->newService("tmp-" . $ObjectServiceNamePan, "tcp", "6500");
                            $tmp_service->set_node_attribute('error', 'ICMP6 Service found [' . $ObjectServiceName . '] with icmptype: "' . $icmptype . '" - Replace it by the ICMP app-id - tcp 6500 is used');
                        }

                    }
                    else
                        mwarning("object: " . $ObjectServiceNamePan . " already available");
                    /*
                    $search = $projectdb->query("SELECT id FROM services WHERE source='$source' AND BINARY name_ext='$ObjectServiceName' AND vsys='$vsys';");
                    if ($search->num_rows == 0) {
                        $icmptype = $netObj[2];
                        $addService[] = "('$ObjectServiceNamePan','$ObjectServiceName','icmp','','',1,'$icmptype','1','$source','$vsys')";
                        add_log('warning', 'Phase 3: Reading Services Objects and Groups', 'ICMP6 Service found [' . $ObjectServiceName . ']', $source, 'Replace it by the ICMP app-id');
                    }
                    */
                }

                elseif( preg_match("/^service tcp-udp/i", $names_line) )
                {
                    $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

                    $tmp_servicegroup = $v->serviceStore->find($ObjectServiceNamePan);
                    if( $tmp_servicegroup === null )
                    {
                        if( $print )
                            print " * create servicegroup " . $ObjectServiceNamePan . "\n";
                        $tmp_servicegroup = $v->serviceStore->newServiceGroup($ObjectServiceNamePan);


                        $tmp_service = $v->serviceStore->find($netObj[4]);
                        if( $tmp_service !== null )
                        {
                            if( $print )
                                print "   * add service " . $tmp_service->name() . " to servicgroup " . $ObjectServiceNamePan . "\n";
                            $tmp_servicegroup->addMember($tmp_service);
                        }
                    }
                }
                elseif( (preg_match("/^service tcp/i", $names_line)) or (preg_match("/^service udp/i", $names_line)) )
                {


                    $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                    //print_r($netObj);
                    $next = 2;
                    $sport = "";
                    $dport = "";
                    $srv_protocol = $netObj[1];
                    #Supported to find first destination  than source and vice versa

                    if( isset($netObj[2]) )
                    {
                        $tmp_return = $this->service_get_source_destination($netObj, $next, $v, $ObjectServiceNamePan, $ObjectServiceName, $names_line);

                        $port_final = $tmp_return[0];
                        $addlog = $tmp_return[1];
                        $next = $tmp_return[2];

                        if( $netObj[2] == "source" )
                        {
                            $sport = $port_final;
                        }
                        else
                        {
                            $dport = $port_final;
                        }
                        /*
                        if (($netObj[2] == "source") or ( $netObj[2] == "destination")) {
                            if ($netObj[3] == "eq") {
                                $port = $netObj[4];
                                $next = 5;
                                if (is_numeric($port)) {
                                    $port_final = $port;
                                } else {
                                    #$searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$port' LIMIT 1;");
                                    #$cisconame = $searchname->fetch_assoc();

                                    $port_final = $this->search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port );

                                }
                                if ($port_final == "") {
                                    $addlog = 'Unknown Service [' . $ObjectServiceName . '] port-name mapping for: ' . $port . 'Using 6500 port. Change it from the GUI';
                                    $port_final = "6500";
                                }
                            }
                            elseif ($netObj[3] == "neq") {
                                $port = $netObj[4];
                                $next = 5;
                                if (is_numeric($port)) {
                                    $port_final = $port;
                                    $srv_port_before = intval($port_final) - 1;
                                    $srv_port_after = intval($port_final) + 1;
                                    $port_final = "1-$srv_port_before,$srv_port_after-65535";
                                } else
                                {
                                    #$searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$port' LIMIT 1;");
                                    #$cisconame = $searchname->fetch_assoc();

                                    $port_final = $this->search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port );

                                    $srv_port_before = intval($port_final) - 1;
                                    $srv_port_after = intval($port_final) + 1;
                                    $port_final = "1-$srv_port_before,$srv_port_after-65535";
                                }
                                if ($port_final == "") {
                                    $addlog = 'Unknown Service [' . $ObjectServiceName . '] port-name mapping for: ' . $port . 'Using 6500 port. Change it from the GUI';
                                    $port_final = "6500";
                                }
                            }
                            elseif ($netObj[3] == "lt") {
                                $port = $netObj[4];
                                $next = 5;
                                if (is_numeric($port)) {
                                    $port_final = $port;
                                    $srv_port_before = intval($port_final);
                                    $port_final = "1-$srv_port_before";
                                } else
                                {
                                    #$searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$port' LIMIT 1;");
                                    #$cisconame = $searchname->fetch_assoc();
                                    $port_final = $this->search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port );

                                    $srv_port_before = intval($port_final);
                                    $port_final = "1-$srv_port_before";
                                }
                                if ($port_final == "") {
                                    $addlog = 'Unknown Service [' . $ObjectServiceName . '] port-name mapping for: ' . $port . 'Using 6500 port. Change it from the GUI';
                                    $port_final = "6500";
                                }
                            }
                            elseif ($netObj[3] == "gt") {
                                $port = $netObj[4];
                                $next = 5;
                                if (is_numeric($port)) {
                                    $port_final = $port;
                                    $srv_port_before = intval($port_final);
                                    $port_final = "$srv_port_before-65535";
                                } else {
                                    #$searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$port' LIMIT 1;");
                                    #$cisconame = $searchname->fetch_assoc();
                                    $port_final = $this->search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port );

                                    $srv_port_before = intval($port_final);
                                    $port_final = "$srv_port_before-65535";
                                }
                                if ($port_final == "") {
                                    $addlog = 'Unknown Service [' . $ObjectServiceName . '] port-name mapping for: ' . $port . 'Using 6500 port. Change it from the GUI';
                                    $port_final = "6500";
                                }
                            }
                            elseif ($netObj[3] == "range") {
                                $port_first = $netObj[4];
                                $port_last = rtrim($netObj[5]);
                                $next = 6;
                                $port_first_port = "";
                                if (is_numeric($port_first)) {
                                    $port_first_port = $port_first;
                                } else {
                                    $port_first_port = $this->search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port_first );

                                    # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                                    #$searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$port_first' LIMIT 1;");
                                    #if ($searchname->num_rows==1){
                                    #    $cisconame = $searchname->fetch_assoc();
                                    #    $port_first_port = $cisconame['dport'];

                                    #}
                                    #else{
                                        if ($port_first_port == "") {
                                            $addlog = 'Unknown Service-Range [' . $ObjectServiceName . '] port-name mapping for: ' . $port_first . 'Using 6500 port. Change it from the GUI';
                                            $port_first_port = "6500";
                                        }
                                    #}
                                }
                                if (is_numeric($port_last)) {
                                    $port_last_port = $port_last;
                                } else {
                                    # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                                    #$searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$port_last' LIMIT 1;");
                                    #$cisconame = $searchname->fetch_assoc();
                                    #$port_last_port = $cisconame['dport'];
                                    $port_last_port = $this->search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port_last );

                                    if ($port_last_port == "") {
                                        $addlog = 'Unknown Service-Range [' . $ObjectServiceName . '] port-name mapping for: ' . $port_first . 'Using 6500 port. Change it from the GUI';
                                        $port_last_port = "6500";
                                    }
                                }

                                # Check first if they are EQUAL
                                if ($port_first_port == $port_last_port) {
                                    $LastPort = "";
                                } else {
                                    $LastPort = "-$port_last_port";
                                }

                                $port_final = $port_first_port . $LastPort;
                            }

                            if ($netObj[2] == "source") {
                                $sport = $port_final;
                            } else {
                                $dport = $port_final;
                            }
                        }
                        */
                    }
                    else
                    {
                        // In case:
                        // object service MPI-MS-SQL-Monitor_Reply
                        // service udp
                        $dport = '1-65535';
                    }

                    if( isset($netObj[$next]) )
                    {
                        $tmp_return = $this->service_get_source_destination($netObj, $next, $v, $ObjectServiceNamePan, $ObjectServiceName, $names_line);

                        $port_final = $tmp_return[0];
                        $addlog = $tmp_return[1];
                        $next = $next;

                        if( $netObj[$next] == "source" )
                        {
                            $sport = $port_final;
                        }
                        else
                        {
                            $dport = $port_final;
                        }
                        /* check function
                        if (($netObj[$next] == "source") or ( $netObj[$next] == "destination")) {
                            if ($netObj[$next + 1] == "eq") {
                                $port = $netObj[$next + 2];
                                if (is_numeric($port)) {
                                    $port_final = $port;
                                } else {
                                    #$searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$port' LIMIT 1;");
                                    #$cisconame = $searchname->fetch_assoc();
                                    #$port_final = $cisconame['dport'];
                                    $port_final = $this->search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port );
                                }
                                if ($port_final == "") {
                                    $addlog =  'Unknown Service [' . $ObjectServiceName . '] port-name mapping for: ' . $port . 'Using 6500 port. Change it from the GUI';
                                    $port_final = "6500";
                                }
                            }
                            elseif ($netObj[$next + 1] == "neq") {
                                $port = $netObj[$next + 2];
                                if (is_numeric($port)) {
                                    $port_final = $port;
                                    $srv_port_before = intval($port_final) - 1;
                                    $srv_port_after = intval($port_final) + 1;
                                    $port_final = "1-$srv_port_before,$srv_port_after-65535";
                                } else {
                                    #$searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$port' LIMIT 1;");
                                    #$cisconame = $searchname->fetch_assoc();
                                    #$port_final = $cisconame['dport'];
                                    $port_final = $this->search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port );

                                    $srv_port_before = intval($port_final) - 1;
                                    $srv_port_after = intval($port_final) + 1;
                                    $port_final = "1-$srv_port_before,$srv_port_after-65535";
                                }
                                if ($port_final == "") {
                                    $addlog =  'Unknown Service [' . $ObjectServiceName . '] port-name mapping for: ' . $port . 'Using 6500 port. Change it from the GUI';
                                    $port_final = "6500";
                                }
                            }
                            elseif ($netObj[$next + 1] == "lt") {
                                $port = $netObj[$next + 2];
                                if (is_numeric($port)) {
                                    $port_final = $port;
                                    $srv_port_before = intval($port_final);
                                    $port_final = "1-$srv_port_before";
                                } else {
                                    #$searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$port' LIMIT 1;");
                                    #$cisconame = $searchname->fetch_assoc();
                                    #$port_final = $cisconame['dport'];
                                    $port_final = $this->search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port );

                                    $srv_port_before = intval($port_final);
                                    $port_final = "1-$srv_port_before";
                                }
                                if ($port_final == "") {
                                    $addlog = 'Unknown Service [' . $ObjectServiceName . '] port-name mapping for: ' . $port . 'Using 6500 port. Change it from the GUI';
                                    $port_final = "6500";
                                }
                            }
                            elseif ($netObj[$next + 1] == "gt") {
                                $port = $netObj[$next + 2];
                                if (is_numeric($port)) {
                                    $port_final = $port;
                                    $srv_port_before = intval($port_final);
                                    $port_final = "$srv_port_before-65535";
                                } else {
                                    #$searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$port' LIMIT 1;");
                                    #$cisconame = $searchname->fetch_assoc();
                                    #$port_final = $cisconame['dport'];
                                    $port_final = $this->search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port );

                                    $srv_port_before = intval($port_final);
                                    $port_final = "$srv_port_before-65535";
                                }
                                if ($port_final == "") {
                                    $addlog =  'Unknown Service [' . $ObjectServiceName . '] port-name mapping for: ' . $port . 'Using 6500 port. Change it from the GUI';
                                    $port_final = "6500";
                                }
                            }
                            elseif ($netObj[$next + 1] == "range") {
                                $port_first = $netObj[$next + 2];
                                $port_last = rtrim($netObj[$next + 3]);
                                if (is_numeric($port_first)) {
                                    $port_first_port = $port_first;
                                } else {
                                    # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                                    #$searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$port_first' LIMIT 1;");
                                    #$cisconame = $searchname->fetch_assoc();
                                    #$port_first_port = $cisconame['dport'];
                                    $port_first_port = $this->search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port_first );

                                    if ($port_first_port == "") {
                                        $addlog = 'Unknown Service-Range [' . $ObjectServiceName . '] port-name mapping for: ' . $port_first . 'Using 6500 port. Change it from the GUI';
                                        $port_first_port = "6500";
                                    }
                                }
                                if (is_numeric($port_last)) {
                                    $port_last_port = $port_last;
                                } else {
                                    # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                                    #$searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$port_last' LIMIT 1;");
                                    #$cisconame = $searchname->fetch_assoc();
                                    #$port_last_port = $cisconame['dport'];
                                    $port_last_port = $this->search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port_last );

                                    if ($port_last_port == "") {
                                        $addlog = 'Unknown Service-Range [' . $ObjectServiceName . '] port-name mapping for: ' . $port_first. 'Using 6500 port. Change it from the GUI';
                                        $port_last_port = "6500";
                                    }
                                }

                                # Check first if they are EQUAL
                                if ($port_first_port == $port_last_port) {
                                    $LastPort = "";
                                } else {
                                    $LastPort = "-$port_last_port";
                                }

                                $port_final = $port_first_port . $LastPort;
                            }

                            if ($netObj[$next] == "source") {
                                $sport = $port_final;
                            } else {
                                $dport = $port_final;
                            }
                        }
                        */
                    }

                    if( $dport == "" )
                    {
                        $addlog = "service with no destination port found: " . $names_line;
                        #mwarning( $addlog, null, false );
                        //if dst port not found bring in a tmp value and report it
                        $dport = '65535';
                    }

                    /** @var Service $tmp_service */
                    if( $sport == "" )
                        $newName = $srv_protocol . "-" . $dport;
                    else
                        $newName = $srv_protocol . "-" . $sport . "-" . $dport;

                    $newName = $ObjectServiceNamePan;

                    $tmp_service = $v->serviceStore->find($newName);
                    if( $tmp_service === null )
                    {
                        if( $print )
                            print " * create service " . $newName . "|-proto:" . $srv_protocol . "|-dport:" . $dport . "|-sport:" . $sport . "|\n";

                        $tmp_service = $v->serviceStore->newService($newName, $srv_protocol, $dport, '', $sport);
                        if( $addlog != "" )
                            $tmp_service->set_node_attribute('error', $addlog);
                    }
                    /*
                    $search = $projectdb->query("SELECT id FROM services WHERE source='$source' AND BINARY name_ext='$ObjectServiceName' AND vsys='$vsys';");
                    //echo "FINAL: SELECT id FROM services WHERE source='$source' AND BINARY name_ext='$ObjectServiceName' AND vsys='$vsys';\n";
                    if ($search->num_rows == 0) {
                        $addService[] = "('$ObjectServiceNamePan','$ObjectServiceName','$srv_protocol','$sport','$dport','0','',0,'$source','$vsys')";
                    }
                    */
                }
                elseif( preg_match("/^description/i", $names_line) )
                {

                }
            }


            if( preg_match("/^object service/i", $names_line) )
            {
                $isObjectService = 1;
                $names = explode(" ", $names_line);
                $ObjectServiceName = rtrim($names[2]);
                $ObjectServiceNamePan = $this->truncate_names($this->normalizeNames($ObjectServiceName));
            }

        }

        /*
        if (count($addService) > 0) {
            $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport,dport,icmp,type,checkit,source,vsys) values" . implode(",", $addService) . ";");

            unset($addService);
        }
        */
    }

    function service_get_source_destination($netObj, $next, $v, $ObjectServiceNamePan, $ObjectServiceName, $names_line)
    {
        if( ($netObj[$next] == "source") or ($netObj[$next] == "destination") )
        {
            $addlog = "";
            if( $netObj[$next + 1] == "eq" )
            {
                $port = $netObj[$next + 2];
                $next = $next + 3;//5
                if( is_numeric($port) )
                {
                    $port_final = $port;
                }
                else
                {
                    #$searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$port' LIMIT 1;");
                    #$cisconame = $searchname->fetch_assoc();
                    #$port_final = $cisconame['dport'];
                    $port_final = $this->search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port);
                }
                if( $port_final == "" )
                {
                    $addlog = 'Unknown Service [' . $ObjectServiceName . '] port-name mapping for: ' . $port . 'Using 6500 port. Change it from the GUI';
                    $port_final = "6500";
                }
            }
            elseif( $netObj[$next + 1] == "neq" )
            {
                $port = $netObj[$next + 2];
                $next = $next + 3;//5
                if( is_numeric($port) )
                {
                    $port_final = $port;
                    $srv_port_before = intval($port_final) - 1;
                    $srv_port_after = intval($port_final) + 1;
                    $port_final = "1-$srv_port_before,$srv_port_after-65535";
                }
                else
                {
                    #$searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$port' LIMIT 1;");
                    #$cisconame = $searchname->fetch_assoc();
                    #$port_final = $cisconame['dport'];
                    $port_final = $this->search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port);

                    $srv_port_before = intval($port_final) - 1;
                    $srv_port_after = intval($port_final) + 1;
                    $port_final = "1-$srv_port_before,$srv_port_after-65535";
                }
                if( $port_final == "" )
                {
                    $addlog = 'Unknown Service [' . $ObjectServiceName . '] port-name mapping for: ' . $port . 'Using 6500 port. Change it from the GUI';
                    $port_final = "6500";
                }
            }
            elseif( $netObj[$next + 1] == "lt" )
            {
                $port = $netObj[$next + 2];
                $next = $next + 3;//5
                if( is_numeric($port) )
                {
                    $port_final = $port;
                    $srv_port_before = intval($port_final);
                    $port_final = "1-$srv_port_before";
                }
                else
                {
                    #$searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$port' LIMIT 1;");
                    #$cisconame = $searchname->fetch_assoc();
                    #$port_final = $cisconame['dport'];
                    $port_final = $this->search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port);

                    $srv_port_before = intval($port_final);
                    $port_final = "1-$srv_port_before";
                }
                if( $port_final == "" )
                {
                    $addlog = 'Unknown Service [' . $ObjectServiceName . '] port-name mapping for: ' . $port . 'Using 6500 port. Change it from the GUI';
                    $port_final = "6500";
                }
            }
            elseif( $netObj[$next + 1] == "gt" )
            {
                $port = $netObj[$next + 2];
                $next = $next + 3;//5
                if( is_numeric($port) )
                {
                    $port_final = $port;
                    $srv_port_before = intval($port_final);
                    $port_final = "$srv_port_before-65535";
                }
                else
                {
                    #$searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$port' LIMIT 1;");
                    #$cisconame = $searchname->fetch_assoc();
                    #$port_final = $cisconame['dport'];
                    $port_final = $this->search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port);

                    $srv_port_before = intval($port_final);
                    $port_final = "$srv_port_before-65535";
                }
                if( $port_final == "" )
                {
                    $addlog = 'Unknown Service [' . $ObjectServiceName . '] port-name mapping for: ' . $port . 'Using 6500 port. Change it from the GUI';
                    $port_final = "6500";
                }
            }
            elseif( $netObj[$next + 1] == "range" )
            {
                $port_first = $netObj[$next + 2];
                $port_last = rtrim($netObj[$next + 3]);
                $next = $next + 4;//6

                /*
                if (is_numeric($port_first)) {
                    $port_first_port = $port_first;
                } else {
                    # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                    #$searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$port_first' LIMIT 1;");
                    #$cisconame = $searchname->fetch_assoc();
                    #$port_first_port = $cisconame['dport'];
                    $port_first_port = $this->search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port_first );

                    if ($port_first_port == "") {
                        $addlog = 'Unknown Service-Range [' . $ObjectServiceName . '] port-name mapping for: ' . $port_first . 'Using 6500 port. Change it from the GUI';
                        $port_first_port = "6500";
                    }
                }
                if (is_numeric($port_last)) {
                    $port_last_port = $port_last;
                } else {
                    # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                    #$searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$port_last' LIMIT 1;");
                    #$cisconame = $searchname->fetch_assoc();
                    #$port_last_port = $cisconame['dport'];
                    $port_last_port = $this->search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port_last );

                    if ($port_last_port == "") {
                        $addlog = 'Unknown Service-Range [' . $ObjectServiceName . '] port-name mapping for: ' . $port_first. 'Using 6500 port. Change it from the GUI';
                        $port_last_port = "6500";
                    }
                }
                */

                $tmp_range_port = $this->range_get_ports($port_first, $port_last, $v, $ObjectServiceName, $ObjectServiceNamePan, $names_line);
                $port_first_port = $tmp_range_port[0];
                $port_last_port = $tmp_range_port[1];

                # Check first if they are EQUAL
                if( $port_first_port == $port_last_port )
                {
                    $LastPort = "";
                }
                else
                {
                    $LastPort = "-$port_last_port";
                }

                $port_final = $port_first_port . $LastPort;
            }

        }

        return array($port_final, $addlog, $next);
    }

    #function get_objectgroup_service($cisco_config_file, $source, $vsys) {
    function get_objectgroup_service($data, $v)
    {
        global $projectdb;
        global $debug;
        global $print;

        $isServiceGroup = 0;
        $addMember = array();
        $addMemberID = array();


        foreach( $data as $line => $names_line )
        {
            $addlog = "";
            $names_line = trim($names_line);
            if( preg_match("/^object-group service/i", $names_line) )
            {
                $isServiceGroup = 1;
                $names = explode(" ", $names_line);
                $HostGroupName = rtrim($names[2]);
                $HostGroupNamePan = $this->truncate_names($this->normalizeNames($HostGroupName));
                if( isset($names[3]) )
                {
                    $Protocol = rtrim($names[3]);
                }
                else
                {
                    $Protocol = "";
                }

                /** @var ServiceGroup $tmp_servicegroup */
                $tmp_servicegroup = $v->serviceStore->find($HostGroupNamePan);
                if( $tmp_servicegroup === null )
                {
                    if( $print )
                        print "     * create servicegroup: " . $HostGroupNamePan . "\n";
                    $tmp_servicegroup = $v->serviceStore->newServiceGroup($HostGroupNamePan);
                }
                else
                {
                    mwarning("servicegroup: " . $HostGroupNamePan . " already available\n");
                    if( $tmp_servicegroup->isService() )
                    {
                        $addlog = "an servicegroup same name; member:" . $names_line;
                        $tmp_servicegroup->set_node_attribute('error', $addlog);


                        if( $print )
                            print "\n * create servicegroup: tmp_" . $HostGroupNamePan . "\n";
                        $tmp_servicegroup = $v->serviceStore->newServiceGroup("tmp_" . $HostGroupNamePan);
                        $tmp_servicegroup->set_node_attribute('error', "service object with same name available");
                    }
                }
                /*
                $getDup = $projectdb->query("SELECT id FROM services_groups_id WHERE source='$source' AND BINARY name_ext='$HostGroupName' AND vsys='$vsys'");
                if ($getDup->num_rows == 0) {
                    $projectdb->query("INSERT INTO services_groups_id (name,name_ext,source,vsys) values ('$HostGroupNamePan','$HostGroupName','$source','$vsys');");
                    $lidgroup = $projectdb->insert_id;
                }
                else{
                    $getDupData=$getDup->fetch_assoc();
                    $lidgroup = $getDupData['id'];
                }
                */

                continue;
            }

            if( (!preg_match("/port-object /", $names_line)) and
                (!preg_match("/description /", $names_line)) and
                (!preg_match("/group-object /", $names_line)) and
                (!preg_match("/service-object /", $names_line)) )
            {
                $isServiceGroup = 0;
            }

            if( $isServiceGroup == 1 )
            {
                if( preg_match("/port-object /", $names_line) )
                {
                    $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                    $operator = $netObj[1];
                    if( $operator == "eq" )
                    {
                        $port = rtrim($netObj[2]);
                        if( is_numeric($port) )
                        {
                            $port_final = $port;
                        }
                        else
                        {
                            # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                            /*
                            $searchname = $projectdb->query("SELECT dport FROM services WHERE vsys='$vsys' AND source='$source' AND BINARY name_ext='$port' LIMIT 1;");
                            $cisconame = $searchname->fetch_assoc();
                            $port_final = $cisconame['dport'];
                            */
                            $port_final = $this->search_for_service_port($v, $HostGroupNamePan, $names_line, $port);

                            if( $port_final == "" )
                            {
                                $addlog = 'Unknown Service port mapping for: ' . $port . ' in ServiceGroup [' . $HostGroupNamePan . '] with Protocol [' . $Protocol . '] - NOT Adding to the DB!!';
                                #$portpan=$this->truncate_names($this->normalizeNames($port));
                                # Im not sure if is useful
                                #$projectdb->query("INSERT INTO services (name,type,name_ext,checkit,project,used) values('$portpan','$newlid','','$port','1','$projectname','0');");
                                #$projectdb->query("INSERT INTO services_groups (lid,member,project) values ('$lidgroup','$port','$projectname');");
                                $port_final = "mt-error";
                            }
                        }

                        if( $port_final == "mt-error" )
                        {

                        }
                        else
                        {
                            if( $Protocol == "tcp-udp" )
                            {
                                # TCP AND UDP
                                /** @var Service $tmp_service */
                                $tmp_service = $v->serviceStore->find('tcp-' . $port_final);
                                if( $tmp_service === null )
                                {
                                    if( $print )
                                        print "   * create service tcp-" . $port_final . "\n";
                                    $tmp_service = $v->serviceStore->newService('tcp-' . $port_final, 'tcp', $port_final);
                                }
                                if( $print )
                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                $tmp_servicegroup->addMember($tmp_service);

                                $tmp_service = $v->serviceStore->find('udp-' . $port_final);
                                if( $tmp_service === null )
                                {
                                    if( $print )
                                        print "   * create service udp-" . $port_final . "\n";
                                    $tmp_service = $v->serviceStore->newService('udp-' . $port_final, 'udp', $port_final);

                                }
                                if( $print )
                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                $tmp_servicegroup->addMember($tmp_service);

                                /*
                                $search = $projectdb->query("SELECT id FROM services WHERE source='$source' AND BINARY name_ext='tcp-$port_final' AND vsys='$vsys';");
                                if ($search->num_rows == 0) {
                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('tcp-$port_final','tcp-$port_final','tcp','$port_final','0','$source','$vsys');");
                                    $serviceID = $projectdb->insert_id;
                                }
                                else{
                                    $data = $search->fetch_assoc();
                                    $serviceID = $data['id'];
                                }
                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                $search = $projectdb->query("SELECT id FROM services WHERE source='$source' AND BINARY name_ext='udp-$port_final' AND vsys='$vsys';");
                                if ($search->num_rows == 0) {
                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('udp-$port_final','udp-$port_final','udp','$port_final','0','$source','$vsys');");
                                    $serviceID = $projectdb->insert_id;
                                }
                                else{
                                    $data = $search->fetch_assoc();
                                    $serviceID = $data['id'];
                                }
                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                */
                            }
                            else
                            {
                                # TCP OR UDP
                                /** @var Service $tmp_service */
                                $tmp_service = $v->serviceStore->find($Protocol . "-" . $port_final);
                                if( $tmp_service === null )
                                {
                                    if( $print )
                                        print "   * create service " . $Protocol . "-" . $port_final . "\n";
                                    $tmp_service = $v->serviceStore->newService($Protocol . "-" . $port_final, $Protocol, $port_final);

                                }
                                if( $print )
                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                $tmp_servicegroup->addMember($tmp_service);
                                /*
                                $search = $projectdb->query("SELECT id FROM services WHERE source='$source' AND BINARY name_ext='$Protocol-$port_final' AND vsys='$vsys';");
                                if ($search->num_rows == 0) {
                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$Protocol-$port_final','$Protocol-$port_final','$Protocol','$port_final','0','$source','$vsys');");
                                    $serviceID = $projectdb->insert_id;
                                }
                                else{
                                    $data = $search->fetch_assoc();
                                    $serviceID = $data['id'];
                                }
                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                */
                            }
                        }
                    }
                    elseif( $operator == "range" )
                    {

                        $port_first = $netObj[2];
                        $port_last = rtrim($netObj[3]);

                        /*
                        if (is_numeric($port_first)) {
                            $port_first_port = $port_first;
                        } else {
                            # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                            $port_first_port = $this->search_for_service_port($v, $HostGroupNamePan, $names_line, $port_first );

                            if ($port_first_port == "") {
                                $addlog =  'Unknown Service Range port-name mapping for: ' . $port_first. 'Using 6500 port. Change it from the GUI';
                                $port_first_port = "6500";
                            }
                        }
                        if (is_numeric($port_last)) {
                            $port_last_port = $port_last;
                        } else {
                            # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                            $port_last_port = $this->search_for_service_port($v, $HostGroupNamePan, $names_line, $port_last );

                            if ($port_last_port == "") {
                                $addlog =  'Unknown Service-Range port-name mapping for: ' . $port_last. 'Using 6500 port. Change it from the GUI';
                                $port_last_port = "6500";
                            }
                        }
                        */

                        $tmp_range_port = $this->range_get_ports($port_first, $port_last, $v, $HostGroupName, $HostGroupNamePan, $names_line);
                        $port_first_port = $tmp_range_port[0];
                        $port_last_port = $tmp_range_port[1];

                        # Check first if they are EQUAL
                        if( $port_first_port == $port_last_port )
                        {
                            $isRange = "";
                            $LastPort = "";
                            $vtype = "";
                            $addlog_warning = 'Moving Service-Range to Service [' . $names_line . '] ports are the same - No Action Required';
                        }
                        else
                        {
                            $isRange = "-range";
                            $isRange = "";
                            $LastPort = "-$port_last_port";
                            $vtype = "range";
                        }

                        $port_final = $port_first_port . $LastPort;
                        $name = $port_final;
                        if( $Protocol == "tcp-udp" )
                        {

                            $tmp_service = $v->serviceStore->find('tcp-' . $name);
                            if( $tmp_service === null )
                            {
                                if( $print )
                                    print "   * create service tcp-" . $port_final . "\n";
                                $tmp_service = $v->serviceStore->newService('tcp-' . $name, 'tcp', $port_final);
                            }
                            if( $print )
                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                            $tmp_servicegroup->addMember($tmp_service);

                            $tmp_service = $v->serviceStore->find('udp-' . $name);
                            if( $tmp_service === null )
                            {
                                if( $print )
                                    print "   * create service udp-" . $name . "\n";
                                $tmp_service = $v->serviceStore->newService('udp-' . $name, 'udp', $port_final);

                            }
                            if( $print )
                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                            $tmp_servicegroup->addMember($tmp_service);

                            /*$search = $projectdb->query("SELECT id FROM services WHERE source='$source' AND name_ext='tcp" . $isRange . "-$port_first_port" . $LastPort . "' AND vsys='$vsys';");
                            if ($search->num_rows == 0) {
                                $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,type,vsys) values ('tcp" . $isRange . "-$port_first_port" . $LastPort . "','tcp" . $isRange . "-$port_first_port" . $LastPort . "','tcp','$port_first_port" . $LastPort . "','0','$source','$vtype','$vsys');");
                                $serviceID = $projectdb->insert_id;
                            }
                            else{
                                $data = $search->fetch_assoc();
                                $serviceID = $data['id'];
                            }
                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";

                            $search = $projectdb->query("SELECT id FROM services WHERE source='$source' AND name_ext='udp" . $isRange . "-$port_first_port" . $LastPort . "' AND vsys='$vsys';");
                            if ($search->num_rows == 0) {
                                $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,type,vsys) values ('udp" . $isRange . "-$port_first_port" . $LastPort . "','udp" . $isRange . "-$port_first_port" . $LastPort . "','udp','$port_first_port" . $LastPort . "','0','$source','$vtype','$vsys');");
                                $serviceID = $projectdb->insert_id;
                            }
                            else{
                                $data = $search->fetch_assoc();
                                $serviceID = $data['id'];
                            }
                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                            */
                        }
                        else
                        {
                            $tmp_service = $v->serviceStore->find($Protocol . "-" . $name);
                            if( $tmp_service === null )
                            {
                                if( $print )
                                    print "   * create service " . $Protocol . "-" . $name . "\n";
                                $tmp_service = $v->serviceStore->newService($Protocol . "-" . $name, $Protocol, $port_final);
                            }
                            if( $print )
                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                            $tmp_servicegroup->addMember($tmp_service);
                            /*
                            $search = $projectdb->query("SELECT id FROM services WHERE source='$source' AND name_ext='$Protocol" . $isRange . "-$port_first_port" . $LastPort . "' ANd vsys='$vsys';");
                            if ($search->num_rows == 0) {
                                $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,type,vsys) values ('$Protocol" . $isRange . "-$port_first_port" . $LastPort . "','$Protocol" . $isRange . "-$port_first_port" . $LastPort . "','$Protocol','$port_first_port" . $LastPort . "','0','$source','$vtype','$vsys');");
                                $serviceID = $projectdb->insert_id;
                            }
                            else{
                                $data = $search->fetch_assoc();
                                $serviceID = $data['id'];
                            }
                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                            */
                        }
                    }
                }
                elseif( preg_match("/group-object /i", $names_line) )
                {

                    $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                    $obj2 = rtrim($netObj[1]);
                    #$addMember[] = "('$lidgroup','$obj2','$source','$vsys')";

                    $tmp_servicegroup2 = $v->serviceStore->find($obj2);
                    if( $tmp_servicegroup2 !== null )
                    {
                        if( $print )
                            print "   * add servicegroup: " . $obj2 . " to servicegroup: " . $tmp_servicegroup->name() . "\n";
                        $tmp_servicegroup->addMember($tmp_servicegroup2);
                    }
                    /* $getServiceObject=$projectdb->query("SELECT application,service_lid,service_table_name FROM cisco_service_objects WHERE source='$source' AND name='$obj2';");
              if ($getServiceObject->num_rows==0){
              $addMember[]="('$lidgroup','$obj2','$source','$vsys')";
              }
              else {
              #Is Service Object Add the content of the group in this service object
              while ($data=mysql_fetch_assoc($getServiceObject)){
              $app=$data['application'];
              $service_lid=$data['service_lid'];
              $service_table_name=$data['service_table_name'];
              //$getDup=$projectdb->query("SELECT * FROM cisco_service_objects WHERE source='$source' AND name='$HostGroupName' AND application='$app' AND service_lid='$service_lid' AND service_table_name='$service_table_name';");
              //if ($getDup->num_rows==0){
              $projectdb->query("INSERT INTO cisco_service_objects (name,project,service_lid,service_table_name,application) VALUES ('$HostGroupName','$projectname','$service_lid','$service_table_name','$app')");
              //}
              }

              }
             */
                }
                elseif( preg_match("/service-object /i", $names_line) )
                {

                    $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                    $Protocol = $netObj[1];
                    if( ($Protocol == "tcp") or ($Protocol == "udp") )
                    {
                        if( !isset($netObj[2]) )
                        {
                            $tmp_service = $v->serviceStore->find($Protocol . "-All");
                            if( $tmp_service === null )
                            {
                                if( $print )
                                    print "  * create service: " . $Protocol . "-All\n";
                                $tmp_service = $v->serviceStore->newService($Protocol . "-All", $Protocol, "1-65535");
                            }
                            if( $print )
                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                            $tmp_servicegroup->addMember($tmp_service);
                            /*
                            $getDup = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND dport='1-65535' AND protocol='$Protocol' AND vsys='$vsys' LIMIT 1;");
                            if ($getDup->num_rows == 0) {
                                $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$Protocol-All','$Protocol-All','$Protocol','1-65535','0','$source','$vsys');");
                                $serviceID = $projectdb->insert_id;
                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                            }
                            else {
                                $data = $getDup->fetch_assoc();
                                $existingname = $data['name'];
                                $serviceID = $data['id'];
                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                            }
                            */
                        }
                        else
                        {
                            $port = $netObj[2];
                            $next = 3;

                            # New addition to cover
                            #

                            if( $port == "eq" )
                            {
                                $port = $netObj[$next]; // 3
                                if( is_numeric($port) )
                                {
                                    $port_final = $port;

                                    $tmp_service = $v->serviceStore->find($Protocol . "-" . $port_final);
                                    if( $tmp_service === null )
                                    {
                                        if( $print )
                                            print "  * create service: " . $Protocol . "-" . $port_final . "\n";
                                        $tmp_service = $v->serviceStore->newService($Protocol . "-" . $port_final, $Protocol, $port_final);
                                    }
                                    if( $print )
                                        print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                    $tmp_servicegroup->addMember($tmp_service);
                                    /*
                                    $getService = $projectdb->query("SELECT name,id  FROM services WHERE source='$source' AND protocol='$Protocol' AND dport='$port_final' AND vsys='$vsys';");
                                    if ($getService->num_rows == 0) {
                                        # Create it
                                        $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$Protocol-$port_final','$Protocol-$port_final','$Protocol','$port_final','0','$source','$vsys');");
                                        $serviceID = $projectdb->insert_id;
                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                    } else {
                                        $data = $getService->fetch_assoc();
                                        $existingname = $data['name'];
                                        $serviceID = $data['id'];
                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                    }
                                    */
                                }
                                else
                                {
                                    $tmp_service = $v->serviceStore->find($port);
                                    if( $tmp_service === null )
                                    {
                                        if( $print )
                                            print "  * create service: " . $port . "\n";
                                        $tmp_service = $v->serviceStore->newService($port, $Protocol, "6500");
                                        $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port . 'Using 6500 port. Change it from the GUI';
                                        $tmp_service->set_node_attribute('error', $addlog);
                                    }
                                    if( $print )
                                        print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                    $tmp_servicegroup->addMember($tmp_service);
                                    /*
                                    $searchname = $projectdb->query("SELECT id FROM services WHERE source='$source' AND BINARY name_ext='$port' AND protocol='$Protocol' AND vsys='$vsys';");
                                    if ($searchname->num_rows == 0) {
                                        add_log('error', 'Reading Services Objects and Groups', 'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port, $source, 'Using 6500 port. Change it from the GUI');
                                        $port_final = "6500";
                                        $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$port','$port','$Protocol','$port_final','0','$source','$vsys');");
                                        $serviceID = $projectdb->insert_id;
                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                    }
                                    else{
                                        $data = $searchname->fetch_assoc();
                                        $serviceID = $data['id'];
                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                    }
                                    */
                                }
                            }
                            elseif( $port == "gt" )
                            {
                                $port = $netObj[$next];
                                if( is_numeric($port) )
                                {
                                    $port = $port + 1;
                                    $port_final = $port . "-65535";

                                    $tmp_service = $v->serviceStore->find($Protocol . "-" . $port_final);
                                    if( $tmp_service === null )
                                    {
                                        if( $print )
                                            print "  * create service: " . $Protocol . "-" . $port_final . "\n";
                                        $tmp_service = $v->serviceStore->newService($Protocol . "-" . $port_final, $Protocol, $port_final);
                                    }
                                    if( $print )
                                        print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                    $tmp_servicegroup->addMember($tmp_service);
                                    /*
                                    $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND protocol='$Protocol' AND dport='$port_final' AND vsys='$vsys';");
                                    if ($getService->num_rows == 0) {
                                        # Create it
                                        $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$Protocol-$port_final','$Protocol-$port_final','$Protocol','$port_final','0','$source','$vsys');");
                                        $serviceID = $projectdb->insert_id;
                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                    }
                                    else {
                                        $data = $getService->fetch_assoc();
                                        $existingname = $data['name'];
                                        $serviceID = $data['id'];
                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                    }*/
                                }
                                else
                                {
                                    $tmp_service = $v->serviceStore->find($port);
                                    if( $tmp_service === null )
                                    {
                                        $port_final = "6500";
                                        $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for GT : ' . $port . ' Using 6500 port. Change it from the GUI';

                                        if( $print )
                                            print "  * create service: " . $port . "\n";
                                        $tmp_service = $v->serviceStore->newService($port, $Protocol, $port_final);
                                        $tmp_service->set_node_attribute('error', $addlog);
                                        if( $print )
                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                        $tmp_servicegroup->addMember($tmp_service);
                                    }
                                    /*
                                    $searchname = $projectdb->query("SELECT id,dport,protocol FROM services WHERE source='$source' AND BINARY name_ext='$port' AND vsys='$vsys';");
                                    if ($searchname->num_rows == 0) {
                                        add_log('error', 'Reading Services Objects and Groups', 'Unknown Service [' . $HostGroupName . '] port-name mapping for GT : ' . $port, $source, 'Using 6500 port. Change it from the GUI');
                                        $port_final = "6500";
                                        $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$port','$port','$Protocol','$port_final','0','$source','$vsys');");
                                        $serviceID = $projectdb->insert_id;
                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                    }
                                    */
                                    else
                                    {
                                        #$searchnameData = $searchname->fetch_assoc();
                                        #$temp_dport = $searchnameData['dport'] + 1;
                                        #$temp_protocol = $searchnameData['protocol'];

                                        $temp_dport = $tmp_service->getDestPort() + 1;
                                        $temp_protocol = $tmp_service->protocol();
                                        $port_final = $temp_dport . "-65535";

                                        $tmp_name = $temp_protocol . "-" . $port_final;

                                        $tmp_service = $v->serviceStore->find($tmp_name);
                                        if( $tmp_service === null )
                                        {
                                            if( $print )
                                                print "  * create service: " . $tmp_name . "\n";
                                            $tmp_service = $v->serviceStore->newService($tmp_name, $Protocol, $port_final);
                                            if( $print )
                                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                            $tmp_servicegroup->addMember($tmp_service);
                                        }
                                        /*
                                        $check = $projectdb->query("SELECT name_ext, id FROM services WHERE source='$source' AND vsys='$vsys' AND dport='$port_final' AND protocol='$temp_protocol' LIMIT 1;");
                                        if ($check->num_rows == 1) {
                                            $checkData = $check->fetch_assoc();
                                            $tmp_name = $checkData['name_ext'];
                                            $serviceID = $checkData['id'];
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        }
                                        else {
                                            $tmp_name = $temp_protocol . "-" . $port_final;
                                            $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$tmp_name','$tmp_name','$temp_protocol','$port_final','0','$source','$vsys');");
                                            $serviceID = $projectdb->insert_id;
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        }
                                        */
                                        // Todo: hay que leer el proto i el dport sumarle uno y poner 65535 y crearlo y añadir como member

                                        //                                        $addMember[] = "('$lidgroup','$port','$source','$vsys')";
                                    }
                                }
                            }
                            elseif( $port == "lt" )
                            {
                                $port = $netObj[$next];
                                if( is_numeric($port) )
                                {
                                    $port = $port - 1;
                                    $port_final = "0-" . $port;

                                    $tmp_service = $v->serviceStore->find($Protocol . "-" . $port_final);
                                    if( $tmp_service === null )
                                    {
                                        if( $print )
                                            print "  * create service: " . $Protocol . "-" . $port_final . "\n";
                                        $tmp_service = $v->serviceStore->newService($Protocol . "-" . $port_final, $Protocol, $port_final);
                                        if( $print )
                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                        $tmp_servicegroup->addMember($tmp_service);
                                    }

                                    /*
                                    $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND protocol='$Protocol' AND dport='$port_final' AND vsys='$vsys';");
                                    if ($getService->num_rows == 0) {
                                        # Create it
                                        $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$Protocol-$port_final','$Protocol-$port_final','$Protocol','$port_final','0','$source','$vsys');");
                                        $serviceID = $projectdb->insert_id;
                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                    }
                                    else {
                                        $data = $getService->fetch_assoc();
                                        $existingname = $data['name'];
                                        $serviceID = $data['id'];
                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                    }
                                    */
                                }
                                else
                                {
                                    $tmp_service = $v->serviceStore->find($port);
                                    if( $tmp_service === null )
                                    {
                                        $port_final = "6500";
                                        $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for LT : ' . $port . 'Using 6500 port. Change it from the GUI';

                                        if( $print )
                                            print "  * create service: " . $port . "\n";
                                        $tmp_service = $v->serviceStore->newService($port, $Protocol, $port_final);
                                        $tmp_service->set_node_attribute('error', $addlog);
                                        if( $print )
                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                        $tmp_servicegroup->addMember($tmp_service);
                                    }
                                    /*
                                    $searchname = $projectdb->query("SELECT id,dport,protocol FROM services WHERE source='$source' AND BINARY name_ext='$port' AND vsys='$vsys';");
                                    if ($searchname->num_rows == 0) {
                                        add_log('error', 'Reading Services Objects and Groups', 'Unknown Service [' . $HostGroupName . '] port-name mapping for LT : ' . $port, $source, 'Using 6500 port. Change it from the GUI');
                                        $port_final = "6500";
                                        $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$port','$port','$Protocol','$port_final','0','$source','$vsys');");
                                        $serviceID = $projectdb->insert_id;
                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                    }
                                    */
                                    else
                                    {
                                        $searchnameData = $tmp_service->name();
                                        $temp_dport = $tmp_service->getDestPort() - 1;
                                        $temp_protocol = $tmp_service->protocol();
                                        $port_final = "0-" . $temp_dport;

                                        $tmp_name = $temp_protocol . "-" . $port_final;

                                        $tmp_service = $v->serviceStore->find($tmp_name);
                                        if( $tmp_service === null )
                                        {
                                            if( $print )
                                                print "  * create service: " . $tmp_name . "\n";
                                            $tmp_service = $v->serviceStore->newService($tmp_name, $temp_protocol, $port_final);

                                            if( $print )
                                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                            $tmp_servicegroup->addMember($tmp_service);
                                        }
                                        /*
                                        $check = $projectdb->query("SELECT name_ext,id FROM services WHERE source='$source' AND vsys='$vsys' AND dport='$port_final' AND protocol='$temp_protocol' LIMIT 1;");
                                        if ($check->num_rows == 1) {
                                            $checkData = $check->fetch_assoc();
                                            $tmp_name = $checkData['name_ext'];
                                            $serviceID = $checkData['id'];
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        }
                                        else {
                                            $tmp_name = $temp_protocol . "-" . $port_final;
                                            $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$tmp_name','$tmp_name','$temp_protocol','$port_final','0','$source','$vsys');");
                                            $serviceID = $projectdb->insert_id;
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        }
    //                                        $addMember[] = "('$lidgroup','$port','$source','$vsys')";
                                        */
                                    }
                                }

                            }
                            elseif( $port == "range" )
                            {
                                //Todo: SVEN remove
                                #return null;

                                $port_first = $netObj[$next]; //3
                                $port_last = rtrim($netObj[$next + 1]); //4

                                if( is_numeric($port_first) )
                                {
                                    $port_first_port = $port_first;
                                }
                                else
                                {
                                    $port_first_port = $this->search_for_service_port($v, $HostGroupNamePan, $names_line, $port_first);

                                    if( $port_first_port == "" )
                                    {
                                        $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port_first . 'Using 6500 port. Change it from the GUI';
                                        $port_first_port = "6500";
                                    }
                                }
                                if( is_numeric($port_last) )
                                {
                                    $port_last_port = $port_last;
                                }
                                else
                                {

                                    $port_last_port = $this->search_for_service_port($v, $HostGroupNamePan, $names_line, $port_last);

                                    if( $port_last_port == "" )
                                    {
                                        $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port_last . 'Using 6500 port. Change it from the GUI';
                                        $port_last_port = "6500";
                                    }
                                }

                                # Check first if they are EQUAL
                                if( $port_first_port == $port_last_port )
                                {
                                    $isRange = "";
                                    $LastPort = "";
                                    $vtype = "";
                                    $addlog = 'Moving Service-Range to Service [' . $names_line . '] ports are the same - No Action Required';
                                }
                                else
                                {
                                    $isRange = "-range";
                                    $isRange = "";
                                    $LastPort = "-" . $port_last_port;
                                    $vtype = "range";
                                }

                                $name_ext = $Protocol . "-" . $port_first_port . $LastPort . "-source";
                                $final_protocol = $Protocol;
                                $final_source_port = $port_first_port . $LastPort;
                                $final_destination_port = '1-65535';
                                $tmp_service = $v->serviceStore->find($name_ext);


                                if( $tmp_service === null )
                                {
                                    if( $print )
                                        print "  * create service: " . $name_ext . "\n";
                                    $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                    if( $addlog != "" )
                                        $tmp_service->set_node_attribute('error', $addlog);
                                    if( $print )
                                        print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                    $tmp_servicegroup->addMember($tmp_service);
                                }
                                else
                                {
                                    if( $print )
                                        print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                    $tmp_servicegroup->addMember($tmp_service);
                                }


                                /*
                                if (is_numeric($port_first)) {
                                    $port_first_port = $port_first;
                                } else {
                                    # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                                    $searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND BINARY name_ext='$port_first' AND vsys='$vsys';");
                                    $cisconame = $searchname->fetch_assoc();
                                    $port_first_port = $cisconame['dport'];

                                    if ($port_first_port == "") {
                                        add_log('error', 'Reading Services Objects and Groups', 'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port_first, $source, 'Using 6500 port. Change it from the GUI');
                                        $port_first_port = "6500";
                                    }
                                }
                                if (is_numeric($port_last)) {
                                    $port_last_port = $port_last;
                                } else {
                                    # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                                    $searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND BINARY name_ext='$port_last' AND vsys='$vsys';");
                                    $cisconame = $searchname->fetch_assoc();
                                    $port_last_port = $cisconame['dport'];

                                    if ($port_last_port == "") {
                                        add_log('error', 'Reading Services Objects and Groups', 'Unknown Service-Range  [' . $HostGroupName . '] port-name mapping for: ' . $port_first, $source, 'Using 6500 port. Change it from the GUI');
                                        $port_last_port = "6500";
                                    }
                                }

                                # Check first if they are EQUAL
                                if ($port_first_port == $port_last_port) {
                                    $isRange = "";
                                    $LastPort = "";
                                    $vtype = "";
                                    add_log('warning', 'Reading Services Objects and Groups', 'Moving Service-Range to Service [' . $names_line . '] ports are the same', $source, 'No Action Required');
                                } else {
                                    $isRange = "-range";
                                    $LastPort = "-$port_last_port";
                                    $vtype = "range";
                                }

                                $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND BINARY name_ext='$Protocol" . $isRange . "-$port_first_port" . $LastPort . "' AND vsys='$vsys';");
                                if ($getService->num_rows == 0) {
                                    # Create it
                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,type,vsys) values ('$Protocol" . $isRange . "-$port_first_port" . $LastPort . "','$Protocol" . $isRange . "-$port_first_port" . $LastPort . "','$Protocol','$port_first_port" . $LastPort . "','0','$source','$vtype','$vsys');");
                                    $serviceID = $projectdb->insert_id;
                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                }
                                else {
                                    #Assign to the service-object in service get table and lid
                                    $getServiceData = $getService->fetch_assoc();
                                    $service_name = $getServiceData['name'];
                                    $serviceID = $getServiceData['id'];
                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                }
                                */
                            }


                            #

                            if( $port == "source" )
                            {
                                //Todo: SVEN remove
                                #return null;

                                $next = 4;
                                $port = $netObj[3];

                                if( !isset($port) )
                                {
                                    $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($Protocol, '1-65535', '1-65535');

                                    if( $tmp_service === null )
                                    {
                                        $tmp_service = $v->serviceStore->newService($Protocol . "-All", $Protocol, '1-65535', "", '1-65535');
                                        $tmp_servicegroup->addMember($tmp_service);
                                    }
                                    else
                                        $tmp_servicegroup->addMember($tmp_service);
                                    /*
                                    $getDup = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND sport='1-65535' AND dport='1-65535' AND protocol='$Protocol' AND vsys='$vsys' LIMIT 1;");
                                    if ($getDup->num_rows == 0) {
                                        $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport, dport,checkit,source,vsys) values ('$Protocol-All','$Protocol-All','$Protocol','1-65535','1-65535','0','$source','$vsys');");
                                        $serviceID = $projectdb->insert_id;
                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                    }
                                    else {
                                        $data = $getDup->fetch_assoc();
                                        $existingname = $data['name'];
                                        $serviceID = $data['id'];
                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                    }*/
                                }

                                if( $port == "eq" )
                                {
                                    $port = $netObj[$next]; // 3
                                    if( is_numeric($port) )
                                    {
                                        $port_final = $port;
                                        $final_source_port = $port_final;
                                        $final_destination_port = '1-65535';
                                        $final_protocol = $Protocol;
                                        //name,name_ext,protocol,sport,dport,
                                        $name = $Protocol . "-" . $port_final . "-source";
                                        $name_ext = $name;
                                        #$getService = $projectdb->query("SELECT name FROM services WHERE source='$source' AND protocol='$Protocol' AND sport='$port_final' AND dport='1-65535' AND vsys='$vsys';");

                                        $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port, $final_source_port);
                                    }
                                    else
                                    {
                                        $addlog = 'Unknown Service [' . $HostGroupName . '] source-port-name mapping for: ' . $port . 'Using 6500 port. Change it from the GUI';
                                        $port_final = "6500";
                                        $final_source_port = $port_final;
                                        $final_destination_port = '1-65535';
                                        $final_protocol = $Protocol;
                                        $name = $port . "-source";
                                        $name_ext = $name;
                                        #$getService = $projectdb->query("SELECT id FROM services WHERE source='$source' AND BINARY name_ext='$port' AND vsys='$vsys';");

                                        $tmp_service = $v->serviceStore->find($port);
                                    }

                                    if( $tmp_service === null )
                                    {
                                        $tmp_service = $v->serviceStore->find($name_ext);
                                        if( $tmp_service === null )
                                        {
                                            if( $print )
                                                print "  * create service: " . $name_ext . "\n";
                                            $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                            if( $addlog != "" )
                                                $tmp_service->set_node_attribute('error', $addlog);
                                        }
                                        if( $print )
                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                        $tmp_servicegroup->addMember($tmp_service);
                                    }
                                    else
                                    {
                                        if( $print )
                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                        $tmp_servicegroup->addMember($tmp_service);
                                    }

                                    /*
                                    if ($getService->num_rows == 0) {
                                        # Create it
                                        $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport,dport,checkit,source,vsys) values ('$name','$name_ext','$final_protocol','$final_source_port','$final_destination_port','0','$source','$vsys');");
                                        $serviceID = $projectdb->insert_id;
                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                    }
                                    else {
                                        $data = $getService->fetch_assoc();
                                        $existingname = $data['name'];
                                        $serviceID = $data['id'];
                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                    }
                                    */
                                }
                                elseif( $port == "gt" )
                                {
                                    $port = $netObj[$next];
                                    if( is_numeric($port) )
                                    {
                                        $port = $port + 1;
                                        $port_final = $port . "-65535";
                                        $final_source_port = $port_final;
                                        $final_destination_port = '1-65535';
                                        $final_protocol = $Protocol;
                                        $name = $Protocol . "-" . $port_final . "-source";
                                        $name_ext = $name;

                                        $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port, $final_source_port);
                                        #$getService = $projectdb->query("SELECT name,id FROM services WHERE source='$source' AND protocol='$Protocol' AND sport='$port_final' AND dport='1-65535' AND vsys='$vsys';");

                                        if( $tmp_service === null )
                                        {
                                            $tmp_service = $v->serviceStore->find($name_ext);
                                            if( $tmp_service === null )
                                            {
                                                if( $print )
                                                    print "  * create service: " . $name_ext . "\n";
                                                $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                            }

                                            if( $addlog != "" )
                                                $tmp_service->set_node_attribute('error', $addlog);
                                            if( $print )
                                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                            $tmp_servicegroup->addMember($tmp_service);
                                        }
                                        else
                                        {
                                            if( $print )
                                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                            $tmp_servicegroup->addMember($tmp_service);
                                        }
                                        /*
                                        if ($getService->num_rows == 0) {
                                            # Create it
                                            $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport,dport,checkit,source,vsys) values ('$name','$name_ext','$final_protocol','$final_source_port','$final_destination_port','0','$source','$vsys');");
                                            $serviceID = $projectdb->insert_id;
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        }
                                        else {
                                            $data = $getService->fetch_assoc();
                                            $existingname = $data['name'];
                                            $serviceID = $data['id'];
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        }
                                        */
                                    }
                                    else
                                    {
                                        $addlog = 'Unknown Service [' . $HostGroupName . '] source-port-name mapping for GT : ' . $port . 'Using 6500 port. Change it from the GUI';
                                        $port_final = "6500";
                                        $final_source_port = $port_final;
                                        $final_destination_port = '1-65535';
                                        $final_protocol = $Protocol;
                                        $name = $port . "-source";
                                        $name_ext = $name;

                                        $tmp_service = $v->serviceStore->find($port);
                                        #$getService = $projectdb->query("SELECT id,dport,protocol FROM services WHERE source='$source' AND BINARY name_ext='$port' AND vsys='$vsys';");

                                        if( $tmp_service === null )
                                        {
                                            $tmp_service = $v->serviceStore->find($name_ext);
                                            if( $tmp_service === null )
                                            {
                                                if( $print )
                                                    print "  * create service: " . $name_ext . "\n";
                                                $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                            }
                                            if( $addlog != "" )
                                                $tmp_service->set_node_attribute('error', $addlog);
                                            if( $print )
                                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                            $tmp_servicegroup->addMember($tmp_service);
                                        }
                                        #else
                                        #$tmp_servicegroup->addMember( $tmp_service );
                                        /*
                                        if ($getService->num_rows == 0) {


                                            $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport, dport,checkit,source,vsys) values ('$port-source','$port-source','$Protocol','$port_final','1-65535','0','$source','$vsys');");
                                            $serviceID = $projectdb->insert_id;
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        }*/
                                        else
                                        {
                                            #$searchnameData = $getService->fetch_assoc();
                                            $temp_dport = $tmp_service->getDestPort() + 1;
                                            $temp_protocol = $tmp_service->protocol();
                                            $port_final = $temp_dport . "-65535";
                                            $final_source_port = $port_final;
                                            $final_destination_port = '1-65535';
                                            $final_protocol = $temp_protocol;
                                            $name = $temp_protocol . "-" . $port_final;
                                            $name_ext = $name;

                                            $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port, $final_source_port);
                                            #$getService = $projectdb->query("SELECT name_ext, id FROM services WHERE source='$source' AND vsys='$vsys' AND sport='$port_final' AND dport='1-65535' AND protocol='$temp_protocol' LIMIT 1;");

                                            if( $tmp_service === null )
                                            {
                                                $tmp_service = $v->serviceStore->find($name_ext);
                                                if( $tmp_service === null )
                                                {
                                                    if( $print )
                                                        print "  * create service: " . $name_ext . "\n";
                                                    $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                }
                                                if( $addlog != "" )
                                                    $tmp_service->set_node_attribute('error', $addlog);
                                                if( $print )
                                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                $tmp_servicegroup->addMember($tmp_service);
                                            }
                                            else
                                            {
                                                if( $print )
                                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                $tmp_servicegroup->addMember($tmp_service);
                                            }
                                            /*
                                            if ($getService->num_rows == 1) {
                                                $checkData = $getService->fetch_assoc();
                                                $tmp_name = $checkData['name_ext'];
                                                $serviceID = $checkData['id'];
                                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                            }
                                            else {
                                                $tmp_name = $temp_protocol . "-" . $port_final;
                                                $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport, dport,checkit,source,vsys) values ('$tmp_name-source','$tmp_name-source','$temp_protocol','$port_final','1-65535','0','$source','$vsys');");
                                                $serviceID = $projectdb->insert_id;
                                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                            }*/
                                            // Todo: hay que leer el proto i el dport sumarle uno y poner 65535 y crearlo y añadir como member

                                            //                                        $addMember[] = "('$lidgroup','$port','$source','$vsys')";
                                        }
                                    }
                                }
                                elseif( $port == "lt" )
                                {
                                    $port = $netObj[$next];
                                    if( is_numeric($port) )
                                    {
                                        $port = $port - 1;
                                        $port_final = "0-" . $port;
                                        $final_source_port = $port_final;
                                        $final_destination_port = '1-65535';
                                        $final_protocol = $Protocol;
                                        $name = $Protocol . "-" . $port_final . "-source";
                                        $name_ext = $name;

                                        $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port, $final_source_port);
                                        #$getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND protocol='$Protocol' AND sport='$port_final' AND dport='1-65535' AND vsys='$vsys';");

                                        if( $tmp_service === null )
                                        {
                                            $tmp_service = $v->serviceStore->find($name_ext);
                                            if( $tmp_service === null )
                                            {
                                                if( $print )
                                                    print "  * create service: " . $name_ext . "\n";
                                                $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                            }
                                            if( $addlog != "" )
                                                $tmp_service->set_node_attribute('error', $addlog);
                                            if( $print )
                                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                            $tmp_servicegroup->addMember($tmp_service);
                                        }
                                        else
                                        {
                                            if( $print )
                                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                            $tmp_servicegroup->addMember($tmp_service);
                                        }

                                        /*
                                        if ($getService->num_rows == 0) {
                                            # Create it
                                            $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport,dport,checkit,source,vsys) values ('$Protocol-$port_final-source','$Protocol-$port_final-source','$Protocol','$port_final','1-65535','0','$source','$vsys');");
                                            $serviceID = $projectdb->insert_id;
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        }
                                        else {
                                            $data = $getService->fetch_assoc();
                                            $existingname = $data['name'];
                                            $serviceID = $data['id'];
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        }*/
                                    }
                                    else
                                    {
                                        $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for LT : ' . $port . 'Using 6500 port. Change it from the GUI';
                                        $port_final = "6500";

                                        $final_source_port = $port_final;
                                        $final_destination_port = '1-65535';
                                        $final_protocol = $Protocol;
                                        $name = $port . "-source";
                                        $name_ext = $name;

                                        $tmp_service = $v->serviceStore->find($port);

                                        if( $tmp_service === null )
                                        {
                                            $tmp_service = $v->serviceStore->find($name_ext);
                                            if( $tmp_service === null )
                                            {
                                                if( $print )
                                                    print "  * create service: " . $name_ext . "\n";
                                                $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                            }
                                            if( $addlog != "" )
                                                $tmp_service->set_node_attribute('error', $addlog);
                                            if( $print )
                                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                            $tmp_servicegroup->addMember($tmp_service);
                                        }
                                        else
                                        {
                                            #$searchnameData = $getService->fetch_assoc();
                                            $temp_dport = $tmp_service->getDestPort() - 1;
                                            $temp_protocol = $tmp_service->protocol();
                                            $port_final = "0-" . $temp_dport;
                                            $final_source_port = $port_final;
                                            $final_destination_port = '1-65535';
                                            $final_protocol = $temp_protocol;
                                            $name = $temp_protocol . "-" . $port_final;
                                            $name_ext = $name;

                                            $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port, $final_source_port);


                                            if( $tmp_service === null )
                                            {
                                                $tmp_service = $v->serviceStore->find($name_ext);
                                                if( $tmp_service === null )
                                                {
                                                    if( $print )
                                                        print "  * create service: " . $name_ext . "\n";
                                                    $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                }
                                                if( $addlog != "" )
                                                    $tmp_service->set_node_attribute('error', $addlog);
                                                if( $print )
                                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                $tmp_servicegroup->addMember($tmp_service);
                                            }
                                            else
                                            {
                                                if( $print )
                                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                $tmp_servicegroup->addMember($tmp_service);
                                            }
                                        }

                                        /*
                                        $searchname = $projectdb->query("SELECT id,dport,protocol FROM services WHERE source='$source' AND BINARY name_ext='$port' AND dport='1-65535' AND vsys='$vsys';");
                                        if ($searchname->num_rows == 0) {
                                            add_log('error', 'Reading Services Objects and Groups', 'Unknown Service [' . $HostGroupName . '] port-name mapping for LT : ' . $port, $source, 'Using 6500 port. Change it from the GUI');
                                            $port_final = "6500";
                                            $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport, dport,checkit,source,vsys) values ('$port-source','$port-source','$Protocol','$port_final','1-65535','0','$source','$vsys');");
                                            $serviceID = $projectdb->insert_id;
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        }
                                        else {
                                            $searchnameData = $searchname->fetch_assoc();
                                            $temp_dport = $searchnameData['dport'] - 1;
                                            $temp_protocol = $searchnameData['protocol'];
                                            $port_final = "0-" . $temp_dport;
                                            $check = $projectdb->query("SELECT name_ext, id FROM services WHERE source='$source' AND vsys='$vsys' AND sport='$port_final' AND dport='1-65535' AND protocol='$temp_protocol' LIMIT 1;");
                                            if ($check->num_rows == 1) {
                                                $checkData = $check->fetch_assoc();
                                                $tmp_name = $checkData['name_ext'];
                                                $serviceID = $checkData['id'];
                                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                            }
                                            else {
                                                $tmp_name = $temp_protocol . "-" . $port_final;
                                                $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport, dport,checkit,source,vsys) values ('$tmp_name-source','$tmp_name-source','$temp_protocol','$port_final','1-65535','0','$source','$vsys');");
                                                $serviceID = $projectdb->insert_id;
                                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                            }
    //                                        $addMember[] = "('$lidgroup','$port','$source','$vsys')";
                                        }
                                        */
                                    }

                                }
                                elseif( $port == "range" )
                                {
                                    $port_first = $netObj[$next]; //3
                                    $port_last = rtrim($netObj[$next + 1]); //4

                                    /*
                                    if (is_numeric($port_first)) {
                                        $port_first_port = $port_first;
                                    }
                                    else {
                                        $port_first_port = $this->search_for_service_port($v, $HostGroupNamePan, $names_line, $port_first );

                                        if ($port_first_port == "") {
                                            $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port_first . 'Using 6500 port. Change it from the GUI';
                                            $port_first_port = "6500";
                                        }
                                    }
                                    if (is_numeric($port_last)) {
                                        $port_last_port = $port_last;
                                    }
                                    else {

                                        $port_last_port = $this->search_for_service_port($v, $HostGroupNamePan, $names_line, $port_last );

                                        if ($port_last_port == "") {
                                            $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port_last . 'Using 6500 port. Change it from the GUI';
                                            $port_last_port = "6500";
                                        }
                                    }*/

                                    $tmp_range_port = $this->range_get_ports($port_first, $port_last, $v, $HostGroupName, $HostGroupNamePan, $names_line);
                                    $port_first_port = $tmp_range_port[0];
                                    $port_last_port = $tmp_range_port[1];

                                    # Check first if they are EQUAL
                                    if( $port_first_port == $port_last_port )
                                    {
                                        $isRange = "";
                                        $LastPort = "";
                                        $vtype = "";
                                        $addlog = 'Moving Service-Range to Service [' . $names_line . '] ports are the same - No Action Required';
                                    }
                                    else
                                    {
                                        $isRange = "-range";
                                        $isRange = "";
                                        $LastPort = "-" . $port_last_port;
                                        $vtype = "range";
                                    }

                                    $name_ext = $Protocol . "-" . $port_first_port . $LastPort . "-source";
                                    $final_protocol = $Protocol;
                                    $final_source_port = $port_first_port . $LastPort;
                                    $final_destination_port = '1-65535';
                                    $tmp_service = $v->serviceStore->find($name_ext);


                                    if( $tmp_service === null )
                                    {
                                        $tmp_service = $v->serviceStore->find($name_ext);
                                        if( $tmp_service === null )
                                        {
                                            if( $print )
                                                print "  * create service: " . $name_ext . "\n";
                                            $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                        }
                                        if( $addlog != "" )
                                            $tmp_service->set_node_attribute('error', $addlog);
                                        if( $print )
                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                        $tmp_servicegroup->addMember($tmp_service);
                                    }
                                    else
                                    {
                                        if( $print )
                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                        $tmp_servicegroup->addMember($tmp_service);
                                    }

                                    /*
                                    $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND BINARY name_ext='$Protocol" . $isRange . "-$port_first_port" . $LastPort . "-source' AND vsys='$vsys';");
                                    if ($getService->num_rows == 0) {
                                        # Create it
                                        $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport,dport,checkit,source,type,vsys) values ('$Protocol" . $isRange . "-$port_first_port" . $LastPort . "-source','$Protocol" . $isRange . "-$port_first_port" . $LastPort . "-source','$Protocol','$port_first_port" . $LastPort . "','1-65535','0','$source','$vtype','$vsys');");
                                        $serviceID = $projectdb->insert_id;
                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                    }
                                    else {
                                        #Assign to the service-object in service get table and lid
                                        $getServiceData = $getService->fetch_assoc();
                                        $service_name = $getServiceData['name'];
                                        $serviceID = $getServiceData['id'];
                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                    }
                                    */
                                }
                            }

                            if( $port == "destination" )
                            {
                                //Todo: SVEN remove
                                #return null;

                                $next = 4;
                                $port = $netObj[3];

                                if( !isset($port) )
                                {
                                    $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($Protocol, '1-65535', '');

                                    if( $tmp_service === null )
                                    {
                                        $tmp_service = $v->serviceStore->find($Protocol . "-All");
                                        if( $tmp_service === null )
                                        {
                                            $tmp_service = $v->serviceStore->newService($Protocol . "-All", $Protocol, '1-65535');
                                        }
                                        $tmp_servicegroup->addMember($tmp_service);
                                    }
                                    else
                                        $tmp_servicegroup->addMember($tmp_service);

                                    /*
                                    $getDup = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND dport='1-65535' AND protocol='$Protocol' AND vsys='$vsys' LIMIT 1;");
                                    if ($getDup->num_rows == 0) {
                                        $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$Protocol-All','$Protocol-All','$Protocol','1-65535','0','$source','$vsys');");
                                        $serviceID = $projectdb->insert_id;
                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                    } else {
                                        $data = $getDup->fetch_assoc();
                                        $existingname = $data['name'];
                                        $serviceID = $data['id'];
                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                    }
                                    */
                                }

                                if( $port == "eq" )
                                {
                                    $port = $netObj[$next]; // 3

                                    if( is_numeric($port) )
                                    {
                                        $port_final = $port;
                                        $final_source_port = "";
                                        $final_destination_port = $port_final;
                                        $final_protocol = $Protocol;
                                        //name,name_ext,protocol,sport,dport,
                                        $name = $Protocol . "-" . $port_final;
                                        $name_ext = $name;
                                        #$getService = $projectdb->query("SELECT name FROM services WHERE source='$source' AND protocol='$Protocol' AND sport='$port_final' AND dport='1-65535' AND vsys='$vsys';");

                                        $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port);
                                    }
                                    else
                                    {
                                        $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port . 'Using 6500 port. Change it from the GUI';
                                        $port_final = "6500";
                                        $final_source_port = "";
                                        $final_destination_port = $port_final;

                                        $final_protocol = $Protocol;
                                        $name = $port;
                                        $name_ext = $name;
                                        #$getService = $projectdb->query("SELECT id FROM services WHERE source='$source' AND BINARY name_ext='$port' AND vsys='$vsys';");

                                        $tmp_service = $v->serviceStore->find($port);
                                    }

                                    if( $tmp_service === null )
                                    {
                                        $tmp_service = $v->serviceStore->find($name_ext);
                                        if( $tmp_service === null )
                                        {
                                            if( $print )
                                                print "  * create service: " . $name_ext . "\n";
                                            $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                        }
                                        if( $addlog != "" )
                                            $tmp_service->set_node_attribute('error', $addlog);
                                        if( $print )
                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                        $tmp_servicegroup->addMember($tmp_service);
                                    }
                                    else
                                    {
                                        if( $print )
                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                        $tmp_servicegroup->addMember($tmp_service);
                                    }
                                    /*
                                    if (is_numeric($port)) {
                                        $port_final = $port;
                                        $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND protocol='$Protocol' AND dport='$port_final' AND vsys='$vsys';");
                                        if ($getService->num_rows == 0) {
                                            # Create it
                                            $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$Protocol-$port_final','$Protocol-$port_final','$Protocol','$port_final','0','$source','$vsys');");
                                            $serviceID = $projectdb->insert_id;
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        } else {
                                            $data = $getService->fetch_assoc();
                                            $existingname = $data['name'];
                                            $serviceID = $data['id'];
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        }
                                    } else {
                                        $que = "SELECT id FROM services WHERE source='$source' AND BINARY name_ext='$port' AND protocol = '$Protocol' AND vsys='$vsys';";
                                        $searchname = $projectdb->query($que);
                                        if ($searchname->num_rows == 0) {
                                            add_log('error', 'Reading Services Objects and Groups', 'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port, $source, 'Using 6500 port. Change it from the GUI');
                                            $port_final = "6500";
                                            $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$port','$port','$Protocol','$port_final','0','$source','$vsys');");
                                            $serviceID = $projectdb->insert_id;
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        } else {
                                            $data = $searchname->fetch_assoc();
                                            $serviceID = $data['id'];
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        }
                                    }
                                    */
                                }
                                elseif( $port == "gt" )
                                {
                                    $port = $netObj[$next];

                                    if( is_numeric($port) )
                                    {
                                        $port = $port + 1;
                                        $port_final = $port . "-65535";
                                        $final_source_port = "";
                                        $final_destination_port = $port_final;
                                        $final_protocol = $Protocol;
                                        $name = $Protocol . "-" . $port_final;
                                        $name_ext = $name;

                                        $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port, $final_source_port);

                                        if( $tmp_service === null )
                                        {
                                            $tmp_service = $v->serviceStore->find($name_ext);
                                            if( $tmp_service === null )
                                            {
                                                if( $print )
                                                    print "  * create service: " . $name_ext . "\n";
                                                $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                            }
                                            if( $addlog != "" )
                                                $tmp_service->set_node_attribute('error', $addlog);
                                            if( $print )
                                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                            $tmp_servicegroup->addMember($tmp_service);
                                        }
                                        else
                                        {
                                            if( $print )
                                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                            $tmp_servicegroup->addMember($tmp_service);
                                        }
                                    }
                                    else
                                    {
                                        $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for GT : ' . $port . 'Using 6500 port. Change it from the GUI';
                                        $port_final = "6500";
                                        $final_source_port = "";
                                        $final_destination_port = $port_final;
                                        $final_protocol = $Protocol;
                                        $name = $port . "-source";
                                        $name_ext = $name;

                                        $tmp_service = $v->serviceStore->find($port);

                                        if( $tmp_service === null )
                                        {
                                            $tmp_service = $v->serviceStore->find($name_ext);
                                            if( $tmp_service === null )
                                            {
                                                if( $print )
                                                    print "  * create service: " . $name_ext . "\n";
                                                $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                            }
                                            if( $addlog != "" )
                                                $tmp_service->set_node_attribute('error', $addlog);
                                            if( $print )
                                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                            $tmp_servicegroup->addMember($tmp_service);
                                        }
                                        else
                                        {
                                            $temp_dport = $tmp_service->getDestPort() + 1;
                                            $temp_protocol = $tmp_service->protocol();
                                            $port_final = $temp_dport . "-65535";
                                            $final_source_port = "";
                                            $final_destination_port = $port_final;
                                            $final_protocol = $temp_protocol;
                                            $name = $temp_protocol . "-" . $port_final;
                                            $name_ext = $name;

                                            $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port, $final_source_port);

                                            if( $tmp_service === null )
                                            {
                                                $tmp_service = $v->serviceStore->find($name_ext);
                                                if( $tmp_service === null )
                                                {
                                                    if( $print )
                                                        print "  * create service: " . $name_ext . "\n";
                                                    $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                }
                                                if( $addlog != "" )
                                                    $tmp_service->set_node_attribute('error', $addlog);
                                                if( $print )
                                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                $tmp_servicegroup->addMember($tmp_service);
                                            }
                                            else
                                            {
                                                if( $print )
                                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                $tmp_servicegroup->addMember($tmp_service);
                                            }
                                        }
                                    }

                                    /*
                                    if (is_numeric($port)) {
                                        $port = $port + 1;
                                        $port_final = $port . "-65535";
                                        $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND protocol='$Protocol' AND dport='$port_final' AND vsys='$vsys';");
                                        if ($getService->num_rows == 0) {
                                            # Create it
                                            $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$Protocol-$port_final','$Protocol-$port_final','$Protocol','$port_final','0','$source','$vsys');");
                                            $serviceID = $projectdb->insert_id;
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        } else {
                                            $data = $getService->fetch_assoc();
                                            $existingname = $data['name'];
                                            $serviceID = $data['id'];
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        }
                                    } else {
                                        $searchname = $projectdb->query("SELECT id,dport,protocol FROM services WHERE source='$source' AND BINARY name_ext='$port' AND vsys='$vsys';");
                                        if ($searchname->num_rows == 0) {
                                            add_log('error', 'Reading Services Objects and Groups', 'Unknown Service [' . $HostGroupName . '] port-name mapping for GT : ' . $port, $source, 'Using 6500 port. Change it from the GUI');
                                            $port_final = "6500";
                                            $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$port','$port','$Protocol','$port_final','0','$source','$vsys');");
                                            $serviceID = $projectdb->insert_id;
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        }
                                        else {
                                            $searchnameData = $searchname->fetch_assoc();
                                            $temp_dport = $searchnameData['dport'] + 1;
                                            $temp_protocol = $searchnameData['protocol'];
                                            $port_final = $temp_dport . "-65535";
                                            $check = $projectdb->query("SELECT name_ext, id FROM services WHERE source='$source' AND vsys='$vsys' AND dport='$port_final' AND protocol='$temp_protocol' LIMIT 1;");
                                            if ($check->num_rows == 1) {
                                                $checkData = $check->fetch_assoc();
                                                $tmp_name = $checkData['name_ext'];
                                                $serviceID = $checkData['id'];
                                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                            }
                                            else {
                                                $tmp_name = $temp_protocol . "-" . $port_final;
                                                $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$tmp_name','$tmp_name','$temp_protocol','$port_final','0','$source','$vsys');");
                                                $serviceID= $projectdb->insert_id;
                                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                            }
                                            // Todo: hay que leer el proto i el dport sumarle uno y poner 65535 y crearlo y añadir como member

    //                                        $addMember[] = "('$lidgroup','$port','$source','$vsys')";
                                        }
                                    }
                                    */
                                }
                                elseif( $port == "lt" )
                                {
                                    $port = $netObj[$next];

                                    if( is_numeric($port) )
                                    {
                                        $port = $port - 1;
                                        $port_final = "0-" . $port;
                                        $final_source_port = "";
                                        $final_destination_port = $port_final;
                                        $final_protocol = $Protocol;
                                        $name = $Protocol . "-" . $port_final;
                                        $name_ext = $name;

                                        $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port, $final_source_port);

                                        if( $tmp_service === null )
                                        {
                                            $tmp_service = $v->serviceStore->find($name_ext);
                                            if( $tmp_service === null )
                                            {
                                                if( $print )
                                                    print "  * create service: " . $name_ext . "\n";
                                                $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                            }
                                            if( $addlog != "" )
                                                $tmp_service->set_node_attribute('error', $addlog);
                                            if( $print )
                                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                            $tmp_servicegroup->addMember($tmp_service);
                                        }
                                        else
                                        {
                                            if( $print )
                                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                            $tmp_servicegroup->addMember($tmp_service);
                                        }
                                    }
                                    else
                                    {
                                        $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for LT : ' . $port . 'Using 6500 port. Change it from the GUI';
                                        $port_final = "6500";

                                        $final_source_port = "";
                                        $final_destination_port = $port_final;
                                        $final_protocol = $Protocol;
                                        $name = $port;
                                        $name_ext = $name;

                                        $tmp_service = $v->serviceStore->find($port);

                                        if( $tmp_service === null )
                                        {
                                            $tmp_service = $v->serviceStore->find($name_ext);
                                            if( $tmp_service === null )
                                            {
                                                if( $print )
                                                    print "  * create service: " . $name_ext . "\n";
                                                $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                            }
                                            if( $addlog != "" )
                                                $tmp_service->set_node_attribute('error', $addlog);
                                            if( $print )
                                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                            $tmp_servicegroup->addMember($tmp_service);
                                        }
                                        else
                                        {
                                            $temp_dport = $tmp_service->getDestPort() - 1;
                                            $temp_protocol = $tmp_service->protocol();
                                            $port_final = "0-" . $temp_dport;
                                            $final_source_port = "";
                                            $final_destination_port = $port_final;
                                            $final_protocol = $temp_protocol;
                                            $name = $temp_protocol . "-" . $port_final;
                                            $name_ext = $name;

                                            $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port, $final_source_port);

                                            if( $tmp_service === null )
                                            {
                                                $tmp_service = $v->serviceStore->find($name_ext);
                                                if( $tmp_service === null )
                                                {
                                                    if( $print )
                                                        print "  * create service: " . $name_ext . "\n";
                                                    $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                }
                                                if( $addlog != "" )
                                                    $tmp_service->set_node_attribute('error', $addlog);
                                                if( $print )
                                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                $tmp_servicegroup->addMember($tmp_service);
                                            }
                                            else
                                            {
                                                if( $print )
                                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                $tmp_servicegroup->addMember($tmp_service);
                                            }
                                        }
                                    }

                                    /*
                                    if (is_numeric($port)) {
                                        $port = $port - 1;
                                        $port_final = "0-" . $port;
                                        $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND protocol='$Protocol' AND dport='$port_final' AND vsys='$vsys';");
                                        if ($getService->num_rows == 0) {
                                            # Create it
                                            $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$Protocol-$port_final','$Protocol-$port_final','$Protocol','$port_final','0','$source','$vsys');");
                                            $serviceID = $projectdb->insert_id;
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        } else {
                                            $data = $getService->fetch_assoc();
                                            $existingname = $data['name'];
                                            $serviceID = $data['id'];
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        }
                                    } else {
                                        $searchname = $projectdb->query("SELECT id,dport,protocol FROM services WHERE source='$source' AND BINARY name_ext='$port' AND vsys='$vsys';");
                                        if ($searchname->num_rows == 0) {
                                            add_log('error', 'Reading Services Objects and Groups', 'Unknown Service [' . $HostGroupName . '] port-name mapping for LT : ' . $port, $source, 'Using 6500 port. Change it from the GUI');
                                            $port_final = "6500";
                                            $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$port','$port','$Protocol','$port_final','0','$source','$vsys');");
                                            $serviceID = $projectdb->insert_id;
                                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                        }
                                        else {
                                            $searchnameData = $searchname->fetch_assoc();
                                            $temp_dport = $searchnameData['dport'] - 1;
                                            $temp_protocol = $searchnameData['protocol'];
                                            $port_final = "0-" . $temp_dport;
                                            $check = $projectdb->query("SELECT name_ext, id FROM services WHERE source='$source' AND vsys='$vsys' AND dport='$port_final' AND protocol='$temp_protocol' LIMIT 1;");
                                            if ($check->num_rows == 1) {
                                                $checkData = $check->fetch_assoc();
                                                $tmp_name = $checkData['name_ext'];
                                                $serviceID = $checkData['id'];
                                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                            }
                                            else {
                                                $tmp_name = $temp_protocol . "-" . $port_final;
                                                $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$tmp_name','$tmp_name','$temp_protocol','$port_final','0','$source','$vsys');");
                                                $serviceID = $projectdb->insert_id;
                                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                            }
    //                                        $addMember[] = "('$lidgroup','$port','$source','$vsys')";
                                        }
                                    }
                                    */

                                }
                                elseif( $port == "range" )
                                {
                                    $port_first = $netObj[$next]; //3
                                    $port_last = rtrim($netObj[$next + 1]); //4

                                    /*
                                    if (is_numeric($port_first)) {
                                        $port_first_port = $port_first;
                                    }
                                    else {
                                        $port_first_port = $this->search_for_service_port($v, $HostGroupNamePan, $names_line, $port_first );

                                        if ($port_first_port == "") {
                                            $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port_first . 'Using 6500 port. Change it from the GUI';
                                            $port_first_port = "6500";
                                        }
                                    }
                                    if (is_numeric($port_last)) {
                                        $port_last_port = $port_last;
                                    }
                                    else {

                                        $port_last_port = $this->search_for_service_port($v, $HostGroupNamePan, $names_line, $port_last );

                                        if ($port_last_port == "") {
                                            $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port_last . 'Using 6500 port. Change it from the GUI';
                                            $port_last_port = "6500";
                                        }
                                    }*/

                                    $tmp_range_port = $this->range_get_ports($port_first, $port_last, $v, $HostGroupName, $HostGroupNamePan, $names_line);
                                    $port_first_port = $tmp_range_port[0];
                                    $port_last_port = $tmp_range_port[1];

                                    # Check first if they are EQUAL
                                    if( $port_first_port == $port_last_port )
                                    {
                                        $isRange = "";
                                        $LastPort = "";
                                        $vtype = "";
                                        $addlog = 'Moving Service-Range to Service [' . $names_line . '] ports are the same - No Action Required';
                                    }
                                    else
                                    {
                                        $isRange = "-range";
                                        $isRange = "";
                                        $LastPort = "-" . $port_last_port;
                                        $vtype = "range";
                                    }

                                    #$name_ext = $Protocol . $port_first_port . $LastPort;
                                    $name_ext = $Protocol . "-" . $port_first_port . $LastPort;
                                    $final_protocol = $Protocol;
                                    $final_source_port = "";
                                    $final_destination_port = $port_first_port . $LastPort;
                                    $tmp_service = $v->serviceStore->find($name_ext);

                                    if( $tmp_service === null )
                                    {
                                        if( $print )
                                            print "  * create service: " . $name_ext . "\n";
                                        $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                        if( $addlog != "" )
                                            $tmp_service->set_node_attribute('error', $addlog);
                                        if( $print )
                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                        $tmp_servicegroup->addMember($tmp_service);
                                    }
                                    else
                                    {
                                        if( $print )
                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                        $tmp_servicegroup->addMember($tmp_service);
                                    }
                                }
                                /*
                                                                if (is_numeric($port_first)) {
                                                                    $port_first_port = $port_first;
                                                                } else {
                                                                    # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                                                                    $searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND BINARY name_ext='$port_first' AND vsys='$vsys';");
                                                                    $cisconame = $searchname->fetch_assoc();
                                                                    $port_first_port = $cisconame['dport'];

                                                                    if ($port_first_port == "") {
                                                                        add_log('error', 'Reading Services Objects and Groups', 'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port_first, $source, 'Using 6500 port. Change it from the GUI');
                                                                        $port_first_port = "6500";
                                                                    }
                                                                }
                                                                if (is_numeric($port_last)) {
                                                                    $port_last_port = $port_last;
                                                                } else {
                                                                    # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                                                                    $searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND BINARY name_ext='$port_last' AND vsys='$vsys';");
                                                                    $cisconame = $searchname->fetch_assoc();
                                                                    $port_last_port = $cisconame['dport'];

                                                                    if ($port_last_port == "") {
                                                                        add_log('error', 'Reading Services Objects and Groups', 'Unknown Service-Range  [' . $HostGroupName . '] port-name mapping for: ' . $port_first, $source, 'Using 6500 port. Change it from the GUI');
                                                                        $port_last_port = "6500";
                                                                    }
                                                                }

                                                                # Check first if they are EQUAL
                                                                if ($port_first_port == $port_last_port) {
                                                                    $isRange = "";
                                                                    $LastPort = "";
                                                                    $vtype = "";
                                                                    add_log('warning', 'Reading Services Objects and Groups', 'Moving Service-Range to Service [' . $names_line . '] ports are the same', $source, 'No Action Required');
                                                                } else {
                                                                    $isRange = "-range";
                                                                    $LastPort = "-$port_last_port";
                                                                    $vtype = "range";
                                                                }

                                                                $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND BINARY name_ext='$Protocol" . $isRange . "-$port_first_port" . $LastPort . "' AND vsys='$vsys';");
                                                                if ($getService->num_rows == 0) {
                                                                    # Create it
                                                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,type,vsys) values ('$Protocol" . $isRange . "-$port_first_port" . $LastPort . "','$Protocol" . $isRange . "-$port_first_port" . $LastPort . "','$Protocol','$port_first_port" . $LastPort . "','0','$source','$vtype','$vsys');");
                                                                    $serviceID = $projectdb->insert_id;
                                                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                                } else {
                                                                    #Assign to the service-object in service get table and lid
                                                                    $getServiceData = $getService->fetch_assoc();
                                                                    $service_name = $getServiceData['name'];
                                                                    $serviceID = $getServiceData['id'];
                                                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                                }
                                                                */
                            }


                        }
                    }
                    elseif( $Protocol == "tcp-udp" )
                    {
                        //Todo: SVEN remove
                        #return null;

                        if( isset($netObj[2]) )
                        {


                            $port = $netObj[2];

                            $next = 3;
                            if( $port == "destination" )
                            {
                                $next = 4;
                                $port = $netObj[3];
                            }

                            if( $port == "eq" )
                            {
                                $port = $netObj[$next];
                                $port_final = "";
                                if( is_numeric($port) )
                                {
                                    $port_final = $port;
                                }
                                else
                                {
                                    $tmp_service = $v->serviceStore->find($port);
                                    if( $tmp_service !== null )
                                    {
                                        if( $tmp_service->isService() )
                                            $port_final = $tmp_service->getDestPort();
                                        else
                                        {
                                            foreach( $tmp_service->members() as $member )
                                            {
                                                if( $port_final != "" )
                                                {
                                                    print "portfinal is: " . $port_final . "\n";
                                                    $tmp_member_port = $member->getDestPort();
                                                    if( $port_final != $tmp_member_port )
                                                        mwarning("servicegroup has different ports available: " . $tmp_member_port, null, FALSE);
                                                }
                                                $port_final = $member->getDestPort();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        $addlog = 'Unknown Service-Range  [' . $HostGroupName . '] source-port-name mapping for: ' . $port_first . 'Using 6500 port. Change it from the GUI';
                                        $port_final = "6500";
                                    }
                                    /*
                                    $searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND BINARY name_ext='$port' AND vsys='$vsys';");
                                    $cisconame = $searchname->fetch_assoc();
                                    $port_final = $cisconame['dport'];
                                    if ($port_final == "") {
                                    $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port . 'Using 6500 port. Change it from the GUI';
                                    $port_final = "6500";
                                }
                                    */
                                }


                                $tmp_service = $v->serviceStore->find('tcp-' . $port_final);
                                if( $tmp_service === null )
                                {
                                    if( $print )
                                        print "   * create service tcp-" . $port_final . "\n";
                                    $tmp_service = $v->serviceStore->newService('tcp-' . $port_final, 'tcp', $port_final);
                                    if( $addlog !== "" )
                                        $tmp_service->set_node_attribute('error', $addlog);
                                }
                                if( $print )
                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                $tmp_servicegroup->addMember($tmp_service);

                                $tmp_service = $v->serviceStore->find('udp-' . $port_final);
                                if( $tmp_service === null )
                                {
                                    if( $print )
                                        print "   * create service udp-" . $port_final . "\n";
                                    $tmp_service = $v->serviceStore->newService('udp-' . $port_final, 'udp', $port_final);
                                    if( $addlog !== "" )
                                        $tmp_service->set_node_attribute('error', $addlog);

                                }
                                if( $print )
                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                $tmp_servicegroup->addMember($tmp_service);

                                /*
                                $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND protocol='tcp' AND dport='$port_final' AND vsys='$vsys';");
                                if ($getService->num_rows == 0) {
                                    # Create it
                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('tcp-$port_final','tcp-$port_final','tcp','$port_final','1','$source','$vsys');");
                                    $serviceID = $projectdb->insert_id;
                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                } else {
                                    #Assign to the service-object in service get table and lid
                                    $getServiceData = $getService->fetch_assoc();
                                    $service_name = $getServiceData['name'];
                                    $serviceID = $getServiceData['id'];
                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                }
                                $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND protocol='udp' AND dport='$port_final' AND vsys='$vsys';");
                                if ($getService->num_rows == 0) {
                                    # Create it
                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('udp-$port_final','udp-$port_final','udp','$port_final','0','$source','$vsys');");
                                    $serviceID = $projectdb->insert_id;
                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                } else {
                                    #Assign to the service-object in service get table and lid
                                    $getServiceData = $getService->fetch_assoc();
                                    $service_name = $getServiceData['name'];
                                    $serviceID = $getServiceData['id'];
                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                }
                                */
                            }
                            elseif( $port == "range" )
                            {
                                $port_first = $netObj[$next];
                                $port_last = rtrim($netObj[$next + 1]);

                                /*
                                if (is_numeric($port_first)) {
                                    $port_first_port = $port_first;
                                } else {
                                    # IS NAME TO SEARCH IN vendor_services_mapping TABLE

                                    $port_first_port = $this->search_for_service_port($v, $HostGroupNamePan, $names_line, $port_first );

                                    if ($port_first_port == "") {
                                        $addlog =  'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port_first.' Using 6500 port. Change it from the GUI';
                                        $port_first_port = "6500";
                                    }
                                }
                                if (is_numeric($port_last)) {
                                    $port_last_port = $port_last;
                                } else {
                                    # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                                    $port_last_port = $this->search_for_service_port($v, $HostGroupNamePan, $names_line, $port_last );

                                    if ($port_last_port == "") {
                                        $addlog =  'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port_last.' Using 6500 port. Change it from the GUI';
                                        $port_last_port = "6500";
                                    }

                                }*/

                                $tmp_range_port = $this->range_get_ports($port_first, $port_last, $v, $HostGroupName, $HostGroupNamePan, $names_line);
                                $port_first_port = $tmp_range_port[0];
                                $port_last_port = $tmp_range_port[1];

                                # Check first if they are EQUAL
                                if( $port_first_port == $port_last_port )
                                {
                                    $isRange = "";
                                    $LastPort = "";
                                    $vtype = "";
                                    $addlog = 'Moving Service-Range to Service [' . $names_line . '] ports are the same' . ' No Action Required';
                                }
                                else
                                {
                                    $isRange = "-range";
                                    $isRange = "";
                                    $LastPort = "-$port_last_port";
                                    $vtype = "range";
                                }

                                $port_final = $port_first_port . $LastPort;
                                $name = $port_final;

                                $tmp_service = $v->serviceStore->find('tcp-' . $name);
                                if( $tmp_service === null )
                                {
                                    if( $print )
                                        print "   * create service tcp-" . $port_final . "\n";
                                    $tmp_service = $v->serviceStore->newService('tcp-' . $name, 'tcp', $port_final);
                                }
                                if( $print )
                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                $tmp_servicegroup->addMember($tmp_service);

                                $tmp_service = $v->serviceStore->find('udp-' . $name);
                                if( $tmp_service === null )
                                {
                                    if( $print )
                                        print "   * create service udp-" . $name . "\n";
                                    $tmp_service = $v->serviceStore->newService('udp-' . $name, 'udp', $port_final);

                                }
                                if( $print )
                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                $tmp_servicegroup->addMember($tmp_service);

                                /*
                                $getService = $projectdb->query("SELECT id FROM services WHERE source='$source' AND name_ext='tcp" . $isRange . "-$port_first_port" . $LastPort . "' AND vsys='$vsys';");
                                if ($getService->num_rows == 0) {
                                    # Create it
                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,type,vsys) values ('tcp" . $isRange . "-$port_first_port" . $LastPort . "','tcp" . $isRange . "-$port_first_port" . $LastPort . "','tcp','$port_first_port" . $LastPort . "','0','$source','$vtype','$vsys');");
                                    $serviceID = $projectdb->insert_id;
                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                } else {
                                    $data = $getService->fetch_assoc();
                                    $serviceID = $data['id'];
                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                }
                                $getService = $projectdb->query("SELECT id FROM services WHERE source='$source' AND name_ext='udp" . $isRange . "-$port_first_port" . $LastPort . "' AND vsys='$vsys';");
                                if ($getService->num_rows == 0) {
                                    # Create it
                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,type,vsys) values ('udp" . $isRange . "-$port_first_port" . $LastPort . "','udp" . $isRange . "-$port_first_port" . $LastPort . "','udp','$port_first_port" . $LastPort . "','0','$source','$vtype','$vsys');");
                                    $serviceID = $projectdb->insert_id;
                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                } else {
                                    #Assign to the service-object in service get table and lid
                                    $data = $getService->fetch_assoc();
                                    $serviceID = $data['id'];
                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                }
                                */
                            }
                        }
                        else
                        {
                            mwarning("netObj[2] not set: " . $names_line);
                        }


                    }
                    elseif( $Protocol == "object" )
                    {

                        $obj2 = $netObj[2];
                        $obj2pan = $this->truncate_names($this->normalizeNames($obj2));

                        $tmp_service = $v->serviceStore->find($obj2pan);
                        if( $tmp_service === null )
                        {
                            $addlog = 'The ObjectName doesn\'t exist [' . $obj2pan . '] -Adding to the DB, please add the right Port and Protocol, tcp/6500 is used temporare';


                            if( $print )
                                print "  * create service: " . $obj2pan . "\n";
                            $tmp_service = $v->serviceStore->newService($obj2pan, "tcp", "6500");
                            if( $print )
                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                            $tmp_servicegroup->addMember($tmp_service);
                        }
                        else
                        {
                            if( $print )
                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                            $tmp_servicegroup->addMember($tmp_service);
                        }

                        /*
                        $search = $projectdb->query("SELECT id FROM services WHERE BINARY name_ext='$obj2' AND source='$source' AND vsys='$vsys';");
                        if ($search->num_rows == 1) {
                            $data = $search->fetch_assoc();
                            $serviceID = $data['id'];
                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                        } else {
                            #Not exists in DB Creating IT
                            add_log('error', 'Reading Service Objects and Groups', 'The ObjectName doesn\'t exist [' . $obj2 . ']', $source, 'Adding to the DB, please add the right Port and Protocol');
                            $obj2pan = $this->truncate_names($this->normalizeNames($obj2));
                            $projectdb->query("INSERT INTO services (name,name_ext,checkit,source,vsys) values ('$obj2pan','$obj2','1','$source','$vsys');");
                            $serviceID = $projectdb->insert_id;
                            $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                        }
                        */
                    }
                    else
                    {
                        //Todo: SVEN remove
                        #return null;

                        if( ($Protocol == "icmp") or ($Protocol == "icmp6") )
                        {
                            if( isset($netObj[2]) )
                            {
                                $code = $netObj[2];
                                $servicename = "$Protocol-$code";
                            }
                            else
                            {
                                $code = "";
                                $servicename = $Protocol;
                            }


                            $tmp_service = $v->serviceStore->find("tmp-" . $servicename);
                            if( $tmp_service === null )
                            {
                                if( $print )
                                    print " * create service tmp-" . $servicename . "\n";
                                $tmp_service = $v->serviceStore->newService("tmp-" . $servicename, "tcp", "6500");
                                $tmp_service->set_node_attribute('error', 'ICMP Service found [' . $servicename . '] with icmptype: "' . $code . '" - Replace it by the ICMP app-id - tcp 6500 is used');
                            }
                            if( $print )
                                print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                            $tmp_servicegroup->addMember($tmp_service);

                            /*
                            $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND vsys='$vsys' AND protocol='$Protocol' AND type='$code' LIMIT 1;");
                            if ($getService->num_rows == 0) {
                                $projectdb->query("INSERT INTO services (name,name_ext,checkit,source,vsys,protocol,icmp,type) values ('$servicename','$servicename','1','$source','$vsys','$Protocol',1,'$code');");
                                $serviceID = $projectdb->insert_id;
                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                            } else {
                                $getData = $getService->fetch_assoc();
                                $servicename = $getData['name'];
                                $serviceID = $getData['id'];
                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                            }
                            */
                        }
                        elseif( $Protocol == 'ip' )
                        {
                            if( isset($netObj[2]) )
                            {
                                if( $netObj[2] == "source" )
                                {
                                    $next = 4;
                                    $port = $netObj[3];
                                    $protocols = ['tcp', 'udp'];

                                    foreach( $protocols as $Protocol )
                                    {

                                        if( !isset($port) )
                                        {
                                            $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($Protocol, '1-65535', '1-65535');

                                            if( $tmp_service === null )
                                            {
                                                $tmp_service = $v->serviceStore->find($name_ext);
                                                if( $tmp_service === null )
                                                {
                                                    $tmp_service = $v->serviceStore->newService($Protocol . "-All", $Protocol, '1-65535', "", '1-65535');
                                                }
                                                $tmp_servicegroup->addMember($tmp_service);
                                            }
                                            else
                                                $tmp_servicegroup->addMember($tmp_service);

                                        }

                                        if( $port == "eq" )
                                        {
                                            $port = $netObj[$next]; // 3
                                            if( is_numeric($port) )
                                            {
                                                $port_final = $port;
                                                $final_source_port = $port_final;
                                                $final_destination_port = '1-65535';
                                                $final_protocol = $Protocol;
                                                //name,name_ext,protocol,sport,dport,
                                                $name = $Protocol . "-" . $port_final . "-source";
                                                $name_ext = $name;

                                                $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port, $final_source_port);
                                            }
                                            else
                                            {
                                                $addlog = 'Unknown Service [' . $HostGroupName . '] source-port-name mapping for: ' . $port . 'Using 6500 port. Change it from the GUI';
                                                $port_final = "6500";
                                                $final_source_port = $port_final;
                                                $final_destination_port = '1-65535';
                                                $final_protocol = $Protocol;
                                                $name = $port . "-source";
                                                $name_ext = $name;

                                                $tmp_service = $v->serviceStore->find($port);
                                            }

                                            if( $tmp_service === null )
                                            {
                                                $tmp_service = $v->serviceStore->find($name_ext);
                                                if( $tmp_service === null )
                                                {
                                                    if( $print )
                                                        print "  * create service: " . $name_ext . "\n";
                                                    $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                }
                                                if( $addlog != "" )
                                                    $tmp_service->set_node_attribute('error', $addlog);
                                                if( $print )
                                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                $tmp_servicegroup->addMember($tmp_service);
                                            }
                                            else
                                            {
                                                if( $print )
                                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                $tmp_servicegroup->addMember($tmp_service);
                                            }
                                        }
                                        elseif( $port == "gt" )
                                        {
                                            $port = $netObj[$next];
                                            if( is_numeric($port) )
                                            {
                                                $port = $port + 1;
                                                $port_final = $port . "-65535";
                                                $final_source_port = $port_final;
                                                $final_destination_port = '1-65535';
                                                $final_protocol = $Protocol;
                                                $name = $Protocol . "-" . $port_final . "-source";
                                                $name_ext = $name;

                                                $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port, $final_source_port);

                                                if( $tmp_service === null )
                                                {
                                                    $tmp_service = $v->serviceStore->find($name_ext);
                                                    if( $tmp_service === null )
                                                    {
                                                        if( $print )
                                                            print "  * create service: " . $name_ext . "\n";
                                                        $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                    }
                                                    if( $addlog != "" )
                                                        $tmp_service->set_node_attribute('error', $addlog);
                                                    if( $print )
                                                        print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                    $tmp_servicegroup->addMember($tmp_service);
                                                }
                                                else
                                                {
                                                    if( $print )
                                                        print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                    $tmp_servicegroup->addMember($tmp_service);
                                                }
                                            }
                                            else
                                            {
                                                $addlog = 'Unknown Service [' . $HostGroupName . '] source-port-name mapping for GT : ' . $port . 'Using 6500 port. Change it from the GUI';
                                                $port_final = "6500";
                                                $final_source_port = $port_final;
                                                $final_destination_port = '1-65535';
                                                $final_protocol = $Protocol;
                                                $name = $port . "-source";
                                                $name_ext = $name;

                                                $tmp_service = $v->serviceStore->find($port);

                                                if( $tmp_service === null )
                                                {
                                                    $tmp_service = $v->serviceStore->find($name_ext);
                                                    if( $tmp_service === null )
                                                    {
                                                        if( $print )
                                                            print "  * create service: " . $name_ext . "\n";
                                                        $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                    }
                                                    if( $addlog != "" )
                                                        $tmp_service->set_node_attribute('error', $addlog);
                                                    if( $print )
                                                        print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                    $tmp_servicegroup->addMember($tmp_service);
                                                }
                                                else
                                                {
                                                    $temp_dport = $tmp_service->getDestPort() + 1;
                                                    $temp_protocol = $tmp_service->protocol();
                                                    $port_final = $temp_dport . "-65535";
                                                    $final_source_port = $port_final;
                                                    $final_destination_port = '1-65535';
                                                    $final_protocol = $temp_protocol;
                                                    $name = $temp_protocol . "-" . $port_final;
                                                    $name_ext = $name;

                                                    $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port, $final_source_port);

                                                    if( $tmp_service === null )
                                                    {
                                                        $tmp_service = $v->serviceStore->find($name_ext);
                                                        if( $tmp_service === null )
                                                        {
                                                            if( $print )
                                                                print "  * create service: " . $name_ext . "\n";
                                                            $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                        }
                                                        if( $addlog != "" )
                                                            $tmp_service->set_node_attribute('error', $addlog);
                                                        if( $print )
                                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                        $tmp_servicegroup->addMember($tmp_service);
                                                    }
                                                    else
                                                    {
                                                        if( $print )
                                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                        $tmp_servicegroup->addMember($tmp_service);
                                                    }
                                                }
                                            }
                                        }
                                        elseif( $port == "lt" )
                                        {
                                            $port = $netObj[$next];
                                            if( is_numeric($port) )
                                            {
                                                $port = $port - 1;
                                                $port_final = "0-" . $port;
                                                $final_source_port = $port_final;
                                                $final_destination_port = '1-65535';
                                                $final_protocol = $Protocol;
                                                $name = $Protocol . "-" . $port_final . "-source";
                                                $name_ext = $name;

                                                $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port, $final_source_port);

                                                if( $tmp_service === null )
                                                {
                                                    $tmp_service = $v->serviceStore->find($name_ext);
                                                    if( $tmp_service === null )
                                                    {
                                                        if( $print )
                                                            print "  * create service: " . $name_ext . "\n";
                                                        $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                    }
                                                    if( $addlog != "" )
                                                        $tmp_service->set_node_attribute('error', $addlog);
                                                    if( $print )
                                                        print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                    $tmp_servicegroup->addMember($tmp_service);
                                                }
                                                else
                                                {
                                                    if( $print )
                                                        print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                    $tmp_servicegroup->addMember($tmp_service);
                                                }
                                            }
                                            else
                                            {
                                                $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for LT : ' . $port . 'Using 6500 port. Change it from the GUI';
                                                $port_final = "6500";

                                                $final_source_port = $port_final;
                                                $final_destination_port = '1-65535';
                                                $final_protocol = $Protocol;
                                                $name = $port . "-source";
                                                $name_ext = $name;

                                                $tmp_service = $v->serviceStore->find($port);

                                                if( $tmp_service === null )
                                                {
                                                    if( $print )
                                                        print "  * create service: " . $name_ext . "\n";
                                                    $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                    if( $addlog != "" )
                                                        $tmp_service->set_node_attribute('error', $addlog);
                                                    if( $print )
                                                        print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                    $tmp_servicegroup->addMember($tmp_service);
                                                }
                                                else
                                                {
                                                    $temp_dport = $tmp_service->getDestPort() - 1;
                                                    $temp_protocol = $tmp_service->protocol();
                                                    $port_final = "0-" . $temp_dport;
                                                    $final_source_port = $port_final;
                                                    $final_destination_port = '1-65535';
                                                    $final_protocol = $temp_protocol;
                                                    $name = $temp_protocol . "-" . $port_final;
                                                    $name_ext = $name;

                                                    $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port, $final_source_port);


                                                    if( $tmp_service === null )
                                                    {
                                                        $tmp_service = $v->serviceStore->find($name_ext);
                                                        if( $tmp_service === null )
                                                        {
                                                            if( $print )
                                                                print "  * create service: " . $name_ext . "\n";
                                                            $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                        }
                                                        if( $addlog != "" )
                                                            $tmp_service->set_node_attribute('error', $addlog);
                                                        if( $print )
                                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                        $tmp_servicegroup->addMember($tmp_service);
                                                    }
                                                    else
                                                    {
                                                        if( $print )
                                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                        $tmp_servicegroup->addMember($tmp_service);
                                                    }
                                                }
                                            }

                                        }
                                        elseif( $port == "range" )
                                        {
                                            $port_first = $netObj[$next]; //3
                                            $port_last = rtrim($netObj[$next + 1]); //4

                                            /*
                                            if (is_numeric($port_first)) {
                                                $port_first_port = $port_first;
                                            }
                                            else {
                                                $port_first_port = $this->search_for_service_port($v, $HostGroupName, $names_line, $port_first );

                                                if ($port_first_port == "") {
                                                    $addlog = 'Unknown Service-Range [' . $HostGroupName . '] port-name mapping for: ' . $port_first . 'Using 6500 port. Change it from the GUI';
                                                    $port_first_port = "6500";
                                                }
                                            }
                                            if (is_numeric($port_last)) {
                                                $port_last_port = $port_last;
                                            }
                                            else {
                                                # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                                                $tmp_service = $v->serviceStore->find( $port_last );
                                                if( $tmp_service !== null )
                                                {
                                                    $port_last_port = $tmp_service->getDestPort();
                                                }
                                                else
                                                {
                                                    $addlog = 'Unknown Service-Range  [' . $HostGroupName . '] source-port-name mapping for: ' . $port_first .  'Using 6500 port. Change it from the GUI';
                                                    $port_last_port = "6500";
                                                }
                                            }*/

                                            $tmp_range_port = $this->range_get_ports($port_first, $port_last, $v, $HostGroupName, $HostGroupNamePan, $names_line);
                                            $port_first_port = $tmp_range_port[0];
                                            $port_last_port = $tmp_range_port[1];

                                            # Check first if they are EQUAL
                                            if( $port_first_port == $port_last_port )
                                            {
                                                $isRange = "";
                                                $LastPort = "";
                                                $vtype = "";
                                                $addlog = 'Moving Service-Range to Service [' . $names_line . '] ports are the same - No Action Required';
                                            }
                                            else
                                            {
                                                $isRange = "-range";
                                                $isRange = "";
                                                $LastPort = "-" . $port_last_port;
                                                $vtype = "range";
                                            }

                                            $name_ext = $Protocol . "-" . $port_first_port . $LastPort . "-source";
                                            $final_protocol = $Protocol;
                                            $final_source_port = $port_first_port . $LastPort;
                                            $final_destination_port = '1-65535';
                                            $tmp_service = $v->serviceStore->find($name_ext);


                                            if( $tmp_service === null )
                                            {
                                                if( $print )
                                                    print "  * create service: " . $name_ext . "\n";
                                                $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                if( $addlog != "" )
                                                    $tmp_service->set_node_attribute('error', $addlog);
                                                if( $print )
                                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                $tmp_servicegroup->addMember($tmp_service);
                                            }
                                            else
                                            {
                                                if( $print )
                                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                $tmp_servicegroup->addMember($tmp_service);
                                            }
                                        }

                                        /*
                                        if (!isset($port)) {
                                            $getDup = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND sport='1-65535' AND dport='1-65535' AND protocol='$Protocol' AND vsys='$vsys' LIMIT 1;");
                                            if ($getDup->num_rows == 0) {
                                                $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport, dport,checkit,source,vsys) values ('$Protocol-All','$Protocol-All','$Protocol','1-65535','1-65535','0','$source','$vsys');");
                                                $serviceID = $projectdb->insert_id;
                                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                            } else {
                                                $data = $getDup->fetch_assoc();
                                                $existingname = $data['name'];
                                                $serviceID = $data['id'];
                                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                            }
                                        }

                                        if ($port == "eq") {
                                            $port = $netObj[$next]; // 3
                                            if (is_numeric($port)) {
                                                $port_final = $port;
                                                $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND protocol='$Protocol' AND sport='$port_final' AND dport='1-65535' AND vsys='$vsys';");
                                                if ($getService->num_rows == 0) {
                                                    # Create it
                                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport,dport, checkit,source,vsys) values ('$Protocol-$port_final-source','$Protocol-$port_final-source','$Protocol','$port_final','1-65535','0','$source','$vsys');");
                                                    $serviceID = $projectdb->insert_id;
                                                    $addMemberID[] = "('$lidgroup','services', $serviceID,'$source','$vsys')";
                                                } else {
                                                    $data = $getService->fetch_assoc();
                                                    $existingname = $data['name'];
                                                    $serviceID = $data['id'];
                                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                }
                                            } else {
                                                $searchname = $projectdb->query("SELECT id FROM services WHERE source='$source' AND BINARY name_ext='$port' AND vsys='$vsys';");
                                                if ($searchname->num_rows == 0) {
                                                    add_log('error', 'Reading Services Objects and Groups', 'Unknown Service [' . $HostGroupName . '] source-port-name mapping for: ' . $port, $source, 'Using 6500 port. Change it from the GUI');
                                                    $port_final = "6500";
                                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport,dport,checkit,source,vsys) values ('$port-source','$port-source','$Protocol','$port_final','1-65535','0','$source','$vsys');");
                                                    $serviceID = $projectdb->insert_id;
                                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                } else {
                                                    $data = $searchname->fetch_assoc();
                                                    $serviceID = $data['id'];
                                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                }
                                            }
                                        }
                                        elseif ($port == "gt") {
                                            $port = $netObj[$next];
                                            if (is_numeric($port)) {
                                                $port = $port + 1;
                                                $port_final = $port . "-65535";
                                                $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND protocol='$Protocol' AND sport='$port_final' AND dport='1-65535' AND vsys='$vsys';");
                                                if ($getService->num_rows == 0) {
                                                    # Create it
                                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport, dport,checkit,source,vsys) values ('$Protocol-$port_final-source','$Protocol-$port_final-source','$Protocol','$port_final','1-65535','0','$source','$vsys');");
                                                    $serviceID = $projectdb->insert_id;
                                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                } else {
                                                    $data = $getService->fetch_assoc();
                                                    $existingname = $data['name'];
                                                    $serviceID = $data['id'];
                                                    $addMemberID[] = "('$lidgroup','services', $serviceID,'$source','$vsys')";
                                                }
                                            } else {
                                                $searchname = $projectdb->query("SELECT id,dport,protocol FROM services WHERE source='$source' AND BINARY name_ext='$port' AND vsys='$vsys';");
                                                if ($searchname->num_rows == 0) {
                                                    add_log('error', 'Reading Services Objects and Groups', 'Unknown Service [' . $HostGroupName . '] source-port-name mapping for GT : ' . $port, $source, 'Using 6500 port. Change it from the GUI');
                                                    $port_final = "6500";
                                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport, dport,checkit,source,vsys) values ('$port-source','$port-source','$Protocol','$port_final','1-65535','0','$source','$vsys');");
                                                    $serviceID = $projectdb->insert_id;
                                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                } else {
                                                    $searchnameData = $searchname->fetch_assoc();
                                                    $temp_dport = $searchnameData['dport'] + 1;
                                                    $temp_protocol = $searchnameData['protocol'];
                                                    $port_final = $temp_dport . "-65535";
                                                    $check = $projectdb->query("SELECT name_ext FROM services WHERE source='$source' AND vsys='$vsys' AND sport='$port_final' AND dport='1-65535' AND protocol='$temp_protocol' LIMIT 1;");
                                                    if ($check->num_rows == 1) {
                                                        $checkData = $check->fetch_assoc();
                                                        $tmp_name = $checkData['name_ext'];
                                                        $serviceID = $checkData['id'];
                                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                    } else {
                                                        $tmp_name = $temp_protocol . "-" . $port_final;
                                                        $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport, dport,checkit,source,vsys) values ('$tmp_name-source','$tmp_name-source','$temp_protocol','$port_final','1-65535','0','$source','$vsys');");
                                                        $serviceID = $projectdb->insert_id;
                                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                    }
                                                    // Todo: hay que leer el proto i el dport sumarle uno y poner 65535 y crearlo y añadir como member

    //                                        $addMember[] = "('$lidgroup','$port','$source','$vsys')";
                                                }
                                            }
                                        }
                                        elseif ($port == "lt") {
                                            $port = $netObj[$next];
                                            if (is_numeric($port)) {
                                                $port = $port - 1;
                                                $port_final = "0-" . $port;
                                                $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND protocol='$Protocol' AND sport='$port_final' AND dport='1-65535' AND vsys='$vsys';");
                                                if ($getService->num_rows == 0) {
                                                    # Create it
                                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport,dport,checkit,source,vsys) values ('$Protocol-$port_final-source','$Protocol-$port_final-source','$Protocol','$port_final','1-65535','0','$source','$vsys');");
                                                    $serviceID = $projectdb->insert_id;
                                                    $addMemberID[] = "('$lidgroup','services', $serviceID,'$source','$vsys')";
                                                } else {
                                                    $data = $getService->fetch_assoc();
                                                    $existingname = $data['name'];
                                                    $serviceID =$data['id'];
                                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                }
                                            } else {
                                                $searchname = $projectdb->query("SELECT id,dport,protocol FROM services WHERE source='$source' AND BINARY name_ext='$port' AND dport='1-65535' AND vsys='$vsys';");
                                                if ($searchname->num_rows == 0) {
                                                    add_log('error', 'Reading Services Objects and Groups', 'Unknown Service [' . $HostGroupName . '] port-name mapping for LT : ' . $port, $source, 'Using 6500 port. Change it from the GUI');
                                                    $port_final = "6500";
                                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport, dport,checkit,source,vsys) values ('$port-source','$port-source','$Protocol','$port_final','1-65535','0','$source','$vsys');");
                                                    $serviceID = $projectdb->insert_id;
                                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                }
                                                else {
                                                    $searchnameData = $searchname->fetch_assoc();
                                                    $temp_dport = $searchnameData['dport'] - 1;
                                                    $temp_protocol = $searchnameData['protocol'];
                                                    $port_final = "0-" . $temp_dport;
                                                    $check = $projectdb->query("SELECT name_ext, id FROM services WHERE source='$source' AND vsys='$vsys' AND sport='$port_final' AND dport='1-65535' AND protocol='$temp_protocol' LIMIT 1;");
                                                    if ($check->num_rows == 1) {
                                                        $checkData = $check->fetch_assoc();
                                                        $tmp_name = $checkData['name_ext'];
                                                        $serviceID = $checkData['id'];
                                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                    } else {
                                                        $tmp_name = $temp_protocol . "-" . $port_final;
                                                        $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport, dport,checkit,source,vsys) values ('$tmp_name-source','$tmp_name-source','$temp_protocol','$port_final','1-65535','0','$source','$vsys');");
                                                        $serviceID = $projectdb->insert_id;
                                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                    }
    //                                        $addMember[] = "('$lidgroup','$port','$source','$vsys')";
                                                }
                                            }

                                        }
                                        elseif ($port == "range") {
                                            $port_first = $netObj[$next]; //3
                                            $port_last = rtrim($netObj[$next + 1]); //4

                                            if (is_numeric($port_first)) {
                                                $port_first_port = $port_first;
                                            } else {
                                                # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                                                $searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND BINARY name_ext='$port_first' AND vsys='$vsys';");
                                                $cisconame = $searchname->fetch_assoc();
                                                $port_first_port = $cisconame['dport'];

                                                if ($port_first_port == "") {
                                                    add_log('error', 'Reading Services Objects and Groups', 'Unknown Service [' . $HostGroupName . '] source-port-name mapping for: ' . $port_first, $source, 'Using 6500 port. Change it from the GUI');
                                                    $port_first_port = "6500";
                                                }
                                            }
                                            if (is_numeric($port_last)) {
                                                $port_last_port = $port_last;
                                            } else {
                                                # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                                                $searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND BINARY name_ext='$port_last' AND vsys='$vsys';");
                                                $cisconame = $searchname->fetch_assoc();
                                                $port_last_port = $cisconame['dport'];

                                                if ($port_last_port == "") {
                                                    add_log('error', 'Reading Services Objects and Groups', 'Unknown Service-Range  [' . $HostGroupName . '] source-port-name mapping for: ' . $port_first, $source, 'Using 6500 port. Change it from the GUI');
                                                    $port_last_port = "6500";
                                                }
                                            }

                                            # Check first if they are EQUAL
                                            if ($port_first_port == $port_last_port) {
                                                $isRange = "";
                                                $LastPort = "";
                                                $vtype = "";
                                                add_log('warning', 'Reading Services Objects and Groups', 'Moving Service-Range to Service [' . $names_line . '] ports are the same', $source, 'No Action Required');
                                            } else {
                                                $isRange = "-range";
                                                $LastPort = "-$port_last_port";
                                                $vtype = "range";
                                            }

                                            $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND BINARY name_ext='$Protocol" . $isRange . "-$port_first_port" . $LastPort . "-source' AND vsys='$vsys';");
                                            if ($getService->num_rows == 0) {
                                                # Create it
                                                $projectdb->query("INSERT INTO services (name,name_ext,protocol,sport,dport,checkit,source,type,vsys) values ('$Protocol" . $isRange . "-$port_first_port" . $LastPort . "-source','$Protocol" . $isRange . "-$port_first_port" . $LastPort . "-source','$Protocol','$port_first_port" . $LastPort . "','1-65535','0','$source','$vtype','$vsys');");
                                                $serviceID = $projectdb->insert_id;
                                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                            } else {
                                                #Assign to the service-object in service get table and lid
                                                $getServiceData = $getService->fetch_assoc();
                                                $service_name = $getServiceData['name'];
                                                $serviceID = $getServiceData['id'];
                                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                            }
                                        }*/
                                    }
                                }
                                elseif( $netObj[2] == "destination" )
                                {
                                    $next = 4;
                                    $port = $netObj[3];
                                    $protocols = ['tcp', 'udp'];
                                    foreach( $protocols as $Protocol )
                                    {

                                        if( !isset($port) )
                                        {
                                            $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($Protocol, '1-65535', '');

                                            if( $tmp_service === null )
                                            {
                                                $tmp_service = $v->serviceStore->find($name_ext);
                                                if( $tmp_service === null )
                                                {
                                                    $tmp_service = $v->serviceStore->newService($Protocol . "-All", $Protocol, '1-65535');
                                                }
                                                $tmp_servicegroup->addMember($tmp_service);
                                            }
                                            else
                                                $tmp_servicegroup->addMember($tmp_service);
                                        }

                                        if( $port == "eq" )
                                        {
                                            $port = $netObj[$next]; // 3

                                            if( is_numeric($port) )
                                            {
                                                $port_final = $port;
                                                $final_source_port = "";
                                                $final_destination_port = $port_final;
                                                $final_protocol = $Protocol;
                                                //name,name_ext,protocol,sport,dport,
                                                $name = $Protocol . "-" . $port_final;
                                                $name_ext = $name;
                                                #$getService = $projectdb->query("SELECT name FROM services WHERE source='$source' AND protocol='$Protocol' AND sport='$port_final' AND dport='1-65535' AND vsys='$vsys';");

                                                $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port);
                                            }
                                            else
                                            {
                                                $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port . 'Using 6500 port. Change it from the GUI';
                                                $port_final = "6500";
                                                $final_source_port = "";
                                                $final_destination_port = $port_final;

                                                $final_protocol = $Protocol;
                                                $name = $port;
                                                $name_ext = $name;
                                                #$getService = $projectdb->query("SELECT id FROM services WHERE source='$source' AND BINARY name_ext='$port' AND vsys='$vsys';");

                                                $tmp_service = $v->serviceStore->find($port);
                                            }

                                            if( $tmp_service === null )
                                            {
                                                $tmp_service = $v->serviceStore->find($name_ext);
                                                if( $tmp_service === null )
                                                {
                                                    if( $print )
                                                        print "  * create service: " . $name_ext . "\n";
                                                    $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                }
                                                if( $addlog != "" )
                                                    $tmp_service->set_node_attribute('error', $addlog);
                                                if( $print )
                                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                $tmp_servicegroup->addMember($tmp_service);
                                            }
                                            else
                                            {
                                                if( $print )
                                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                $tmp_servicegroup->addMember($tmp_service);
                                            }
                                        }
                                        elseif( $port == "gt" )
                                        {
                                            $port = $netObj[$next];

                                            if( is_numeric($port) )
                                            {
                                                $port = $port + 1;
                                                $port_final = $port . "-65535";
                                                $final_source_port = "";
                                                $final_destination_port = $port_final;
                                                $final_protocol = $Protocol;
                                                $name = $Protocol . "-" . $port_final;
                                                $name_ext = $name;

                                                $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port, $final_source_port);

                                                if( $tmp_service === null )
                                                {
                                                    $tmp_service = $v->serviceStore->find($name_ext);
                                                    if( $tmp_service === null )
                                                    {
                                                        if( $print )
                                                            print "  * create service: " . $name_ext . "\n";
                                                        $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                    }
                                                    if( $addlog != "" )
                                                        $tmp_service->set_node_attribute('error', $addlog);
                                                    if( $print )
                                                        print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                    $tmp_servicegroup->addMember($tmp_service);
                                                }
                                                else
                                                {
                                                    if( $print )
                                                        print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                    $tmp_servicegroup->addMember($tmp_service);
                                                }
                                            }
                                            else
                                            {
                                                $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for GT : ' . $port . 'Using 6500 port. Change it from the GUI';
                                                $port_final = "6500";
                                                $final_source_port = "";
                                                $final_destination_port = $port_final;
                                                $final_protocol = $Protocol;
                                                $name = $port . "-source";
                                                $name_ext = $name;

                                                $tmp_service = $v->serviceStore->find($port);

                                                if( $tmp_service === null )
                                                {
                                                    $tmp_service = $v->serviceStore->find($name_ext);
                                                    if( $tmp_service === null )
                                                    {
                                                        if( $print )
                                                            print "  * create service: " . $name_ext . "\n";
                                                        $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                    }
                                                    if( $addlog != "" )
                                                        $tmp_service->set_node_attribute('error', $addlog);
                                                    if( $print )
                                                        print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                    $tmp_servicegroup->addMember($tmp_service);
                                                }
                                                else
                                                {
                                                    $temp_dport = $tmp_service->getDestPort() + 1;
                                                    $temp_protocol = $tmp_service->protocol();
                                                    $port_final = $temp_dport . "-65535";
                                                    $final_source_port = "";
                                                    $final_destination_port = $port_final;
                                                    $final_protocol = $temp_protocol;
                                                    $name = $temp_protocol . "-" . $port_final;
                                                    $name_ext = $name;

                                                    $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port, $final_source_port);

                                                    if( $tmp_service === null )
                                                    {
                                                        $tmp_service = $v->serviceStore->find($name_ext);
                                                        if( $tmp_service === null )
                                                        {
                                                            if( $print )
                                                                print "  * create service: " . $name_ext . "\n";
                                                            $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                        }
                                                        if( $addlog != "" )
                                                            $tmp_service->set_node_attribute('error', $addlog);
                                                        if( $print )
                                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                        $tmp_servicegroup->addMember($tmp_service);
                                                    }
                                                    else
                                                    {
                                                        if( $print )
                                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                        $tmp_servicegroup->addMember($tmp_service);
                                                    }
                                                }
                                            }
                                        }
                                        elseif( $port == "lt" )
                                        {
                                            $port = $netObj[$next];

                                            if( is_numeric($port) )
                                            {
                                                $port = $port - 1;
                                                $port_final = "0-" . $port;
                                                $final_source_port = "";
                                                $final_destination_port = $port_final;
                                                $final_protocol = $Protocol;
                                                $name = $Protocol . "-" . $port_final;
                                                $name_ext = $name;

                                                $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port, $final_source_port);

                                                if( $tmp_service === null )
                                                {
                                                    $tmp_service = $v->serviceStore->find($name_ext);
                                                    if( $tmp_service === null )
                                                    {
                                                        if( $print )
                                                            print "  * create service: " . $name_ext . "\n";
                                                        $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                    }
                                                    if( $addlog != "" )
                                                        $tmp_service->set_node_attribute('error', $addlog);
                                                    if( $print )
                                                        print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                    $tmp_servicegroup->addMember($tmp_service);
                                                }
                                                else
                                                {
                                                    if( $print )
                                                        print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                    $tmp_servicegroup->addMember($tmp_service);
                                                }
                                            }
                                            else
                                            {
                                                $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for LT : ' . $port . 'Using 6500 port. Change it from the GUI';
                                                $port_final = "6500";

                                                $final_source_port = "";
                                                $final_destination_port = $port_final;
                                                $final_protocol = $Protocol;
                                                $name = $port;
                                                $name_ext = $name;

                                                $tmp_service = $v->serviceStore->find($name_ext);

                                                if( $tmp_service === null )
                                                {
                                                    if( $print )
                                                        print "  * create service: " . $name_ext . "\n";
                                                    $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                    if( $addlog != "" )
                                                        $tmp_service->set_node_attribute('error', $addlog);
                                                    if( $print )
                                                        print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                    $tmp_servicegroup->addMember($tmp_service);
                                                }
                                                else
                                                {
                                                    $temp_dport = $tmp_service->getDestPort() - 1;
                                                    $temp_protocol = $tmp_service->protocol();
                                                    $port_final = "0-" . $temp_dport;
                                                    $final_source_port = "";
                                                    $final_destination_port = $port_final;
                                                    $final_protocol = $temp_protocol;
                                                    $name = $temp_protocol . "-" . $port_final;
                                                    $name_ext = $name;

                                                    $tmp_service = $v->serviceStore->findByProtocolDstSrcPort($final_protocol, $final_destination_port, $final_source_port);

                                                    if( $tmp_service === null )
                                                    {
                                                        $tmp_service = $v->serviceStore->find($name_ext);
                                                        if( $tmp_service === null )
                                                        {
                                                            if( $print )
                                                                print "  * create service: " . $name_ext . "\n";
                                                            $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                        }
                                                        if( $addlog != "" )
                                                            $tmp_service->set_node_attribute('error', $addlog);
                                                        if( $print )
                                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                        $tmp_servicegroup->addMember($tmp_service);
                                                    }
                                                    else
                                                    {
                                                        if( $print )
                                                            print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                        $tmp_servicegroup->addMember($tmp_service);
                                                    }
                                                }
                                            }
                                        }
                                        elseif( $port == "range" )
                                        {
                                            $port_first = $netObj[$next]; //3
                                            $port_last = rtrim($netObj[$next + 1]); //4

                                            /*
                                            if (is_numeric($port_first)) {
                                                $port_first_port = $port_first;
                                            }
                                            else {
                                                $port_first_port = $this->search_for_service_port($v, $HostGroupName, $names_line, $port_first );

                                                if ($port_first_port == "") {
                                                    $addlog = 'Unknown Service-Range [' . $HostGroupName . '] port-name mapping for: ' . $port_first . 'Using 6500 port. Change it from the GUI';
                                                    $port_first_port = "6500";
                                                }
                                            }
                                            if (is_numeric($port_last)) {
                                                $port_last_port = $port_last;
                                            }
                                            else {
                                                # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                                                $tmp_service = $v->serviceStore->find( $port_last );
                                                if( $tmp_service !== null )
                                                {
                                                    $port_first_port = $tmp_service->getDestPort();
                                                }
                                                else
                                                {
                                                    $addlog = 'Unknown Service-Range  [' . $HostGroupName . '] port-name mapping for: ' . $port_first .  'Using 6500 port. Change it from the GUI';
                                                    $port_last_port = "6500";
                                                }
                                            }*/

                                            $tmp_range_port = $this->range_get_ports($port_first, $port_last, $v, $HostGroupName, $HostGroupNamePan, $names_line);
                                            $port_first_port = $tmp_range_port[0];
                                            $port_last_port = $tmp_range_port[1];


                                            # Check first if they are EQUAL
                                            if( $port_first_port == $port_last_port )
                                            {
                                                $isRange = "";
                                                $LastPort = "";
                                                $vtype = "";
                                                $addlog = 'Moving Service-Range to Service [' . $names_line . '] ports are the same - No Action Required';
                                            }
                                            else
                                            {
                                                $isRange = "-range";
                                                $isRange = "";
                                                $LastPort = "-" . $port_last_port;
                                                $vtype = "range";
                                            }

                                            #$name_ext = $Protocol . $port_first_port . $LastPort;
                                            $name_ext = $Protocol . $port_first_port . $LastPort;
                                            $final_protocol = $Protocol;
                                            $final_source_port = "";
                                            $final_destination_port = $port_first_port . $LastPort;
                                            $tmp_service = $v->serviceStore->find($name_ext);

                                            if( $tmp_service === null )
                                            {
                                                if( $print )
                                                    print "  * create service: " . $name_ext . "\n";
                                                $tmp_service = $v->serviceStore->newService($name_ext, $final_protocol, $final_destination_port, "", $final_source_port);
                                                if( $addlog != "" )
                                                    $tmp_service->set_node_attribute('error', $addlog);
                                                if( $print )
                                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                $tmp_servicegroup->addMember($tmp_service);
                                            }
                                            else
                                            {
                                                if( $print )
                                                    print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                                $tmp_servicegroup->addMember($tmp_service);
                                            }
                                        }

                                        /*
                                        if (!isset($port)) {
                                            $getDup = $projectdb->query("SELECT name,id FROM services WHERE source='$source' AND dport='1-65535' AND protocol='$Protocol' AND vsys='$vsys' LIMIT 1;");
                                            if ($getDup->num_rows == 0) {
                                                $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$Protocol-All','$Protocol-All','$Protocol','1-65535','0','$source','$vsys');");
                                                $serviceID = $projectdb->insert_id;
                                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                            } else {
                                                $data = $getDup->fetch_assoc();
                                                $existingname = $data['name'];
                                                $serviceID = $data['id'];
                                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                            }
                                        }

                                        if ($port == "eq") {
                                            $port = $netObj[$next]; // 3
                                            if (is_numeric($port)) {
                                                $port_final = $port;
                                                $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND protocol='$Protocol' AND dport='$port_final' AND vsys='$vsys';");
                                                if ($getService->num_rows == 0) {
                                                    # Create it
                                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$Protocol-$port_final','$Protocol-$port_final','$Protocol','$port_final','0','$source','$vsys');");
                                                    $serviceID = $projectdb->insert_id;
                                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                } else {
                                                    $data = $getService->fetch_assoc();
                                                    $existingname = $data['name'];
                                                    $serviceID =$data['id'];
                                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                }
                                            } else {
                                                $searchname = $projectdb->query("SELECT id FROM services WHERE source='$source' AND BINARY name_ext='$port' AND vsys='$vsys';");
                                                if ($searchname->num_rows == 0) {
                                                    add_log('error', 'Reading Services Objects and Groups', 'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port, $source, 'Using 6500 port. Change it from the GUI');
                                                    $port_final = "6500";
                                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$port','$port','$Protocol','$port_final','0','$source','$vsys');");
                                                    $serviceID = $projectdb->insert_id;
                                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                } else {
                                                    $data = $searchname->fetch_assoc();
                                                    $serviceID = $data['id'];
                                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                }
                                            }
                                        } elseif ($port == "gt") {
                                            $port = $netObj[$next];
                                            if (is_numeric($port)) {
                                                $port = $port + 1;
                                                $port_final = $port . "-65535";
                                                $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND protocol='$Protocol' AND dport='$port_final' AND vsys='$vsys';");
                                                if ($getService->num_rows == 0) {
                                                    # Create it
                                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$Protocol-$port_final','$Protocol-$port_final','$Protocol','$port_final','0','$source','$vsys');");
                                                    $serviceID = $projectdb->insert_id;
                                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                } else {
                                                    $data = $getService->fetch_assoc();
                                                    $existingname = $data['name'];
                                                    $serviceID = $data['id'];
                                                    $addMemberID[] = "('$lidgroup','services', $serviceID,'$source','$vsys')";
                                                }
                                            } else {
                                                $searchname = $projectdb->query("SELECT id,dport,protocol FROM services WHERE source='$source' AND BINARY name_ext='$port' AND vsys='$vsys';");
                                                if ($searchname->num_rows == 0) {
                                                    add_log('error', 'Reading Services Objects and Groups', 'Unknown Service [' . $HostGroupName . '] port-name mapping for GT : ' . $port, $source, 'Using 6500 port. Change it from the GUI');
                                                    $port_final = "6500";
                                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$port','$port','$Protocol','$port_final','0','$source','$vsys');");
                                                    $serviceID = $projectdb->insert_id;
                                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                } else {
                                                    $searchnameData = $searchname->fetch_assoc();
                                                    $temp_dport = $searchnameData['dport'] + 1;
                                                    $temp_protocol = $searchnameData['protocol'];
                                                    $port_final = $temp_dport . "-65535";
                                                    $check = $projectdb->query("SELECT name_ext, id FROM services WHERE source='$source' AND vsys='$vsys' AND dport='$port_final' AND protocol='$temp_protocol' LIMIT 1;");
                                                    if ($check->num_rows == 1) {
                                                        $checkData = $check->fetch_assoc();
                                                        $tmp_name = $checkData['name_ext'];
                                                        $serviceID = $checkData['id'];
                                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                    } else {
                                                        $tmp_name = $temp_protocol . "-" . $port_final;
                                                        $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$tmp_name','$tmp_name','$temp_protocol','$port_final','0','$source','$vsys');");
                                                        $serviceID = $projectdb->insert_id;
                                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                    }
                                                    // Todo: hay que leer el proto i el dport sumarle uno y poner 65535 y crearlo y añadir como member

    //                                        $addMember[] = "('$lidgroup','$port','$source','$vsys')";
                                                }
                                            }
                                        } elseif ($port == "lt") {
                                            $port = $netObj[$next];
                                            if (is_numeric($port)) {
                                                $port = $port - 1;
                                                $port_final = "0-" . $port;
                                                $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND protocol='$Protocol' AND dport='$port_final' AND vsys='$vsys';");
                                                if ($getService->num_rows == 0) {
                                                    # Create it
                                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$Protocol-$port_final','$Protocol-$port_final','$Protocol','$port_final','0','$source','$vsys');");
                                                    $serviceID = $projectdb->insert_id;
                                                    $addMemberID[] = "('$lidgroup','services', $serviceID,'$source','$vsys')";
                                                } else {
                                                    $data = $getService->fetch_assoc();
                                                    $existingname = $data['name'];
                                                    $serviceID = $data['id'];
                                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                }
                                            } else {
                                                $searchname = $projectdb->query("SELECT id,dport,protocol FROM services WHERE source='$source' AND BINARY name_ext='$port' AND vsys='$vsys';");
                                                if ($searchname->num_rows == 0) {
                                                    add_log('error', 'Reading Services Objects and Groups', 'Unknown Service [' . $HostGroupName . '] port-name mapping for LT : ' . $port, $source, 'Using 6500 port. Change it from the GUI');
                                                    $port_final = "6500";
                                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$port','$port','$Protocol','$port_final','0','$source','$vsys');");
                                                    $serviceID = $projectdb->insert_id;
                                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                } else {
                                                    $searchnameData = $searchname->fetch_assoc();
                                                    $temp_dport = $searchnameData['dport'] - 1;
                                                    $temp_protocol = $searchnameData['protocol'];
                                                    $port_final = "0-" . $temp_dport;
                                                    $check = $projectdb->query("SELECT name_ext, id FROM services WHERE source='$source' AND vsys='$vsys' AND dport='$port_final' AND protocol='$temp_protocol' LIMIT 1;");
                                                    if ($check->num_rows == 1) {
                                                        $checkData = $check->fetch_assoc();
                                                        $tmp_name = $checkData['name_ext'];
                                                        $serviceID = $checkData['id'];
                                                        $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                                    } else {
                                                        $tmp_name = $temp_protocol . "-" . $port_final;
                                                        $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys) values ('$tmp_name','$tmp_name','$temp_protocol','$port_final','0','$source','$vsys');");
                                                        $serviceID = $projectdb->insert_id;
                                                        $addMemberID[] = "('$lidgroup','services', $serviceID,'$source','$vsys')";
                                                    }
    //                                        $addMember[] = "('$lidgroup','$port','$source','$vsys')";
                                                }
                                            }

                                        } elseif ($port == "range") {
                                            $port_first = $netObj[$next]; //3
                                            $port_last = rtrim($netObj[$next + 1]); //4

                                            if (is_numeric($port_first)) {
                                                $port_first_port = $port_first;
                                            } else {
                                                # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                                                $searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND BINARY name_ext='$port_first' AND vsys='$vsys';");
                                                $cisconame = $searchname->fetch_assoc();
                                                $port_first_port = $cisconame['dport'];

                                                if ($port_first_port == "") {
                                                    add_log('error', 'Reading Services Objects and Groups', 'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port_first, $source, 'Using 6500 port. Change it from the GUI');
                                                    $port_first_port = "6500";
                                                }
                                            }
                                            if (is_numeric($port_last)) {
                                                $port_last_port = $port_last;
                                            } else {
                                                # IS NAME TO SEARCH IN vendor_services_mapping TABLE
                                                $searchname = $projectdb->query("SELECT dport FROM services WHERE source='$source' AND BINARY name_ext='$port_last' AND vsys='$vsys';");
                                                $cisconame = $searchname->fetch_assoc();
                                                $port_last_port = $cisconame['dport'];

                                                if ($port_last_port == "") {
                                                    add_log('error', 'Reading Services Objects and Groups', 'Unknown Service-Range  [' . $HostGroupName . '] port-name mapping for: ' . $port_first, $source, 'Using 6500 port. Change it from the GUI');
                                                    $port_last_port = "6500";
                                                }
                                            }

                                            # Check first if they are EQUAL
                                            if ($port_first_port == $port_last_port) {
                                                $isRange = "";
                                                $LastPort = "";
                                                $vtype = "";
                                                add_log('warning', 'Reading Services Objects and Groups', 'Moving Service-Range to Service [' . $names_line . '] ports are the same', $source, 'No Action Required');
                                            } else {
                                                $isRange = "-range";
                                                $LastPort = "-$port_last_port";
                                                $vtype = "range";
                                            }

                                            $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND BINARY name_ext='$Protocol" . $isRange . "-$port_first_port" . $LastPort . "' AND vsys='$vsys';");
                                            if ($getService->num_rows == 0) {
                                                # Create it
                                                $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,type,vsys) values ('$Protocol" . $isRange . "-$port_first_port" . $LastPort . "','$Protocol" . $isRange . "-$port_first_port" . $LastPort . "','$Protocol','$port_first_port" . $LastPort . "','0','$source','$vtype','$vsys');");
                                                $serviceID = $projectdb->insert_id;
                                                $addMemberID[] = "('$lidgroup','services', $serviceID,'$source','$vsys')";
                                            } else {
                                                #Assign to the service-object in service get table and lid
                                                $getServiceData = $getService->fetch_assoc();
                                                $service_name = $getServiceData['name'];
                                                $serviceID = $getServiceData['id'];
                                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                            }
                                        }*/
                                    }
                                }
                            }
                            else
                            {
                                //Todo: SVEN
                                #continue;

                                /*
                                $getDup = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND dport='1-65535' AND protocol='tcp' AND vsys='$vsys' LIMIT 1;");
                                if ($getDup->num_rows == 0) {
                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys,type,description) values ('all_TCP_ports','all_TCP_ports','tcp','1-65535','0','$source','$vsys','range','All TCP ports');");
                                    $serviceID = $projectdb->insert_id;
                                    $addMemberID[] = "('$lidgroup','services', $serviceID,'$source','$vsys')";
                                }
                                else {
                                    $data = $getDup->fetch_assoc();
                                    $existingname = $data['name'];
                                    $serviceID = $data['id'];
                                    $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                                }

                                $getDup = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND dport='1-65535' AND protocol='udp' AND vsys='$vsys' LIMIT 1;");
                                if ($getDup->num_rows == 0) {
                                    $projectdb->query("INSERT INTO services (name,name_ext,protocol,dport,checkit,source,vsys,type,description) values ('all_UDP_ports','all_UDP_ports','udp','1-65535','0','$source','$vsys','range','All TCP ports');");
                                    $serviceID = $projectdb->insert_id;
                                    $addMemberID[] = "('$lidgroup','services', $serviceID,'$source','$vsys')";
                                }
                                else {
                                    $data = $getDup->fetch_assoc();
                                    $existingname = $data['name'];
                                    $serviceID = $data['id'];
                                    $addMemberID[] = "('$lidgroup','services', $serviceID,'$source','$vsys')";
                                }
                                */
                                $Protocol_array = array("tcp", "udp");
                                foreach( $Protocol_array as $Protocol )
                                {
                                    $tmp_service = $v->serviceStore->find($Protocol . "-All");
                                    if( $tmp_service === null )
                                    {
                                        if( $print )
                                            print "  * create service: " . $Protocol . "-All\n";
                                        $tmp_service = $v->serviceStore->newService($Protocol . "-All", $Protocol, "1-65535");
                                    }
                                    if( $print )
                                        print "   * add service: " . $tmp_service->name() . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                    $tmp_servicegroup->addMember($tmp_service);
                                }
                            }

                        }
                        else
                        {
                            //Todo: SVEN
                            #mwarning( "add service with Protocol: ".$Protocol." add something like service Port 1: ".$names_line );


                            $tmp_service = $v->serviceStore->find("tmp-" . $Protocol);
                            if( $tmp_service === null )
                            {
                                if( $print )
                                    print " * create service Object: tmp-" . $Protocol . ", tcp, 6500\n";
                                $tmp_service = $v->serviceStore->newService("tmp-" . $Protocol, "tcp", "6500");
                                $tmp_service->set_node_attribute('error', "no service protocol - tcp is used - 6500");
                                $tmp_servicegroup->addMember($tmp_service);
                            }
                            else
                                $tmp_servicegroup->addMember($tmp_service);

                            /*
                            //TODO: we should remove all the other ports for this security rule
                            $getService = $projectdb->query("SELECT name, id FROM services WHERE source='$source' AND vsys='$vsys' AND protocol='$Protocol' LIMIT 1;");
                            if ($getService->num_rows == 0) {
                                $projectdb->query("INSERT INTO services (name,name_ext,checkit,source,vsys,protocol) values ('$Protocol','$Protocol','1','$source','$vsys','$Protocol');");
                                $serviceID = $projectdb->insert_id;
                                $addMemberID[] = "('$lidgroup','services', $serviceID,'$source','$vsys')";
                            } else {
                                $getData = $getService->fetch_assoc();
                                $servicename = $getData['name'];
                                $serviceID = $getData['id'];
                                $addMemberID[] = "('$lidgroup','services',$serviceID,'$source','$vsys')";
                            }
                            */
                        }
                    }
                }
            }
        }

        /*
        if (count($addMember) > 0) {
            $unique = array_unique($addMember);
            $query = "INSERT INTO services_groups (lid,member,source,vsys) VALUES " . implode(",", $unique) . ";";
            $projectdb->query($query);
            unset($addMember);
        }

        if (count($addMemberID) > 0) {
            $unique = array_unique($addMemberID);
            $query = "INSERT INTO services_groups (lid,table_name, member_lid, source,vsys) VALUES " . implode(",", $unique) . ";";
            $projectdb->query($query);
            unset($addMemberID);
        }
        */
    }

    function search_for_service_port($v, $ObjectServiceNamePan, $names_line, $port)
    {
        $port_final = "";
        $tmp_service = $v->serviceStore->find($port);
        if( $tmp_service !== null )
        {
            if( $tmp_service->isService() )
                $port_final = $tmp_service->getDestPort();
            else
            {
                $tmp_service = $v->serviceStore->find("tcp-" . $port);
                if( $tmp_service !== null )
                {
                    if( $tmp_service->isService() )
                        $port_final = $tmp_service->getDestPort();
                }
                else
                {
                    $tmp_service = $v->serviceStore->find("udp-" . $port);
                    if( $tmp_service !== null )
                    {
                        if( $tmp_service->isService() )
                            $port_final = $tmp_service->getDestPort();
                    }
                    else
                        mwarning("servicegroup found: " . $port . " with line: " . $names_line);
                }
            }
        }
        else
        {
            $tmp_service = $v->serviceStore->find("tmp-" . $port);
            if( $tmp_service !== null )
                $port_final = $tmp_service->getDestPort();
            else
                mwarning("service not found: tmp-" . $port . " name: " . $ObjectServiceNamePan . " line: " . $names_line);
        }

        return $port_final;
    }


    function range_get_ports($port_first, $port_last, $v, $HostGroupName, $HostGroupNamePan, $names_line)
    {
        if( is_numeric($port_first) )
        {
            $port_first_port = $port_first;
        }
        else
        {
            $port_first_port = $this->search_for_service_port($v, $HostGroupNamePan, $names_line, $port_first);

            if( $port_first_port == "" )
            {
                $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port_first . 'Using 6500 port. Change it from the GUI';
                $port_first_port = "6500";
            }
        }
        if( is_numeric($port_last) )
        {
            $port_last_port = $port_last;
        }
        else
        {

            $port_last_port = $this->search_for_service_port($v, $HostGroupNamePan, $names_line, $port_last);

            if( $port_last_port == "" )
            {
                $addlog = 'Unknown Service [' . $HostGroupName . '] port-name mapping for: ' . $port_last . 'Using 6500 port. Change it from the GUI';
                $port_last_port = "6500";
            }
        }

        return array($port_first_port, $port_last_port);
    }

}
