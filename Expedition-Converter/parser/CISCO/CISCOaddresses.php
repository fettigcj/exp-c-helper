<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */

trait CISCOaddresses
{
    //TODO: Change this function to use Arrays instead of so many SQL Queries
#function get_object_network($cisco_config_file, $source, $vsys) {
    public function get_object_network($data, $v)
    {
        global $projectdb;

        global $debug;
        global $print;

        $addHost = array();
        $AddDescription = array();
        $isObjectNetwork = 0;
        $nat_lid = "";
        $ObjectNetworkName = "";
        $ObjectNetworkNamePan = "";
        $vsys = $v->name();

        $source = "";

        foreach( $data as $line => $names_line )
        {
            #$names_line = strip_hidden_chars($names_line);
            $names_line = trim($names_line);

            if( $names_line == "!" || preg_match("/^object-group network/i", $names_line) )
            {
                $isObjectNetwork = 0;
            }

            if( $isObjectNetwork == 1 )
            {
                $found = FALSE;
                $tmp_address = $v->addressStore->find($ObjectNetworkNamePan);

                if( preg_match("/^host/i", $names_line) )
                {
                    $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                    $ipaddress = $netObj[1];

                    if( $tmp_address !== null )
                    {
                        if( $tmp_address->isAddress() && $tmp_address->isType_ipNetmask() && $ipaddress == $tmp_address->value() )
                            $found = TRUE;
                    }

                    if( !$found && $tmp_address === null )
                    {
                        $ipversion = $this->ip_version($ipaddress);
                        if( $ipversion == "v4" )
                        {
                            $hostCidr = "32";
                            $addHost["ipv4"][] = array($ObjectNetworkNamePan, 'ip-netmask', $ObjectNetworkName, '0', $source, '0', $ipaddress, $hostCidr, '1', $vsys, 'object');
                        }
                        elseif( $ipversion == "v6" )
                        {
                            $hostCidr = "128";
                            $addHost["ipv6"][] = array($ObjectNetworkNamePan, 'ip-netmask', $ObjectNetworkName, '0', $source, '0', $ipaddress, $hostCidr, '1', $vsys, 'object', '0');
                        }
                    }
                    else
                        mwarning("object: " . $ObjectNetworkNamePan . " already available", null, FALSE);
                    /*
                    $search = $projectdb->query("SELECT id FROM address WHERE source='$source' AND BINARY name_ext='$ObjectNetworkName' AND ipaddress='$ipaddress' AND vsys='$vsys' AND vtype='object';");
                    if ($search->num_rows == 0) {
                        $ipversion = $this->ip_version($ipaddress);
                        if ($ipversion == "v4") {
                            $hostCidr = "32";
                            $addHost["ipv4"][] = "('$ObjectNetworkNamePan','ip-netmask','$ObjectNetworkName','0','$source','0','$ipaddress','$hostCidr','1','$vsys','object')";
                        } elseif ($ipversion == "v6") {
                            $hostCidr = "128";
                            $addHost["ipv6"][] = "('$ObjectNetworkNamePan','ip-netmask','$ObjectNetworkName','0','$source','0','$ipaddress','$hostCidr','1','$vsys','object','0')";
                        }

                    }
                    */
                }
                elseif( preg_match("/^\bfqdn\b/i", $names_line) )
                {
                    $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                    $fqdn = $netObj[1];
                    if( ($fqdn == "v4") or ($fqdn == "v6") )
                    {
                        $fqdn = $netObj[2];
                    }

                    if( $tmp_address !== null )
                    {
                        if( $tmp_address->isAddress() && $tmp_address->isType_FQDN() && $fqdn == $tmp_address->value() )
                            $found = TRUE;
                    }

                    if( !$found )
                    {
                        $addHost["ipv4"][] = array($ObjectNetworkNamePan, 'fqdn', $ObjectNetworkName, '0', $source, '0', $fqdn, '', '1', $vsys, 'object');
                    }
                    /*
                    $search = $projectdb->query("SELECT id FROM address WHERE source='$source' AND BINARY name_ext='$ObjectNetworkName' AND fqdn='$fqdn' AND vsys='$vsys' AND vtype='object';");
                    if ($search->num_rows == 0) {
                        $addHost["ipv4"][] = "('$ObjectNetworkNamePan','fqdn','$ObjectNetworkName','0','$source','0','$fqdn','','1','$vsys','object')";
                    }
                    */
                }
                elseif( preg_match("/^\bsubnet\b/i", $names_line) )
                {
                    $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                    $ipaddress = $netObj[1];

                    if( $tmp_address !== null )
                    {
                        if( $tmp_address->isAddress() && $tmp_address->isType_ipNetmask() && $ipaddress == $tmp_address->value() )
                            $found = TRUE;
                    }

                    if( !$found )
                    {
                        $ipversion = $this->ip_version($ipaddress);
                        if( $ipversion == "v4" )
                        {
                            $hostCidr = $this->mask2cidrv4(rtrim($netObj[2]));
                            $addHost["ipv4"][] = array($ObjectNetworkNamePan, 'ip-netmask', $ObjectNetworkName, '0', $source, '0', $ipaddress, $hostCidr, '1', $vsys, 'object');
                        }
                        elseif( $ipversion == "v6" )
                        {
                            $split = explode("/", $ipaddress);
                            $ipaddress = $split[0];
                            $hostCidr = $split[1];

                            $addHost["ipv6"][] = array($ObjectNetworkNamePan, 'ip-netmask', $ObjectNetworkName, '0', $source, '0', $ipaddress, $hostCidr, '1', $vsys, 'object', '0');
                        }
                    }
                    /*
                    $search = $projectdb->query("SELECT id FROM address WHERE source='$source' AND BINARY name_ext='$ObjectNetworkName' AND ipaddress='$ipaddress' AND vsys='$vsys' AND vtype='object';");
                    if ($search->num_rows == 0) {
                        $ipversion = $this->ip_version($ipaddress);
                        if ($ipversion == "v4") {
                            $hostCidr = $this->mask2cidrv4(rtrim($netObj[2]));
                            $addHost["ipv4"][] = "('$ObjectNetworkNamePan','ip-netmask','$ObjectNetworkName','0','$source','0','$ipaddress','$hostCidr','1','$vsys','object')";
                        }
                    elseif ($ipversion == "v6") {
                            $split=explode("/",$ipaddress);
                            $ipaddress=$split[0];
                            $hostCidr=$split[1];
                            $addHost["ipv6"][] = "('$ObjectNetworkNamePan','ip-netmask','$ObjectNetworkName','0','$source','0','$ipaddress','$hostCidr','1','$vsys','object','0')";
                        }

                    }
                    */
                }
                elseif( preg_match("/^\brange\b/i", $names_line) )
                {
                    $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                    $first_ipaddress = $netObj[1];
                    $last_ipaddress = $netObj[2];

                    if( $tmp_address !== null )
                    {
                        if( $tmp_address->isAddress() && $tmp_address->isType_ipRange() && ($first_ipaddress . "-" . $last_ipaddress) == $tmp_address->value() )
                            $found = TRUE;
                    }

                    if( !$found )
                    {
                        $ipversion = $this->ip_version($first_ipaddress);
                        if( $ipversion == "v4" )
                        {
                            $addHost["ipv4"][] = array($ObjectNetworkNamePan, 'ip-range', $ObjectNetworkName, '0', $source, '0', $first_ipaddress . "-" . $last_ipaddress, '', '1', $vsys, 'object');
                        }
                        elseif( $ipversion == "v6" )
                        {
                            $addHost["ipv6"][] = array($ObjectNetworkNamePan, 'ip-range', $ObjectNetworkName, '0', $source, '0', $first_ipaddress . "-" . $last_ipaddress, '', '1', $vsys, 'object', '0');
                        }
                    }
                    /*
                    $search = $projectdb->query("SELECT id FROM address WHERE source='$source' AND BINARY name_ext='$ObjectNetworkName' AND ipaddress='$first_ipaddress-$last_ipaddress' AND type='ip-range' AND vtype='object';");
                    if ($search->num_rows == 0) {
                        $ipversion = $this->ip_version($first_ipaddress);
                        if ($ipversion == "v4") {
                            $addHost["ipv4"][] = "('$ObjectNetworkNamePan','ip-range','$ObjectNetworkName','0','$source','0','$first_ipaddress-$last_ipaddress','','1','$vsys','object')";
                        } elseif ($ipversion == "v6") {
                            $addHost["ipv6"][] = "('$ObjectNetworkNamePan','ip-range','$ObjectNetworkName','0','$source','0','$first_ipaddress-$last_ipaddress','','1','$vsys','object','0')";
                        }

                    }
                    */
                }
                elseif( preg_match("/^\bdescription\b/i", $names_line) )
                {
                    $netObj = preg_split('/description /', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                    $description = addslashes($netObj[0]);
                    $description = str_replace("\n", '', $description); // remove new lines
                    $description = str_replace("\r", '', $description);
                    #$AddDescription[] = "UPDATE address SET description='$description' WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$ObjectNetworkName' AND vtype='object'";
                    if( isset($AddDescription[$ObjectNetworkNamePan]) )
                        $AddDescription[$ObjectNetworkNamePan] .= $description;
                    else
                        $AddDescription[$ObjectNetworkNamePan] = $description;
                }
            }

            if( preg_match("/^object network/i", $names_line) )
            {
                $isObjectNetwork = 1;
                $names = explode(" ", $names_line);
                $ObjectNetworkName = rtrim($names[2]);
                $ObjectNetworkNamePan = $this->truncate_names($this->normalizeNames($ObjectNetworkName));
            }
        }

        #return null;

        if( isset($addHost["ipv4"]) && count($addHost["ipv4"]) > 0 )
        {
            #print_r( $addHost["ipv4"]);
            //array($ObjectNetworkNamePan,'ip-range',$ObjectNetworkName,'0',$source,'0',$first_ipaddress."-".$last_ipaddress,'','1',$vsys,'object','0');

            foreach( $addHost["ipv4"] as $address_object )
            {
                //newAddress($name , $type, $value, $description = '')
                $tmp_address = $v->addressStore->find($address_object[0]);
                if( $tmp_address === null )
                {
                    if( $address_object[1] == "ip-range" || $address_object[1] == "fqdn" )
                    {
                        if( $print )
                            print " * create object: " . $address_object[0] . " type: " . $address_object[1] . " value: " . $address_object[6] . "\n";
                        $tmp_address = $v->addressStore->newAddress($address_object[0], $address_object[1], $address_object[6]);
                    }
                    elseif( $address_object[1] == "ip-netmask" )
                    {
                        if( $print )
                            print " * create object: " . $address_object[0] . " type: " . $address_object[1] . " value: " . $address_object[6] . "/" . $address_object[7] . "\n";
                        $tmp_address = $v->addressStore->newAddress($address_object[0], $address_object[1], $address_object[6] . "/" . $address_object[7]);
                    }

                }
                else
                {
                    mwarning("object: " . $address_object[0] . " already available\n", null, FALSE);
                }
            }
            unset($addHost["ipv4"]);
        }
        if( isset($addHost["ipv6"]) && count($addHost["ipv6"]) > 0 )
        {
            #print_r( $addHost["ipv6"]);

            foreach( $addHost["ipv6"] as $address_object )
            {
                //newAddress($name , $type, $value, $description = '')
                $tmp_address = $v->addressStore->find($address_object[0]);
                if( $tmp_address === null )
                {

                    if( $address_object[1] == "ip-range" || $address_object[1] == "fqdn" )
                    {
                        if( $print )
                            print " * create object: " . $address_object[0] . " type: " . $address_object[1] . " value: " . $address_object[6] . "\n";
                        $tmp_address = $v->addressStore->newAddress($address_object[0], $address_object[1], $address_object[6]);
                    }
                    elseif( $address_object[1] == "ip-netmask" )
                    {
                        if( $print )
                            print " * create object: " . $address_object[0] . " type: " . $address_object[1] . " value: " . $address_object[6] . "/" . $address_object[7] . "\n";
                        $tmp_address = $v->addressStore->newAddress($address_object[0], $address_object[1], $address_object[6] . "/" . $address_object[7]);
                    }
                }
                else
                {
                    mwarning("object: " . $address_object[0] . " already available\n", null, FALSE);
                }
            }
            unset($addHost["ipv6"]);
        }
        if( count($AddDescription) > 0 )
        {

            foreach( $AddDescription as $address_object => $description )
            {
                $tmp_address = $v->addressStore->find($address_object);
                if( $tmp_address !== null )
                    $tmp_address->setDescription($description);
            }
#        foreach ($AddDescription as $key => $value) {
#            $projectdb->query($value . ";");
#        }
            unset($AddDescription);
        }
    }


//TODO: Change this method to use Array information
    /*
    function get_objectgroup_network($cisco_config_file, $source, $vsys) {
        global $projectdb;
        $isObjectGroup = 0;
        $addHost = array();
        $addMember = array();
        $addGroups = array();


        $getPosition = $projectdb->query("SELECT max(id) as t FROM address_groups_id WHERE vsys='$vsys' AND source='$source';");
        if ($getPosition->num_rows == 0) {
            $groupLid = 0;  //We set it to 0, because we increase by 1 as soon as we find a object-group network
        }
        else {
            $ddata = $getPosition->fetch_assoc();
            $groupLid = $ddata['t'];
        }

        foreach ($cisco_config_file as $line => $names_line) {
            $names_line = trim($names_line);

            if (preg_match("/^object-group network/i", $names_line)) {
                $groupLid++;
                $isObjectGroup = 1;
                $names = explode(" ", $names_line);
                $HostGroupName = rtrim($names[2]);
                $HostGroupNamePan = $this->truncate_names($this->normalizeNames($HostGroupName));
    //            $getDup = $projectdb->query("SELECT id FROM address_groups_id WHERE source='$source' AND BINARY name_ext='$HostGroupName' AND vsys='$vsys'");
    //            if ($getDup->num_rows == 0) {
                    $addGroups[] = "($groupLid,'$HostGroupNamePan','$HostGroupName','$source','$vsys')";
    //                $projectdb->query("INSERT INTO address_groups_id (name,name_ext,source,vsys) values ('$HostGroupNamePan','$HostGroupName','$source','$vsys');");
    //                $groupLid = $projectdb->insert_id;
    //            }
    //            else{
    //                $getDupData=$getDup->fetch_assoc();
    //                $groupLid = $getDupData['id'];
    //            }
            }
            else {
                if (($isObjectGroup == 1) AND
                    (!preg_match("/\bnetwork-object\b/", $names_line)) AND
                    (!preg_match("/\bdescription\b/", $names_line)) AND
                     (!preg_match("/\bgroup-object\b/", $names_line))) {
                    $isObjectGroup = 0;
                }

                if ($isObjectGroup == 1) {
                    if (preg_match("/network-object/i", $names_line)) {
                        $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                        $type = $netObj[1];
                        $obj2 = rtrim($netObj[2]);

                        if ($type == "host") {
                            $ipversion = $this->ip_version($obj2);
                            if ($ipversion == "v4") {
                                $hostCidr = "32";
                            }
                            elseif ($ipversion == "v6") {
                                $hostCidr = "128";
                            }
                            else{
                                $hostCidr='';
                            }

                            if (($ipversion == "v4") or ($ipversion == "v6")) {
                                $getname = $projectdb->query("SELECT name_ext,cidr FROM address WHERE ipaddress='$obj2' AND cidr='$hostCidr' AND source='$source' AND vsys='$vsys';");
                                if ($getname->num_rows == 0) {
                                    #Not Exists - Creating
                                    $getDup = $projectdb->query("SELECT id FROM address WHERE source='$source' AND vsys='$vsys' AND name_ext='H-$obj2';");
                                    if ($getDup->num_rows == 0) {
                                        $addHost[] = "('H-$obj2','ip-netmask','H-$obj2','0','$source','0','$obj2','$hostCidr','1','$vsys','object')";
                                        $addMember[] = "('$groupLid','H-$obj2','$source','$vsys')";
                                    }
                                    else {
                                        $addMember[] = "('$groupLid','H-$obj2','$source','$vsys')";
                                    }
                                }
                                else {
                                    $data = $getname->fetch_assoc();
                                    $name = $data['name_ext'];
                                    $addMember[] = "('$groupLid','$name','$source','$vsys')";
                                }
                            }
                            else {
                                #NAME
                                $getname = $projectdb->query("SELECT ipaddress FROM address WHERE source='$source' AND BINARY name_ext='$obj2' AND cidr='0' AND vsys='$vsys' AND vtype!='object';");
                                if ($getname->num_rows == 1) {
                                    $data = $getname->fetch_assoc();
                                    $ipaddress = $data['ipaddress'];
                                    $ipversion = $this->ip_version($ipaddress);

                                    $getCheck = $projectdb->query("SELECT name_ext FROM address WHERE source='$source' AND ipaddress='$ipaddress' AND cidr='32' AND vsys='$vsys';");
                                    if ($getCheck->num_rows == 0) {
                                        $name = "$obj2-32";
                                        $name_int = $this->truncate_names($this->normalizeNames($name));
                                        $addHost[] = "('$name_int','ip-netmask','$name','0','$source','0','$ipaddress','32','1','$vsys','object')";
                                        $addMember[] = "('$groupLid','$obj2-32','$source','$vsys')";
                                    }
                                    else {
                                        $data = $getCheck->fetch_assoc();
                                        $name = $data['name_ext'];
                                        $addMember[] = "('$groupLid','$name','$source','$vsys')";
                                    }
                                }
                                elseif ($getname->num_rows == 0) {
                                    add_log('error', 'Reading Address Objects and Groups', 'ObjectName [' . $obj2 . '] is not in the Database.', $source, 'Adding to the DB with [ip:1.1.1.1], please modify it with the right IP Address');
                                    $name_int = $this->truncate_names($this->normalizeNames($obj2));
                                    $addHost[] = "('$name_int','ip-netmask','$obj2','1','$source',0,'1.1.1.1','32','1','$vsys','object')";
                                    $addMember[] = "('$groupLid','$obj2','$source','$vsys')";
                                }
                            }
                        }
                        elseif ($type == "object") {
                            $search = $projectdb->query("SELECT id FROM address WHERE BINARY name_ext='$obj2' AND source='$source'  AND vsys='$vsys' AND vtype='object';");
                            if ($search->num_rows == 1) {
                                $addMember[] = "('$groupLid','_mtobj_$obj2','$source','$vsys')";
                            }
                            else {
                                #Not exists in DB Creating IT
                                add_log('error', 'Reading Address Objects and Groups', 'ObjectName [' . $obj2 . '] is not in the Database.', $source, 'Adding to the DB with [ip:1.1.1.1], please modify it with the right IP Address');
                                $name_int = $this->truncate_names($this->normalizeNames($obj2));
                                $addHost[] = "('$name_int','ip-netmask','$obj2','1','$source',0,'1.1.1.1','32','1','$vsys','object')";
                                $addMember[] = "('$groupLid','$obj2','$source','$vsys')";
                            }
                        }
                        else {
                            $ipversion = $this->ip_version($type);
                            $obj2 = $type;
                            if ($ipversion == "v4") {
                                $hostCidr = $this->mask2cidrv4(rtrim($netObj[2]));
                                if ($hostCidr == "32") {
                                    $NameComplete = "H-$obj2";
                                }
                                else {
                                    $NameComplete = "N-$obj2-$hostCidr";
                                }
                                $getname = $projectdb->query("SELECT name_ext FROM address WHERE ipaddress='$obj2' AND cidr='$hostCidr' AND source='$source' AND vsys='$vsys';");
                                if ($getname->num_rows == 0) {
                                    #Not Exists - Creating
                                    $getDup = $projectdb->query("SELECT name_ext FROM address WHERE source='$source' AND BINARY name='$NameComplete' AND vsys='$vsys' AND vtype!='object';");
                                    if ($getDup->num_rows == 0) {
                                        $name_int = $this->truncate_names($this->normalizeNames($NameComplete));
                                        $addHost[] = "('$name_int','ip-netmask','$NameComplete','0','$source','1','$obj2','$hostCidr','1','$vsys','object')";
                                        $addMember[] = "('$groupLid','$NameComplete','$source','$vsys')";
                                    } else {
                                        $addMember[] = "('$groupLid','$NameComplete','$source','$vsys')";
                                    }
                                } else {
                                    $data = $getname->fetch_assoc();
                                    $objectname = $data['name_ext'];
                                    $addMember[] = "('$groupLid','$objectname','$source','$vsys')";
                                }
                            }
                            elseif ($ipversion == "v6") {
                                //TODO
                                # TO BE IMPLEMENTED
                            }
                            else {
                                #NAME CHECKAR si name y cidr o name solo o si es name solo o name-cidr
                                $hostCidr = $this->mask2cidrv4(rtrim($netObj[2]));
                                #NAME
                                $getname = $projectdb->query("SELECT ipaddress FROM address WHERE source='$source' AND BINARY name_ext='$obj2' AND cidr='0' AND vsys='$vsys' AND vtype!='object';");
                                if ($getname->num_rows == 1) {
                                    $data = $getname->fetch_assoc();
                                    $ipaddress = $data['ipaddress'];
                                    $ipversion = $this->ip_version($ipaddress);

                                    $getCheck = $projectdb->query("SELECT name_ext FROM address WHERE source='$source' AND ipaddress='$ipaddress' AND cidr='$hostCidr' AND vsys='$vsys' AND vtype!='object';");
                                    if ($getCheck->num_rows == 0) {
                                        $name = "$obj2-$hostCidr";
                                        $name_int = $this->truncate_names($this->normalizeNames($name));
                                        $addHost[] = "('$name_int','ip-netmask','$name','0','$source','0','$ipaddress','$hostCidr','1','$vsys','object')";
                                        $addMember[] = "('$groupLid','$obj2-$hostCidr','$source','$vsys')";
                                    }
                                    else {
                                        $data = $getCheck->fetch_assoc();
                                        $name = $data['name_ext'];
                                        $addMember[] = "('$groupLid','$name','$source','$vsys')";
                                    }
                                }
                                elseif ($getname->num_rows == 0) {
                                    add_log('error', 'Reading Address Objects and Groups', 'ObjectName [' . $obj2 . '] is not in the Database.', $source, 'Adding to the DB with [ip:1.1.1.1], please modify it with the right IP Address');
                                    $name_int = $this->truncate_names($this->normalizeNames($obj2));
                                    $addHost[] = "('$name_int','ip-netmask','$obj2','1','$source',0,'1.1.1.1','32','1','$vsys','object')";
                                    $addMember[] = "('$groupLid','$obj2','$source','$vsys')";
                                }
                            }
                        }
                    }
                    elseif (preg_match("/group-object/i", $names_line)) {
                        $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                        $obj2 = rtrim($netObj[1]);
                        $addMember[] = "('$groupLid','$obj2','$source','$vsys')";
                    }
                }

            }
        }

        if (count($addGroups)>0){
            $unique = array_unique($addGroups);
            $query = "INSERT INTO address_groups_id (id, name,name_ext,source,vsys) values " . implode(",", $unique) . ";";
            $projectdb->query($query);
            unset($addGroups);
        }

        if (count($addHost) > 0) {
            $unique = array_unique($addHost);
            $projectdb->query("INSERT INTO address (name,type,name_ext,checkit,source,used,ipaddress,cidr,v4,vsys, vtype) values " . implode(",", $unique) . ";");
            unset($addHost);
        }
        if (count($addMember) > 0) {
            $projectdb->query("INSERT INTO address_groups (lid,member,source,vsys) values " . implode(",", $addMember) . ";");
            unset($addMember);
        }
    }
    */

#function get_objectgroup_network2($cisco_config_file, $source, $vsys, MemoryObjectsHandlerCisco $objectsInMemory, STRING $fileName) {
    public function get_objectgroup_network2($data, $v)
    {
        global $projectdb;
        global $debug;
        global $print;

        $isObjectGroup = 0;
        $addHost = array();
        $addMember = array();
        $addMember2 = array();
        $addGroups = array();

        $source = '';
        $vsys = $v->name();
        /*
            $getPosition = $projectdb->query("SELECT max(id) as t FROM address_groups_id");
            if ($getPosition->num_rows == 0) {
                $groupLid = 0;  //We set it to 0, because we increase by 1 as soon as we find a object-group network
            }
            else {
                $ddata = $getPosition->fetch_assoc();
                $groupLid = $ddata['t'];
            }
            */

        foreach( $data as $line => $names_line )
        {
            $names_line = trim($names_line);

            if( preg_match("/^object-group network/i", $names_line) )
            {
                #$groupLid++;
                $isObjectGroup = 1;
                $names = explode(" ", $names_line);
                $HostGroupName = rtrim($names[2]);
                $HostGroupNamePan = $this->truncate_names($this->normalizeNames($HostGroupName));
                #$addGroups[] = "($groupLid,'$HostGroupNamePan','$HostGroupName','$source','$vsys')";

                $addressGroupData = [
                    'name' => $HostGroupName,
                    'type' => 'static',
                    #'id'    => $groupLid,
                    'source' => $source,
                    'vsys' => $vsys
                ];

                $tmp_addressgroup = $v->addressStore->find($HostGroupNamePan);
                if( $tmp_addressgroup === null )
                {
                    if( $print )
                        print "\n * create addressgroup: " . $HostGroupNamePan . "\n";
                    $tmp_addressgroup = $v->addressStore->newAddressGroup($HostGroupNamePan);
                }
                else
                {
                    mwarning("addressgroup: " . $HostGroupNamePan . " already available\n", null, FALSE);
                    if( $tmp_addressgroup->isAddress() )
                    {
                        $addlog = "an addressgroup same name; member:" . $names_line;
                        $tmp_addressgroup->set_node_attribute('error', $addlog);


                        if( $print )
                            print "\n * create addressgroup: tmp_" . $HostGroupNamePan . "\n";
                        $tmp_addressgroup = $v->addressStore->newAddressGroup("tmp_" . $HostGroupNamePan);
                    }

                }

                /*
                $response = $objectsInMemory->addAddressGroup($addressGroupData);
                if(!$response['success']){
                    echo "Group $HostGroupName with lid $groupLid not created\n";
                }
                else{
                }
                */
            }
            else
            {
                if( ($isObjectGroup == 1) and
                    (!preg_match("/\bnetwork-object\b/", $names_line)) and
                    (!preg_match("/\bdescription\b/", $names_line)) and
                    (!preg_match("/\bgroup-object\b/", $names_line)) )
                {
                    $isObjectGroup = 0;
                }

                if( $isObjectGroup == 1 )
                {
                    if( preg_match("/network-object/i", $names_line) )
                    {
                        $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

                        if( isset($netObj[2]) )
                        {
                            $type = $netObj[1];
                            $obj2 = rtrim($netObj[2]);
                            $value = $obj2;
                            $obj2 = str_replace(":", "_", $obj2);
                            $obj2 = str_replace("/", "m", $obj2);
                            $obj2 = $this->truncate_names($this->normalizeNames($obj2));
                        }

                        else
                        {
                            $obj2 = rtrim($netObj[1]);
                            $value = $obj2;
                            $type = $value;
                            $obj2 = str_replace(":", "_", $obj2);
                            $obj2 = str_replace("/", "m", $obj2);
                            $obj2 = $this->truncate_names($this->normalizeNames($obj2));

                            $tmp_hostCidr = explode("/", $value);
                            $hostCidr = $tmp_hostCidr[1];

                            #print_r( $netObj );
                            #mwarning( "netObj[2] not set [could be IPv6????]: ".$names_line , null, false);
                            #continue;
                        }


                        if( $type == "host" )
                        {
                            $ipversion = $this->ip_version($value);
                            if( $ipversion == "v4" )
                            {
                                $hostCidr = 32;
                                $tmp_prefix = "H-";
                            }
                            elseif( $ipversion == "v6" )
                            {
                                $hostCidr = 128;
                                $tmp_prefix = "H-";
                            }
                            else
                            {
                                $hostCidr = null;
                                #$tmp_prefix = "XYZ";
                                $tmp_prefix = "";
                            }

                            $tmp_address = $v->addressStore->find($tmp_prefix . $obj2);
                            if( $tmp_address === null )
                            {
                                if( $hostCidr !== null )
                                    $value = $value . "/" . $hostCidr;
                                else
                                {
                                    $value = $value;
                                    #print "LINE: ".$names_line."\n";
                                }


                                if( $print )
                                    print "  * create address object: " . $tmp_prefix . $obj2 . " , value: " . $value . "\n";
                                $tmp_address = $v->addressStore->newAddress($tmp_prefix . $obj2, 'ip-netmask', $value);
                            }
                            if( $tmp_address !== null )
                            {
                                if( !$tmp_addressgroup->hasObjectRecursive($tmp_address) && $tmp_addressgroup != $tmp_address )
                                {
                                    if( $print )
                                    {
                                        print "    * add addressobject: " . $tmp_address->name() . " to addressgroup: " . $tmp_addressgroup->name() . "\n";
                                    }

                                    $tmp_addressgroup->addMember($tmp_address);
                                }
                                else
                                {
                                    if( $print )
                                    {
                                        print "    * addressobject: " . $tmp_address->name() . " already a member or submember of addressgroup: " . $tmp_addressgroup->name() . "\n";
                                    }
                                }

                            }


                            /*
                            if (($ipversion == "v4") or ($ipversion == "v6")) {
                                $ipWithCidr = $obj2."-".$hostCidr;
                                $addressObj = $objectsInMemory->getIPAddressReference($fileName, $source, $vsys,$obj2, $hostCidr);
                                $addMember2[] = "('$groupLid','H-$obj2','$source','$vsys','$addressObj->location','$addressObj->name')"
                            }
                            else {
                                #NAME
                                $addressObj = $objectsInMemory->getAddressReference($fileName, $source, $vsys,$obj2, $hostCidr);
                                $addMember2[] = "('$groupLid','H-$obj2','$source','$vsys','$addressObj->location','$addressObj->name')";

                            }
                            */
                        }
                        elseif( $type == "object" )
                        {
                            #$addressObj = $objectsInMemory->getAddressReference($fileName, $source, $vsys,$obj2);
                            #$addMember2[] = "('$groupLid','H-$obj2','$source','$vsys','$addressObj->location','$addressObj->name')";

                            $tmp_address = $v->addressStore->find($obj2);
                            if( $tmp_address === null )
                            {
                                mwarning("addressobject: " . $obj2 . " not found \n", null, FALSE);
                            }
                            if( $tmp_address !== null )
                            {
                                if( !$tmp_addressgroup->hasObjectRecursive($tmp_address) && $tmp_addressgroup != $tmp_address )
                                {
                                    if( $print )
                                    {
                                        print "    * add addressobject: " . $tmp_address->name() . " to addressgroup: " . $tmp_addressgroup->name() . "\n";
                                    }

                                    $tmp_addressgroup->addMember($tmp_address);
                                }
                                else
                                {
                                    if( $print )
                                    {
                                        print "    * addressobject: " . $tmp_address->name() . " already a member or submember of addressgroup: " . $tmp_addressgroup->name() . "\n";
                                    }
                                }
                            }

                        }
                        else
                        {
                            $ipversion = $this->ip_version($type);


                            $obj2 = $type;
                            if( $ipversion == "v4" )
                            {
                                #$hostCidr = $this->mask2cidrv4(rtrim($netObj[2]));
                                $hostCidr = $this->mask2cidrv4(rtrim($value));
                                if( $hostCidr == "32" )
                                {
                                    $NameComplete = "H-$obj2";
                                }
                                else
                                {
                                    $NameComplete = "N-$obj2-$hostCidr";
                                }

                                #$addressObj = $objectsInMemory->getAddressReference($fileName, $source, $vsys, $obj2, $hostCidr);
                                #$addMember2[] = "('$groupLid','$NameComplete','$source','$vsys','$addressObj->location','$addressObj->name')";
                                $tmp_address = $v->addressStore->find($NameComplete);
                                if( $tmp_address === null )
                                {
                                    if( $print )
                                        print "  * create address object: " . $NameComplete . " | value: " . $obj2 . ", CIDR: " . $hostCidr . "\n";
                                    $tmp_address = $v->addressStore->newAddress($NameComplete, 'ip-netmask', $obj2 . "/" . $hostCidr);
                                }
                                if( $tmp_address !== null )
                                {
                                    if( !$tmp_addressgroup->hasObjectRecursive($tmp_address) && $tmp_addressgroup != $tmp_address )
                                    {
                                        if( $print )
                                        {
                                            print "    * add addressobject: " . $tmp_address->name() . " to addressgroup: " . $tmp_addressgroup->name() . "\n";
                                        }

                                        $tmp_addressgroup->addMember($tmp_address);
                                    }
                                    else
                                    {
                                        if( $print )
                                        {
                                            print "    * addressobject: " . $tmp_address->name() . " already a member or submember of addressgroup: " . $tmp_addressgroup->name() . "\n";
                                        }
                                    }
                                }

                            }
                            elseif( $ipversion == "v6" )
                            {
                                //TODO SVEN waschkut 20190625
                                #mwarning( "IPv6 found, not implemented in this case" );
                                # TO BE IMPLEMENTED

                                $obj2 = rtrim($netObj[1]);
                                $value = $obj2;
                                $type = $value;
                                $obj2 = str_replace(":", "_", $obj2);
                                $obj2 = str_replace("/", "m", $obj2);
                                $obj2 = $this->truncate_names($this->normalizeNames($obj2));

                                $tmp_hostCidr = explode("/", $value);
                                $hostCidr = $tmp_hostCidr[1];

                                $tmp_address = $v->addressStore->find($obj2);
                                if( $tmp_address === null )
                                {
                                    if( $print )
                                        print "  * create address object: " . $obj2 . " with value: " . $value . "\n";
                                    $tmp_address = $v->addressStore->newAddress($obj2, 'ip-netmask', $value);
                                }

                                if( $tmp_address !== null )
                                {
                                    if( !$tmp_addressgroup->hasObjectRecursive($tmp_address) && $tmp_addressgroup != $tmp_address )
                                    {
                                        if( $print )
                                        {
                                            print "    * add addressobject: " . $tmp_address->name() . " to addressgroup: " . $tmp_addressgroup->name() . "\n";
                                        }

                                        $tmp_addressgroup->addMember($tmp_address);
                                    }
                                    else
                                    {
                                        if( $print )
                                        {
                                            print "    * addressobject: " . $tmp_address->name() . " already a member or submember of addressgroup: " . $tmp_addressgroup->name() . "\n";
                                        }
                                    }
                                }
                            }
                            else
                            {
                                #NAME CHECKAR si name y cidr o name solo o si es name solo o name-cidr
                                $hostCidr = $this->mask2cidrv4(rtrim($netObj[2]));

                                #$addressObj = $objectsInMemory->getAddressReference($fileName, $source, $vsys,$obj2, $hostCidr);
                                #$addMember2[] = "('$groupLid','H-$obj2','$source','$vsys','$addressObj->location','$addressObj->name')";
                                $tmp_address = $v->addressStore->find($obj2);
                                if( $tmp_address === null )
                                {
                                    if( $hostCidr !== null )
                                    {
                                        $value = $obj2 . "/" . $hostCidr;
                                        $tmp_prefix = "H-";
                                    }

                                    else
                                    {
                                        $value = $obj2;
                                        $tmp_prefix = "";
                                        #print "LINE: ".$names_line."\n";
                                    }

                                    $tmp_address = $v->addressStore->find($tmp_prefix . $obj2);
                                    if( $tmp_address === null )
                                    {
                                        if( $print )
                                            print "  * create address object: " . $tmp_prefix . $obj2 . " with value: " . $value . "\n";
                                        $tmp_address = $v->addressStore->newAddress($tmp_prefix . $obj2, 'ip-netmask', $value);
                                    }
                                }
                                else
                                {
                                    //check if netmask is 0
                                    if( $tmp_address->isAddress() && $tmp_address->getNetworkMask() == 32 )
                                    {
                                        if( $hostCidr !== null )
                                        {
                                            $value = $obj2 . "/" . $hostCidr;
                                            $tmp_prefix = "H-";
                                        }

                                        $tmp_value = $tmp_address->value();
                                        $tmp_value1 = explode("/", $tmp_value);
                                        $tmp_value = $tmp_value1[0];

                                        $tmp_name = $tmp_address->name() . "_" . $hostCidr;
                                        $tmp_address = $v->addressStore->find($tmp_name);
                                        if( $tmp_address === null )
                                        {
                                            if( $print )
                                                print "  * create address object: " . $tmp_name . " change value to: " . $tmp_value . "/" . $hostCidr . "\n";
                                            $tmp_address = $v->addressStore->newAddress($tmp_name, 'ip-netmask', $tmp_value . "/" . $hostCidr);
                                        }
                                    }
                                }


                                if( $tmp_address !== null )
                                {
                                    if( !$tmp_addressgroup->hasObjectRecursive($tmp_address) && $tmp_addressgroup != $tmp_address )
                                    {
                                        if( $print )
                                        {
                                            print "    * add addressobject: " . $tmp_address->name() . " to addressgroup: " . $tmp_addressgroup->name() . "\n";
                                        }

                                        $tmp_addressgroup->addMember($tmp_address);
                                    }
                                    else
                                    {
                                        if( $print )
                                        {
                                            print "    * addressobject: " . $tmp_address->name() . " already a member or submember of addressgroup: " . $tmp_addressgroup->name() . "\n";
                                        }
                                    }
                                }
                            }
                        }
                    }
                    elseif( preg_match("/group-object/i", $names_line) )
                    {

                        $netObj = preg_split('/\s/', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                        $obj2 = rtrim($netObj[1]);
                        $obj2 = $this->truncate_names($this->normalizeNames($obj2));

                        #$addressObj = $objectsInMemory->getAddressGroupReference($fileName, $source, $vsys,$obj2);
                        #$addMember2[] = "('$groupLid','$obj2','$source','$vsys','$addressObj->location','$addressObj->name')";

                        $tmp_address = $v->addressStore->find($obj2);
                        if( $tmp_address === null )
                        {
                            mwarning("addressgroup object: " . $obj2 . " not found \n", null, FALSE);
                        }
                        if( $tmp_address !== null )
                        {
                            if( !$tmp_addressgroup->hasObjectRecursive($tmp_address) && $tmp_addressgroup != $tmp_address )
                            {
                                if( $print )
                                {
                                    print "    * add addressobject: " . $tmp_address->name() . " to addressgroup: " . $tmp_addressgroup->name() . "\n";
                                }

                                $tmp_addressgroup->addMember($tmp_address);

                            }
                            #else
                            {
                                if( $print )
                                {
                                    print "    * addressobject: " . $tmp_address->name() . " already a member or submember of addressgroup: " . $tmp_addressgroup->name() . "\n";
                                }
                            }

                        }
                    }
                    elseif( preg_match("/description/i", $names_line) )
                    {
                        $netObj = preg_split('/description /', $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                        $description = addslashes($netObj[0]);
                        $description = str_replace("\n", '', $description); // remove new lines
                        $description = str_replace("\r", '', $description);
                        #$AddDescription[] = "UPDATE address SET description='$description' WHERE source='$source' AND vsys='$vsys' AND BINARY name_ext='$ObjectNetworkName' AND vtype='object'";

                        $tmp_description = $tmp_addressgroup->description();

                        if( $print )
                            print "      * set Description to: " . $tmp_description . " " . $description . "\n";
                        $tmp_addressgroup->setDescription($tmp_description . " " . $description);

                    }
                }

            }
        }

        /*
        if (count($addGroups)>0){
            $unique = array_unique($addGroups);
            $query = "INSERT INTO address_groups_id (id, name,name_ext,source,vsys) values " . implode(",", $unique) . ";";
            $projectdb->query($query);
            unset($addGroups);
        }


        if (count($addMember2) > 0) {
            $query = "INSERT INTO address_groups (lid,member,source,vsys, table_name, member_lid) values " . implode(",", $addMember2) . ";";
            $projectdb->query($query);
            unset($addMember2);
        }

        $objectsInMemory->insertNewAddresses($projectdb);
        */
    }
}
