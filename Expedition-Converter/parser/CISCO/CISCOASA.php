<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */


/*
//Loads all global PHP definitions
require_once '/var/www/html/libs/common/definitions.php';

//User management control
//require_once INC_ROOT.'/userManager/start.php';
//include INC_ROOT.'/bin/authentication/sessionControl.php';

//Dependencies
require_once INC_ROOT.'/libs/database.php';

require_once INC_ROOT.'/libs/xmlapi.php';

require_once INC_ROOT.'/libs/common/MemberObject.php';
require_once INC_ROOT.'/libs/common/lib-rules.php';

require_once INC_ROOT.'/libs/projectdb.php';
*/

#require_once 'SecurityGroup.php';
/*
require_once INC_ROOT.'/libs/common/rules/SecurityRuleCisco.php';
require_once INC_ROOT.'/libs/common/MemberObject.php';

require_once INC_ROOT.'/libs/common/rules/MemoryObjectsHandlerCisco.php';
require_once INC_ROOT.'/libs/objects/SecurityRulePANObject.php';
use \PaloAltoNetworks\Policy\Objects\SecurityGroup;
use \PaloAltoNetworks\Policy\Objects\SecurityRuleCisco;
use \PaloaltoNetworks\Policy\Objects\MemberObject;

global $vrid;

require_once INC_ROOT.'/userManager/API/accessControl_CLI.php';
global $app;

//Capture request paramenters
include INC_ROOT.'/bin/configurations/parsers/readVars.php';
global $projectdb;
$projectdb = selectDatabase($project);

*/

//---------------------------------------------
//        Parser Logic starts here
//----------------------------------------------

# Classes

/*
class EXP_IkeGateway {
    public $name;

    public $interface;
    public $local_address_ip;
    public $peerIPtype = "static";
    public $peer_ip_address;
    public $type_authentication ="pre-shared-key";
    public $pre_shared_key = null;
    public $localID ="none";
    public $localIDvalue="";
    public $peerID ="none";
    public $peerIDvalue="";
    public $version="ikev1";
    public $peer_ip_type="static";

    public $passive_mode='no';
    public $nat_traversal='yes';
    public $exchange_mode_ikev1="auto";
    public $dpd_ikev1="yes";
    public $ike_crypto_profile_ikev1;
    public $ike_interval_ikev1=2;
    public $retry_ikev1=2;

    public $address_type="ipv4";
    public $ike_crypto_profile_ikev2;
    public $dpd_ikev2;
    public $ike_interval_ikev2=2;
    public $require_cookie_ikev2='no';

    public $fragmentation;
    public $allow_id_payload_mismatch;
    public $strict_validation_revocation;

    function __construct() {
        $a = func_get_args();
        $i = func_num_args();
        if (method_exists($this,$f='__construct'.$i)) {
            call_user_func_array(array($this,$f),$a);
        }
    }

    public function getSQL($source,$template,$vsys){
        $sql="('$this->address_type','$this->name','$vsys','$source','$template','$this->version','$this->dpd_ikev1','$this->ike_interval_ikev1','$this->retry_ikev1','$this->ike_crypto_profile_ikev1','$this->exchange_mode_ikev1','$this->dpd_ikev2','$this->ike_interval_ikev2','$this->ike_crypto_profile_ikev2','$this->require_cookie_ikev2','$this->local_address_ip','$this->interface','$this->type_authentication','$this->pre_shared_key','$this->allow_id_payload_mismatch','$this->strict_validation_revocation','$this->nat_traversal','$this->passive_mode','$this->fragmentation','$this->peer_ip_type','$this->peer_ip_address')";
        return $sql;
    }

    public function __construct13($name, $interface, $localIP, $peerIPtype, $peer_ip_address, $authentication, $pre_shared_key, $localID, $localIDvalue, $peerID, $peerIDvalue, $enablePassiveMode, $enableNatTraversal){
        $this->name = $name;
        $this->interface = $interface;
        $this->local_address_ip=$localIP;
        $this->peerIPtype=$peerIPtype;
        $this->peer_ip_address=$peer_ip_address;
        $this->type_authentication = $authentication;
        $this->pre_shared_key=$pre_shared_key;
        $this->localID = $localID;
        $this->localIDvalue = $localIDvalue;
        $this->peerID = $peerID;
        $this->peerIDvalue=$peerIDvalue;
        $this->enablePassiveMode=$enablePassiveMode;
        $this->enableNatTraversal=$enableNatTraversal;
    }

    public function __construct2($name,$peer_ip_address){
        $this->name = $name;
        $this->peer_ip_address=$peer_ip_address;
    }

    public function addPeer($peer_ip_address){
        $this->peer_ip_address=$peer_ip_address;
    }

    public function addInterface($interface){
        $this->interface=$interface;
    }

    public function addInterfaceIP($local_address_ip){
        $this->local_address_ip=$local_address_ip;
    }

    public function printMemberObject() {
        var_dump(get_object_vars($this));
    }

}

class EXP_IkeCryptoProfile {
    public $name;

    public $dh_group;
    public $hash;
    public $encryption;
    public $keyLifeTimeType="hours";
    public $keyLifeTimeValue="24";
    public $authentication_multiple=0;

    function __construct()
    {
        $a = func_get_args();
        $i = func_num_args();
        if (method_exists($this,$f='__construct'.$i)) {
            call_user_func_array(array($this,$f),$a);
        }
    }

    public function __construct1($name){
        $this->name = $name;
    }

    public function __construct7($name, $dhgroup, $hash, $encryption, $keyLifeTimeType, $keyLifeTimeValue, $ikev2AuthMulti){
        $this->name = $name;
        $this->dhgroup = array();
        if(isset($dhgroup) && is_array($dhgroup)){
            foreach ($dhgroup as $ip){
                $this->dhgroup[] = $ip;
            }
        }

        $this->authentication = array();
        if(isset($authentication) && is_array($authentication)){
            foreach ($authentication as $ip){
                $this->authentication[] = $ip;
            }
        }

        #if(isset($hash) && is_array($hash)){
        #    foreach ($hash as $ip){
        #        $this->$hash[] = $ip;
        #    }
        #}

        $this->encryption = array();
        if(isset($encryption) && is_array($encryption)){
            foreach ($encryption as $ip){
                $this->encryption[] = $ip;
            }
        }

        $this->keyLifeTimeType = $keyLifeTimeType;
        $this->keyLifeTimeValue = $keyLifeTimeValue;
        $this->ikev2AuthMulti = $ikev2AuthMulti;
    }

    public function addDHgroup($dh_group){
        $this->dh_group[]=$dh_group;
    }

    public function addHash($hash){
        if (($hash=="sha") OR ($hash=="sha-1")){
            $hash="sha1";
        }
        elseif ($hash=="sha-256"){
            $hash="sha256";
        }
        elseif ($hash=="sha-384"){
            $hash="sha384";
        }
        elseif ($hash=="sha-512"){
            $hash="sha512";
        }

        $this->hash[]=$hash;
    }

    public function addEncryption($encryption){
        $this->encryption[]=$encryption;
    }

    public function getSQL($source,$template,$vsys){
        $dh_group=implode(",",$this->dh_group);
        $encryption=implode(",",$this->encryption);
        $hash=implode(",",$this->hash);

        $seconds="";$minutes="";$hours="";$days="";
        if ($this->keyLifeTimeType=="seconds"){
            $seconds=$this->keyLifeTimeValue;
        }
        elseif($this->keyLifeTimeType=="minutes"){
            $minutes=$this->keyLifeTimeValue;
        }
        elseif($this->keyLifeTimeType=="hours"){
            $hours=$this->keyLifeTimeValue;
        }
        elseif($this->keyLifeTimeType=="days"){
            $days=$this->keyLifeTimeValue;
        }

        $sql="('$this->name','$vsys','$source','$template','$encryption','$hash','$dh_group','$seconds','$minutes','$hours','$days')";
        return $sql;
    }

    public function printMemberObject() {
        var_dump(get_object_vars($this));
    }

}

/*
class EXP_IpsecCryptoProfile {
    public $name;

    public $ipsecProtocol="esp";
    public $dhgroup = "no-pfs";
    public $authentication;
    public $encryption;

    public $keyLifeTimeType;
    public $keyLifeTimeValue;
    public $lifeSizeType;
    public $lifeSizeValue;

    function __construct()
    {
        $a = func_get_args();
        $i = func_num_args();
        if (method_exists($this,$f='__construct'.$i)) {
            call_user_func_array(array($this,$f),$a);
        }
    }

    public function __construct8($name, $dhgroup, $authentication, $encryption, $keyLifeTimeType, $keyLifeTimeValue, $lifeSizeType, $lifeSizeValue){
        $this->name = $name;
        $this->dhgroup = $dhgroup;

        $this->authentication = array();
        if(isset($authentication) && is_array($authentication)){
            foreach ($authentication as $ip){
                $this->authentication[] = $ip;
            }
        }
        $this->encryption = array();
        if(isset($encryption) && is_array($encryption)){
            foreach ($encryption as $ip){
                $this->encryption[] = $ip;
            }
        }
        $this->keyLifeTimeType = $keyLifeTimeType;
        $this->keyLifeTimeValue = $keyLifeTimeValue;
        $this->lifeSizeType = $lifeSizeType;
        $this->lifeSizeValue = $lifeSizeValue;
    }

    public function addAuthentication($authentication){
        $this->authentication[]=$authentication;
    }

    public function addEncryption($encryption){
        $this->encryption[]=$encryption;
    }

    public function printMemberObject() {
        var_dump(get_object_vars($this));
    }

    public function addDHgroup($dhgroup){
        $this->dhgroup=$dhgroup;
    }

    public function addLifeSize($lifeSizeType,$lifeSizeValue){
        $this->lifeSizeType=$lifeSizeType;
        $this->lifeSizeValue=$lifeSizeValue;
    }

    public function addLifeTime($keyLifeTimeType,$keyLifeTimeValue){
        $this->keyLifeTimeType=$keyLifeTimeType;
        $this->keyLifeTimeValue=$keyLifeTimeValue;
    }

    public function getHash(){
        $tmp=$this->ipsecProtocol.$this->dhgroup.implode(",",$this->authentication).implode(",",$this->encryption).$this->keyLifeTimeType.$this->keyLifeTimeValue.$this->lifeSizeType.$this->lifeSizeValue;
        return $tmp;
    }

}



class EXP_IpsecTunnel {
    public $name;
    public $priority;
    public $accesslist;
    public $peerIPaddress;
    public $transformSet;
    public $lifetime_seconds;
    public $lifetime_kilobytes;
    public $interface;
    public $tunnelInterface;
    public $address_type="ipv4";
    public $type_tunnel="auto-key";
    public $ipsecCryptoProfile;
    public $ike_gateway;

    function __construct()
    {
        $a = func_get_args();
        $i = func_num_args();
        if (method_exists($this,$f='__construct'.$i)) {
            call_user_func_array(array($this,$f),$a);
        }
    }

    public function __construct4($name,$priority,$accesslist,$tunnelInterface){
        $this->name = $name;
        $this->priority = $priority;
        $this->accesslist = $accesslist;
        $this->tunnelInterface=$tunnelInterface;
    }

    public function addPeer($peerIPaddress){
        $this->peerIPaddress=$peerIPaddress;
    }

    public function addTransformSet(IpsecCryptoProfile $transformSet){
        $this->transformSet=$transformSet;
        $this->ipsecCryptoProfile=$transformSet->name;
    }

    public function addLifetimeSeconds($lifetime_seconds){
        $this->lifetime_seconds=$lifetime_seconds;
    }

    public function addLifetime_kilobytes($lifetime_kilobytes){
        $this->lifetime_kilobytes=$lifetime_kilobytes;
    }

    public function addInterface($interface){
        $this->interface=$interface;
    }

    public function addInterfaceIP($interfaceIP){
        $this->interfaceIP=$interfaceIP;
    }

    public function getIpsecCryptoName(){
        return $this->transformSet->name;
    }

    public function getIpsecCrypto(){
        return $this->transformSet;
    }

    public function getSQL($source,$template,$vsys){
        $sql="('$this->name','$vsys','$source','$template','$this->tunnelInterface','$this->address_type','$this->type_tunnel','$this->name','$this->ipsecCryptoProfile')";
        return $sql;
    }
}
*/


////////////////////// WASCHKUT new parser


require_once('CISCOappid.php');

require_once("CISCOnetwork.php");
require_once("CISCOaddresses.php");
require_once("CISCOservices.php");
require_once("CISCOvpn.php");
require_once("CISCOsecurityrules.php");
require_once("CISCOnat.php");


class CISCOASA extends PARSER
{
    use CISCOaddresses;
    use CISCOnat;
    use CISCOnetwork;
    use CISCOsecurityrules;
    use CISCOservices;
    use CISCOvpn;
    use CISCOappid;

    use SHAREDNEW;

    function addPrefixSuffix($check_prefix, $text_prefix, $check_suffix, $text_suffix, $name_pan, $max_char)
    {

        $name_pan_fin = $name_pan;

        if( $check_prefix == "on" )
        {
            $name_pan_fin = $text_prefix . $name_pan;
            if( strlen($name_pan_fin) > $max_char )
            {
                $total_name_pan_fin = strlen($name_pan_fin);
                $total_subs = $total_name_pan_fin - $max_char;
                $name_pan = substr($name_pan, 0, -$total_subs);
                $name_pan_fin = $text_prefix . $name_pan;
            }
        }
        if( $check_suffix == "on" )
        {
            $name_pan = $name_pan_fin;
            $name_pan_fin = $name_pan_fin . $text_suffix;
            if( strlen($name_pan_fin) > $max_char )
            {
                $total_name_pan_fin = strlen($name_pan_fin);
                $total_subs = $total_name_pan_fin - $max_char;
                $name_pan = substr($name_pan, 0, -$total_subs);
                $name_pan_fin = $name_pan . $text_suffix;
            }
        }
        return $name_pan_fin;
    }


    public function vendor_main($config_filename, $pan, $routetable = "")
    {

        $this->logger->start();

        parent::preCheck();


        //swaschkut - tmp, until class migration is done
        global $print;
        $print = TRUE;

        //CISCO specific
        //------------------------------------------------------------------------
        $path = "";
        $project = "";

        $config_path = $path . $config_filename;
        $filename = $config_filename;
        $filenameParts = pathinfo($config_filename);
        $verificationName = $filenameParts['filename'];


        $data = $this->clean_config($config_path, $project, $config_filename);


        $v = $this->import_config($data, $pan, $routetable); //This should update the $source


        /*
                    update_progress($project, '0.70', 'Replacing DM_INLINE groups by Members, can take minutes',$jobid);
                    #Remove DM_INLINE
                    $getRules = $projectdb->query("SELECT id FROM security_rules WHERE source='$getSourceVsys[0]' AND vsys='$getSourceVsys[1]';");
                    if ($getRules->num_rows > 0) {
                        while ($getRulesData = $getRules->fetch_assoc()) {
                            $rule_lid = $getRulesData['id'];
                            replace_dminline_by_members($getSourceVsys[1], $getSourceVsys[0], $rule_lid);
                        }
                    }
                    #From Nat rules
                    $getRules = $projectdb->query("SELECT id FROM nat_rules WHERE source='$getSourceVsys[0]' AND vsys='$getSourceVsys[1]';");
                    if ($getRules->num_rows > 0) {
                        while ($getRulesData = $getRules->fetch_assoc()) {
                            $rule_lid = $getRulesData['id'];
                            replace_nat_dminline_by_members($getSourceVsys[1], $getSourceVsys[0], $rule_lid);
                        }
                    }

                    update_progress($project, '0.80', 'Fixing rules based on Destination Nats',$jobid);
                    fix_destination_nat($config_path, $getSourceVsys[0], $getSourceVsys[1]);
                    $template_name = $filename . "_template";
                    $getTemplate = $projectdb->query("SELECT id FROM templates_mapping WHERE filename='$filename';");
                    if($getTemplate->num_rows==1){
                        $data = $getTemplate->fetch_assoc();
                        $template = $data['id'];
                        $query = "SELECT id FROM virtual_routers WHERE template='$template' AND source='".$getSourceVsys[0]."';";
                        //$my = fopen("DNAT.txt",'a');
                        //fwrite($my, $query);
                        //fclose($my);
                        $getVR = $projectdb->query($query);
                        if ($getVR->num_rows == 1) {
                            $getVRData = $getVR->fetch_assoc();
                            $vr_id = $getVRData['id'];

                            require_once INC_ROOT.'/libs/common/lib-rules.php';
                            recalculate_Dst_basedOn_NAT($projectdb, $getSourceVsys[0], $getSourceVsys[1], $vr_id, $project, 'Cisco');
                        }
                    }

                    #Calculate Layer4-7
                    $source = $getSourceVsys[0];
                    $queryRuleIds = "SELECT id from security_rules WHERE source = $source;";
                    $resultRuleIds = $projectdb->query($queryRuleIds);
                    if($resultRuleIds->num_rows>0){
                        $rules = array();
                        while($dataRuleIds = $resultRuleIds->fetch_assoc()){
                            $rules[] = $dataRuleIds['id'];
                        }
                        $rulesString = implode(",", $rules);
                        $securityRulesMan = new \SecurityRulePANObject();
                        $securityRulesMan->updateLayerLevel($projectdb, $rulesString, $source);
                    }


                    update_progress($project, '0.95', 'Reading VPN Rules',$jobid);
                    get_ipsec_vpn($config_path, $getSourceVsys[0], $getSourceVsys[1],$getSourceVsys[2],$jobid,$project);
                    unlink($path.$config_filename);
         */


        //Todo: validation if GLOBAL rule
        echo PH::boldText("Zone Calculation for Security and NAT policy");
        Converter::calculate_zones($pan, "append");
        $this->logger->increaseCompleted();

        echo PH::boldText("\nAuto Zone Assignment - before it was called: fix destination NAT:\n");
        //[0] -> $source; [1] -> $vsys
        //-> fix_destination_nat($config_path, $getSourceVsys[0], $getSourceVsys[1]);

        //disabled as calculate_zones() was done before
        #$this->fix_destination_nat( $data, $v );


        //fix needed for:
        //Todo: NAT - security NAT calculation in specific object change
        echo PH::boldText("\nrecalculation DST based on NAT:\n");
        echo "implementation missing\n";
        //Todo: if
        //????require_once INC_ROOT.'/libs/common/lib-rules.php';
        //-> recalculate_Dst_basedOn_NAT($projectdb, $getSourceVsys[0], $getSourceVsys[1], $vr_id, $project, 'Cisco');
        #$this->recalculate_Dst_basedOn_NAT( VirtualSystem $vsys, VirtualRouter $vr, STRING $vendor=null)


        //this is old one, replace with get_ipsec_vpn2
        //-> get_ipsec_vpn($config_path, $getSourceVsys[0], $getSourceVsys[1],$getSourceVsys[2],$jobid,$project);
        #get_ipsec_vpn2($config_path, $getSourceVsys[0], $getSourceVsys[1],$getSourceVsys[2],$jobid,$project);
        echo PH::boldText("\nget IPsec config - Reading VPN Rules\n");
        $this->get_ipsec_vpn2($data, $pan);
        //------------------------------------------------------------------------
        $this->logger->increaseCompleted();


        echo PH::boldText("\nVALIDATION - interface name and change into PAN-OS confirm naming convention\n");
        CONVERTER::validate_interface_names($pan);
        $this->logger->increaseCompleted();

        echo PH::boldText("\nVALIDATION - cleanup Cisco predefined services - which are unused in PAN-OS\n");
        CONVERTER::cleanup_unused_predefined_services($pan, "default");
        $this->logger->increaseCompleted();


        //CISCO ASA specific  best practise set
        echo PH::boldText("\nreplace DM_INLINE address-/service-group by members [these are create from Cisco Device Manager]\n");
        $this->replaceByMembersAndDelete($pan);
        $this->logger->increaseCompleted();


        echo PH::boldText("\nVALIDATION -  bidir NAT rules, disable bidir if not possible (e.g. SRC IP has addressgroup)\n");
        CONVERTER::validation_nat_bidir($pan);
        $this->logger->increaseCompleted();

        echo PH::boldText("\nVALIDATION - Region name must not be used as a address / addressgroup object name\n");
        CONVERTER::validation_region_object($pan);
        $this->logger->increaseCompleted();

        //if security rule count is very high => memory leak problem
        //Todo: where to place custom table for app-migration if needed
        echo PH::boldText("\nVALIDATION - replace tmp services with APP-id if possible\n");
        CONVERTER::AppMigration($pan);
        $this->logger->increaseCompleted();


        //Todo: service with IP/tcp/udp - ANY are migrated as any;
        //DONE: BUT
        // if protocol ip found -> right now TCP is used !!!!!


        //Todo: global is not handled
        //Todo: bring in all warnings into XML file
        //Todo: custom App-ID are created but not available in XML file, WHY??

        /*
                echo PH::boldText("\nrecalculation DST based on NAT:\n");
                echo "move up - not at the end\n";
                #$this->recalculate_Dst_basedOn_NAT( VirtualSystem $vsys, VirtualRouter $vr, STRING $vendor=null)
                $this->recalculate_Dst_basedOn_NAT( $pan, "cisco");
                */

        $this->logger->setCompletedSilent();

    }


    /*
    $sourcesAdded = array();
    global $source;
    if ($action == "import") {
        ini_set('max_execution_time', PARSER_max_execution_time);
        ini_set("memory_limit", PARSER_max_execution_memory);

        $path = USERSPACE_PATH."/projects/" . $project . "/";
        $i = 0;
        $dirrule = opendir($path);
        if(!isset($jobid)){
            $jobid = 0;
        }
        update_progress($project, '0.00', 'Reading config files',$jobid);
        while ($config_filename = readdir($dirrule)) {

            if (checkFiles2Import($config_filename)){
                $config_path = $path . $config_filename;
                $filename = $config_filename;
                $filenameParts = pathinfo($config_filename);
                $verificationName = $filenameParts['filename'];
                $isUploaded = $projectdb->query("SELECT id FROM device_mapping WHERE filename='$verificationName';");
                if ($isUploaded->num_rows == 0) {
                    $getSourceVsys = import_config($config_path, $project, $config_filename, $jobid);
                    $source = $getSourceVsys[0];
                    $sourcesAdded[] = $source;

                    update_progress($project, '0.70', 'Replacing DM_INLINE groups by Members, can take minutes',$jobid);
                    #Remove DM_INLINE
                    $getRules = $projectdb->query("SELECT id FROM security_rules WHERE source='$getSourceVsys[0]' AND vsys='$getSourceVsys[1]';");
                    if ($getRules->num_rows > 0) {
                        while ($getRulesData = $getRules->fetch_assoc()) {
                            $rule_lid = $getRulesData['id'];
                            replace_dminline_by_members($getSourceVsys[1], $getSourceVsys[0], $rule_lid);
                        }
                    }
                    #From Nat rules
                    $getRules = $projectdb->query("SELECT id FROM nat_rules WHERE source='$getSourceVsys[0]' AND vsys='$getSourceVsys[1]';");
                    if ($getRules->num_rows > 0) {
                        while ($getRulesData = $getRules->fetch_assoc()) {
                            $rule_lid = $getRulesData['id'];
                            replace_nat_dminline_by_members($getSourceVsys[1], $getSourceVsys[0], $rule_lid);
                        }
                    }

                    update_progress($project, '0.80', 'Fixing rules based on Destination Nats',$jobid);
                    fix_destination_nat($config_path, $getSourceVsys[0], $getSourceVsys[1]);
                    $template_name = $filename . "_template";
                    $getTemplate = $projectdb->query("SELECT id FROM templates_mapping WHERE filename='$filename';");
                    if($getTemplate->num_rows==1){
                        $data = $getTemplate->fetch_assoc();
                        $template = $data['id'];
                        $query = "SELECT id FROM virtual_routers WHERE template='$template' AND source='".$getSourceVsys[0]."';";
                        //$my = fopen("DNAT.txt",'a');
                        //fwrite($my, $query);
                        //fclose($my);
                        $getVR = $projectdb->query($query);
                        if ($getVR->num_rows == 1) {
                            $getVRData = $getVR->fetch_assoc();
                            $vr_id = $getVRData['id'];

                            require_once INC_ROOT.'/libs/common/lib-rules.php';
                            recalculate_Dst_basedOn_NAT($projectdb, $getSourceVsys[0], $getSourceVsys[1], $vr_id, $project, 'Cisco');
                        }
                    }

                    #Calculate Layer4-7
                    $source = $getSourceVsys[0];
                    $queryRuleIds = "SELECT id from security_rules WHERE source = $source;";
                    $resultRuleIds = $projectdb->query($queryRuleIds);
                    if($resultRuleIds->num_rows>0){
                        $rules = array();
                        while($dataRuleIds = $resultRuleIds->fetch_assoc()){
                            $rules[] = $dataRuleIds['id'];
                        }
                        $rulesString = implode(",", $rules);
                        $securityRulesMan = new \SecurityRulePANObject();
                        $securityRulesMan->updateLayerLevel($projectdb, $rulesString, $source);
                    }


                    update_progress($project, '0.95', 'Reading VPN Rules',$jobid);
                    get_ipsec_vpn($config_path, $getSourceVsys[0], $getSourceVsys[1],$getSourceVsys[2],$jobid,$project);
                    unlink($path.$config_filename);
                }
                else {
                    update_progress($project, '0.00', 'This filename '.$filename.' its already uploaded. Skipping...',$jobid);
                    //if (!preg_match("/\.xml/",$filename)){unlink($path.$config_filename);}
                }
            }
        }
        #Check used
        update_progress($project, '0.99', 'Calculating Used Objects',$jobid);
        check_used_objects_new($sourcesAdded);

        # Remove zones if they dont have a name in security and nat and appoverride rules (happens when is not a default gateway
        $projectdb->query("DELETE FROM security_rules_from WHERE name='';");
        $projectdb->query("DELETE FROM security_rules_to WHERE name='';");
        $projectdb->query("DELETE FROM nat_rules_from WHERE name='';");
        $projectdb->query("DELETE FROM appoverride_rules_from WHERE name='';");
        $projectdb->query("DELETE FROM appoverride_rules_to WHERE name='';");

        update_progress($project, '1.00', 'Done.',$jobid);
    //    unlink($path.$config_filename);  //Already deleted in the while loop
    }
    */

    function clean_config($config_path, $project, $config_filename)
    {

        $cisco_config_file = file($config_path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        $data = array();
        foreach( $cisco_config_file as $line => $names_line )
        {

            if( (preg_match("/description/", $names_line)) or (preg_match("/remark/", $names_line)) )
            {
                #$data[] = $names_line;

                //Todo: SVEN 20191203 - problem with config "
                $tmp_array = explode("\r", $names_line);
                foreach( $tmp_array as $tmp_line )
                    $data[] = $tmp_line;
            }
            else
            {
                #"<--- More --->"
                if( preg_match("/^<--- More --->/", $names_line) || preg_match("/^              /", $names_line) )
                {

                }
                elseif( preg_match("/\'/", $names_line) )
                {
                    $data[] = str_replace("'", "_", $names_line);
                }
                elseif( preg_match("/\\r/", $names_line) )
                {
                    $tmp_array = explode("\r", $names_line);
                    foreach( $tmp_array as $tmp_line )
                        $data[] = $tmp_line;
                }
                else
                {
                    $data[] = $names_line;
                }
            }
        }
        #file_put_contents("test_output.txt", $data);
        #file_put_contents('test_output.txt', print_r($data, true));
        return $data;
    }

    /**
     * @param array $data
     * @param PANConf $pan
     */

#import_config( $data, $pan ); //This should update the $source
#function import_config($config_path, $project, STRING $config_filename, INT $jobid)
    function import_config($data, $pan, $routetable)
    {
        /*
        global $projectdb;


        $objectsInMemory = new MemoryObjectsHandlerCisco();

        $filename = $config_filename;
        #CLEAN CONFIG FROM EMPTY LINES AND CTRL+M
        file_put_contents($config_path, implode(PHP_EOL, file($config_path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)));

        clean_config($config_path, $project, $config_filename);

        #LOAD THE FILE
        $cisco_config_file = file($config_path);
        */


        /*
            #Check if is the first vsys
            $getVsys = $projectdb->query("SELECT id,vsys FROM device_mapping WHERE filename='$filename';");
            if ($getVsys->num_rows == 0) {
                $projectdb->query("INSERT INTO device_mapping (device,version,ispanorama,active,project,filename,vsys,vendor) VALUES ('$filename','',0,1,'$project','$filename','vsys1','Cisco')");
                $vsys = "vsys1";
                $source = $projectdb->insert_id;
            }
            else {
                $getVsysData = $getVsys->fetch_assoc();
                $thename = $getVsysData['vsys'];
                $getVsysData1 = str_replace("vsys", "", $thename);
                $result = intval($getVsysData1) + 1;
                $vsys = "vsys" . $result;
                $projectdb->query("INSERT INTO device_mapping (device,version,ispanorama,active,project,filename,vsys,vendor) VALUES ('$filename','',0,1,'$project','$filename','$vsys','Cisco')");
                #Get Source (First row for this filename)
                $getSource = $projectdb->query("SELECT id FROM device_mapping WHERE filename='$filename' GROUP by filename;");
                $getSourceData = $getSource->fetch_assoc();
                $source = $getSourceData['id'];
            }
        */


        $vsysName = "Cisco";
        $vsysID = 1;

        $v = $pan->findVSYS_by_displayName($vsysName);
        if( $v === null )
        {
            #print "VSYS: ".$vsysID." already available - check displayName ".$vsysName."\n";
            $v = $pan->findVirtualSystem('vsys' . $vsysID);
            $v->setAlternativeName($vsysName);
        }
        else
        {
            //create new vsys, search for latest ID
            do
            {
                $vsysID++;
                $v = $pan->findVirtualSystem('vsys' . $vsysID);
            } while( $v !== null );

            if( $v === null )
            {
                $v = $pan->createVirtualSystem(intval($vsysID), $vsysName . $vsysID);
                if( $v === null )
                {
                    derr("vsys" . $vsysID . " could not be created ? Exit\n");
                }
                #print "create VSYS: ".$vsysID." - ".$vsysName."\n";
            }
        }


        /*
        $parametersProject = [
            "source"        =>$source,
            "projectdb"     =>$projectdb
        ];
    */

        #add_default_services($source);
        #add_default_profiles($source);
        /*
            #Config Template
            $getTemplate = $projectdb->query("SELECT id FROM templates_mapping WHERE filename='$filename';");
            $template_name = $filename . "_template";
            $projectdb->query("INSERT INTO templates_mapping (project,name,filename,source) VALUES ('$project','$template_name','$filename','$source');");
            $template = $projectdb->insert_id;

            update_progress($project, '0.01', 'File:' . $filename . ' Phase 2 Loading Address Objects',$jobid);
        */

        if( $routetable != "" )
        {
            echo PH::boldText("\nimport dynamic Routing:\n");
            #$ciscoFile = "/Users/swaschkut/Downloads/Show-Routes.log";
            $cisco = file_get_contents($routetable);
            $this->importDynamicRoutes($cisco, $v);
        }


        echo PH::boldText("\nObject_network loaded\n");
        $this->get_object_network($data, $v);


        echo PH::boldText("\nsave_names:\n");
        $this->save_names($data, $v);


        echo PH::boldText("\nload custome application:\n");
        $this->load_customer_application($v);


        #get_interfaces($cisco_config_file, $source, $vsys, $template);
        echo PH::boldText("\nget interfaces:\n");
        $this->get_interfaces($data, $v);

        #get_static_routes($cisco_config_file, $source, $vsys, $template);
        echo PH::boldText("\nget static routes\n");
        $this->get_static_routes($data, $v);

        /* moved because of existing objects problem
            #get_object_network($cisco_config_file, $source, $vsys, $filename);
            get_object_network($data, $v);
            #$objectsInMemory->load_addresses_inMemory($parametersProject);
            echo "Object_network loaded\n";
        */

        #get_objectgroup_network2($cisco_config_file, $source, $vsys, $objectsInMemory, $filename);
        echo PH::boldText("\nObjectGroup_network loaded\n");
        $this->get_objectgroup_network2($data, $v);

        #update_progress($project, '0.20', 'File:' . $filename . ' Phase 2 Loading Service Objects',$jobid);

        echo PH::boldText("\nCisco Services loaded\n");
        $this->add_cisco_services($data, $v);


        echo PH::boldText("\nServices loaded\n");
        #get_object_service($cisco_config_file, $source, $vsys, $filename);
        $this->get_object_service($data, $v);
        #$objectsInMemory->load_services_inMemory($parametersProject);


        echo PH::boldText("\nObject_services loaded\n");
        #get_objectgroup_service($cisco_config_file, $source, $vsys, $filename);
        $this->get_objectgroup_service($data, $v);


        echo PH::boldText("\nProtocol groups loaded\n");
        #get_protocol_groups($cisco_config_file, $source, $vsys);
        $this->get_protocol_groups($data, $v);


        #get_icmp_groups($cisco_config_file, $source, $vsys);
        echo PH::boldText("\nICMPGroups loaded\n");
        $this->get_icmp_groups($data, $v);


        #update_progress($project, '0.30', 'File:' . $filename . ' Phase 2 Loading Nat Rules',$jobid);


        //Todo: continue working Sven Waschkut
        #get_twice_nats($cisco_config_file, $source, $vsys, $template,"before");
        echo PH::boldText("\nNAT twice 'before':\n");
        $this->get_twice_nats($data, $v, "before");


        #get_objects_nat($cisco_config_file, $source, $vsys);
        echo PH::boldText("\nNAT objects:'\n");
        $this->get_objects_nat($data, $v);


        #get_twice_nats($cisco_config_file, $source, $vsys, $template,"after");
        echo PH::boldText("\nNAT twice 'after':\n");
        $this->get_twice_nats($data, $v, "after");


        //Todo: SWASCHKUT if NAT rules count == 0 use old natpre83

        print "NAT counter: " . $v->natRules->count() . "\n";

        if( $v->natRules->count() == 0 )
        {
            echo PH::boldText("\nNAT pre83:\n");
            $this->natpre83($data, $v);
        }


        /*
         *     $getNats=$projectdb->query("SELECT id FROM nat_rules WHERE source='$source' AND vsys='$vsys' LIMIT 1;");
            if ($getNats->num_rows==0){
                natpre83($source,$vsys,$cisco_config_file,$template);
            }
            */
        //update_progress($project, '0.45', 'File:' . $filename . ' Phase 2 Loading Security Rules',$jobid);


        $userObj = array();
        #get_objectgroup_user($cisco_config_file, $source, $vsys, $userObj);
        echo PH::boldText("\nget objectgroup user\n");
        $this->get_objectgroup_user($data, $v, $userObj);


        /*
         * //Todo: needed??? Sven Waschkut
        $devicegroup = $filename;

        $objectsInMemory->createAllPortServices($devicegroup, $projectdb, $source, $vsys);
        $objectsInMemory->load_AllObjects_inMemory($parametersProject);
        echo "Objects loaded\n";
        //TODO: Remove this print_r
    //    print_r($objectsInMemory);
        $objectsInMemory->explodeAllGroups2Addresses($source, $vsys);
        echo "Groups expanded\n";
        $objectsInMemory->explodeAllGroups2Services($source, $vsys);
        echo "Services Expanded\n";
        $objectsInMemory->updateAddressGroupReferences($projectdb,$source, $vsys);
        echo "Address Groups updated\n";
        $objectsInMemory->updateServiceGroupReferences($projectdb,$source, $vsys);
        echo "Service Groups updated\n";
        $objectsInMemory->addUsers($devicegroup, $source, $vsys, $userObj);
    */


//    print_r($objectsInMemory);
        #get_security_policies2($devicegroup, $cisco_config_file, $source, $vsys, $objectsInMemory);
        echo PH::boldText("\nget security Policy\n");
        $this->get_security_policies2($data, $v);

        //Todo: temp for working script
        return $v;

#    update_progress($project, '0.65', 'File:' . $filename . ' Phase 2 Cleaning Zones',$jobid);

//    optimize_names2($cisco_config_file, $source, $vsys);
//    add_filename_to_objects($source, $vsys, $filename);
        //Todo: SVEN what to do here???????
        $this->clean_zone_any($source, $vsys);

        /*
        check_invalid($source,$vsys,$template);
        // Call function to generate initial consumptions
        //deviceUsage("initial", "get", $project, $dusage_platform, $dusage_version, $vsys, $source, $dusage_template);
        deviceUsage("initial", "get", $project, "", "", $vsys, $source, $template_name);
        return array("$source", "$vsys","$template");
        */
    }

    static public function removeEnclosingQuotes(STRING $value): string
    {
        return trim($value, '"');
    }

    /***
     * @param STRING $value Such as US\\myUser
     * @return string        US\myUser
     */
    static public function removeDoubleBackSlash(STRING $value): string
    {
        $value = preg_replace('/\\\\+/', '\\', $value);
        return $value;
    }

#function get_objectgroup_user($cisco_config_file,$source,$vsys,ARRAY &$userObj){
    function get_objectgroup_user($data, $v, array &$userObj)
    {
        global $projectdb;
        $isObjGroupUser = FALSE;
        $userObj = array();
        foreach( $data as $line => $names_line )
        {
            $names_line = trim($names_line);
            if( preg_match("/object-group user /", $names_line) )
            {
                $names = explode(" ", $names_line);
                $isObjGroupUser = TRUE;
                $name = $names[2];
                $userObj[$name] = array();
                continue;
            }
            if( $isObjGroupUser === TRUE )
            {
                if( preg_match("/user /", $names_line) )
                {
                    $user = str_replace("user ", "", $names_line);
                    $userObj[$name][] = trim($user, '"');
                }
                elseif( preg_match("/user-group /", $names_line) )
                {
                    $usergroup = str_replace("user-group ", "", $names_line);
                    $userObj[$name][] = trim($usergroup, '"');
                }
                else
                {
                    $isObjGroupUser = FALSE;
                }
            }
        }
    }


    function add_filename_to_objects($source, $vsys, $filename)
    {
        global $projectdb;

        $projectdb->query("UPDATE address SET devicegroup='$filename' WHERE source='$source' AND vsys='$vsys';");
        $projectdb->query("UPDATE address_groups_id SET devicegroup='$filename' WHERE source='$source' AND vsys='$vsys';");
        $projectdb->query("UPDATE services SET devicegroup='$filename' WHERE source='$source' AND vsys='$vsys';");
        $projectdb->query("UPDATE services_groups_id SET devicegroup='$filename' WHERE source='$source' AND vsys='$vsys';");
        $projectdb->query("UPDATE tag SET devicegroup='$filename' WHERE source='$source' AND vsys='$vsys';");
    }


    function removeUnusedLabels(MySQLi $projectdb, STRING $source, STRING $vsys)
    {
        $objects = array();
        $labelObjects = array();
        $query = "SELECT * FROM address WHERE source='$source' AND vsys='$vsys'";
        $results = $projectdb->query($query);
        if( $results->num_rows > 0 )
        {
            while( $data = $results->fetch_assoc() )
            {
                $name = $data['name'];
                $objects[$name]['objects'][] = [
                    'name' => $data['name'],
                    'id' => $data['id'],
                    'vtype' => $data['vtype'],
                    'ipaddress' => $data['ipaddress'],
                    'cidr' => $data['cidr'],
                ];

                if( $data['vtype'] == 'label' )
                {
                    $labelObjects[$name] = [
                        'name' => $data['name'],
                        'id' => $data['id'],
                        'cidr' => $data['cidr'],
                    ];
                }
            }
        }

        $deleteableLables = array();
        foreach( $labelObjects as $labelName => $labelArray )
        {
            $count = $objects[$labelName]['objects'];
            if( $count > 1 )
            {
                $deleteableLables[] = $labelArray['id'];
            }
        }

        $query = "DELETE FROM address WHERE id in (" . implode(',', $deleteableLables) . ") AND description ='' AND cidr=0 ";
//    echo $query;
//    $projectdb->query($query);

    }


#function save_names($cisco_config_file, $source, $vsys, $filename) {
    function save_names($data, $v)
    {
        #global $projectdb;
        #$addName = array();

        global $debug;
        global $print;

        foreach( $data as $line => $names_line )
        {
            $names_line = trim($names_line);
            if( preg_match("/^name /i", $names_line) )
            {
                print "LINE: " . $names_line . "\n";

                $ipaddress = "";
                $name = "";
                $descriptiontrimmed = "";
                $description = "";

                $names = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                $count = count($names);

                $ipaddress = $names[1];
                $ipversion = $this->ip_version($ipaddress);

                if( $ipversion == "v4" )
                {
                    $hostCidr = "32";
                }
                elseif( $ipversion == "v6" )
                {
                    //Todo: swaschkut 20191014 fix it
                    $hostCidr = "0";
                }
                else
                {
                    $ipversion = "v4";
                    $hostCidr = "32";
                }

                $name = rtrim($names[2]);
                $name_int = $this->truncate_names($this->normalizeNames($name));

                if( ($count > 3) and ($names[3] == "description") )
                {
                    $descriptiontrimmed = $names[4];
                    for( $i = 5; $i <= $count; $i++ )
                    {
                        if( isset($names[$i]) )
                        {
                            $descriptiontrimmed .= " " . $names[$i];
                        }
                    }
                }

                $description = addslashes($this->normalizeComments($descriptiontrimmed));
                $description = str_replace("\n", '', $description); // remove new lines
                $description = str_replace("\r", '', $description);

                $tmp_address = $v->addressStore->find($name_int);
                if( $tmp_address === null )
                {
                    #newAddress($name , $type, $value, $description = '')
                    if( $print )
                        print " * create address object: " . $name_int . " , value: " . $ipaddress . ", CIDR: " . $hostCidr . "\n";
                    $tmp_address = $v->addressStore->newAddress($name_int, 'ip-netmask', $ipaddress . "/" . $hostCidr);
                    $tmp_address->setDescription($description);
                }

                /*
                $getDup = $projectdb->query("SELECT id FROM address WHERE BINARY name_ext='$name' AND source='$source';");
                if ($getDup->num_rows == 0) {
                    if ($ipversion == "v6") {
                        $addName[] = "('$name_int','ip-netmask','$ipaddress','$hostCidr','$name','0','$description','$source','0','1','label')";
                    } else {
                        $addName[] = "('$name_int','ip-netmask','$ipaddress','$hostCidr','$name','0','$description','$source','1','0','label')";
                    }
                }
                */
            }
        }

        /*
        if (count($addName) > 0) {
            $projectdb->query("INSERT INTO address (name,type,ipaddress,cidr,name_ext,checkit,description,source,v4,v6,vtype) values" . implode(",", $addName) . ";");
            unset($addName);
        }
        */
    }


    function optimize_names2($cisco_config_file, $source, $vsys)
    {
        global $projectdb;

        $get32s = $projectdb->query("SELECT id,name,ipaddress,cidr FROM address WHERE source='$source' AND vsys='$vsys' AND type='ip-netmask' AND name like '%-32';");
        if( $get32s->num_rows > 0 )
        {

        }

    }


//No se utiliza
    /*function optimize_names($cisco_config_file, $source, $vsys) {
        global $projectdb;

        $getAll = $projectdb->query("SELECT id,name,ipaddress,cidr FROM address WHERE source='$source' AND vsys='$vsys' AND type='ip-netmask' AND cidr='0';");
        if ($getAll->num_rows > 0) {
            while ($data = $getAll->fetch_assoc()) {
                $original_name_int = $data['name'];
                $original_ipaddress = $data['ipaddress'];
                $original_id = $data['id'];
                $getNew = $projectdb->query("SELECT id,name_ext,name FROM address WHERE source='$source' AND vsys='$vsys' AND ipaddress='$original_ipaddress' AND cidr!='0';");
                if ($getNew->num_rows == 1) {
                    $data2 = $getNew->fetch_assoc();
                    $newid = $data2['id'];
                    $newname2 = $data2['name_ext'];
                    $newname = $data2['name'];
                    if (preg_match("/$original_name_int\-/", $newname)) {
                        $projectdb->query("UPDATE address SET name_ext='$original_name_int', name='$original_name_int' WHERE id='$newid';");
                        $projectdb->query("UPDATE address_groups SET member='$original_name_int' WHERE member='$newname2' AND source='$source' AND vsys='$vsys';");
                        $projectdb->query("DELETE FROM address where id='$original_id';");
                        #Update all tables
                        $projectdb->query("UPDATE address_groups SET member_lid='$newid' WHERE member_lid='$original_id' AND table_name='address';");
                        $projectdb->query("UPDATE security_rules_src SET member_lid='$newid' WHERE member_lid='$original_id' AND table_name='address' ;");
                        $projectdb->query("UPDATE security_rules_dst SET member_lid='$newid' WHERE member_lid='$original_id' AND table_name='address' ;");
                        $projectdb->query("UPDATE nat_rules_src SET member_lid='$newid' WHERE member_lid='$original_id' AND table_name='address' ;");
                        $projectdb->query("UPDATE nat_rules_dst SET member_lid='$newid' WHERE member_lid='$original_id' AND table_name='address' ;");
                        $projectdb->query("UPDATE nat_rules SET tp_dat_address_lid='$newid' WHERE tp_dat_address_lid='$original_id' AND tp_dat_address_table='address' ;");
                        $projectdb->query("UPDATE nat_rules_translated_address SET member_lid='$newid' WHERE member_lid='$original_id' AND table_name='address' ;");
                        $projectdb->query("UPDATE nat_rules_translated_address_fallback SET member_lid='$newid' WHERE member_lid='$original_id' AND table_name='address' ;");
                    }
                }
            }
        }
    #Change the Netmask from 0 to 32 to all the others
        $projectdb->query("UPDATE address SET cidr='32' WHERE cidr='0' AND type='ip-netmask' AND source='$source';");
    }*/

    function clean_zone_any($source, $vsys)
    {

        global $projectdb;

        $getZoneAny = $projectdb->query("SELECT id FROM zones WHERE source = '$source' AND vsys = '$vsys' AND name = 'any';");
        if( $getZoneAny->num_rows > 0 )
        {
            $dataZ = $getZoneAny->fetch_assoc();
            $id_zone_any = $dataZ['id'];
            $projectdb->query("UPDATE zones SET name = 'any1' WHERE id = '$id_zone_any';");
            $projectdb->query("UPDATE nat_rules_from SET name = 'any1' WHERE source = '$source' AND vsys = '$vsys' AND name = 'any';");
            $projectdb->query("UPDATE nat_rules SET op_zone_to = 'any1' WHERE source = '$source' AND vsys = '$vsys' AND op_zone_to = 'any';");
        }
        else
        {
            // Clean any's
            $projectdb->query("DELETE FROM nat_rules_from WHERE source = '$source' AND vsys = '$vsys' AND name = 'any';");
            $projectdb->query("UPDATE nat_rules SET op_zone_to = '' WHERE source = '$source' AND vsys = '$vsys' AND op_zone_to = 'any';");
        }

    }

    function array_orderby()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach( $args as $n => $field )
        {
            if( is_string($field) )
            {
                $tmp = array();
                foreach( $data as $key => $row )
                    $tmp[$key] = $row[$field];
                $args[$n] = $tmp;
            }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }

    function whatipaddress($ipaddress, $v)
    {

        if( $this->ip_version($ipaddress) == "noip" )
        {
            $tmp_address = $v->addressStore->find($ipaddress);
            if( $tmp_address !== null )
            {
                return $tmp_address->value();
            }
            else
            {
                mwarning("natpre83 - object: " . $ipaddress . " not found");
                return null;
            }
            /*
            $getIP = $projectdb->query("SELECT ipaddress FROM address WHERE BINARY name_ext='$ipaddress' AND source='$source' AND vsys='$vsys' LIMIT 1;");
            if ($getIP->num_rows == 1) {
                $getIPData = $getIP->fetch_assoc();
                return $getIPData['ipaddress'];
            }
            */
        }
        else
        {
            return $ipaddress;
        }
    }


# Library Functions
    static public function convertLifeTime($input, $entero, &$output_unit, &$output_value)
    {
        $output_value = $input;
        $output_unit = 1;
        //Seconds
        if( $output_value > 65535 )
        {
            $output_value = $output_value / 60;
            $output_unit++;
        }
        //Minutes
        if( $output_value > 65535 )
        {
            $output_value = $output_value / 60;
            $output_unit++;
        }
        //Hours
        if( $output_value > 65535 )
        {
            $output_value = $output_value / 24;
            $output_unit++;
        }
        //Days

        switch ($output_unit)
        {
            case 1:
                $output_unit = "seconds";
                break;
            case 2:
                $output_unit = "minutes";
                break;
            case 3:
                $output_unit = "hours";
                break;
            case 4:
                $output_unit = "days";
                break;
            default:
                $output_unit = "seconds";
                break;
        }

        if( $entero )
        {
            $output_value = round($output_value);
        }
    }

    function convertLifeSize($input, $entero, &$output_unit, &$output_value)
    {
        $output_value = $input;
        $output_unit = 1;
        while( $output_value > 65535 )
        {
            $output_value = $output_value / 1024;
            $output_unit++;
        }

        switch ($output_unit)
        {
            case 1:
                $output_unit = "kb";
                break;
            case 2:
                $output_unit = "mb";
                break;
            case 3:
                $output_unit = "gb";
                break;
            case 4:
                $output_unit = "tb";
                break;
            default:
                $output_unit = "kb";
                break;
        }

        if( $entero )
        {
            $output_value = round($output_value);
        }
    }

    /*
     //already available in Framework
    function getApplicationSnippet(MySQLi $projectdb, STRING $name, $source, STRING $vsys): MemberObject
    {
        require_once INC_ROOT . '/libs/projectdb.php';
        $pandb = selectDatabase('pandb');

        $getDup = $projectdb->query("SELECT id FROM applications WHERE name='$name' AND vsys='$vsys' AND source='$source';");
        if( $getDup->num_rows == 1 )
        {
            $data = $getDup->fetch_assoc();
            $member = new MemberObject($data['id'], 'applications');
        }
        else
        {
            $getSnippet = $pandb->query("SELECT name, type, data, panos FROM snippets WHERE name='$name' and type ='appid';");
            if( $getSnippet->num_rows == 1 )
            {
                $getSnippetData = $getSnippet->fetch_assoc();
                $data = $getSnippetData['data'];
                $version = $getSnippetData['panos'];

                $sql = array();
                $profilesArray = simplexml_load_string($data);
                if( $profilesArray != FALSE )
                {
                    $name = $profilesArray->attributes()->name;
                    $subcategory = $profilesArray->subcategory;
                    $category = $profilesArray->category;
                    $technology = $profilesArray->technology;
                    $risk = $profilesArray->risk;
                    $evasive_behavior = $profilesArray->{'evasive-behavior'};
                    $consume_big_bandwidth = $profilesArray->{'consume-big-bandwidth'};
                    $used_by_malware = $profilesArray->{'used-by-malware'};
                    $able_to_transfer_file = $profilesArray->{'able-to-transfer-file'};
                    $has_known_vulnerability = $profilesArray->{'has-known-vulnerability'};
                    $tunnel_other_application = $profilesArray->{'tunnel-other-application'};
                    $tunnel_applications = $profilesArray->{'tunnel-applications'};
                    $prone_to_misuse = $profilesArray->{'prone-to-misuse'};
                    $pervasive_use = $profilesArray->{'pervasive-use'};
                    $file_type_ident = $profilesArray->{'file-type-ident'};
                    $virus_ident = $profilesArray->{'virus-ident'};
                    $spyware_ident = $profilesArray->{'spyware-ident'};
                    $data_ident = $profilesArray->{'data-ident'};
                    $parent_app = $profilesArray->{'parent-app'};
                    $timeout = $profilesArray->timeout;
                    $timeout_tcp = $profilesArray->{'tcp-timeout'};
                    $timeout_udp = $profilesArray->{'udp-timeout'};
                    $tcp_half_closed_timeout = $profilesArray->{'tcp-half-closed-timeout'};
                    $tcp_time_wait_timeout = $profilesArray->{'tcp-time-wait-timeout'};
                    $icmp_code = "";

                    if( isset($profilesArray->default->port) )
                    {
                        $default_type = "port";
                        $default_value_array = array();
                        foreach( $profilesArray->default->port->member as $vvvalue )
                        {
                            $default_value_array[] = $vvvalue;
                        }
                        //$default_value = implode(",", $default_value_array);
                    }
                    elseif( isset($profilesArray->default->{'ident-by-ip-protocol'}) )
                    {
                        $default_type = "ident-by-ip-protocol";
                        $default_value = $profilesArray->default->{'ident-by-ip-protocol'};
                    }
                    elseif( isset($profilesArray->default->{'ident-by-icmp-type'}) )
                    {
                        $default_type = "ident-by-icmp-type";
                        $default_value = $profilesArray->default->{'ident-by-icmp-type'};
                        $icmp_type = $profilesArray->default->{'ident-by-icmp-type'};

                        if( isset($profilesArray->default->{'ident-by-icmp-type'}->type) )
                        {
                            $icmp_type = $profilesArray->default->{'ident-by-icmp-type'}->type;
                            $icmp_code = $profilesArray->default->{'ident-by-icmp-type'}->code;
                        }

                    }
                    elseif( isset($profilesArray->default->{'ident-by-icmp6-type'}) )
                    {
                        $default_type = "ident-by-icmp6-type";
                        $icmp_type = $profilesArray->default->{'ident-by-icmp6-type'}->type;
                        $icmp_code = $profilesArray->default->{'ident-by-icmp6-type'}->code;
                        $default_value = "";
                    }
                    elseif( !isset($profilesArray->default) )
                    {
                        $default_type = "None";
                        $default_value = "";
                    }

                    $query = "INSERT INTO applications (default_type,value,type,code,parent_app,source,name,vsys,devicegroup,subcategory,category,technology,risk,evasive_behavior,consume_big_bandwidth,used_by_malware,able_to_transfer_file,has_known_vulnerability,tunnel_other_application,tunnel_applications,prone_to_misuse,pervasive_use,file_type_ident,virus_ident,spyware_ident,data_ident,timeout,tcp_timeout,udp_timeout,tcp_half_closed_timeout, tcp_time_wait_timeout) "
                        . "VALUES ('$default_type','$default_value','$icmp_type','$icmp_code','$parent_app','$source','$name','$vsys','','$subcategory','$category','$technology','$risk','$evasive_behavior','$consume_big_bandwidth','$used_by_malware','$able_to_transfer_file','$has_known_vulnerability','$tunnel_other_application','$tunnel_applications','$prone_to_misuse','$pervasive_use','$file_type_ident','$virus_ident','$spyware_ident','$data_ident','$timeout','$timeout_tcp','$timeout_udp','$tcp_half_closed_timeout', '$tcp_time_wait_timeout');";
                    $projectdb->query($query);
                    $appid = $projectdb->insert_id;
                    if( isset($profilesArray->signature) )
                    {
                        foreach( $profilesArray->signature->entry as $signature )
                        {
                            $signatureXML = $signature->asXML();
                            $projectdb->query("INSERT INTO applications_signatures (member_lid,table_name,signature,vsys,devicegroup,source) VALUES ('$appid','applications','$signatureXML','$vsys','',$source);");
                        }
                    }

                    if( $default_type == "port" )
                    {
                        if( count($default_value_array) > 0 )
                        {
                            foreach( $default_value_array as $port )
                            {
                                $projectdb->query("INSERT INTO applications_ports (member_lid, table_name, ports, vsys, devicegroup, source) VALUES ('$appid','applications','$port','$vsys','','$source');");
                                //echo "INSERT INTO applications_ports (member_lid, table_name, ports, vsys, devicegroup, source) VALUES ('$appid','applications','$port','$vsys','$devicegroup','$source');\n";
                            }
                        }
                    }

                    $member = new MemberObject($projectdb->insert_id, 'applications');
                }
                else
                {
                    $member = new MemberObject();
                }
            }
            else
            {
                $member = new MemberObject();
            }
        }

        return $member;
    }
    */

//function explodeGroups2Members($members,$projectdb, $source, $vsys, $level){
//    $tmp_members=array();
//    foreach($members as $member){
//
//        //Not a group. Let's add it directly
//        if(strcmp($member->location, 'address')==0){
//            if(!isset($member->value) || !isset($member->cidr)){
//                $query ="SELECT id, ipaddress, cidr FROM address WHERE id=$member->name AND source='$source' AND vsys='$vsys';";
//                $getMember = $projectdb->query($query);
//                if($getMember->num_rows == 1){
//                    $row=$getMember->fetch_assoc();
//                    $member->cidr=$row['cidr'];
//                    $member->value=$row['ipaddress'];
//                }
//            }
//            $tmp_members[]=$member;
//        }
//
//        //This is a group. Let's expand it
//        else{
//            $query = "SELECT member_lid, table_name ".
//                "FROM address_groups adg ".
//                "WHERE adg.source='$source' AND adg.vsys='$vsys' AND ".
//                " BINARY adg.lid='$member->name' ";
//            $getMember = $projectdb->query($query);
//            while($row = $getMember->fetch_assoc()) {
//                $member_id= $row['member_lid'];
//                $member_location = $row['table_name'];
//                $member2[] = new MemberObject($member_id, $member_location);
//            }
//            $tmp_members = array_merge($tmp_members, explodeGroups2Members($member2, $projectdb, $source, $vsys, $level+1));
//        }
//    }
//
//    return $tmp_members;
//}

    /**
     * @param PANConf $pan
     */
    public function replaceByMembersAndDelete($pan)
    {
        $vsyss = $pan->getVirtualSystems();
        foreach( $vsyss as $v )
        {
            foreach( $v->addressStore->addressGroups() as $group )
            {
                if( strpos($group->name(), "INLINE") !== FALSE )
                    $group->replaceByMembersAndDelete("", FALSE, TRUE, TRUE);
            }

            foreach( $v->serviceStore->serviceGroups() as $group )
            {
                if( strpos($group->name(), "INLINE") !== FALSE )
                    $group->replaceByMembersAndDelete("", FALSE, TRUE, TRUE);
            }
        }
    }

}
