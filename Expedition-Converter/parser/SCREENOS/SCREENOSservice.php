<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */


trait SCREENOSservice
{
    function get_services($screenos_config_file, $pan)
    {
        global $debug;
        global $print;


        $v = $pan->findVirtualSystem('vsys1');
        if( $v === null )
        {
            derr("vsys: vsys1 could not be found ! Exit\n");
        }


        $vsys = "root";
        $serviceObj = array();


        foreach( $screenos_config_file as $line => $names_line )
        {

            $names_line = trim($names_line);
            if( preg_match("/^set vsys /i", $names_line) )
            {
                $v = $this->vsys_parser($names_line, $pan);
            }

            if( preg_match("/^set service /i", $names_line) )
            {
                $data = $this->name_preg_split($names_line);
                $protocol = "";

                if( isset($data[3]) )
                {
                    $name_ext = $data[2];
                    $name_ext = $this->truncate_names($this->normalizeNames($name_ext));
                    switch ($data[3])
                    {
                        case "protocol":
                            $protocol = $data[4];

                            if( ($protocol == "tcp") or ($protocol == "udp") )
                            {
                                $dport = $data[8];
                                $sport = $data[6];

                                $dportsplit = explode("-", $dport);
                                if( $dportsplit[0] == $dportsplit[1] )
                                {
                                    $dport = $dportsplit[0];
                                }
                                $sportsplit = explode("-", $sport);
                                if( $sportsplit[0] == $sportsplit[1] )
                                {
                                    $sport = $sportsplit[0];
                                }
                                if( ($sport == "1-65535") or ($sport == "0-65535") )
                                {
                                    $sport = "";
                                }
                            }
                            else
                            {
                                $sport = "";
                                $dport = "";
                            }


                            if( ($protocol == "tcp") or ($protocol == "udp") )
                            {
                                $tmp_service = $v->serviceStore->find($name_ext);
                                if( $tmp_service == null )
                                {
                                    if( $print )
                                        print "create service Object: " . $name_ext . ", " . $protocol . ", " . $dport . ", " . $sport . "\n";
                                    $tmp_service = $v->serviceStore->newService($name_ext, $protocol, $dport, "", $sport);
                                }

                            }
                            else
                            {
                                if( $debug )
                                    print "service: " . $name_ext . " protocol: " . $protocol . " and port: " . $dport . " not supported\n";
                            }

                            if( isset($data[9]) )
                            {
                                if( $data[9] == "timeout" )
                                {
                                    $timeout = $data[10];
                                    if( $timeout != "never" )
                                    {
                                        //20190429 Sven - how to implement, new PAN-OS version 8.1 only support service timeout
                                        #$serviceObj[$vsys][$name_ext]->setTimeout($timeout);
                                    }
                                    else
                                    {
                                        //https://kb.juniper.net/InfoCenter/index?page=content&id=KB9354&cat=NS_5400&actp=LIST
                                        $timeout = 13107;
                                    }

                                    //Netscreen timeout is in minutes
                                    $timeout = intval($timeout) * 60;
                                    if( $print )
                                        print " * set timeout to: " . $timeout . "\n";

                                    $tmp_service->setTimeout($timeout);
                                    #add_log2('warning', 'Reading Services Objects and Groups', 'The Service [' . $name_ext . '] has a custom Time-Out [' . $timeout . ']', $source, 'If you are migrating to PanOS 8.1 it will be applied if not you will have to AppOverride it.', 'objects', $lid, 'services');
                                }
                            }
                            break;
                        case "+":

                            $tmp_service = $v->serviceStore->find($name_ext);
                            $newProtocol = $data[4];
                            if( $tmp_service !== null && !$tmp_service->isTmpSrv() && !$tmp_service->isGroup() )
                            {
                                $protocol = $tmp_service->protocol();

                                if( $newProtocol == $protocol )
                                {

                                    if( ($newProtocol == "tcp") or ($newProtocol == "udp") )
                                    {
                                        $dport = $data[8];
                                        $sport = $data[6];

                                        $dportsplit = explode("-", $dport);
                                        if( $dportsplit[0] == $dportsplit[1] )
                                        {
                                            $dport = $dportsplit[0];
                                        }
                                        $sportsplit = explode("-", $sport);
                                        if( $sportsplit[0] == $sportsplit[1] )
                                        {
                                            $sport = $sportsplit[0];
                                        }
                                        if( ($sport == "1-65535") or ($sport == "0-65535") )
                                        {
                                            $sport = "";
                                        }
                                    }
                                    else
                                    {
                                        $sport = "";
                                        $dport = "";
                                    }

                                    $tmp_dport = $tmp_service->getDestPort();
                                    $tmp_sport = $tmp_service->getSourcePort();
                                    if( $dport != "" )
                                        $tmp_service->setDestPort($tmp_dport . "," . $dport);

                                    if( $sport != "" )
                                        $tmp_service->setSourcePort($tmp_sport . "," . $sport);

                                    if( isset($data[9]) )
                                    {
                                        if( $data[9] == "timeout" )
                                        {
                                            $timeout = $data[10];
                                            if( $timeout != "never" )
                                            {
                                                #$serviceObj[$vsys][$name_ext]->setTimeout($timeout);
                                            }
                                            else
                                            {
                                                # Its never ToDo: Check what is the max tmout
                                            }
                                            #add_log2('warning', 'Reading Services Objects and Groups', 'The Service [' . $name_ext . '] has a custom Time-Out [' . $timeout . ']', $source, 'If you are migrating to PanOS 8.1 it will be applied if not you will have to AppOverride it.', 'objects', $thisLid, 'services');
                                        }
                                    }

                                }
                                else
                                {
                                    $tmp_name = $tmp_service->name();
                                    $tmp_service->setName($protocol . "-" . $tmp_name);

                                    //add SNMP, maybe better to add at shared level
                                    if( $print )
                                        print "create servicegroup Object: " . $tmp_name . "\n";
                                    $tmp_servicegroup = $v->serviceStore->newServiceGroup($tmp_name);
                                    $tmp_servicegroup->addMember($tmp_service);


                                    # Create a ServiceGroup
                                    //echo "CREATE A GROUP for $name_ext".PHP_EOL;
                                    #add_log2("error",'Importing Services','The service called [' . $name_ext . '] is using different protocols',$source,'Replace the Service by the App-id or Create a ServiceGroup with the different services','objects',$thisLid,'services');
                                    //echo $names_line.PHP_EOL;

                                    if( ($newProtocol == "tcp") or ($newProtocol == "udp") )
                                    {
                                        $dport = $data[8];
                                        $sport = $data[6];

                                        $dportsplit = explode("-", $dport);
                                        if( $dportsplit[0] == $dportsplit[1] )
                                        {
                                            $dport = $dportsplit[0];
                                        }
                                        $sportsplit = explode("-", $sport);
                                        if( $sportsplit[0] == $sportsplit[1] )
                                        {
                                            $sport = $sportsplit[0];
                                        }
                                        if( ($sport == "1-65535") or ($sport == "0-65535") )
                                        {
                                            $sport = "";
                                        }

                                        $tmp_name = $newProtocol . "-" . $name_ext;
                                    }
                                    else
                                    {
                                        $sport = "";
                                        $dport = "65535";
                                        $tmp_name = "tmp" . $newProtocol . "-" . $name_ext;
                                        $newProtocol = "tcp";
                                    }

                                    if( $print )
                                        print "create service Object: " . $newProtocol . "-" . $name_ext . ", " . $newProtocol . ", " . $dport . ", " . $sport . "\n";
                                    $tmp_newservice = $v->serviceStore->newService($newProtocol . "-" . $name_ext, $newProtocol, $dport, "", $sport);
                                    $tmp_servicegroup->addMember($tmp_newservice);

                                    #print "service | ".$name_ext ." | problem TCP/UDP \n";
                                }
                            }
                            elseif( is_object($tmp_service) && $tmp_service->isGroup() )
                            {
                                if( ($newProtocol == "tcp") or ($newProtocol == "udp") )
                                {
                                    $dport = $data[8];
                                    $sport = $data[6];

                                    $dportsplit = explode("-", $dport);
                                    if( $dportsplit[0] == $dportsplit[1] )
                                    {
                                        $dport = $dportsplit[0];
                                    }
                                    $sportsplit = explode("-", $sport);
                                    if( $sportsplit[0] == $sportsplit[1] )
                                    {
                                        $sport = $sportsplit[0];
                                    }
                                    if( ($sport == "1-65535") or ($sport == "0-65535") )
                                    {
                                        $sport = "";
                                    }
                                    $tmp_name = $newProtocol . "-" . $name_ext;
                                }
                                else
                                {
                                    $sport = "";
                                    $dport = "65535";
                                    $tmp_name = "tmp" . $newProtocol . "-" . $name_ext;
                                    $newProtocol = "tcp";
                                }

                                if( $print )
                                    print "create service Object: " . $tmp_name . "-" . $dport . ", " . $newProtocol . ", " . $dport . ", " . $sport . "\n";
                                $tmp_newservice = $v->serviceStore->newService($tmp_name . "-" . $dport, $newProtocol, $dport, "", $sport);
                                $tmp_service->addMember($tmp_newservice);
                            }
                            else
                            {
                                print "Error: Adding info to a ghost service " . $names_line . PHP_EOL;
                            }
                            break;
                        case "timeout":
                            $timeout = $data[4];
                            if( $timeout != "never" )
                            {
                                if( isset($serviceObj[$vsys][$name_ext]) )
                                {

                                }
                                else
                                {

                                }
                            }
                            else
                            {
                                # Its never ToDo: Check what is the max tmout
                                if( $debug )
                                    mwarning("service timeout not implemented\n", null, FALSE);
                            }
                            #add_log2('warning', 'Reading Services Objects and Groups', 'The Service [' . $name_ext . '] has a custom Time-Out [' . $timeout . ']', $source, 'If you are migrating to PanOS 8.1 it will be applied if not you will have to AppOverride it.', 'objects', $lid, 'services');
                            break;
                        default:
                            break;

                    }
                }
            }
        }

    }

    function get_services_groups($screenos_config_file, $pan, $servicegroup)
    {

        global $debug;
        global $print;
        global $add_srv;

        //20190430 SVen add addressgroup SNMP with member TCP-SNMP and UDP-SNMP
        //better do add do shared, but now added to each VSYS


        $v = $pan->findVirtualSystem('vsys1');
        if( $v === null )
        {
            derr("vsys: vsys1 could not be found ! Exit\n");
        }


        foreach( $screenos_config_file as $line => $names_line )
        {
            $names_line = trim($names_line);

            if( preg_match("/^set vsys /i", $names_line) )
            {
                $v = $this->vsys_parser($names_line, $pan);

                //add SNMP, maybe better to add at shared level
                $tmp_servicegroup = $v->serviceStore->find('SNMP');
                if( $tmp_servicegroup == null )
                {
                    if( $print )
                        print "create servicegroup Object: SNMP\n";
                    $tmp_servicegroup = $v->serviceStore->newServiceGroup('SNMP');
                    $tmp_object = $v->serviceStore->find('TCP-SNMP');
                    if( $tmp_object !== null )
                    {
                        $tmp_servicegroup->addMember($tmp_object);
                    }
                    $tmp_object = $v->serviceStore->find('UDP-SNMP');
                    if( $tmp_object !== null )
                    {
                        $tmp_servicegroup->addMember($tmp_object);
                    }
                }


            }


            if( preg_match("/^set group service /i", $names_line) )
            {
                $data = $this->name_preg_split($names_line);

                $name_ext = $data[3];
                $name_ext = $this->truncate_names($this->normalizeNames($name_ext));

                $tmp_servicegroup = $v->serviceStore->find($name_ext);
                if( $tmp_servicegroup == null )
                {
                    if( $print )
                        print "create servicegroup Object: " . $name_ext . "\n";
                    $tmp_servicegroup = $v->serviceStore->newServiceGroup($name_ext);

                    #print "create servicegroup object: ".$name_ext."\n";
                }

                if( isset($data[4]) )
                {
                    switch ($data[4])
                    {
                        case "comment":
                            $comment = $data[5];

                            //PAN-OS does not has servicegroup description possiblities
                            #$tmp_servicegroup->setDescription( $comment );
                            break;

                        case "add":
                            $member = $data[5];
                            #$member = $this->truncate_names($this->normalizeNames($member));
                            $tmp_object = $v->serviceStore->find($member);
                            if( $tmp_object !== null )
                            {
                                $tmp_servicegroup->addMember($tmp_object);
                            }
                            else
                            {
                                $member = $this->truncate_names($this->normalizeNames($member));
                                $tmp_object = $v->serviceStore->find($member);
                                if( $tmp_object !== null )
                                {
                                    $tmp_servicegroup->addMember($tmp_object);
                                }

                                else
                                {
                                    $found = FALSE;
                                    foreach( $add_srv as $key => $tmp_srv )
                                    {
                                        if( $tmp_srv[0] == $member )
                                        {
                                            $tmp_service = $v->serviceStore->find($tmp_srv[1]);
                                            if( $tmp_service !== null )
                                            {
                                                $tmp_servicegroup->addMember($tmp_service);
                                                $found = TRUE;
                                            }
                                        }
                                    }

                                    if( !$found )
                                    {
                                        $tmp_service = $v->serviceStore->newService($member, 'tcp', "65000");
                                        $tmp_service->setDescription("FIX service - protocol / port / custom app");

                                        if( $tmp_service !== null )
                                        {
                                            $tmp_servicegroup->addMember($tmp_service);
                                        }
                                        if( $debug )
                                            print "servicegroup: " . $tmp_servicegroup->name() . " service object not available : " . $member . " \n";
                                        $servicegroup[$tmp_servicegroup->name()][$v->name()][] = $member;
                                    }
                                }
                            }
                            break;

                        default:

                    }
                }
            }
        }

        return $servicegroup;
    }


    function add_netscreen_services($pan)
    {
        global $add_srv;
        global $print;

        #$add_srv[]="('$source','$vsys','junos-ftp','junos-ftp','21','tcp')";
        $add_srv[] = array('FTP', 'FTP', '21', 'tcp');
        $add_srv[] = array('SSH', 'SSH', '22', 'tcp');
        $add_srv[] = array('TELNET', 'TELNET', '23', 'tcp');
        $add_srv[] = array('MAIL', 'MAIL', '25', 'tcp');
        $add_srv[] = array('SMTP', 'SMTP', '25', 'tcp');
        $add_srv[] = array('WHOIS', 'WHOIS', '43', 'tcp');
        $add_srv[] = array('GOPHER', 'GOPHER', '70', 'tcp');
        $add_srv[] = array('FINGER', 'FINGER', '79', 'tcp');
        $add_srv[] = array('POP3', 'POP3', '110', 'tcp');
        $add_srv[] = array('IDENT', 'IDENT', '113', 'tcp');
        $add_srv[] = array('NNTP', 'NNTP', '119', 'tcp');
        $add_srv[] = array('SMB', 'SMB', '139', 'tcp');
        $add_srv[] = array('IMAP', 'IMAP', '143', 'tcp');
        $add_srv[] = array('BGP', 'BGP', '179', 'tcp');
        $add_srv[] = array('WAIS', 'WAIS', '210', 'tcp');
        $add_srv[] = array('LDAP', 'LDAP', '389', 'tcp');
        $add_srv[] = array('REXEC', 'REXEC', '512', 'tcp');
        $add_srv[] = array('RLOGIN', 'RLOGIN', '513', 'tcp');
        $add_srv[] = array('RSH', 'RSH', '514', 'tcp');
        $add_srv[] = array('LPR', 'LPR', '515', 'tcp');
        $add_srv[] = array('RTSP', 'RTSP', '554', 'tcp');
        $add_srv[] = array('MS-SQL', 'MS-SQL', '1433', 'tcp');
        $add_srv[] = array('WINFRAME', 'WINFRAME', '1494', 'tcp');
        $add_srv[] = array('SQL*Net V2', 'SQLNet V2', '1521', 'tcp');
        $add_srv[] = array('SQL*Net V1', 'SQLNet V1', '1525', 'tcp');
        $add_srv[] = array('H.323', 'H.323', '1720', 'tcp');
        $add_srv[] = array('NetMeeting', 'NetMeeting', '1720', 'tcp');
        $add_srv[] = array('MSN', 'MSN', '1863', 'tcp');
        $add_srv[] = array('SCCP', 'SCCP', '2000', 'tcp');
        $add_srv[] = array('YMSG', 'YMSG', '5050', 'tcp');
        $add_srv[] = array('VNC', 'VNC', '5800', 'tcp');
        $add_srv[] = array('HTTP-EXT', 'HTTP-EXT', '7001', 'tcp');
        $add_srv[] = array('Real Media', 'Real Media', '7070', 'tcp');
        $add_srv[] = array('NS Global', 'NS Global', '15397', 'tcp');
        $add_srv[] = array('NS Global PRO', 'NS Global PRO', '15397', 'tcp');
        $add_srv[] = array('ECHO', 'ECHO', '7', 'udp');
        $add_srv[] = array('DISCARD', 'DISCARD', '9', 'udp');
        $add_srv[] = array('CHARGEN', 'CHARGEN', '19', 'udp');
        $add_srv[] = array('DNS', 'DNS', '53', 'udp');
        $add_srv[] = array('DHCP-Relay', 'DHCP-Relay', '67', 'udp');
        $add_srv[] = array('NSM', 'NSM', '69', 'udp');
        $add_srv[] = array('TFTP', 'TFTP', '69', 'udp');
        $add_srv[] = array('NFS', 'NFS', '111', 'udp');
        $add_srv[] = array('SUN-RPC-PORTMAPPER', 'SUN-RPC-PORTMAPPER', '111', 'udp');
        $add_srv[] = array('NTP', 'NTP', '123', 'udp');
        $add_srv[] = array('MS-RPC-EPM', 'MS-RPC-EPM', '135', 'udp');
        $add_srv[] = array('NBNAME', 'NBNAME', '137', 'udp');
        $add_srv[] = array('NBDS', 'NBDS', '138', 'udp');
        $add_srv[] = array('IKE', 'IKE', '500', 'udp');
        $add_srv[] = array('IKE-NAT', 'IKE-NAT', '500', 'udp');
        $add_srv[] = array('SYSLOG', 'SYSLOG', '514', 'udp');
        $add_srv[] = array('RIP', 'RIP', '520', 'udp');
        $add_srv[] = array('UUCP', 'UUCP', '540', 'udp');
        $add_srv[] = array('SQL Monitor', 'SQL Monitor', '1434', 'udp');
        $add_srv[] = array('L2TP', 'L2TP', '1701', 'udp');
        $add_srv[] = array('GTP', 'GTP', '2123', 'udp');
        $add_srv[] = array('MGCP-UA', 'MGCP-UA', '2427', 'udp');
        $add_srv[] = array('MGCP-CA', 'MGCP-CA', '2727', 'udp');
        $add_srv[] = array('SIP', 'SIP', '5060', 'udp');
        $add_srv[] = array('PC-Anywhere', 'PC-Anywhere', '5632', 'udp');
        $add_srv[] = array('X-WINDOWS', 'X-WINDOWS', '6000-6063', 'tcp');
        $add_srv[] = array('SNMP', 'SNMP', '161', 'udp');
        $add_srv[] = array('AOL', 'AOL', '5190-5194', 'tcp');
        $add_srv[] = array('APPLE-ICHAT-SNATMAP', 'APPLE-ICHAT-SNATMAP', '5678', 'udp');
        $add_srv[] = array('GNUTELLA', 'GNUTELLA', '6346-6347', 'udp');
        $add_srv[] = array('IRC', 'IRC', '6660-6669', 'tcp');
        $add_srv[] = array('PPTP', 'PPTP', '1723', 'tcp');
        $add_srv[] = array('RADIUS', 'RADIUS', '1812-1813', 'udp');
        $add_srv[] = array('TALK', 'TALK', '517-518', 'udp');
        $add_srv[] = array('VDO_Live', 'VDO Live', '7000-7010', 'tcp');
        $add_srv[] = array('UDP-ANY', 'UDP-ANY', '0-65535', 'udp');
        $add_srv[] = array('TCP-ANY', 'TCP-ANY', '0-65535', 'tcp');

        //wrong name in PAN KB????
        $add_srv[] = array('Internet Locator Service', 'Internet Locator Service', '389', 'tcp');

        //not available in PAN knowledgebase
        $add_srv[] = array('FTP-Get', 'FTP-Get', '21', 'tcp');
        $add_srv[] = array('FTP-Put', 'FTP-Put', '21', 'tcp');
        $add_srv[] = array('HTTP', 'HTTP', '80', 'tcp');
        $add_srv[] = array('HTTPS', 'HTTPS', '443', 'tcp');
        $add_srv[] = array('VNC_XH', 'VNC_XH', '5901', 'tcp');
        $add_srv[] = array('VNC_guest', 'VNC_guest', '5902', 'tcp');
        $add_srv[] = array('UDP-SNMP', 'UDP-SNMP', '161', 'udp');
        $add_srv[] = array('TCP-SNMP', 'TCP-SNMP', '161', 'tcp');
        $add_srv[] = array('MSRPC', 'MSRPC', '135', 'tcp');
        $add_srv[] = array('SQL', 'SQL', '1521', 'tcp');
        $add_srv[] = array('XING', 'XING', '1558', 'udp');


        $add_srv[] = array('MS-RPC-ANY', 'MS-RPC-ANY', '', '');


        $add_srv[] = array('PING', 'PING', '', 'icmp');
        $add_srv[] = array('TRACEROUTE', 'TRACEROUTE', '', 'icmp');
        $add_srv[] = array('ICMP-ANY', 'ICMP-ANY', '', 'icmp');
        $add_srv[] = array('ICMP-INFO', 'ICMP-INFO', '', 'icmp');
        $add_srv[] = array('ICMP-TIMESTAMP', 'ICMP-TIMESTAMP', '', 'icmp');
        $add_srv[] = array('ICMP Address Mask', 'ICMP Address Mask', '', 'icmp');


        $add_srv2 = array();
        $add_srv2[] = array('AOL', 'AOL', 'tcp', '5190-5194');
        $add_srv2[] = array('APPLE-ICHAT-SNATMAP', 'APPLE-ICHAT-SNATMAP', 'udp', '5678');
        $add_srv2[] = array('BGP', 'BGP', 'tcp', '179');
        $add_srv2[] = array('CHARGEN', 'CHARGEN', 'udp', '19');
        $add_srv2[] = array('DHCP-Relay', 'DHCP-Relay', 'udp', '67');
        $add_srv2[] = array('DISCARD', 'DISCARD', 'udp', '9');
        $add_srv2[] = array('DNS', 'DNS', 'udp', '53');
        $add_srv2[] = array('ECHO', 'ECHO', 'udp', '7');
        $add_srv2[] = array('FINGER', 'FINGER', 'tcp', '79');
        $add_srv2[] = array('FTP', 'FTP', 'tcp', '21');
        $add_srv2[] = array('GNUTELLA', 'GNUTELLA', 'udp', '6346-6347');
        $add_srv2[] = array('GOPHER', 'GOPHER', 'tcp', '70');
        $add_srv2[] = array('GTP', 'GTP', 'tcp', '3386');
        $add_srv2[] = array('H323', 'H323', 'tcp', '1720');
        $add_srv2[] = array('HTTP-EXT', 'HTTP-EXT', 'tcp', '8000-8001');
        $add_srv2[] = array('IDENT', 'IDENT', 'tcp', '113');
        $add_srv2[] = array('IKE', 'IKE', 'udp', '500');
        $add_srv2[] = array('IKE-NAT', 'IKE-NAT', 'udp', '500');
        $add_srv2[] = array('IMAP', 'IMAP', 'tcp', '143');
        $add_srv2[] = array('InternetLocatorService', 'InternetLocatorService', 'tcp', '389');
        $add_srv2[] = array('IRC', 'IRC', 'tcp', '6660-6669');
        $add_srv2[] = array('L2TP', 'L2TP', 'udp', '1701');
        $add_srv2[] = array('LDAP', 'LDAP', 'tcp', '389');
        $add_srv2[] = array('LPR', 'LPR', 'tcp', '515');
        $add_srv2[] = array('MAIL', 'MAIL', 'tcp', '25');
        $add_srv2[] = array('MGCP-CA', 'MGCP-CA', 'udp', '2727');
        $add_srv2[] = array('MGCP-UA', 'MGCP-UA', 'udp', '2427');
        $add_srv2[] = array('MS-RPC-EPM', 'MS-RPC-EPM', 'udp', '135');
        $add_srv2[] = array('MS-SQL', 'MS-SQL', 'tcp', '1433');
        $add_srv2[] = array('MSN', 'MSN', 'tcp', '1863');
        $add_srv2[] = array('NBDS', 'NBDS', 'udp', '138');
        $add_srv2[] = array('NBNAME', 'NBNAME', 'udp', '137');
        $add_srv2[] = array('NetMeeting', 'NetMeeting', 'tcp', '1720');
        $add_srv2[] = array('NFS', 'NFS', 'udp', '111');
        $add_srv2[] = array('NNTP', 'NNTP', 'tcp', '119');
        $add_srv2[] = array('NS_Global', 'NS_Global', 'tcp', '15397');
        $add_srv2[] = array('NS_Global_Pro', 'NS_Global_Pro', 'tcp', '15397');
        $add_srv2[] = array('NSM', 'NSM', 'udp', '69');
        $add_srv2[] = array('NTP', 'NTP', 'udp', '123');
        $add_srv2[] = array('PC-Anywhere', 'PC-Anywhere', 'udp', '5632');
        $add_srv2[] = array('POP3', 'POP3', 'tcp', '110');
        $add_srv2[] = array('PPTP', 'PPTP', 'tcp', '1723');
        $add_srv2[] = array('RADIUS', 'RADIUS', 'udp', '1812-1813');
        $add_srv2[] = array('Real_Media', 'Real_Media', 'tcp', '7070');
        $add_srv2[] = array('REXEC', 'REXEC', 'tcp', '512');
        $add_srv2[] = array('RIP', 'RIP', 'udp', '520');
        $add_srv2[] = array('RLOGIN', 'RLOGIN', 'tcp', '513');
        $add_srv2[] = array('RSH', 'RSH', 'tcp', '514');
        $add_srv2[] = array('RTSP', 'RTSP', 'tcp', '554');
        $add_srv2[] = array('SCCP', 'SCCP', 'tcp', '2000');
        $add_srv2[] = array('SIP', 'SIP', 'udp', '5060');
        $add_srv2[] = array('SMB', 'SMB', 'tcp', '139');
        $add_srv2[] = array('SMTP', 'SMTP', 'tcp', '25');
        $add_srv2[] = array('SNMP', 'SNMP', 'udp', '161');
        $add_srv2[] = array('SQL_Monitor', 'SQL_Monitor', 'udp', '1434');
        $add_srv2[] = array('SQLNet_V1', 'SQLNet_V1', 'tcp', '1525');
        $add_srv2[] = array('SQLNet_V2', 'SQLNet_V2', 'tcp', '1521');
        $add_srv2[] = array('SSH', 'SSH', 'tcp', '22');
        $add_srv2[] = array('SUN-RPC-PORTMAPPER', 'SUN-RPC-PORTMAPPER', 'udp', '111');
        $add_srv2[] = array('SYSLOG', 'SYSLOG', 'udp', '514');
        $add_srv2[] = array('TALK', 'TALK', 'udp', '517-518');
        $add_srv2[] = array('TCP-ANY', 'TCP-ANY', 'tcp', '0-65535');
        $add_srv2[] = array('TELNET', 'TELNET', 'tcp', '23');
        $add_srv2[] = array('TFTP', 'TFTP', 'udp', '69');
        $add_srv2[] = array('UDP-ANY', 'UDP-ANY', 'udp', '0-65535');
        $add_srv2[] = array('UUCP', 'UUCP', 'udp', '540');
        $add_srv2[] = array('VDO_Live', 'VDO_Live', 'tcp', '7000-7010');
        $add_srv2[] = array('VNC', 'VNC', 'tcp', '5800');
        $add_srv2[] = array('WAIS', 'WAIS', 'tcp', '210');
        $add_srv2[] = array('WHOIS', 'WHOIS', 'tcp', '43');
        $add_srv2[] = array('WINFRAME', 'WINFRAME', 'tcp', '1494');
        $add_srv2[] = array('X-WINDOWS', 'X-WINDOWS', 'tcp', '6000-6063');
        $add_srv2[] = array('YMSG', 'YMSG', 'tcp', '5050');

        /*
         * //20190516 SVEN - needed to compare PAN KB information of predefined Netscreen services
                foreach( $add_srv2 as $srv2 )
                {
                    $available = false;
                    #print "search for ".$srv2[1]."\n";
                    foreach( $add_srv as $srv1 )
                    {
                        #print "search2: ".$srv1[1]."\n";
                        if( $srv1[1] == $srv2[1] )
                            $available = true;
                    }

                    if( !$available )
                        print_r( $srv2 );

                }

                foreach( $add_srv as $srv1 )
                {
                    $available = false;
                    #print "search for ".$srv2[1]."\n";
                    foreach( $add_srv2 as $srv2 )
                    {
                        #print "search2: ".$srv1[1]."\n";
                        if( $srv1[1] == $srv2[1] )
                            $available = true;
                    }

                    if( !$available )
                        print_r( $srv1 );

                }
                exit;
        */
        $vsyss = $pan->virtualSystems;

        foreach( $add_srv as $id => $service )
        {
            #print_r($service);
            if( $service[3] == 'tcp' || $service[3] == 'udp' )
            {
                //create service
                foreach( $vsyss as $vsys )
                {
                    $tmp_service = $vsys->serviceStore->find($service[1]);
                    if( $tmp_service === FALSE )
                    {
                        if( $print )
                            print "create service Object: " . $service[1] . ", " . $service[3] . ", " . $service[2] . "\n";
                        $tmp_service = $vsys->serviceStore->newService($service[1], $service[3], $service[2]);
                    }
                }
            }
            elseif( $service[3] != '' )
            {
                #$tmp_service = $vsys->serviceStore->findOrCreate($service[1]);
                #print $id.": service |".$service[1]."| with protocol: |".$service[3]."| can not created - use custom app-id\n";

                $newname = "tmp-" . $service[1];
                $tmp_service = $vsys->serviceStore->find($newname);
                if( $tmp_service === FALSE )
                {
                    if( $print )
                        print "create service Object: " . $newname . ", tcp, 6500\n";
                    $tmp_service = $vsys->serviceStore->newService($newname, 'tcp', '6500');
                }
            }
        }
    }

    function fix_servicegroup_tmp_service($pan, $servicegroup)
    {
        global $debug;
        global $print;


        foreach( $servicegroup as $key => $vsyss )
        {
            foreach( $vsyss as $key2 => $groups )
            {
                //search vsys $key2
                $v = $pan->findVirtualSystem($key2);
                if( $v === null )
                {
                    derr("vsys: vsys1 could not be found ! Exit\n");
                }

                $tmp_servicegroup = $v->serviceStore->find($key);
                $tmp_rule_references = $tmp_servicegroup->refrules;


                foreach( $tmp_rule_references as $ref )
                {

                    $refClass = get_class($ref);
                    if( $refClass == 'ServiceGroup' )
                    {

                    }
                    elseif( $refClass == 'ServiceRuleContainer' )
                    {
                        /** @var ServiceRuleContainer $ref */

                        $ruleClass = get_class($ref->owner);
                        if( $ruleClass == 'SecurityRule' )
                        {
                            foreach( $groups as $key3 => $group )
                            {

                                $tmp_service2 = $v->serviceStore->findOrCreate($group);
                                $ref->add($tmp_service2);
                            }


                        }
                        elseif( $ruleClass == 'NatRule' )
                        {
                            if( $debug )
                                mwarning('unsupported use case in ' . $ref->_PANC_shortName(), null, FALSE);
                        }
                        else
                            if( $debug )
                                mwarning('unsupported owner_class: ' . $ruleClass, null, FALSE);
                    }
                    else
                        if( $debug )
                            mwarning('unsupport class : ' . $refClass, null, FALSE);
                }


            }
        }
    }


    function fix_services_multiple_protocol($vsys, $source)
    {
        global $projectdb;
        $getDup = $projectdb->query("SELECT name_ext,name,count(id) as duplicates FROM services WHERE source='$source' AND dport!='' AND vsys='$vsys' GROUP BY name HAVING duplicates > 1;");
        while( $names = $getDup->fetch_assoc() )
        {
            $name_int = $names['name'];
            $originalname = $names['name'];
            add_log('1', 'Phase 3: Reading Services Objects and Groups', 'Creating ServiceGroup [' . $name_int . ']. Original SRV contains more than one Protocol', $source, 'No Action Requierd.');
            $getSGDup = $projectdb->query("SELECT id FROM services_groups_id WHERE source='$source' AND BINARY name='$name_int';");
            if( $getSGDup->num_rows == 0 )
            {
                $projectdb->query("INSERT INTO services_groups_id (name,name_ext,source,vsys) values ('$name_int','$originalname','$source','$vsys');");
                $grplid = $projectdb->insert_id;
            }
            else
            {
                #EXISTS Raise error?
            }

            $getElement = $projectdb->query("SELECT id,name_ext,name,protocol FROM services WHERE source='$source' AND BINARY name='$name_int';");
            while( $data2 = $getElement->fetch_assoc() )
            {
                $id = $data2['id'];
                $protocol = $data2['protocol'];
                $mynewname = $name_int . "-" . $protocol;
                $mynewname_int = $this->truncate_names($this->normalizeNames($mynewname));
                $projectdb->query("UPDATE services SET name_ext='$mynewname', name='$mynewname_int' WHERE id='$id';");
                $projectdb->query("INSERT INTO services_groups (lid,member_lid,table_name,vsys,source) VALUES ('$grplid','$id','services','$vsys','$source')");
            }
        }
    }


}



