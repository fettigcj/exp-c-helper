<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */

trait FORTINETservices
{
    function add_fortinet_services($ismultiornot, $pan, $source)
    {

        global $projectdb;

        global $debug;
        global $print;

        $table = "services";
        $table_group = "services_groups";
        $table_group_id = "services_groups_id";

        if( $ismultiornot == "multivsys" )
        {
            $vsys = "shared";
            //$table = "shared_services";
            //$table_group = "shared_services_groups";
            //$table_group_id = "shared_services_groups_id";

            $v = $pan;
        }
        else
        {
            $vsys = "root";
            /*
                    $all_vsys = $pan->getVirtualSystems();
                    $counter = 0;
                    foreach( $all_vsys as $vsys)
                    {
                        print "vsys: ".$vsys->name()." - ".$vsys->alternativeName()."\n";
                        $counter++;
                    }

                    $v = $pan->findVSYS_by_displayName( $vsys );
            */
            $v = $pan->findVirtualSystem('vsys1');
        }


        if( $vsys !== "shared" && $v === null )
        {
            derr("VSYS " . $vsys . " not found");
        }

        $tmp_service_array = array();
        $tmp_servicegroup_array = array();

        /*
         * Todo: swaschkut 20190930
         * 1) what about "TRACEROUTE / PING / ICMP-ANY"????
         */
        //https://help.fortinet.com/fos40hlp/43/wwhelp/wwhimpl/common/html/wwhelp.htm?context=fgt&file=fw_components.12.23.html

        $tmp_service_array[] = array('FTP', 'FTP', '21', 'tcp');
        $tmp_service_array[] = array('FTP_Get', 'FTP_Get', '21', 'tcp');
        $tmp_service_array[] = array('FTP_Put', 'FTP_Put', '21', 'tcp');
        $tmp_service_array[] = array('SSH', 'SSH', '22', 'tcp');
        $tmp_service_array[] = array('TELNET', 'TELNET', '23', 'tcp');
        $tmp_service_array[] = array('MAIL', 'MAIL', '25', 'tcp');
        $tmp_service_array[] = array('IMAP', 'IMAP', '143', 'tcp');
        $tmp_service_array[] = array('IMAPS', 'IMAPS', '993', 'tcp');
        $tmp_service_array[] = array('SMTP', 'SMTP', '25', 'tcp');
        $tmp_service_array[] = array('WHOIS', 'WHOIS', '43', 'tcp');
        $tmp_service_array[] = array('GOPHER', 'GOPHER', '70', 'tcp');
        $tmp_service_array[] = array('FINGER', 'FINGER', '79', 'tcp');
        $tmp_service_array[] = array('HTTP', 'HTTP', '80', 'tcp');
        $tmp_service_array[] = array('POP3', 'POP3', '110', 'tcp');
        $tmp_service_array[] = array('IDENT', 'IDENT', '113', 'tcp');
        $tmp_service_array[] = array('NNTP', 'NNTP', '119', 'tcp');
        $tmp_service_array[] = array('SMB', 'SMB', '445', 'tcp');
        $tmp_service_array[] = array('BGP', 'BGP', '179', 'tcp');
        $tmp_service_array[] = array('WAIS', 'WAIS', '210', 'tcp');
        $tmp_service_array[] = array('Internet Locator Service', 'Internet Locator Service', '389', 'tcp');
        $tmp_service_array[] = array('LDAP', 'LDAP', '389', 'tcp');
        $tmp_service_array[] = array('HTTPS', 'HTTPS', '443', 'tcp');
        $tmp_service_array[] = array('REXEC', 'REXEC', '512', 'tcp');
        $tmp_service_array[] = array('RLOGIN', 'RLOGIN', '513', 'tcp');
        $tmp_service_array[] = array('RSH', 'RSH', '514', 'tcp');
        $tmp_service_array[] = array('LPR', 'LPR', '515', 'tcp');

        $tmp_service_array[] = array('MS-SQL', 'MS-SQL', '1433,1434', 'tcp');
        $tmp_service_array[] = array('MYSQL', 'MYSQL', '3306', 'tcp');
        $tmp_service_array[] = array('WINFRAME', 'WINFRAME', '1494', 'tcp');

        //Todo: * not a valid PAN-OS name - needed?
        #$tmp_service_array[] = array( 'SQL*Net V2','SQL Net V2','1521','tcp' );
        #$tmp_service_array[] = array( 'SQL*Net V1','SQL Net V1','1525','tcp' );

        $tmp_service_array[] = array('H.323', 'H.323', '1720', 'tcp');
        $tmp_service_array[] = array('NetMeeting', 'NetMeeting', '1720', 'tcp');
        $tmp_service_array[] = array('MSN', 'MSN', '1863', 'tcp');
        $tmp_service_array[] = array('SCCP', 'SCCP', '2000', 'tcp');
        $tmp_service_array[] = array('YMSG', 'YMSG', '5050', 'tcp');
        $tmp_service_array[] = array('VNC', 'VNC', '5900', 'tcp');
        $tmp_service_array[] = array('VNC_XH', 'VNC_XH', '5901', 'tcp');
        $tmp_service_array[] = array('VNC_guest', 'VNC_guest', '5902', 'tcp');
        $tmp_service_array[] = array('HTTP-EXT', 'HTTP-EXT', '7001', 'tcp');
        $tmp_service_array[] = array('Real Media', 'Real Media', '7070', 'tcp');
        $tmp_service_array[] = array('NS Global', 'NS Global', '15397', 'tcp');
        $tmp_service_array[] = array('NS Global PRO', 'NS Global PRO', '15397', 'tcp');
        $tmp_service_array[] = array('ECHO', 'ECHO', '7', 'udp');
        $tmp_service_array[] = array('DISCARD', 'DISCARD', '9', 'udp');
        $tmp_service_array[] = array('CHARGEN', 'CHARGEN', '19', 'udp');
        $tmp_service_array[] = array('DHCP-Relay', 'DHCP-Relay', '67', 'udp');
        $tmp_service_array[] = array('DHCP', 'DHCP', '67-68', 'udp');
        $tmp_service_array[] = array('NSM', 'NSM', '69', 'udp');
        $tmp_service_array[] = array('TFTP', 'TFTP', '69', 'udp');

        $tmp_service_array[] = array('SUN-RPC-PORTMAPPER', 'SUN-RPC-PORTMAPPER', '111', 'udp');

        $tmp_service_array[] = array('MS-RPC-EPM', 'MS-RPC-EPM', '135', 'udp');
        $tmp_service_array[] = array('NBNAME', 'NBNAME', '137', 'udp');
        $tmp_service_array[] = array('NBDS', 'NBDS', '138', 'udp');

        $tmp_service_array[] = array('IKE', 'IKE', '500,4500', 'udp');
        $tmp_service_array[] = array('IKE-NAT', 'IKE-NAT', '500', 'udp');
        $tmp_service_array[] = array('SYSLOG', 'SYSLOG', '514', 'udp');
        $tmp_service_array[] = array('RIP', 'RIP', '520', 'udp');
        $tmp_service_array[] = array('UUCP', 'UUCP', '540', 'udp');
        $tmp_service_array[] = array('SQL Monitor', 'SQL Monitor', '1434', 'udp');

        $tmp_service_array[] = array('GTP', 'GTP', '2123', 'udp');
        $tmp_service_array[] = array('MGCP-UA', 'MGCP-UA', '2427', 'udp');
        $tmp_service_array[] = array('MGCP-CA', 'MGCP-CA', '2727', 'udp');

        $tmp_service_array[] = array('X-WINDOWS', 'X-WINDOWS', '6000-6063', 'tcp');

        $tmp_service_array[] = array('MSRPC', 'MSRPC', '135', 'tcp');
        $tmp_service_array[] = array('SQL', 'SQL', '1521', 'tcp');
        $tmp_service_array[] = array('XING', 'XING', '1558', 'udp');
        $tmp_service_array[] = array('RADIUS', 'RADIUS', '1812-1813', 'udp');
        $tmp_service_array[] = array('SQUID', 'SQUID', '3128', 'tcp');
        $tmp_service_array[] = array('RDP', 'RDP', '3389', 'tcp');
        $tmp_service_array[] = array('SAMBA', 'SAMBA', '139', 'tcp');
        $tmp_service_array[] = array('SAMBA_UDP', 'SAMBA_UDP', '139', 'udp');


        $tmp_servicegroup_array[] = "RTSP";
        $tmp_service_array[] = array('RTSP_UDP', 'RTSP_UDP', '554', 'udp');
        $tmp_service_array[] = array('RTSP_TCP', 'RTSP_TCP', '554,7070,8554', 'tcp');


        $tmp_servicegroup_array[] = "DNS";
        $tmp_service_array[] = array('DNS_UDP', 'DNS_UDP', '53', 'udp');
        $tmp_service_array[] = array('DNS_TCP', 'DNS_TCP', '53', 'tcp');


        $tmp_servicegroup_array[] = "SNMP";
        $tmp_service_array[] = array('SNMP_UDP', 'SNMP_UDP', '161-162', 'udp');
        $tmp_service_array[] = array('SNMP_TCP', 'SNMP_TCP', '161-162', 'tcp');


        $tmp_servicegroup_array[] = "DCE-RPC";
        $tmp_service_array[] = array('DCE-RPC_UDP', 'DCE-RPC_UDP', '135', 'udp');
        $tmp_service_array[] = array('DCE-RPC_TCP', 'DCE-RPC_TCP', '135', 'tcp');


        $tmp_servicegroup_array[] = "Kerberos";
        $tmp_service_array[] = array('Kerberos_UDP', 'Kerberos_UDP', '88', 'udp');
        $tmp_service_array[] = array('Kerberos_TCP', 'Kerberos_TCP', '88', 'tcp');


        $tmp_servicegroup_array[] = "NTP";
        $tmp_service_array[] = array('NTP_UDP', 'NTP_UDP', '123', 'udp');
        $tmp_service_array[] = array('NTP_TCP', 'NTP_TCP', '123', 'tcp');


        $tmp_servicegroup_array[] = "H323";
        $tmp_service_array[] = array('H323_TCP', 'H323_TCP', '1720,1503', 'tcp');
        $tmp_service_array[] = array('H323_UDP', 'H323_UDP', '1719', 'udp');


        $tmp_servicegroup_array[] = "SIP";
        $tmp_service_array[] = array('SIP_UDP', 'SIP_UDP', '5060', 'udp');
        $tmp_service_array[] = array('SIP_TCP', 'SIP_TCP', '5060', 'tcp');


        $tmp_servicegroup_array[] = "L2TP";
        $tmp_service_array[] = array('L2TP_UDP', 'L2TP_UDP', '1701', 'udp');
        $tmp_service_array[] = array('L2TP_TCP', 'L2TP_TCP', '1701', 'tcp');

        $tmp_servicegroup_array[] = "NFS";
        $tmp_service_array[] = array('NFS_UDP', 'NFS_UDP', '111,2049', 'udp');
        $tmp_service_array[] = array('NFS_TCP', 'NFS_TCP', '111,2049', 'tcp');

        $tmp_servicegroup_array[] = "PC-Anywhere";
        $tmp_service_array[] = array('PC-Anywhere_UDP', 'PC-Anywhere_UDP', '5632', 'udp');
        $tmp_service_array[] = array('PC-Anywhere_TCP', 'PC-Anywhere_TCP', '5631', 'tcp');

        foreach( $tmp_service_array as $tmp_service )
        {
            $name = $tmp_service[0];
            $dport = $tmp_service[2];
            $srv_protocol = $tmp_service[3];

            $name = $this->truncate_names($this->normalizeNames($name));
            $tmp_service = $v->serviceStore->find($name);
            if( $tmp_service === null )
            {
                if( $print )
                    print " * create service " . $name . " -proto:" . $srv_protocol . " -dport:|" . $dport . "\n";

                $tmp_service = $v->serviceStore->newService($name, $srv_protocol, $dport);
                $tmp_tag = $v->tagStore->findorCreate('predefined');
                $tmp_service->tags->addTag($tmp_tag);
            }
            else
            {
                if( $print )
                    mwarning(' X service with name: ' . $name . " already available", null, FALSE);
            }

        }


        foreach( $tmp_servicegroup_array as $tmp_servicegroup )
        {
            $name = $tmp_servicegroup;

            $name = $this->truncate_names($this->normalizeNames($name));
            $tmp_servicegroup = $v->serviceStore->find($name);
            if( $tmp_servicegroup == null )
            {
                if( $print )
                    print "\n * create servicegroup Object: " . $name . "\n";
                $tmp_servicegroup = $v->serviceStore->newServiceGroup($name);
                //TCP
                $tmp_object = $v->serviceStore->find($name . '_TCP');
                if( $tmp_object !== null )
                {
                    $tmp_servicegroup->addMember($tmp_object);
                }
                //UDP
                $tmp_object = $v->serviceStore->find($name . '_UDP');
                if( $tmp_object !== null )
                {
                    $tmp_servicegroup->addMember($tmp_object);
                }
                if( $print )
                    print "  * add service Objects: " . $name . "_TCP and " . $name . "_UDP - to servicegroup " . $tmp_servicegroup->name() . "\n";
            }
            else
            {
                mwarning(' X servicegroup with name: ' . $name . " already available", null, FALSE);
            }
        }
        /*
            # Create Groups when service is TCP and UDP defined
            $serviceName="RTSP";
             #$projectdb->query("INSERT INTO $table_group_id (source,vsys,name_ext,name) VALUES ('$source','$vsys','$serviceName','$serviceName');");
            $glid=$projectdb->insert_id;
            $tmp_service_array[] = array( 'RTSP_UDP','RTSP_UDP','554','udp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','RTSP_UDP','$mlid','$table','$glid' );
            $tmp_service_array[] = array( 'RTSP_TCP','RTSP_TCP','554,7070,8554','tcp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','RTSP_TCP','$mlid','$table','$glid' );

            $serviceName="DNS";
             #$projectdb->query("INSERT INTO $table_group_id (source,vsys,name_ext,name) VALUES ('$source','$vsys','$serviceName','$serviceName');");
            $glid=$projectdb->insert_id;
            $tmp_service_array[] = array( 'DNS_UDP','DNS_UDP','53','udp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','DNS_UDP','$mlid','$table','$glid' );
            $tmp_service_array[] = array( 'DNS_TCP','DNS_TCP','53','tcp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','DNS_TCP','$mlid','$table','$glid' );

            $serviceName="SNMP";
             #$projectdb->query("INSERT INTO $table_group_id (source,vsys,name_ext,name) VALUES ('$source','$vsys','$serviceName','$serviceName');");
            $glid=$projectdb->insert_id;
            $tmp_service_array[] = array( 'SNMP_UDP','SNMP_UDP','161-162','udp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','SNMP_UDP','$mlid','$table','$glid' );
            $tmp_service_array[] = array( 'SNMP_TCP','SNMP_TCP','161-162','tcp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','SNMP_TCP','$mlid','$table','$glid' );

            $serviceName="DCE-RPC";
             #$projectdb->query("INSERT INTO $table_group_id (source,vsys,name_ext,name) VALUES ('$source','$vsys','$serviceName','$serviceName');");
            $glid=$projectdb->insert_id;
            $tmp_service_array[] = array( 'DCE-RPC_UDP','DCE-RPC_UDP','135','udp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','DCE-RPC_UDP','$mlid','$table','$glid' );
            $tmp_service_array[] = array( 'DCE-RPC_TCP','DCE-RPC_TCP','135','tcp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','DCE-RPC_TCP','$mlid','$table','$glid' );

            $serviceName="Kerberos";
             #$projectdb->query("INSERT INTO $table_group_id (source,vsys,name_ext,name) VALUES ('$source','$vsys','$serviceName','$serviceName');");
            $glid=$projectdb->insert_id;
            $tmp_service_array[] = array( 'Kerberos_UDP','Kerberos_UDP','88','udp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','Kerberos_UDP','$mlid','$table','$glid' );
            $tmp_service_array[] = array( 'Kerberos_TCP','Kerberos_TCP','88','tcp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','Kerberos_TCP','$mlid','$table','$glid' );

            $serviceName="NTP";
             #$projectdb->query("INSERT INTO $table_group_id (source,vsys,name_ext,name) VALUES ('$source','$vsys','$serviceName','$serviceName');");
            $glid=$projectdb->insert_id;
            $tmp_service_array[] = array( 'NTP_UDP','NTP_UDP','123','udp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','NTP_UDP','$mlid','$table','$glid' );
            $tmp_service_array[] = array( 'NTP_TCP','NTP_TCP','123','tcp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','NTP_TCP','$mlid','$table','$glid' );

            $serviceName="H323";
             #$projectdb->query("INSERT INTO $table_group_id (source,vsys,name_ext,name) VALUES ('$source','$vsys','$serviceName','$serviceName');");
            $glid=$projectdb->insert_id;
            $tmp_service_array[] = array( 'H323_TCP','H323_TCP','1720,1503','tcp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','H323_TCP','$mlid','$table','$glid' );
            $tmp_service_array[] = array( 'H323_UDP','H323_UDP','1719','udp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','H323_UDP','$mlid','$table','$glid' );

            $serviceName="SIP";
             #$projectdb->query("INSERT INTO $table_group_id (source,vsys,name_ext,name) VALUES ('$source','$vsys','$serviceName','$serviceName');");
            $glid=$projectdb->insert_id;
            $tmp_service_array[] = array( 'SIP_UDP','SIP_UDP','5060','udp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','SIP_UDP','$mlid','$table','$glid' );
            $tmp_service_array[] = array( 'SIP_TCP','SIP_TCP','5060','tcp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','SIP_TCP','$mlid','$table','$glid' );

            $serviceName="L2TP";
             #$projectdb->query("INSERT INTO $table_group_id (source,vsys,name_ext,name) VALUES ('$source','$vsys','$serviceName','$serviceName');");
            $glid=$projectdb->insert_id;
            $tmp_service_array[] = array( 'L2TP_UDP','L2TP_UDP','1701','udp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','L2TP_UDP','$mlid','$table','$glid' );
            $tmp_service_array[] = array( 'L2TP_TCP','L2TP_TCP','1701','tcp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','L2TP_TCP','$mlid','$table','$glid' );

            $serviceName="NFS";
             #$projectdb->query("INSERT INTO $table_group_id (source,vsys,name_ext,name) VALUES ('$source','$vsys','$serviceName','$serviceName');");
            $glid=$projectdb->insert_id;
            $tmp_service_array[] = array( 'NFS_UDP','NFS_UDP','111,2049','udp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','NFS_UDP','$mlid','$table','$glid' );
            $tmp_service_array[] = array( 'NFS_TCP','NFS_TCP','111,2049','tcp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','NFS_TCP','$mlid','$table','$glid' );

            $serviceName="PC-Anywhere";
             #$projectdb->query("INSERT INTO $table_group_id (source,vsys,name_ext,name) VALUES ('$source','$vsys','$serviceName','$serviceName');");
            $glid=$projectdb->insert_id;
            $tmp_service_array[] = array( 'PC-Anywhere_UDP','Anywhere_UDP','5632','udp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','Anywhere_UDP','$mlid','$table','$glid' );
            $tmp_service_array[] = array( 'Anywhere_TCP','Anywhere_TCP','5631','tcp' );
             #$mlid=$projectdb->insert_id;
             #$projectdb->query("INSERT INTO $table_group (source,vsys,member,member_lid,table_name,lid) VALUES ('$source','$vsys','Anywhere_TCP','$mlid','$table','$glid' );

             #$projectdb->query("UPDATE $table SET devicegroup='predefined' WHERE source='$source' AND vsys='$vsys';");
             #$projectdb->query("UPDATE $table_group_id SET devicegroup='predefined' WHERE source='$source' AND vsys='$vsys';");
        */
    }


    function get_services2($data, $source, $v, $ismultiornot)
    {

        global $debug;
        global $print;
        $vsys = $v->alternativeName();

        global $projectdb;
        $isAddress = FALSE;
        $isObject = FALSE;
        $sql = array();
        $type = "";
        $ipaddress = "";
        $netmask = "";
        $cidr = "";
        $addressName = "";
        $protocol_number = "";
        $addressNamePan = "";
        $description = "";
        $getRangeEnd = "";
        $getRangeStart = "";
        $fqdn = "";
        $sql = array();
        $protocol_udp = [];
        $protocol_tcp = [];
        $tcp_src_elements_array = array();
        $tcp_src_elements = "";
        $protocol = "";
        $tcp_dst_elements = "";

        if( $ismultiornot == "singlevsys" )
        {
            $START = TRUE;
            $VDOM = FALSE;
        }
        else
        {
            $START = FALSE;
            $VDOM = FALSE;
        }
        $lastline = "";
        foreach( $data as $line => $names_line )
        {


            $this->check_vdom_start($START, $VDOM, $names_line, $lastline, $vsys);

            if( $START )
            {
                if( preg_match("/^config firewall service custom/i", $names_line) )
                {
                    $isObject = TRUE;
                }

                if( $isObject )
                {
                    if( preg_match("/^end/i", $names_line) )
                    {
                        $isObject = FALSE;
                        $START = FALSE;
                    }
                    if( preg_match("/edit /i", $names_line) )
                    {
                        $isAddress = TRUE;
                        $meta = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                        $addressNamePan = $this->truncate_names($this->normalizeNames($meta[1]));
                        $addressName = trim($meta[1]);
                    }

                    if( $isAddress )
                    {
                        if( preg_match("/next/i", $names_line) )
                        {
                            $isAddress = FALSE;
                            if( (preg_match("/TCP\/UDP/i", $protocol)) or preg_match("/TCP/i", $protocol) or preg_match("/UDP/i", $protocol) )
                            {
                                if( ((count($protocol_udp) == 0) and (is_array($protocol_udp))) and ((count($protocol_tcp) > 0) and (is_array($protocol_tcp))) )
                                {
                                    # IS TCP ONLY
                                    foreach( $protocol_tcp as $k => $v1 )
                                    {
                                        $tcp = explode(":", $v1);
                                        $tcp_dst = $tcp[0];
                                        if( isset($tcp[1]) )
                                        {
                                            $tcp_src = $tcp[1];
                                        }
                                        else
                                        {
                                            $tcp_src = "";
                                        }

                                        # Dst port
                                        if( preg_match("/-/", $tcp_dst) )
                                        {
                                            $tcp_dst_elements = explode("-", $tcp_dst);
                                            if( trim($tcp_dst_elements[0]) == trim($tcp_dst_elements[1]) )
                                            {
                                                $tcp_dst_elements_array[] = $tcp_dst_elements[0];
                                            }
                                            else
                                            {
                                                $tcp_dst_elements_array[] = $tcp_dst;
                                            }
                                        }
                                        else
                                        {
                                            $tcp_dst_elements_array[] = $tcp_dst;
                                        }

                                        # Src Port
                                        if( $tcp_src != "" )
                                        {
                                            if( preg_match("/-/", $tcp_src) )
                                            {
                                                $tcp_src_elements = explode("-", $tcp_src);
                                                if( $tcp_src_elements[0] == $tcp_src_elements[1] )
                                                {
                                                    $tcp_src_elements_array[] = $tcp_src_elements[0];
                                                }
                                                else
                                                {
                                                    $tcp_src_elements_array[] = $tcp_src;
                                                }
                                            }
                                            else
                                            {
                                                $tcp_src_elements_array[] = $tcp_src;
                                            }
                                        }
                                    }


                                    if( count($tcp_src_elements_array) > 0 )
                                    {
                                        $tcp_src_elements = implode(",", $tcp_src_elements_array);
                                    }
                                    else
                                    {
                                        $tcp_src_elements = '';
                                    }

                                    $tcp_dst_elements = implode(",", $tcp_dst_elements_array);
                                    $sql[] = array($source, $addressName, $addressNamePan, $vsys, $description, $tcp_src_elements, $tcp_dst_elements, 'tcp');
                                    $tcp_src_elements = "";
                                    $tcp_src_elements_array = [];
                                    $tcp_dst_elements = "";
                                    $tcp_dst_elements_array = [];
                                }
                                elseif( (count($protocol_udp) > 0) and (count($protocol_tcp) == 0) )
                                {
                                    # IS UDP ONLY
                                    foreach( $protocol_udp as $k => $v1 )
                                    {
                                        $tcp = explode(":", $v1);
                                        $tcp_dst = $tcp[0];
                                        if( isset($tcp[1]) )
                                        {
                                            $tcp_src = $tcp[1];
                                        }
                                        else
                                        {
                                            $tcp_src = "";
                                        }

                                        # Dst port
                                        if( preg_match("/-/", $tcp_dst) )
                                        {
                                            $tcp_dst_elements = explode("-", $tcp_dst);
                                            if( trim($tcp_dst_elements[0]) == trim($tcp_dst_elements[1]) )
                                            {
                                                $tcp_dst_elements_array[] = $tcp_dst_elements[0];
                                            }
                                            else
                                            {
                                                $tcp_dst_elements_array[] = $tcp_dst;
                                            }
                                        }
                                        else
                                        {
                                            $tcp_dst_elements_array[] = $tcp_dst;
                                        }

                                        # Src Port
                                        if( $tcp_src != "" )
                                        {
                                            if( preg_match("/-/", $tcp_src) )
                                            {
                                                $tcp_src_elements = explode("-", $tcp_src);
                                                if( $tcp_src_elements[0] == $tcp_src_elements[1] )
                                                {
                                                    $tcp_src_elements_array[] = $tcp_src_elements[0];
                                                }
                                                else
                                                {
                                                    $tcp_src_elements_array[] = $tcp_src;
                                                }
                                            }
                                            else
                                            {
                                                $tcp_src_elements_array[] = $tcp_src;
                                            }
                                        }


                                    }

                                    $tcp_src_elements = implode(",", $tcp_src_elements_array);
                                    $tcp_dst_elements = implode(",", $tcp_dst_elements_array);
                                    $sql[] = array($source, $addressName, $addressNamePan, $vsys, $description, $tcp_src_elements, $tcp_dst_elements, 'udp');
                                    $tcp_src_elements = "";
                                    $tcp_src_elements_array = [];
                                    $tcp_dst_elements = "";
                                    $tcp_dst_elements_array = [];
                                }
                                elseif( ((count($protocol_udp) > 0) and (is_array($protocol_udp))) and ((is_array($protocol_tcp)) and (count($protocol_tcp) > 0)) )
                                {

                                    //Todo: SWASCHKUT 20190920 - general method for such as other vendors has this situation too
                                    # IS TCP AND UDP CREATING GROUP
                                    #Create Group and get ID

                                    #$projectdb->query("INSERT INTO services_groups_id (source,vsys,name_ext,name) VALUES ('$source','$vsys','$addressName','$addressNamePan')");
                                    #$grpID = $projectdb->insert_id;

                                    $addressNamePan = $this->truncate_names($this->normalizeNames($addressNamePan));
                                    $tmp_servicegroup = $v->serviceStore->find($addressNamePan);
                                    if( $tmp_servicegroup == null )
                                    {
                                        if( $print )
                                            print "\n * create servicegroup Object: " . $addressNamePan . "\n";
                                        $tmp_servicegroup = $v->serviceStore->newServiceGroup($addressNamePan);
                                    }
                                    else
                                    {
                                        if( !$tmp_servicegroup->isGroup() )
                                        {
                                            mwarning("service found not a Group for: '" . $addressNamePan . "'");
                                            if( $tmp_servicegroup->tags->hasTag('predefined') )
                                            {
                                                $v->serviceStore->remove($tmp_servicegroup);
                                                $newname = "";
                                            }
                                            else
                                            {
                                                $newname = "g_tmp_";
                                            }

                                            $name = $this->truncate_names($this->normalizeNames($newname . $addressNamePan));
                                            if( $print )
                                                print "\n * create servicegroup Object: " . $name . "\n";
                                            $tmp_servicegroup = $v->serviceStore->newServiceGroup($name);

                                            if( $newname != "" )
                                            {
                                                $add_log = 'Service with same name already available, added prefix: g_tmp_';
                                                $tmp_servicegroup->set_node_attribute('error', $add_log);
                                            }

                                        }
                                        else
                                            if( $print )
                                                print " X servicegroup: " . $addressNamePan . " already available.\n";
                                    }


                                    foreach( $protocol_tcp as $k => $v1 )
                                    {
                                        $tcp = explode(":", $v1);
                                        $tcp_dst = $tcp[0];
                                        if( isset($tcp[1]) )
                                        {
                                            $tcp_src = $tcp[1];
                                        }
                                        else
                                        {
                                            $tcp_src = "";
                                        }

                                        # Dst port
                                        if( preg_match("/-/", $tcp_dst) )
                                        {
                                            $tcp_dst_elements = explode("-", $tcp_dst);
                                            if( trim($tcp_dst_elements[0]) == trim($tcp_dst_elements[1]) )
                                            {
                                                $tcp_dst_elements_array[] = $tcp_dst_elements[0];
                                            }
                                            else
                                            {
                                                $tcp_dst_elements_array[] = $tcp_dst;
                                            }
                                        }
                                        else
                                        {
                                            $tcp_dst_elements_array[] = $tcp_dst;
                                        }

                                        # Src Port
                                        if( $tcp_src != "" )
                                        {
                                            if( preg_match("/-/", $tcp_src) )
                                            {
                                                $tcp_src_elements = explode("-", $tcp_src);
                                                if( $tcp_src_elements[0] == $tcp_src_elements[1] )
                                                {
                                                    $tcp_src_elements_array[] = $tcp_src_elements[0];
                                                }
                                                else
                                                {
                                                    $tcp_src_elements_array[] = $tcp_src;
                                                }
                                            }
                                            else
                                            {
                                                $tcp_src_elements_array[] = $tcp_src;
                                            }
                                        }


                                    }

                                    if( count($tcp_src_elements_array) > 0 )
                                    {
                                        $tcp_src_elements = implode(",", $tcp_src_elements_array);
                                    }
                                    else
                                    {
                                        $tcp_src_elements = "";
                                    }

                                    $tcp_dst_elements = implode(",", $tcp_dst_elements_array);
                                    if( $tcp_src_elements != "" )
                                    {
                                        #$tcp_src_elements_name=str_replace(",","_",$tcp_src_elements);
                                        #$tcp_dst_elements_name=str_replace(",","_",$tcp_dst_elements);
                                        $tcp_src_elements_name = $tcp_src_elements;
                                        $tcp_dst_elements_name = $tcp_dst_elements;
                                        $srvname = $this->truncate_names("tcp-" . $tcp_src_elements_name . "-" . $tcp_dst_elements_name);
                                    }
                                    else
                                    {
                                        $tcp_src_elements_name = "";
                                        #$tcp_dst_elements_name=str_replace(",","_",$tcp_dst_elements);
                                        $tcp_dst_elements_name = $tcp_dst_elements;
                                        $srvname = $this->truncate_names("tcp-" . $tcp_dst_elements_name);
                                    }

                                    #$sql[] = array($source,$srvname,$srvname,$vsys,$description,$tcp_src_elements,$tcp_dst_elements,'tcp');

                                    $srvname = $this->truncate_names($this->normalizeNames($srvname));
                                    $tmp_service = $v->serviceStore->find($srvname);
                                    if( $tmp_service === null )
                                    {
                                        if( $print )
                                            print " * create service " . $srvname . " -proto: tcp -dport:|" . $tcp_dst_elements_name . "| -description '' -sport:" . $tcp_src_elements_name . "\n";

                                        $tmp_service = $v->serviceStore->newService($srvname, 'tcp', $tcp_dst_elements_name, '', $tcp_src_elements_name);

                                        if( !$tmp_servicegroup->hasNamedObjectRecursive($srvname) )
                                        {
                                            if( $print )
                                                print "  * add service Objects: " . $srvname . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                            $tmp_servicegroup->addMember($tmp_service);
                                        }
                                        else
                                            if( $print )
                                                print "  - service Objects: " . $srvname . " already a member of this service group\n";
                                    }

                                    #$projectdb->query("INSERT INTO services_groups (source,vsys,member,lid) VALUES ('$source','$vsys','$srvname','$grpID')");

                                    $tcp_src_elements = "";
                                    $tcp_src_elements_array = [];
                                    $tcp_dst_elements = "";
                                    $tcp_dst_elements_array = [];
                                    #UDP
                                    $tcp_src_elements = "";
                                    $tcp_dst_elements = "";
                                    $tcp_src_elements_name = "";
                                    $tcp_dst_elements_name = "";

                                    foreach( $protocol_udp as $k => $v1 )
                                    {
                                        $tcp = explode(":", $v1);
                                        $tcp_dst = $tcp[0];
                                        if( isset($tcp[1]) )
                                        {
                                            $tcp_src = $tcp[1];
                                        }
                                        else
                                        {
                                            $tcp_src = "";
                                        }

                                        # Dst port
                                        if( preg_match("/-/", $tcp_dst) )
                                        {
                                            $tcp_dst_elements = explode("-", $tcp_dst);
                                            if( trim($tcp_dst_elements[0]) == trim($tcp_dst_elements[1]) )
                                            {
                                                $tcp_dst_elements_array[] = $tcp_dst_elements[0];
                                            }
                                            else
                                            {
                                                $tcp_dst_elements_array[] = $tcp_dst;
                                            }
                                        }
                                        else
                                        {
                                            $tcp_dst_elements_array[] = $tcp_dst;
                                        }

                                        # Src Port
                                        if( $tcp_src != "" )
                                        {
                                            if( preg_match("/-/", $tcp_src) )
                                            {
                                                $tcp_src_elements = explode("-", $tcp_src);
                                                if( $tcp_src_elements[0] == $tcp_src_elements[1] )
                                                {
                                                    $tcp_src_elements_array[] = $tcp_src_elements[0];
                                                }
                                                else
                                                {
                                                    $tcp_src_elements_array[] = $tcp_src;
                                                }
                                            }
                                            else
                                            {
                                                $tcp_src_elements_array[] = $tcp_src;
                                            }
                                        }


                                    }

                                    if( count($tcp_src_elements_array) > 0 )
                                    {
                                        $tcp_src_elements = implode(",", $tcp_src_elements_array);
                                    }
                                    else
                                    {
                                        $tcp_src_elements = "";
                                    }
                                    $tcp_dst_elements = implode(",", $tcp_dst_elements_array);
                                    if( $tcp_src_elements != "" )
                                    {
                                        #$tcp_src_elements_name=str_replace(",","_",$tcp_src_elements);
                                        #$tcp_dst_elements_name=str_replace(",","_",$tcp_dst_elements);
                                        $tcp_src_elements_name = $tcp_src_elements;
                                        $tcp_dst_elements_name = $tcp_dst_elements;
                                        $srvname = $this->truncate_names("udp-" . $tcp_src_elements_name . "-" . $tcp_dst_elements_name);
                                    }
                                    else
                                    {
                                        $tcp_src_elements_name = "";
                                        #$tcp_dst_elements_name=str_replace(",","_",$tcp_dst_elements);
                                        $tcp_dst_elements_name = $tcp_dst_elements;
                                        $srvname = $this->truncate_names("udp-" . $tcp_dst_elements_name);
                                    }
                                    #$sql[] = array($source,$srvname,$srvname,$vsys,$description,$tcp_src_elements,$tcp_dst_elements,'udp');

                                    #$projectdb->query("INSERT INTO services_groups (source,vsys,member,lid) VALUES ('$source','$vsys','$srvname','$grpID')");

                                    $srvname = $this->truncate_names($this->normalizeNames($srvname));
                                    $tmp_service = $v->serviceStore->find($srvname);
                                    if( $tmp_service === null )
                                    {
                                        if( $print )
                                            print " * create service " . $srvname . " -proto: udp -dport:|" . $tcp_dst_elements_name . "| -description '' -sport:" . $tcp_src_elements_name . "\n";

                                        $tmp_service = $v->serviceStore->newService($srvname, 'udp', $tcp_dst_elements_name, '', $tcp_src_elements_name);

                                        if( !$tmp_servicegroup->hasNamedObjectRecursive($srvname) )
                                        {
                                            if( $print )
                                                print "  * add service Objects: " . $srvname . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                            $tmp_servicegroup->addMember($tmp_service);
                                        }
                                        else
                                            if( $print )
                                                print "  - service Objects: " . $srvname . " already a member of this service group\n";
                                    }

                                    $tcp_src_elements = "";
                                    $tcp_src_elements_array = [];
                                    $tcp_dst_elements = "";
                                    $tcp_dst_elements_array = [];
                                    $tcp_src_elements_name = "";
                                    $tcp_dst_elements_name = "";
                                }
                            }
                            else
                            {
                                if( $protocol == "IP" )
                                {
                                    $protocol = $protocol_number;
                                }
                                $sql[] = array($source, $addressName, $addressNamePan, $vsys, $description, '', '', $protocol);
                            }
                            $protocol = "";
                            $protocol_tcp = [];
                            $protocol_udp = [];
                            $description = "";
                            $tcp_src_elements = "";
                            $tcp_dst_elements = "";
                        }

                        if( preg_match("/set protocol /i", $names_line) )
                        {
                            $data1 = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                            $protocol = $data1[2]; #TCP/UDP o IP o ICMP o TCP/UDP/SCTP
                        }

                        if( preg_match("/set udp-portrange/i", $names_line) )
                        {
                            $data1 = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                            if( (isset($data1[2])) and ($data1[2] == "0:0") )
                            {

                            }
                            else
                            {
                                unset($data1[0]);
                                unset($data1[1]);
                                $protocol_udp = array_values($data1);
                                $protocol = "UDP";
                            }
                        }

                        if( preg_match("/set tcp-portrange/i", $names_line) )
                        {
                            $data1 = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                            if( (isset($data1[2])) and ($data1[2] == "0:0") )
                            {

                            }
                            else
                            {
                                unset($data1[0]);
                                unset($data1[1]);
                                $protocol_tcp = array_values($data1);
                                $protocol = "TCP";
                            }
                        }

                        if( preg_match("/set protocol-number/i", $names_line) )
                        {
                            $data1 = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                            if( isset($data1[2]) )
                            {
                                $protocol_number = $data1[2];
                            }
                            else
                            {
                                echo "Error: " . $names_line . PHP_EOL;
                            }

                        }

                        if( preg_match("/^set icmptype/i", $names_line) )
                        {
                            $data1 = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                            if( isset($data1[2]) )
                            {
                                $protocol_icmp_type = $data1[2];
                            }
                            else
                            {
                                echo "Error: " . $names_line . PHP_EOL;
                            }

                        }

                        if( preg_match("/^set icmpcode/i", $names_line) )
                        {
                            $data1 = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                            if( isset($data1[2]) )
                            {
                                $protocol_icmp_code = $data1[2];
                            }
                            else
                            {
                                echo "Error: " . $names_line . PHP_EOL;
                            }

                        }

                        if( preg_match("/set comment/i", $names_line) )
                        {
                            $data1 = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                            $description = $data1[2];
                        }
                    }
                }
            }


            $lastline = $names_line;
        }


        #print "\ndo migration for VSYS: ".$v->name()."\n\n";
        if( count($sql) > 0 )
        {
            #$sql_unique = array_unique($sql);

            #print_r( $sql );

            foreach( $sql as $srv_object )
            {

                $ObjectServiceNamePan = $srv_object[2];
                $srv_protocol = $srv_object[7];
                $dport = $srv_object[6];
                $sport = $srv_object[5];

                if( $srv_protocol == "tcp" || $srv_protocol == "udp" )
                {
                    $ObjectServiceNamePan = $this->truncate_names($this->normalizeNames($ObjectServiceNamePan));
                    $tmp_service = $v->serviceStore->find($ObjectServiceNamePan);
                    if( $tmp_service === null )
                    {
                        if( $print )
                            print " * create service " . $ObjectServiceNamePan . " -proto:" . $srv_protocol . " -dport:|" . $dport . "| -description '' -sport:" . $sport . "\n";

                        $tmp_service = $v->serviceStore->newService($ObjectServiceNamePan, $srv_protocol, $dport, '', $sport);
                    }
                }
                else
                {
                    if( $srv_protocol == "" )
                    {
                        $ObjectServiceNamePan = $this->truncate_names($this->normalizeNames($ObjectServiceNamePan));
                        $tmp_service = $v->serviceStore->find("tmp-" . $ObjectServiceNamePan);
                        if( $tmp_service === null )
                        {
                            if( $print )
                                print " * create service 'tmp-" . $ObjectServiceNamePan . "' with dummy -dport '6500' and -proto 'TCP' because no information is available\n";

                            $tmp_service = $v->serviceStore->newService("tmp-" . $ObjectServiceNamePan, "tcp", "6500", '', '');
                        }
                    }
                    else
                    {
                        $ObjectServiceNamePan = $this->truncate_names($this->normalizeNames($ObjectServiceNamePan));
                        $tmp_service = $v->serviceStore->find("app-" . $ObjectServiceNamePan);
                        if( $tmp_service === null )
                        {
                            if( $print )
                                print " * create service 'app-" . $ObjectServiceNamePan . "' with description: " . $srv_protocol . "\n";

                            $tmp_service = $v->serviceStore->newService("app-" . $ObjectServiceNamePan, "tcp", "6500", $srv_protocol, '');
                        }
                        #print_r( $srv_object );
                        #mwarning( "no service protocol found is not TCP or UDP\n" , null, false);
                    }

                }

            }

            #$projectdb->query("INSERT INTO services (source,name_ext,name,vsys,description,sport,dport,protocol) VALUES " . implode(",", $sql_unique) . ";");
        }
    }

// old "
// <editor-fold desc="  ****  old get_services   ****" defaultstate="collapsed" >
    function get_services($fortinet_config_file, $source, $vsys, $ismultiornot)
    {

        global $projectdb;
        $isAddress = FALSE;
        $isObject = FALSE;
        $sql = array();
        $type = "";
        $ipaddress = "";
        $netmask = "";
        $cidr = "";
        $addressName = "";
        $addressNamePan = "";
        $description = "";
        $getRangeEnd = "";
        $getRangeStart = "";
        $protocol_number = "";
        $tcp_src_elements_array = array();
        $fqdn = "";
        $sql = array();
        $protocol_udp = "";
        $protocol_tcp = "";
        $protocol = "";
        $tcp_src_elements = "";
        $tcp_dst_elements = "";

        if( $ismultiornot == "singlevsys" )
        {
            $START = TRUE;
            $VDOM = FALSE;
        }
        else
        {
            $START = FALSE;
            $VDOM = FALSE;
        }
        $lastline = "";
        foreach( $fortinet_config_file as $line => $names_line )
        {

            $this->check_vdom_start($START, $VDOM, $names_line, $lastline, $vsys);

            if( $START )
            {
                if( preg_match("/^config firewall service custom/i", $names_line) )
                {
                    $isObject = TRUE;
                }

                if( $isObject )
                {
                    if( preg_match("/^end/i", $names_line) )
                    {
                        $isObject = FALSE;
                        $START = FALSE;
                    }
                    if( preg_match("/edit /i", $names_line) )
                    {
                        $isAddress = TRUE;
                        $meta = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                        $addressNamePan = $this->truncate_names($this->normalizeNames($meta[1]));
                        $addressName = $meta[1];
                    }

                    if( $isAddress )
                    {
                        if( preg_match("/next/i", $names_line) )
                        {
                            $isAddress = FALSE;
                            if( (preg_match("/TCP\/UDP/i", $protocol)) or preg_match("/TCP/i", $protocol) or preg_match("/UDP/i", $protocol) )
                            {
                                if( ($protocol_udp == "") and ($protocol_tcp != "") )
                                {
                                    # IS TCP ONLY
                                    $tcp = explode(":", $protocol_tcp);
                                    $tcp_dst = $tcp[0];
                                    if( isset($tcp[1]) )
                                    {
                                        $tcp_src = $tcp[1];
                                    }
                                    else
                                    {
                                        $tcp_src = "";
                                    }
                                    # Dst port
                                    if( preg_match("/-/", $tcp_dst) )
                                    {
                                        $tcp_dst_elements = explode("-", $tcp_dst);
                                        if( trim($tcp_dst_elements[0]) == trim($tcp_dst_elements[1]) )
                                        {
                                            $tcp_dst_elements = $tcp_dst_elements[0];
                                        }
                                        else
                                        {
                                            $tcp_dst_elements = $tcp_dst;
                                        }
                                    }
                                    else
                                    {
                                        $tcp_dst_elements = $tcp_dst;
                                    }
                                    # Src Port
                                    if( preg_match("/-/", $tcp_src) )
                                    {
                                        $tcp_src_elements = explode("-", $tcp_src);
                                        if( $tcp_src_elements[0] == $tcp_src_elements[1] )
                                        {
                                            $tcp_src_elements = $tcp_src_elements[0];
                                        }
                                        else
                                        {
                                            $tcp_src_elements = $tcp_src;
                                        }
                                    }
                                    else
                                    {
                                        $tcp_src_elements = $tcp_src;
                                    }
                                    $sql[] = "('$source','$addressName','$addressNamePan','$vsys','$description','$tcp_src_elements','$tcp_dst_elements','tcp')";
                                }
                                elseif( ($protocol_udp != "") and ($protocol_tcp == "") )
                                {
                                    # IS UDP ONLY
                                    $tcp = explode(":", $protocol_udp);
                                    $tcp_dst = $tcp[0];
                                    if( isset($tcp[1]) )
                                    {
                                        $tcp_src = $tcp[1];
                                    }
                                    else
                                    {
                                        $tcp_src = "";
                                    }

                                    if( preg_match("/-/", $tcp_dst) )
                                    {
                                        $tcp_dst_elements = explode("-", $tcp_dst);
                                        if( $tcp_dst_elements[0] == $tcp_dst_elements[1] )
                                        {
                                            $tcp_dst_elements = $tcp_dst_elements[0];
                                        }
                                        else
                                        {
                                            $tcp_dst_elements = $tcp_dst;
                                        }
                                    }
                                    else
                                    {
                                        $tcp_dst_elements = $tcp_dst;
                                    }

                                    if( preg_match("/-/", $tcp_src) )
                                    {
                                        $tcp_src_elements = explode("-", $tcp_src);
                                        if( $tcp_src_elements[0] == $tcp_src_elements[1] )
                                        {
                                            $tcp_src_elements = $tcp_src_elements[0];
                                        }
                                        else
                                        {
                                            $tcp_src_elements = $tcp_src;
                                        }
                                    }
                                    else
                                    {
                                        $tcp_src_elements = $tcp_src;
                                    }
                                    $sql[] = "('$source','$addressName','$addressNamePan','$vsys','$description','$tcp_src_elements','$tcp_dst_elements','udp')";
                                }
                                elseif( ($protocol_udp != "") and ($protocol_tcp != "") )
                                {
                                    # IS TCP AND UDP CREATING GROUP
                                    #Create Group and get ID
                                    $projectdb->query("INSERT INTO services_groups_id (source,vsys,name_ext,name) VALUES ('$source','$vsys','$addressName','$addressNamePan')");
                                    $grpID = $projectdb->insert_id;

                                    $tcp = explode(":", $protocol_tcp);
                                    $tcp_dst = $tcp[0];
                                    if( isset($tcp[1]) )
                                    {
                                        $tcp_src = $tcp[1];
                                    }
                                    else
                                    {
                                        $tcp_src = "";
                                    }

                                    if( preg_match("/-/", $tcp_dst) )
                                    {
                                        $tcp_dst_elements = explode("-", $tcp_dst);
                                        if( trim($tcp_dst_elements[0]) == trim($tcp_dst_elements[1]) )
                                        {
                                            $tcp_dst_elements = $tcp_dst_elements[0];
                                        }
                                        else
                                        {
                                            $tcp_dst_elements = $tcp_dst;
                                        }
                                    }
                                    else
                                    {
                                        $tcp_dst_elements = $tcp_dst;
                                    }

                                    if( preg_match("/-/", $tcp_src) )
                                    {
                                        $tcp_src_elements = explode("-", $tcp_src);
                                        if( $tcp_src_elements[0] == $tcp_src_elements[1] )
                                        {
                                            $tcp_src_elements = $tcp_src_elements[0];
                                        }
                                        else
                                        {
                                            $tcp_src_elements = $tcp_src;
                                        }
                                    }
                                    else
                                    {
                                        $tcp_src_elements = $tcp_src;
                                    }
                                    if( $tcp_src_elements != "" )
                                    {
                                        $srvname = "tcp-" . $tcp_src_elements . "-" . $tcp_dst_elements;
                                    }
                                    else
                                    {
                                        $srvname = "tcp-" . $tcp_dst_elements;
                                    }

                                    $sql[] = "('$source','$srvname','$srvname','$vsys','$description','$tcp_src_elements','$tcp_dst_elements','tcp')";
                                    $projectdb->query("INSERT INTO services_groups (source,vsys,member,lid) VALUES ('$source','$vsys','$srvname','$grpID')");

                                    #UDP
                                    $tcp_src_elements = "";
                                    $tcp_dst_elements = "";

                                    $tcp = explode(":", $protocol_udp);
                                    $tcp_dst = $tcp[0];
                                    if( isset($tcp[1]) )
                                    {
                                        $tcp_src = $tcp[1];
                                    }
                                    else
                                    {
                                        $tcp_src = "";
                                    }

                                    if( preg_match("/-/", $tcp_dst) )
                                    {
                                        $tcp_dst_elements = explode("-", $tcp_dst);
                                        if( $tcp_dst_elements[0] == $tcp_dst_elements[1] )
                                        {
                                            $tcp_dst_elements = $tcp_dst_elements[0];
                                        }
                                        else
                                        {
                                            $tcp_dst_elements = $tcp_dst;
                                        }
                                    }
                                    else
                                    {
                                        $tcp_dst_elements = $tcp_dst;
                                    }

                                    if( preg_match("/-/", $tcp_src) )
                                    {
                                        $tcp_src_elements = explode("-", $tcp_src);
                                        if( $tcp_src_elements[0] == $tcp_src_elements[1] )
                                        {
                                            $tcp_src_elements = $tcp_src_elements[0];
                                        }
                                        else
                                        {
                                            $tcp_src_elements = $tcp_src;
                                        }
                                    }
                                    else
                                    {
                                        $tcp_src_elements = $tcp_src;
                                    }
                                    if( $tcp_src_elements != "" )
                                    {
                                        $srvname = "udp-" . $tcp_src_elements . "-" . $tcp_dst_elements;
                                    }
                                    else
                                    {
                                        $srvname = "udp-" . $tcp_dst_elements;
                                    }
                                    $sql[] = "('$source','$srvname','$srvname','$vsys','$description','$tcp_src_elements','$tcp_dst_elements','udp')";
                                    $projectdb->query("INSERT INTO services_groups (source,vsys,member,lid) VALUES ('$source','$vsys','$srvname','$grpID')");
                                }
                            }
                            elseif( $protocol == "IP" )
                            {

                            }
                            elseif( $protocol == "ICMP" )
                            {

                            }
                            $protocol = "";
                            $protocol_tcp = "";
                            $protocol_udp = "";
                            $description = "";
                            $tcp_src_elements = "";
                            $tcp_dst_elements = "";
                        }

                        if( preg_match("/set protocol /i", $names_line) )
                        {
                            $data = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                            $protocol = $data[2]; #TCP/UDP o IP o ICMP o TCP/UDP/SCTP
                        }

                        if( preg_match("/set udp-portrange/i", $names_line) )
                        {
                            $data = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                            if( $data[2] == "0:0" )
                            {

                            }
                            else
                            {
                                $protocol_udp = $data[2];
                                $protocol = "UDP";
                            }
                        }

                        if( preg_match("/set tcp-portrange/i", $names_line) )
                        {
                            $data = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                            if( $data[2] == "0:0" )
                            {

                            }
                            else
                            {
                                $protocol_tcp = $data[2];
                                $protocol = "TCP";
                            }
                        }

                        if( preg_match("/set protocol-number/i", $names_line) )
                        {
                            $data = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                            $protocol_number = $data[2];
                        }

                        if( preg_match("/set icmptype/i", $names_line) )
                        {
                            $data = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                            $protocol_icmp_type = $data[2];
                        }

                        if( preg_match("/set icmpcode/i", $names_line) )
                        {
                            $data = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                            $protocol_icmp_code = $data[2];
                        }

                        if( preg_match("/set comment/i", $names_line) )
                        {
                            $data = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                            $description = $data[2];
                        }
                    }
                }
            }


            $lastline = $names_line;
        }
        if( count($sql) > 0 )
        {
            $sql_unique = array_unique($sql);
            $projectdb->query("INSERT INTO services (source,name_ext,name,vsys,description,sport,dport,protocol) VALUES " . implode(",", $sql_unique) . ";");
        }
    }
// </editor-fold>

#function get_services_groups($fortinet_config_file, $source, $vsys, $ismultiornot) {
    function get_services_groups($data, $source, $v, $ismultiornot)
    {

        global $debug;
        global $print;

        global $projectdb;
        $isAddress = FALSE;
        $isObject = FALSE;
        $sql = array();
        $sql1 = array();
        $lid = "";
        $addressNamePan = "";
        $addressName = "";

        $vsys = $v->alternativeName();

        if( $ismultiornot == "singlevsys" )
        {
            $START = TRUE;
            $VDOM = FALSE;
        }
        else
        {
            $START = FALSE;
            $VDOM = FALSE;
        }
        $lastline = "";
        foreach( $data as $line => $names_line )
        {


            $this->check_vdom_start($START, $VDOM, $names_line, $lastline, $vsys);

            if( $START )
            {
                if( preg_match("/config firewall service group/i", $names_line) )
                {
                    $isObject = TRUE;
                }


                if( $isObject )
                {
                    if( preg_match("/^\bend\b/i", $names_line) )
                    {
                        $isObject = FALSE;
                        $START = FALSE;
                    }
                    if( preg_match("/\bedit\b/i", $names_line) )
                    {
                        $isAddress = TRUE;
                        $meta = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                        $newname = str_replace('/', '-', $meta[1]);
                        $addressNamePan = $this->truncate_names($this->normalizeNames($newname));
                        $addressName = trim($meta[1]);

                        #$projectdb->query("INSERT INTO services_groups_id (name_ext,name,source,vsys) VALUES ('$addressName','$addressNamePan','$source','$vsys')");

                        $addressNamePan = $this->truncate_names($this->normalizeNames($addressNamePan));
                        $tmp_servicegroup = $v->serviceStore->find($addressNamePan);
                        if( $tmp_servicegroup == null )
                        {
                            if( $print )
                                print "\n * create servicegroup Object: " . $addressNamePan . "\n";
                            $tmp_servicegroup = $v->serviceStore->newServiceGroup($addressNamePan);
                        }
                        else
                        {
                            if( $print )
                                mwarning(" X servicegroup: " . $addressNamePan . " already available", null, FALSE);
                        }


                        #$lid = $projectdb->insert_id;
                    }

                    if( $isAddress )
                    {
                        if( preg_match("/\bnext\b/i", $names_line) )
                        {
                            $isAddress = FALSE;
                            $lid = "";
                            $addressNamePan = "";
                            $addressName = "";
                        }

                        if( preg_match("/set member /i", $names_line) )
                        {
                            $data = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                            foreach( $data as $value => $datos )
                            {
                                if( ($datos == "set") or ($datos == "member") )
                                {

                                }
                                else
                                {
                                    $memberPan = $this->truncate_names($this->normalizeNames(trim($datos)));
                                    #$sql[] = "('$datos','$source','$vsys','$lid')";
                                    $tmp_object = $v->serviceStore->find($memberPan);
                                    if( $tmp_object !== null )
                                    {
                                        $tmp_servicegroup->addMember($tmp_object);
                                        if( !$tmp_servicegroup->hasNamedObjectRecursive($memberPan) )
                                        {
                                            if( $print )
                                                print "  * add service Objects: " . $memberPan . " to servicegroup " . $tmp_servicegroup->name() . "\n";
                                            $tmp_servicegroup->addMember($tmp_object);
                                        }
                                        else
                                            if( $print )
                                                print "  - service Objects: " . $memberPan . " already a member of this service group\n";
                                    }
                                    else
                                    {
                                        //Todo swaschkut 20190930 - what about tmp servcies
                                        if( $print )
                                            print " X service object: " . $memberPan . " not found. can not be added to service group\n";
                                    }

                                }
                            }
                        }
                    }
                }
            }


            $lastline = $names_line;
        }
        if( count($sql) > 0 )
        {
            #$projectdb->query("INSERT INTO services_groups (member,source,vsys,lid) VALUES " . implode(",", $sql));
        }
    }

}

