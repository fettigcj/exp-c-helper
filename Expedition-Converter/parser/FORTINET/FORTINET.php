<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */


require_once("FORTINETaddresses.php");

require_once("FORTINETnatrules.php");
require_once("FORTINETnetwork.php");

require_once("FORTINETsecurityrules.php");
require_once("FORTINETservices.php");


class FORTINET extends PARSER
{

    use FORTINETaddresses;
    use FORTINETnatrules;
    use FORTINETnetwork;
    use FORTINETsecurityrules;
    use FORTINETservices;

    use SHAREDNEW;

    function vendor_main($config_filename, $pan, $routetable = "")
    {
        $this->logger->start();

        parent::preCheck();


        //swaschkut - tmp, until class migration is done
        global $print;
        $print = TRUE;


        //Fortinet specific
        //------------------------------------------------------------------------
        $path = "";
        $project = "";

        $config_path = $path . $config_filename;
        $filename = $config_filename;
        $filenameParts = pathinfo($config_filename);
        $verificationName = $filenameParts['filename'];

        $this->logger->increaseCompleted();

        $data = $this->clean_config($config_path, $project, $config_filename);

        $this->logger->increaseCompleted();
        $this->logger->setCompletedSilent();

        $this->import_config($data, $pan); //This should update the $source
        //------------------------------------------------------------------------


        //Todo: validation if GLOBAL rule
        echo PH::boldText("Zone Calculation for Security and NAT policy");
        Converter::calculate_zones($pan, "append");


        echo PH::boldText("\nVALIDATION - interface name and change into PAN-OS confirm naming convention\n");
        CONVERTER::validate_interface_names($pan);

        //Todo: where to place custom table for app-migration if needed
        echo PH::boldText("\nVALIDATION - replace tmp services with APP-id if possible\n");
        CONVERTER::AppMigration($pan);

    }

    function clean_config($config_path, $project, $config_filename)
    {

        $config_file = file($config_path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        $data = array();
        foreach( $config_file as $line => $names_line )
        {
            $data[] = $names_line;
            /*
                if ((preg_match("/description/", $names_line)) OR ( preg_match("/remark/", $names_line))) {
                    $data[] = $names_line;
                }
                else {
                    #"<--- More --->"
                    if( preg_match("/^<--- More --->/", $names_line) || preg_match("/^              /", $names_line) )
                    {

                    }
                    elseif (preg_match("/\'/", $names_line)) {
                        $data[] = str_replace("'", "_", $names_line);
                    } else {
                        $data[] = $names_line;
                    }
                }
            */
        }
        #file_put_contents($config_path, $data);
        return $data;
    }

    function check_vdom_start(&$START, &$VDOM, $names_line, $lastline, $vsys)
    {
        if( $START == FALSE )
        {
            if( preg_match("/^config vdom/i", $names_line) )
            {
                $VDOM = TRUE;
            }
            if( $VDOM == TRUE )
            {
                #if (preg_match("/^edit $vsys/i", $names_line)) {
                if( "edit " . $vsys == $names_line )
                {
                    $START = TRUE;
                }
            }
        }
        else
        {
            if( (preg_match("/^end/i", $lastline)) and (preg_match("/^end/i", $names_line)) )
            {
                $START = FALSE;
                $VDOM = FALSE;
            }
            elseif( (preg_match("/^edit/i", $lastline)) and (preg_match("/^end/i", $names_line)) )
            {
                $START = FALSE;
                $VDOM = FALSE;
            }
        }
    }

//---------------------------------------------
//        Parser Logic starts here
//----------------------------------------------

    /*
    $sourcesAdded = array();
    global $source;

    // old $actions == "import"
    // <editor-fold desc="  ****  old $actions == 'import'   ****" defaultstate="collapsed" >
    if ($action == "import") {
        ini_set('max_execution_time', PARSER_max_execution_time);
        ini_set("memory_limit",PARSER_max_execution_memory);

        $path = USERSPACE_PATH."/projects/" . $project . "/";
        $i = 0;
        $dirrule = opendir($path);

        update_progress($project, '0.00', 'Reading config files',$jobid);
        while ($config_filename = readdir($dirrule)) {
            //if (($config_filename != ".") && ($config_filename != "..") && ($config_filename != "parsers.txt") && ($config_filename != "Backups") && ($config_filename != "Pcaps") && ($config_filename != "Reports") AND ( $config_filename != "CPviewer.html") AND ( !preg_match("/^MT-/", $config_filename)) AND ( !preg_match("/^\._/", $config_filename))) {
            if (checkFiles2Import($config_filename)){
                $config_path = $path . $config_filename;
                $filename = $config_filename;
                $filenameParts = pathinfo($config_filename);
                $verificationName = $filenameParts['filename'];
                $isUploaded = $projectdb->query("SELECT id FROM device_mapping WHERE filename='$verificationName';");
    //            $isUploaded = $projectdb->query("SELECT id FROM device_mapping WHERE filename='$config_filename';");
                if ($isUploaded->num_rows == 0) {
                    import_config($config_path, $project, $config_filename,$jobid); //This will update the $source
                    $sourcesAdded[] = $source;
                    update_progress($project, '0.70', 'File:' . $filename . ' Phase 7 Referencing Groups',$jobid);
                    GroupMember2IdAddress($config_filename);
                    GroupMember2IdServices($config_filename);
                } else {
                    update_progress($project, '0.00', 'This filename '.$filename.' its already uploaded. Skipping...',$jobid);
                    //unlink($path.$config_filename);
                }
            }
        }

        #Calculate Layer4-7
        $queryRuleIds = "SELECT id from security_rules WHERE source = $source;";
        $resultRuleIds = $projectdb->query($queryRuleIds);
        if($resultRuleIds->num_rows>0){
            $rules = array();
            while($dataRuleIds = $resultRuleIds->fetch_assoc()){
                $rules[] = $dataRuleIds['id'];
            }
            $rulesString = implode(",", $rules);
            $securityRulesMan = new \SecurityRulePANObject();
            $securityRulesMan->updateLayerLevel($projectdb, $rulesString, $source);
        }


        #Check used
        update_progress($project, '0.90', 'File:' . $filename . ' Calculating Used Objects',$jobid);
        check_used_objects_new($sourcesAdded);
        update_progress($project, '1.00', 'Done.',$jobid);
        unlink($config_path);
    }
    // </editor-fold>
    */

#function import_config($config_path, $project, $config_filename,$jobid) {
    function import_config($data, $pan)
    {
        global $projectdb;
        global $source;

        global $debug;
        global $print;

        /*
            $filename = $config_filename;
            #CLEAN CONFIG FROM EMPTY LINES AND CTRL+M
            file_put_contents($config_path, implode(PHP_EOL, file($config_path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)));
            #LOAD THE FILE
            #Remove break lines
            $str = file($config_path);
            $test = preg_replace("/!$\n/i", "mtreplaceme2015", $str);
            file_put_contents($config_path, $test);
            $str = file($config_path);
            $test = preg_replace("/mtreplaceme2015 /i", "", $str);
            file_put_contents($config_path, $test);

            $fortinet_config_file = file($config_path);
            $fortinet_config_file2 = str_replace("!\r ", "", $fortinet_config_file);
            file_put_contents($config_path, $fortinet_config_file2);

            $fortinet_config_file = file($config_path);
        */


        # Capture Sources Filename+vsys(vdom)
        $invdom = FALSE;
        $vsysID = 1;
        $ismultiornot = "singlevsys";

        foreach( $data as $line => $names_line )
        {
            if( preg_match("/^config vdom/i", $names_line) )
            {
                $invdom = TRUE;
            }
            elseif( (preg_match("/^end/i", $names_line)) and ($invdom == TRUE) )
            {
                $invdom = FALSE;
            }
            elseif( preg_match("/config\-version=/i", $names_line) )
            {
                $getVer = explode(":", $names_line);
                $ver = explode("=", $getVer[0]);
                $version = $ver[1];
            }

            if( ($invdom) and (preg_match("/^edit /i", $names_line)) )
            {
                $data1 = preg_split("/[\s ]*\\\"([^\\\"]+)\\\"[\s,]*|[\s,]+/", $names_line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

                $vsys = $data1[1];
                $vsysName = $data1[1];


                #print "find: ".$vsysName."\n";
                $v = $pan->findVSYS_by_displayName($vsysName);

                if( $v === null && $vsysID == 1 )
                {
                    $v = $pan->findVirtualSystem('vsys' . $vsysID);

                    if( $v !== null )
                    {
                        if( $print )
                            print "set vsys" . $vsysID . " alternativeName to: " . $vsysName . "\n";

                        $v->setAlternativeName($vsysName);
                        $vsysID++;
                    }
                }

                if( $v === null )
                {
                    //create new vsys, search for latest ID
                    do
                    {
                        $v = $pan->findVirtualSystem('vsys' . $vsysID);
                        if( $v !== null )
                            $vsysID++;
                    } while( $v !== null );

                    if( $v === null )
                    {
                        if( $print )
                            print " * create vsys: " . $vsysID . " - " . $vsysName . "\n";
                        $v = $pan->createVirtualSystem(intval($vsysID), $vsysName);
                        $vsysID++;
                        if( $v === null )
                        {
                            derr("vsys" . $vsysID . " could not be created ? Exit\n");
                        }
                    }
                }

                /*
                $getDup = $projectdb->query("SELECT id FROM device_mapping WHERE filename='$filename' AND vsys='$vsys';");
                if ($getDup->num_rows == 0) {
                    $projectdb->query("INSERT INTO device_mapping (device,version,ispanorama,active,project,filename,vsys,baseconfig,vendor) VALUES ('$filename','$version',0,1,'$project','$filename','$vsys','0','FORTINET')");
                    $config_filename = $projectdb->insert_id;
                    $source = $config_filename;
                }
                */
            }
        }

        $all_vsys = $pan->getVirtualSystems();
        $counter = 0;
        foreach( $all_vsys as $vsys )
        {
            #print "vsys: ".$vsys->name()." - ".$vsys->alternativeName()."\n";
            $counter++;
        }

        if( $counter == 1 )
        {
            $ismultiornot = "singlevsys";
        }
        elseif( $counter > 1 )
        {
            $ismultiornot = "multivsys";
        }

        /*
            #Check if Vdoms were created or its a simple config, then create a default vsys1
            $isVdom = $projectdb->query("SELECT id,vsys FROM device_mapping WHERE filename='$filename'");
            if ($isVdom->num_rows == 0) {
                $projectdb->query("INSERT INTO device_mapping (device,version,ispanorama,active,project,filename,vsys,baseconfig,vendor) VALUES ('$filename','$version',0,1,'$project','$filename','root','0','FORTINET')");
                $config_filename = $projectdb->insert_id;
                $source = $config_filename;
            }


            # Bucle from Devices_mappings to get Objects and Rules
            $getDM = $projectdb->query("SELECT id,vsys FROM device_mapping WHERE filename='$filename';");
            if ($getDM->num_rows == 1) {
                $ismultiornot = "singlevsys";
            } elseif ($getDM->num_rows > 1) {
                $ismultiornot = "multivsys";
                #Add Shared context
                $projectdb->query("INSERT INTO device_mapping (device,version,ispanorama,active,project,filename,vsys,baseconfig,vendor) VALUES ('$filename','$version',0,1,'$project','$filename','shared','0','FORTINET')");
            }
            #Get Source (First row for this filename)
            $getSource = $projectdb->query("SELECT id FROM device_mapping WHERE filename='$filename' GROUP by filename;");
            $getSourceData = $getSource->fetch_assoc();
            $source = $getSourceData['id'];
        */


        #Add Defautl Services
        print "\nadd fortinet services:\n";
        $this->add_fortinet_services($ismultiornot, $pan, $source);
#    add_default_services($source);
        //add_default_profiles($source);
        #Config Template
        /*
        $template_name = "default_template" . $source;
        $projectdb->query("INSERT INTO templates_mapping (project,name,filename,source) VALUES ('$project','$template_name','$filename','$source');");
        $template = $projectdb->insert_id;
    */


        #Add Interfaces
        #get_interfaces($fortinet_config_file, $vsys, $source, $template, $ismultiornot);
        print "\nget interfaces:\n";
        $this->get_interfaces($data, $pan, $ismultiornot);


        $all_vsys = $pan->getVirtualSystems();
        foreach( $all_vsys as $v )
        {
            $regions = array();
            $addVIP = array();

            print PH::boldText("\n\ndo migration for VSYS: " . $v->name() . "\n\n");

            #update_progress($project, '0.10', 'File:' . $filename . ' Phase 1 Reading Zones ('.$vsys_name.')',$jobid);
            print PH::boldText("\nget zones:\n");
            $this->get_zones($data, $v, $source, $ismultiornot);


            #update_progress($project, '0.20', 'File:' . $filename . ' Phase 2 Reading IP Pools and VIPs ('.$vsys_name.')',$jobid);
            print PH::boldText("\nget ippools:\n");
            $this->get_ippools($data, $source, $v, $ismultiornot);

            print PH::boldText("\nget vip:\n");
            $this->get_vip($data, $source, $v, $ismultiornot, $addVIP);

            print PH::boldText("\nget vipgrp:\n");
            $this->get_vipgrp($data, $source, $v, $ismultiornot, $addVIP);


            #update_progress($project, '0.30', 'File:' . $filename . ' Phase 3 Reading Address and Groups ('.$vsys_name.')',$jobid);

            #get_address_Fortinet($fortinet_config_file, $source, $vsys_name, $ismultiornot, $regions);
            print PH::boldText("\nget address objects:\n");
            $this->get_address_Fortinet($data, $source, $v, $ismultiornot, $regions);

            print PH::boldText("\nget IPv6 address objects:\n");
            $this->get_addressv6($data, $source, $v, $ismultiornot, $regions);

            print PH::boldText("\nget addressgroup objects:\n");
            $this->get_address_groups($data, $source, $v, $ismultiornot, $regions);
            //Todo: SWASCHKUT 20190920
            //WHAT about "config firewall addrgrp6" ?????
            //check in multivsys devices why so many objects are already available within one VSYS


            #update_progress($project, '0.40', 'File:' . $filename . ' Phase 4 Reading Services and Groups ('.$vsys.')',$jobid);
            print PH::boldText("\nget service objects:\n");
            $this->get_services2($data, $source, $v, $ismultiornot);

            print PH::boldText("\nget servicegroup objects:\n");
            $this->get_services_groups($data, $source, $v, $ismultiornot);
            #clean_duplicated_services($source, $vsys, $ismultiornot);


            #update_progress($project, '0.50', 'File:' . $filename . ' Phase 5 Reading Security Policies ('.$vsys.')',$jobid);
            print PH::boldText("\nget security policy:\n");
            $this->get_security_policy($data, $source, $v, $ismultiornot, $regions);


            #update_progress($project, '0.60', 'File:' . $filename . ' Phase 6 Reading Nat Policies ('.$vsys.')',$jobid);
            //Todo: SWASCHKUT 20191009
            //continue here:
            print PH::boldText("\nadd nat from VIP:\n");
            $this->add_nat_from_vip($data, $v, $source, $ismultiornot, $addVIP);
            /*
                    fix_dupli1cated_rulenames($source,$vsys,'security_rules',0);
                    fix_duplicated_rulenames($source,$vsys,'nat_rules',0);

                    remove_regions_from_nat($source,$vsys);
                */
        }


        return null;

        /*
        $getDM = $projectdb->query("SELECT id,vsys FROM device_mapping WHERE filename='$filename';");
        while ($data = $getDM->fetch_assoc()) {
            $vsys = $data['vsys'];
            update_progress($project, '0.10', 'File:' . $filename . ' Phase 1 Reading Zones ('.$vsys.')',$jobid);
            get_zones($fortinet_config_file, $vsys, $source, $template, $ismultiornot);
            update_progress($project, '0.20', 'File:' . $filename . ' Phase 2 Reading IP Pools and VIPs ('.$vsys.')',$jobid);
            get_ippools($fortinet_config_file, $source, $vsys, $ismultiornot);
            get_vip($fortinet_config_file, $source, $vsys, $ismultiornot);
            get_vipgrp($fortinet_config_file, $source, $vsys, $ismultiornot);
            update_progress($project, '0.30', 'File:' . $filename . ' Phase 3 Reading Address and Groups ('.$vsys.')',$jobid);
            $regions=array();
            get_address_Fortinet($fortinet_config_file, $source, $vsys, $ismultiornot, $regions);
            get_addressv6($fortinet_config_file, $source, $vsys, $ismultiornot, $regions);
            get_address_groups($fortinet_config_file, $source, $vsys, $ismultiornot, $regions);
            update_progress($project, '0.40', 'File:' . $filename . ' Phase 4 Reading Services and Groups ('.$vsys.')',$jobid);
            get_services2($fortinet_config_file, $source, $vsys, $ismultiornot);
            get_services_groups($fortinet_config_file, $source, $vsys, $ismultiornot);
            clean_duplicated_services($source, $vsys, $ismultiornot);
            update_progress($project, '0.50', 'File:' . $filename . ' Phase 5 Reading Security Policies ('.$vsys.')',$jobid);
            get_security_policy($fortinet_config_file, $source, $vsys, $ismultiornot,$regions);
            update_progress($project, '0.60', 'File:' . $filename . ' Phase 6 Reading Nat Policies ('.$vsys.')',$jobid);
            add_nat_from_vip($fortinet_config_file, $vsys, $source, $template, $ismultiornot);
            fix_dupli1cated_rulenames($source,$vsys,'security_rules',0);
            fix_duplicated_rulenames($source,$vsys,'nat_rules',0);
            remove_regions_from_nat($source,$vsys);
            #check_used_objects($source);
        }
        */
        ##check_used_objects_new();
// Call function to generate initial consumptions
        ##deviceUsage("initial", "get", $project, "", "", $vsys, $source, $template_name);
    }

    /*
     * not needed anymore
    function clean_duplicated_services($source, $vsys, $ismultiornot){

        global $projectdb;

        $table = "services";
        $table_group = "services_groups";
        $table_group_id = "services_groups_id";

        if ($ismultiornot == "multivsys") {
            $vsys = "shared";
        } else {
            $vsys = "root";
        }

        $getDup=$projectdb->query("SELECT name,count(id) as t FROM $table WHERE source='$source' AND vsys='$vsys' GROUP BY name HAVING t>1 ORDER BY id;");
        if ($getDup->num_rows > 0){
            $srvname=[];
            while($data=$getDup->fetch_assoc()){
                $srvname[]=$data['name'];
            }
            foreach ($srvname as $name){
                $projectdb->query("DELETE FROM $table WHERE source='$source' AND vsys='$vsys' AND name='$name' AND devicegroup='predefined';");
            }
        }

        $getDup=$projectdb->query("SELECT name,count(id) as t FROM $table_group_id WHERE source='$source' AND vsys='$vsys' GROUP BY name HAVING t>1 ORDER BY id;");
        if ($getDup->num_rows > 0){
            $srvname=[];
            while($data=$getDup->fetch_assoc()){
                $srvname[]=$data['name'];
            }
            foreach ($srvname as $name){
                $getMembers=$projectdb->query("SELECT id FROM $table_group_id WHERE source='$source' AND vsys='$vsys' AND name='$name' AND devicegroup='predefined';");
                if ($getMembers->num_rows==1){
                    $getMembersData=$getMembers->fetch_assoc();
                    $glid=$getMembersData['id'];
                    $projectdb->query("DELETE FROM $table_group_id WHERE source='$source' AND vsys='$vsys' AND name='$name' AND devicegroup='predefined';");
                    $projectdb->query("DELETE FROM $table_group WHERE lid='$glid';");
                }

            }
        }

    }

    function add_vsys($source, $projectdb, $vsys) {
    # ADD VSYS
        $getVsys = $projectdb->query("SELECT name FROM virtual_systems WHERE source='$source' ORDER BY name DESC LIMIT 1;");

        if ($getVsys->num_rows == 0) {
            $vsys_name = "root";
            $projectdb->query("INSERT INTO virtual_systems (source,name,display_name) VALUES ('$source','$vsys_name','$vsys')");
        } else {
            $getVsysData = $getVsys->fetch_assoc();
            $thename = $getVsysData['name'];
            $getVsysData1 = str_replace("vsys", "", $thename);
            $result = intval($getVsysData1) + 1;
            $vsys_name = "vsys" . $result;
            $projectdb->query("INSERT INTO virtual_systems (source,name,display_name) VALUES ('$source','$vsys_name','$vsys')");
        }
    }
    */

}


