<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */

require_once("lib/pan_php_framework.php");
require_once("parser/lib/CONVERTER.php");
require_once("parser/lib/PARSER.php");
require_once("parser/lib/SHAREDNEW.php");

$converter = new CONVERTER();
$converter->initial();
$converter->main("pfsense");
