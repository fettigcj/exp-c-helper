<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */

//Todo:
//1) Validation needed that vendor argument fit to the file via file argument; original config file must be check that this is for the correct vendor
//2) display usage and exit
//3) supported arguments
//4)

$debug = FALSE;
$print = FALSE;
$expedition = null;

$configFILE = FALSE;
$configIN = FALSE;
$configOUT = FALSE;


require_once("lib/pan_php_framework.php");

require_once("parser/lib/CONVERTER.php");
require_once("parser/lib/PARSER.php");
require_once("parser/lib/SHAREDNEW.php");


PH::processCliArgs();

function display_usage_and_exit($shortMessage = FALSE)
{
    global $argv;
    global $vendor_array;

    echo "\n";
    echo PH::boldText("USAGE: ") . "php " . basename(__FILE__) . " [in=inputfile.xml] out=outputfile.xml file=vendor_origfile vendor=ciscoasa \n";
    #echo "php ".basename(__FILE__)." listactions   : list supported actions\n";
    #echo "php ".basename(__FILE__)." listfilters   : list supported filter\n";
    #echo "php ".basename(__FILE__)." help          : more help messages\n";

    echo "          supported vendors: ";
    foreach( $vendor_array as $key => $vendor )
    {
        print " - " . $key . " ";
    }
    print "\n";

    #echo PH::boldText("\nExamples:\n");

    #echo " - php ".basename(__FILE__)." type=panorama in=api://192.169.50.10 location=DMZ-Firewall-Group actions=displayReferences 'filter=(name eq Mail-Host1)'\n";
    #echo " - php ".basename(__FILE__)." type=panos in=config.xml out=output.xml location=any actions=delete\n";

    if( !$shortMessage )
    {
        echo PH::boldText("\nListing available arguments\n\n");

        global $supportedArguments;

        ksort($supportedArguments);
        foreach( $supportedArguments as &$arg )
        {
            echo " - " . PH::boldText($arg['niceName']);
            if( isset($arg['argDesc']) )
                echo '=' . $arg['argDesc'];
            //."=";
            if( isset($arg['shortHelp']) )
                echo "\n     " . $arg['shortHelp'];
            echo "\n\n";
        }

        echo "\n\n";
    }

    exit(1);
}

function display_error_usage_exit($msg)
{
    fwrite(STDERR, PH::boldText("\n**ERROR** ") . $msg . "\n\n");
    display_usage_and_exit(TRUE);
}


$vendor_array = array();


//This is only to define the $vendor_array information; e.g. CISCO_parser.php is no longer called via exec()
$vendor_array['ciscoasa'] = "CISCO/CISCO_parser.php";
$vendor_array['pfsense'] = "PFSENSE/PFSENSE_parser.php";
$vendor_array['sophos'] = "SOPHOS/SOPHOS_parser.php";
$vendor_array['sonicwall'] = "SONICWAL/SONICwal_parser.php";
$vendor_array['netscreen'] = "SCREENOS/SCREENOS_parser.php";
$vendor_array['fortinet'] = "FORTINET/FORTINET_parser.php";
$vendor_array['srx'] = "SRX/SRX_parser.php";
$vendor_array['cp-r80'] = "CP_R80/CP_R80.php";
$vendor_array['cp'] = "CP/CP.php";
$vendor_array['cp-beta'] = "CP/develop/CPnew.php";


//THIS is still needed; migration needed of ciscoISR and ciscoSWITCH to class based
$vendor_array['ciscoswitch'] = "cisco_switch_acl/acl.php";
$vendor_array['ciscoisr'] = "cisco_isr/isr_acl.php";


$supportedArguments = array();
$supportedArguments['in'] = array('niceName' => 'in', 'shortHelp' => 'input file. Basic PAN-OS config, the migrated 3rd party config is merged with', 'argDesc' => '[filename]');
$supportedArguments['out'] = array('niceName' => 'out', 'shortHelp' => 'output file to save config after migration. ie: out=save-config.xml', 'argDesc' => '[filename]');
$supportedArguments['file'] = array('niceName' => 'file', 'shortHelp' => 'original 3rd party vendor config file. ie: file=cisco_config-export.txt', 'argDesc' => '[filename]');
$supportedArguments['vendor'] = array('niceName' => 'vendor', 'shortHelp' => 'vendor name ie: vendor=ciscoasa', 'argDesc' => '[vendor]');
$supportedArguments['print'] = array('niceName' => 'print', 'shortHelp' => 'display migration information',);
$supportedArguments['debug'] = array('niceName' => 'debug', 'shortHelp' => 'display debug migration information');
$supportedArguments['expedition'] = array('niceName' => 'expedition', 'shortHelp' => 'only used if called from Expedition Tool');
$supportedArguments['routetable'] = array('niceName' => 'RouteTable', 'shortHelp' => 'Routing table of: CheckPoint FW ["netstat -nr" or "show route all"] / Cisco [show routes] - create static routing for dynamic routes to calculate Zones', 'argDesc' => '[filename]');
$supportedArguments['reducexml'] = array('niceName' => 'reduceXML', 'shortHelp' => 'remove NewLine and blank from migrated PAN-OS XML file');

if( isset(PH::$args['help']) )
{
    $pos = array_search('help', $argv);

    if( $pos === FALSE )
        display_usage_and_exit(FALSE);

    $keys = array_keys($argv);

    if( $pos == end($keys) )
        display_usage_and_exit(FALSE);

    display_usage_and_exit(FALSE);

    /*
    $action = $argv[(array_search($pos, $keys) +1)];

    if( !isset(RuleCallContext::$supportedActions[strtolower($action)]) )
        derr("request help for action '{$action}' but it does not exist");

    $action = & RuleCallContext::$supportedActions[strtolower($action)];

    $args = Array();
    if( isset($action['args']) )
    {
        foreach( $action['args'] as $argName => &$argDetails )
        {
            if( $argDetails['default'] == '*nodefault*' )
                $args[] = "{$argName}";
            else
                $args[] = "[{$argName}]";
        }
    }

    $args = PH::list_to_string($args);
    print "*** help for Action ".PH::boldText($action['name']).":".$args."\n";

    if( isset($action['help']) )
        print $action['help'];

    if( !isset($args) || !isset($action['args']) )
    {
        print "\n\n**No arguments required**";
    }
    else
    {
        print "\nListing arguments:\n\n";
        foreach( $action['args'] as $argName => &$argDetails )
        {
            print "-- ".PH::boldText($argName)." :";
            if( $argDetails['default'] != "*nodefault" )
                print " OPTIONAL";
            print " type={$argDetails['type']}";
            if( isset($argDetails['choices']) )
            {
                print "     choices: ".PH::list_to_string($argDetails['choices']);
            }
            print "\n";
            if( isset($argDetails['help']) )
                print " ".str_replace("\n", "\n ",$argDetails['help']);
            else
                print "  *no help avaiable*";
            print "\n\n";
        }
    }
    */

    print "\n\n";

    exit(0);
}

if( isset(PH::$args['debug']) )
    $debug = TRUE;

if( isset(PH::$args['print']) )
    $print = TRUE;

if( isset(PH::$args['expedition']) )
    $expedition = PH::$args['expedition'];

if( isset(PH::$args['file']) )
    $configFILE = PH::$args['file'];
else
    display_error_usage_exit('"file" is missing from arguments');

if( isset(PH::$args['out']) )
    $configOUT = PH::$args['out'];
else
    display_error_usage_exit('"out" is missing from arguments');

if( isset(PH::$args['in']) )
    $configIN = PH::$args['in'];

if( isset(PH::$args['vendor']) )
{
    $vendor = strtolower(PH::$args['vendor']);
    if( !isset($vendor_array[$vendor]) )
    {
        print "\n\navailable entries for 'vendor' argument:\n\n";
        foreach( array_keys($vendor_array) as $tmp_vendor )
        {
            print "  vendor=" . $tmp_vendor . "\n";
        }
        derr("Vendor: " . $vendor . " not supported yet TEST." . print_r($vendor, TRUE));
    }
}
else
    display_error_usage_exit('"vendor" is missing from arguments');


/*
//if( $vendor == "ciscoswitchacl" || $vendor == "ciscoisr" )
if( $vendor == "ciscoswitchacl"  )
{
    $cli = "php " . __DIR__ . "/" . $vendor_array[$vendor] . " 'file={$configFILE}' 'out={$configOUT}' 'vendor={$vendor}'";

    if( strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' )
    {
        $cli = str_replace( "/", "\\", $cli );
        $cli = str_replace( "'", "\"", $cli );
    }

    if( $configIN != false )
        $cli .= " 'in={$configIN}'";

    if( $debug )
        $cli .= ' debug';
    if( $print )
        $cli .= ' print';
    if( isset(PH::$args['expedition']) )
        $cli .= " 'expedition={$expedition}'";

    #$cli .= ' 2>&1';
    #$cli .= " | tee {$output_folder}/output_{$configFILE}.txt";

    echo " * Executing CLI: {$cli}\n";

    $output = Array();
    $retValue = 0;




    PH::enableExceptionSupport();
    try
    {
        exec($cli, $output, $retValue);
    }
    catch(Exception $e)
    {
        print $padding." ***** an error occured : ".$e->getMessage()."\n\n";
        return;
    }
    PH::disableExceptionSupport();



    foreach( $output as $line )
    {
        echo '   ##  ';
        echo $line;
        echo "\n";
    }

    if( $retValue != 0 )
        derr("CLI exit with error code '{$retValue}'");

}
else
{
*/
$converter = new CONVERTER();
$converter->initial();
$converter->main($vendor);

//}

echo "\n";
