<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */


require_once("CPtest.php");
require_once("CP_R80_objects.php");
require_once("CP_R80_accesslayer.php");
require_once("CP_R80_natlayer.php");
require_once("CP_R80_staticroute.php");


class CP_R80 extends PARSER
{
    public $mainfolder = "/tmp/test";
    public $newfolder = "";
    public $folder_path = "";

    public $v = null;

    use CPtest;
    use CP_R80_objects;
    use CP_R80_accesslayer;
    use CP_R80_staticroute;
    use CP_R80_natlayer;


    use SHAREDNEW;

    function vendor_main($config_filename, $pan, $routetable = "")
    {
        $this->logger->start();

        parent::preCheck();


        //swaschkut - tmp, until class migration is done
        global $print;
        $print = TRUE;


        //Fortinet specific
        //------------------------------------------------------------------------
        $path = "";
        $project = "";

        $config_path = $path . $config_filename;
        $filename = $config_filename;
        $filenameParts = pathinfo($config_filename);
        $verificationName = $filenameParts['filename'];

        $this->logger->increaseCompleted();

        $data = $this->clean_config($config_path, $project, $config_filename);

        $this->logger->increaseCompleted();
        $this->logger->setCompletedSilent();

        $this->import_config($data, $pan, $routetable); //This should update the $source
        //------------------------------------------------------------------------

        /*
                //Todo: validation if GLOBAL rule
                echo PH::boldText( "Zone Calculation for Security and NAT policy" );
                Converter::calculate_zones( $pan, "append" );


                echo PH::boldText( "\nVALIDATION - interface name and change into PAN-OS confirm naming convention\n" );
                CONVERTER::validate_interface_names($pan);

                //Todo: where to place custom table for app-migration if needed
                echo PH::boldText( "\nVALIDATION - replace tmp services with APP-id if possible\n" );
                CONVERTER::AppMigration( $pan );
        */


        //todo delete all created files and folders

        #delete_files($mainfolder);
        #$this->delete_directory( $this->mainfolder );
    }

    function clean_config($config_path, $project, $config_filename)
    {
        //validation if file has .tar.gz
        if( strpos($config_filename, ".tar.gz") === FALSE && strpos($config_filename, ".tgz") === FALSE )
        {
            derr("specified filename with argument 'FILE' is not 'tar.gz' ");
        }
        else
        {
            $srcfile = $config_filename;


            //Todo check if it is better to create this under Tool folder and clean it up at the end


            $this->newfolder = $this->mainfolder . "/test123";

            print "folder: " . $this->newfolder . "\n";

            if( file_exists($this->newfolder) )
                $this->delete_directory($this->newfolder);

            if( !file_exists($this->newfolder) )
                mkdir($this->newfolder, 0700, TRUE);

            $destfile = $this->mainfolder . '/test1.tar.gz';

            if( !copy($srcfile, $destfile) )
            {
                echo "File cannot be copied! \n";
            }
            else
            {
                #echo "File has been copied!\n";
            }

            //extract into specified folder
            exec('tar -C ' . $this->newfolder . '/' . ' -zxvf ' . $destfile . ' 2>&1');

            #print "sleep 15 seconds: wait for tar extract complete";
            #sleep(15);
        }

        $this->folder_path = $this->newfolder . "/";
        $config_path = "index.json";

        if( !file_exists($this->folder_path . $config_path) )
        {
            //print out all file / folder information
            $files10 = scandir($this->newfolder);
            unset($files10[0]);
            unset($files10[1]);
            print_r($files10);


            $this->folder_path = $this->newfolder . "/" . $files10[2] . "/";


            $files10 = scandir($this->folder_path);
            unset($files10[0]);
            unset($files10[1]);
            print_r($files10);

            foreach( $files10 as $tarFile )
            {
                exec('tar -C ' . $this->folder_path . '/' . ' -zxvf ' . $this->folder_path . "/" . $tarFile . ' 2>&1');
            }

            $files10 = scandir($this->folder_path);
            unset($files10[0]);
            unset($files10[1]);
            print_r($files10);

        }


        #$someJSON = file($config_path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        $someJSON = file_get_contents($this->folder_path . $config_path);


        // Convert JSON string to Array
        $someArray = json_decode($someJSON, TRUE);
        #print_r($someArray);        // Dump all data of the Array


        $this->jsonERROR();

        if( !is_array($someArray) )
            derr("json_decode not working");

        return $someArray;
    }


//---------------------------------------------
//        Parser Logic starts here
//----------------------------------------------


    function import_config($data, $pan, $routetable)
    {
        global $projectdb;
        global $source;

        global $debug;
        global $print;

        $this->v = $pan->findVirtualSystem('vsys1');


        if( $routetable != "" )
        {
            echo PH::boldText("\nimport dynamic Routing\n");

            $cisco = file_get_contents($routetable);
            $this->importRoutes($cisco, $pan, $this->v);
        }


        $padding = "";
        $this->print_array($data, $padding);

    }


    //CLEANUP function

    function delete_directory($mainfolder)
    {
        if( is_dir($mainfolder) )
            $dir_handle = opendir($mainfolder);
        if( !$dir_handle )
            return FALSE;

        while( $file = readdir($dir_handle) )
        {
            if( $file != "." && $file != ".." )
            {
                if( !is_dir($mainfolder . "/" . $file) )
                {
                    #print "unlink: ".$dirname.'/'.$file."\n";
                    unlink($mainfolder . "/" . $file);
                }

                else
                    $this->delete_directory($mainfolder . '/' . $file);
            }
        }
        closedir($dir_handle);
        #print "DEL folder: ".$dirname."\n";
        rmdir($mainfolder);
        return TRUE;
    }

    function anything_to_utf8($var, $deep = TRUE)
    {
        if( is_array($var) )
        {
            foreach( $var as $key => $value )
            {
                if( $deep )
                {
                    $var[$key] = anything_to_utf8($value, $deep);
                }
                elseif( !is_array($value) && !is_object($value) && !mb_detect_encoding($value, 'utf-8', TRUE) )
                {
                    $var[$key] = utf8_encode($var);
                }
            }
            return $var;
        }
        elseif( is_object($var) )
        {
            foreach( $var as $key => $value )
            {
                if( $deep )
                {
                    $var->$key = anything_to_utf8($value, $deep);
                }
                elseif( !is_array($value) && !is_object($value) && !mb_detect_encoding($value, 'utf-8', TRUE) )
                {
                    $var->$key = utf8_encode($var);
                }
            }
            return $var;
        }
        else
        {
            return (!mb_detect_encoding($var, 'utf-8', TRUE)) ? utf8_encode($var) : $var;
        }
    }

}


