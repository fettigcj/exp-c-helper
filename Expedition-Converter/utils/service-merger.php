<?php
/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */

echo "\n***********************************************\n";
echo "*********** " . basename(__FILE__) . " UTILITY **************\n\n";

set_include_path(dirname(__FILE__) . '/../' . PATH_SEPARATOR . get_include_path());
require_once("lib/pan_php_framework.php");

require_once("utils/lib/UTIL.php");
require_once("utils/lib/MERGER.php");

$supportedArguments = array();
$supportedArguments[] = array('niceName' => 'in', 'shortHelp' => 'input file ie: in=config.xml', 'argDesc' => '[filename]');
$supportedArguments[] = array('niceName' => 'out', 'shortHelp' => 'output file to save config after changes. Only required when input is a file. ie: out=save-config.xml', 'argDesc' => '[filename]');
$supportedArguments[] = array('niceName' => 'Location', 'shortHelp' => 'specify if you want to limit your query to a VSYS/DG. By default location=shared for Panorama, =vsys1 for PANOS. ie: location=any or location=vsys2,vsys1', 'argDesc' => 'sub1[,sub2]');
$supportedArguments[] = array('niceName' => 'DupAlgorithm',
    'shortHelp' => "Specifies how to detect duplicates:\n" .
        "  - SameDstSrcPorts: objects with same Dst and Src ports will be replaced by the one picked (default)\n" .
        "  - SamePorts: objects with same Dst ports will be replaced by the one picked\n" .
        "  - WhereUsed: objects used exactly in the same location will be merged into 1 single object and all ports covered by these objects will be aggregated\n",
    'argDesc' => 'SameDstSrcPorts|SamePorts|WhereUsed');
$supportedArguments[] = array('niceName' => 'mergeCountLimit', 'shortHelp' => 'stop operations after X objects have been merged', 'argDesc' => '100');
$supportedArguments[] = array('niceName' => 'pickFilter',
    'shortHelp' => "specify a filter a pick which object will be kept while others will be replaced by this one.\n" .
        "   ie: 2 services are found to be mergeable: 'H-1.1.1.1' and 'Server-ABC'. Then by using pickFilter=(name regex /^H-/) you would ensure that object H-1.1.1.1 would remain and Server-ABC be replaced by it.",
    'argDesc' => '(name regex /^g/)');
$supportedArguments[] = array('niceName' => 'excludeFilter', 'shortHelp' => 'specify a filter to exclude objects from merging process entirely', 'argDesc' => '(name regex /^g/)');
$supportedArguments[] = array('niceName' => 'allowMergingWithUpperLevel', 'shortHelp' => 'when this argument is specified, it instructs the script to also look for duplicates in upper level');
$supportedArguments[] = array('niceName' => 'help', 'shortHelp' => 'this message');

$usageMsg = PH::boldText("USAGE: ") . "php " . basename(__FILE__) . " in=inputfile.xml [out=outputfile.xml] location=shared [dupAlgorithm=XYZ] [MergeCountLimit=100] ['pickFilter=(name regex /^H-/)']...";


$nestedQueries = array();
$deletedObjects = array();
$debugAPI = FALSE;

$PHP_FILE = __FILE__;
$utilType = "service-merger";


$merger = new MERGER($utilType, $argv, $PHP_FILE, $supportedArguments, $usageMsg);

$merger->prepareSupportedArgumentsArray();

PH::processCliArgs();

$merger->arg_validation();
$merger->help($argv);
$merger->inDebugapiArgument();
$merger->inputValidation();
$merger->location_provided();


if( isset(PH::$args['mergecountlimit']) )
    $mergeCountLimit = PH::$args['mergecountlimit'];
else
    $mergeCountLimit = FALSE;

if( isset(PH::$args['dupalgorithm']) )
{
    $dupAlg = strtolower(PH::$args['dupalgorithm']);
    if( $dupAlg != 'sameports' && $dupAlg != 'whereused' && $dupAlg != 'samedstsrcports' )
        display_error_usage_exit('unsupported value for dupAlgorithm: ' . PH::$args['dupalgorithm']);
}
else
    $dupAlg = 'samedstsrcports';


$merger->load_config();

$pan = $merger->pan;

$location_array = array();
$location_array = $merger->merger_location_array($utilType, $merger->objectsLocation, $pan);


$merger->filterArgument($pickFilter, $excludeFilter, $upperLevelSearch);


foreach( $location_array as $tmp_location )
{
    $store = $tmp_location['store'];
    $findLocation = $tmp_location['findLocation'];
    $parentStore = $tmp_location['parentStore'];
    $childDeviceGroups = $tmp_location['childDeviceGroups'];

    echo " - upper level search status : " . boolYesNo($upperLevelSearch) . "\n";
    if( is_string($findLocation) )
        echo " - location 'shared' found\n";
    else
        echo " - location '{$findLocation->name()}' found\n";
    echo " - found {$store->countServices()} services\n";
    echo " - DupAlgorithm selected: {$dupAlg}\n";
    echo " - computing service values database ... ";
    sleep(1);


//
// Building a hash table of all service based on their REAL port mapping
//
    if( $upperLevelSearch )
        $objectsToSearchThrough = $store->nestedPointOfView();
    else
        $objectsToSearchThrough = $store->serviceObjects();

    $hashMap = array();
    $upperHashMap = array();
    if( $dupAlg == 'sameports' || $dupAlg == 'samedstsrcports' )
        foreach( $objectsToSearchThrough as $object )
        {
            if( !$object->isService() )
                continue;
            if( $object->isTmpSrv() )
                continue;

            if( $excludeFilter !== null && $excludeFilter->matchSingleObject(array('object' => $object, 'nestedQueries' => &$nestedQueries)) )
                continue;


            $skipThisOne = FALSE;

            // Object with descendants in lower device groups should be excluded
            if( $pan->isPanorama() && $object->owner === $store )
            {
                foreach( $childDeviceGroups as $dg )
                {
                    if( $dg->serviceStore->find($object->name(), null, FALSE) !== null )
                    {
                        $tmp_obj = $dg->serviceStore->find($object->name(), null, FALSE);

                        print "\n- object '" . $object->name() . "' [value '{$object->value()}'] skipped because of same object name [ ";
                        if( $tmp_obj->isService() )
                            print "with value '{$tmp_obj->value()}'";
                        else
                            print "but as SERVICEGROUP";
                        print " ] available at lower level DG: " . $dg->name() . "\n";

                        $skipThisOne = TRUE;
                        break;
                    }
                }
                if( $skipThisOne )
                    continue;
            }

            $value = $object->dstPortMapping()->mappingToText();

            if( $object->owner === $store )
            {
                $hashMap[$value][] = $object;
                if( $parentStore !== null )
                {
                    $findAncestor = $parentStore->find($object->name(), null, TRUE);
                    if( $findAncestor !== null )
                        $object->ancestor = $findAncestor;
                }
            }
            else
                $upperHashMap[$value][] = $object;

        }
    elseif( $dupAlg == 'whereused' )
        foreach( $objectsToSearchThrough as $object )
        {
            if( !$object->isService() )
                continue;
            if( $object->isTmpSrv() )
                continue;

            if( $object->countReferences() == 0 )
                continue;

            if( $excludeFilter !== null && $excludeFilter->matchSingleObject(array('object' => $object, 'nestedQueries' => &$nestedQueries)) )
                continue;

            $value = $object->getRefHashComp() . $object->protocol();
            if( $object->owner === $store )
            {
                $hashMap[$value][] = $object;
                if( $parentStore !== null )
                {
                    $findAncestor = $parentStore->find($object->name(), null, TRUE);
                    if( $findAncestor !== null )
                        $object->ancestor = $findAncestor;
                }
            }
            else
                $upperHashMap[$value][] = $object;
        }
    else derr("unsupported use case");

//
// Hashes with single entries have no duplicate, let's remove them
//
    $countConcernedObjects = 0;
    foreach( $hashMap as $index => &$hash )
    {
        if( count($hash) == 1 && !isset($upperHashMap[$index]) && !isset(reset($hash)->ancestor) )
            unset($hashMap[$index]);
        else
            $countConcernedObjects += count($hash);
    }
    unset($hash);
    echo "OK!\n";

    echo " - found " . count($hashMap) . " duplicates values totalling {$countConcernedObjects} service objects which are duplicate\n";

    echo "\n\nNow going after each duplicates for a replacement\n";

    $countRemoved = 0;
    if( $dupAlg == 'sameports' || $dupAlg == 'samedstsrcports' )
        foreach( $hashMap as $index => &$hash )
        {
            echo "\n";
            echo " - value '{$index}'\n";

            $pickedObject = null;

            if( $pickFilter !== null )
            {
                if( isset($upperHashMap[$index]) )
                {
                    foreach( $upperHashMap[$index] as $object )
                    {
                        if( $pickFilter->matchSingleObject(array('object' => $object, 'nestedQueries' => &$nestedQueries)) )
                        {
                            $pickedObject = $object;
                            break;
                        }
                    }
                    if( $pickedObject === null )
                        $pickedObject = reset($upperHashMap[$index]);

                    echo "   * using object from upper level : '{$pickedObject->name()}'\n";
                }
                else
                {
                    foreach( $hash as $object )
                    {
                        if( $pickFilter->matchSingleObject(array('object' => $object, 'nestedQueries' => &$nestedQueries)) )
                        {
                            $pickedObject = $object;
                            break;
                        }
                    }
                    if( $pickedObject === null )
                        $pickedObject = reset($hash);

                    echo "   * keeping object '{$pickedObject->name()}'\n";
                }
            }
            else
            {
                if( isset($upperHashMap[$index]) )
                {
                    $pickedObject = reset($upperHashMap[$index]);
                    echo "   * using object from upper level : '{$pickedObject->name()}'\n";
                }
                else
                {
                    $pickedObject = reset($hash);
                    echo "   * keeping object '{$pickedObject->name()}'\n";
                }
            }

            foreach( $hash as $object )
            {
                /** @var Service $object */

                if( isset($object->ancestor) )
                {
                    $ancestor = $object->ancestor;

                    if( !$ancestor->isService() )
                    {
                        echo "    - SKIP: object name '{$object->name()}' as one ancestor is of type servicegroup\n";
                        continue;
                    }

                    /** @var Service $ancestor */
                    if( $upperLevelSearch && !$ancestor->isGroup() && !$ancestor->isTmpSrv() )
                    {
                        if( $object->dstPortMapping()->equals($ancestor->dstPortMapping()) )
                        {
                            if( !$object->srcPortMapping()->equals($ancestor->srcPortMapping()) && $dupAlg == 'samedstsrcports' )
                            {
                                echo "    - object '{$object->name()}' cannot be merged because of different SRC port information";
                                echo "  object value: " . $object->srcPortMapping()->mappingToText() . " | pickedObject value: " . $ancestor->srcPortMapping()->mappingToText() . "\n";
                                continue;
                            }
                            elseif( $object->getOverride() != $ancestor->getOverride() )
                            {
                                echo "    - object '{$object->name()}' cannot be merged because of different Override information";
                                echo "  object value: " . $object->getOverride() . " | pickedObject value: " . $ancestor->getOverride() . "\n";
                                continue;
                            }

                            echo "    - object '{$object->name()}' merged with its ancestor, deleting this one... ";
                            $object->replaceMeGlobally($ancestor);
                            if( $merger->apiMode )
                                $object->owner->API_remove($object, TRUE);
                            else
                                $object->owner->remove($object, TRUE);

                            echo "OK!\n";

                            echo "         anchestor name: '{$ancestor->name()}' DG: ";
                            if( $ancestor->owner->owner->name() == "" ) print "'shared'";
                            else print "'{$ancestor->owner->owner->name()}'";
                            print  "  value: '{$ancestor->getDestPort()}' \n";

                            if( $pickedObject === $object )
                                $pickedObject = $ancestor;

                            $countRemoved++;
                            continue;
                        }
                    }
                    echo "    - object '{$object->name()}' cannot be merged because it has an ancestor\n";

                    echo "         anchestor name: '{$ancestor->name()}' DG: ";
                    if( $ancestor->owner->owner->name() == "" ) print "'shared'";
                    else print "'{$ancestor->owner->owner->name()}'";
                    print  "  value: '{$ancestor->getDestPort()}' \n";

                    continue;
                }
                else
                {
                    if( !$object->srcPortMapping()->equals($pickedObject->srcPortMapping()) && $dupAlg == 'samedstsrcports' )
                    {
                        echo "    - object '{$object->name()}' cannot be merged because of different SRC port information";
                        echo "  object value: " . $object->srcPortMapping()->mappingToText() . " | pickedObject value: " . $pickedObject->srcPortMapping()->mappingToText() . "\n";
                        continue;
                    }
                    elseif( $object->getOverride() != $pickedObject->getOverride() )
                    {
                        echo "    - object '{$object->name()}' cannot be merged because of different Override information";
                        echo "  object value: " . $object->getOverride() . " | pickedObject value: " . $pickedObject->getOverride() . "\n";
                        continue;
                    }
                }

                if( $object === $pickedObject )
                    continue;


                echo "    - replacing '{$object->_PANC_shortName()}' ...\n";
                $object->__replaceWhereIamUsed($merger->apiMode, $pickedObject, TRUE, 5);

                echo "    - deleting '{$object->_PANC_shortName()}'\n";
                if( $merger->apiMode )
                {
                    $object->owner->API_remove($object, TRUE);
                }
                else
                {
                    $object->owner->remove($object, TRUE);
                }

                $countRemoved++;

                if( $mergeCountLimit !== FALSE && $countRemoved >= $mergeCountLimit )
                {
                    echo "\n *** STOPPING MERGE OPERATIONS NOW SINCE WE REACHED mergeCountLimit ({$mergeCountLimit})\n";
                    break 2;
                }
            }
        }
    elseif( $dupAlg == 'whereused' )
        foreach( $hashMap as $index => &$hash )
        {
            echo "\n";

            $setList = array();
            foreach( $hash as $object )
            {
                /** @var Service $object */
                $setList[] = PH::getLocationString($object->owner->owner) . '/' . $object->name();
            }
            echo " - duplicate set : '" . PH::list_to_string($setList) . "'\n";

            /** @var Service $pickedObject */
            $pickedObject = null;

            if( $pickFilter !== null )
            {
                foreach( $hash as $object )
                {
                    if( $pickFilter->matchSingleObject(array('object' => $object, 'nestedQueries' => &$nestedQueries)) )
                    {
                        $pickedObject = $object;
                        break;
                    }
                }
            }

            if( $pickedObject === null )
                $pickedObject = reset($hash);

            echo "   * keeping object '{$pickedObject->name()}'\n";

            foreach( $hash as $object )
            {
                /** @var Service $object */

                if( isset($object->ancestor) )
                {
                    $ancestor = $object->ancestor;
                    /** @var Service $ancestor */
                    echo "    - object '{$object->name()}' cannot be merged because it has an ancestor\n";

                    echo "         anchestor name: '{$ancestor->name()}' DG: ";
                    if( $ancestor->owner->owner->name() == "" ) print "'shared'";
                    else print "'{$ancestor->owner->owner->name()}'";
                    print  "  value: '{$ancestor->value()}' \n";

                    continue;
                }

                if( $object === $pickedObject )
                    continue;

                $localMapping = $object->dstPortMapping();
                echo "    - adding the following ports to first service: " . $localMapping->mappingToText() . "\n";
                $localMapping->mergeWithMapping($pickedObject->dstPortMapping());

                if( $merger->apiMode )
                {
                    if( $pickedObject->isTcp() )
                        $pickedObject->API_setDestPort($localMapping->tcpMappingToText());
                    else
                        $pickedObject->API_setDestPort($localMapping->udpMappingToText());
                    echo "    - removing '{$object->name()}' from places where it's used:\n";
                    $object->API_removeWhereIamUsed(TRUE, 7);
                    $object->owner->API_remove($object);
                    $countRemoved++;
                }
                else
                {
                    if( $pickedObject->isTcp() )
                        $pickedObject->setDestPort($localMapping->tcpMappingToText());
                    else
                        $pickedObject->setDestPort($localMapping->udpMappingToText());

                    echo "    - removing '{$object->name()}' from places where it's used:\n";
                    $object->removeWhereIamUsed(TRUE, 7);
                    $object->owner->remove($object);
                    $countRemoved++;
                }


                if( $mergeCountLimit !== FALSE && $countRemoved >= $mergeCountLimit )
                {
                    echo "\n *** STOPPING MERGE OPERATIONS NOW SINCE WE REACH mergeCountLimit ({$mergeCountLimit})\n";
                    break 2;
                }

            }
            echo "   * final mapping for service '{$pickedObject->name()}': {$pickedObject->getDestPort()}\n";

            echo "\n";
        }
    else derr("unsupported use case");


    echo "\n\nDuplicates removal is now done. Number of objects after cleanup: '{$store->countServices()}' (removed {$countRemoved} services)\n\n";

    echo "\n\n***********************************************\n\n";

}

echo "\n\n";

$merger->save_our_work();

echo "\n************* END OF SCRIPT " . basename(__FILE__) . " ************\n\n";

