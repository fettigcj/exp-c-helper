<?php

/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */

echo "\n***********************************************\n";
echo "*********** ADDRESS-EDIT UTILITY **************\n\n";

#require_once("lib/autoloader.php");
#spl_autoload_register('myAutoloader');


set_include_path(dirname(__FILE__) . '/../' . PATH_SEPARATOR . get_include_path());
require_once("lib/pan_php_framework.php");
require_once("common/actions.php");

require_once("utils/lib/UTIL.php");


echo "\n";

$util = new UTIL("address", $argv, __FILE__);


echo "\n\n********** END OF ADDRESS-EDIT UTILITY ***********\n";
echo "**************************************************\n";
echo "\n\n";
