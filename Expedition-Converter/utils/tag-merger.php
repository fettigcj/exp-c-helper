<?php
/**
 * © 2019 Palo Alto Networks, Inc.  All rights reserved.
 *
 * Licensed under SCRIPT SOFTWARE AGREEMENT, Palo Alto Networks, Inc., at https://www.paloaltonetworks.com/legal/script-software-license-1-0.pdf
 *
 */

echo "\n***********************************************\n";
echo "*********** " . basename(__FILE__) . " UTILITY **************\n\n";

set_include_path(dirname(__FILE__) . '/../' . PATH_SEPARATOR . get_include_path());
require_once("lib/pan_php_framework.php");

require_once("utils/lib/UTIL.php");
require_once("utils/lib/MERGER.php");

//Todo: 20180927
//location=any
//if NOT allowmergingwithupperlevel - start script
//if allowmergingwithupperlevel ->
//- build DG hierarchy array
//- start from youngest child up to parent and merge with shared


$supportedArguments = array();
$supportedArguments[] = array('niceName' => 'in', 'shortHelp' => 'input file or api. ie: in=config.xml  or in=api://192.168.1.1 or in=api://0018CAEC3@panorama.company.com', 'argDesc' => '[filename]|[api://IP]|[api://serial@IP]');
$supportedArguments[] = array('niceName' => 'out', 'shortHelp' => 'output file to save config after changes. Only required when input is a file. ie: out=save-config.xml', 'argDesc' => '[filename]');
$supportedArguments[] = array('niceName' => 'Location', 'shortHelp' => 'specify if you want to limit your query to a VSYS/DG. By default location=shared for Panorama, =vsys1 for PANOS', 'argDesc' => 'vsys1|shared|dg1');
$supportedArguments[] = array('niceName' => 'DupAlgorithm',
    'shortHelp' => "Specifies how to detect duplicates:\n" .
        "  - SameColor: objects with same TAG-color will be replaced by the one picked (default)\n" .
        "  - Identical: objects with same TAG-color and same name will be replaced by the one picked\n" .
        "  - WhereUsed: objects used exactly in the same location will be merged into 1 single object and all ports covered by these objects will be aggregated\n",
    'argDesc' => 'SameColor | Identical | WhereUsed');
$supportedArguments[] = array('niceName' => 'mergeCountLimit', 'shortHelp' => 'stop operations after X objects have been merged', 'argDesc' => '100');
$supportedArguments[] = array('niceName' => 'pickFilter', 'shortHelp' => 'specify a filter a pick which object will be kept while others will be replaced by this one', 'argDesc' => '(name regex /^g/)');
$supportedArguments[] = array('niceName' => 'excludeFilter', 'shortHelp' => 'specify a filter to exclude objects from merging process entirely', 'argDesc' => '(name regex /^g/)');
$supportedArguments[] = array('niceName' => 'allowMergingWithUpperLevel', 'shortHelp' => 'when this argument is specified, it instructs the script to also look for duplicates in upper level');
$supportedArguments[] = array('niceName' => 'help', 'shortHelp' => 'this message');
$supportedArguments[] = array('niceName' => 'exportCSV', 'shortHelp' => 'when this argument is specified, it instructs the script to print out the kept and removed objects per value');
$supportedArguments[] = array('niceName' => 'DebugAPI', 'shortHelp' => 'prints API calls when they happen');

$usageMsg = PH::boldText('USAGE: ') . "php " . basename(__FILE__) . " in=inputfile.xml [out=outputfile.xml] location=shared ['pickFilter=(name regex /^H-/)']\n" .
    "       php " . basename(__FILE__) . " in=api://192.169.50.10 location=shared ['pickFilter=(name regex /^H-/)']";


$nestedQueries = array();
$deletedObjects = array();
$debugAPI = FALSE;

$PHP_FILE = __FILE__;
$utilType = "tag-merger";


$merger = new MERGER($utilType, $argv, $PHP_FILE, $supportedArguments, $usageMsg);

$merger->prepareSupportedArgumentsArray();

PH::processCliArgs();

$merger->arg_validation();
$merger->help($argv);
$merger->inDebugapiArgument();
$merger->inputValidation();
$merger->location_provided();


if( isset(PH::$args['mergecountlimit']) )
    $mergeCountLimit = PH::$args['mergecountlimit'];
else
    $mergeCountLimit = FALSE;

if( isset(PH::$args['dupalgorithm']) )
{
    $dupAlg = strtolower(PH::$args['dupalgorithm']);
    if( $dupAlg != 'samecolor' && $dupAlg != 'whereused' && $dupAlg != 'identical' )
        display_error_usage_exit('unsupported value for dupAlgorithm: ' . PH::$args['dupalgorithm']);
}
else
    $dupAlg = 'identical';


$merger->load_config();


$pan = $merger->pan;

$location_array = array();
$location_array = $merger->merger_location_array($utilType, $merger->objectsLocation, $pan);


$merger->filterArgument($pickFilter, $excludeFilter, $upperLevelSearch);


foreach( $location_array as $tmp_location )
{
    $store = $tmp_location['store'];
    $findLocation = $tmp_location['findLocation'];
    $parentStore = $tmp_location['parentStore'];
    $childDeviceGroups = $tmp_location['childDeviceGroups'];

    echo " - upper level search status : " . boolYesNo($upperLevelSearch) . "\n";
    if( is_string($findLocation) )
        echo " - location 'shared' found\n";
    else
        echo " - location '{$findLocation->name()}' found\n";
    echo " - found {$store->count()} tag Objects\n";
    echo " - DupAlgorithm selected: {$dupAlg}\n";
    echo " - computing tag values database ... \n";
    sleep(1);

//
// Building a hash table of all tag objects with same value
//
    if( $upperLevelSearch )
        $objectsToSearchThrough = $store->nestedPointOfView();
    else
        $objectsToSearchThrough = $store->tags();

    $hashMap = array();
    $upperHashMap = array();
    if( $dupAlg == 'samecolor' || $dupAlg == 'identical' )
    {
        foreach( $objectsToSearchThrough as $object )
        {
            if( !$object->isTag() )
                continue;
            if( $object->isTmp() )
                continue;

            if( $excludeFilter !== null && $excludeFilter->matchSingleObject(array('object' => $object, 'nestedQueries' => &$nestedQueries)) )
                continue;

            $skipThisOne = FALSE;

            // Object with descendants in lower device groups should be excluded
            if( $pan->isPanorama() && $object->owner === $store )
            {
                foreach( $childDeviceGroups as $dg )
                {
                    if( $dg->tagStore->find($object->name(), null, FALSE) !== null )
                    {
                        $tmp_obj = $dg->tagStore->find($object->name(), null, FALSE);
                        print PH::boldText("\n\n - object '" . $object->name() . "' [value '{$object->getColor()}'] skipped because of same object name [with value '{$tmp_obj->getColor()}'] available at lower level DG: " . $dg->name() . "\n\n");
                        $skipThisOne = TRUE;
                        break;
                    }
                }
                if( $skipThisOne )
                    continue;
            }

            $value = $object->getColor();
            $value = $object->name();

            /*
            // if object is /32, let's remove it to match equivalent non /32 syntax
            if( $object->isType_ipNetmask() && strpos($object->value(), '/32') !== FALSE )
                $value = substr($value, 0, strlen($value) - 3);

            $value = $object->type() . '-' . $value;
            */

            if( $object->owner === $store )
            {
                $hashMap[$value][] = $object;
                if( $parentStore !== null )
                {
                    $findAncestor = $parentStore->find($object->name(), null, TRUE);
                    if( $findAncestor !== null )
                        $object->ancestor = $findAncestor;
                }
            }
            else
                $upperHashMap[$value][] = $object;
        }
    }
    elseif( $dupAlg == 'whereused' )
        foreach( $objectsToSearchThrough as $object )
        {
            if( !$object->isTag() )
                continue;
            if( $object->isTmp() )
                continue;

            if( $object->countReferences() == 0 )
                continue;

            if( $excludeFilter !== null && $excludeFilter->matchSingleObject(array('object' => $object, 'nestedQueries' => &$nestedQueries)) )
                continue;

            #$value = $object->getRefHashComp() . $object->getNetworkValue();
            $value = $object->getRefHashComp() . $object->name();
            if( $object->owner === $store )
            {
                $hashMap[$value][] = $object;
                if( $parentStore !== null )
                {
                    $findAncestor = $parentStore->find($object->name(), null, TRUE);
                    if( $findAncestor !== null )
                        $object->ancestor = $findAncestor;
                }
            }
            else
                $upperHashMap[$value][] = $object;
        }
    else derr("unsupported use case");

//
// Hashes with single entries have no duplicate, let's remove them
//
    $countConcernedObjects = 0;
    foreach( $hashMap as $index => &$hash )
    {
        if( count($hash) == 1 && !isset($upperHashMap[$index]) && !isset(reset($hash)->ancestor) )
            unset($hashMap[$index]);
        else
            $countConcernedObjects += count($hash);
    }
    unset($hash);
    echo "OK!\n";

    echo " - found " . count($hashMap) . " duplicates values totalling {$countConcernedObjects} tag objects which are duplicate\n";

    echo "\n\nNow going after each duplicates for a replacement\n";

    $countRemoved = 0;
    foreach( $hashMap as $index => &$hash )
    {
        echo "\n";
        echo " - name '{$index}'\n";
        $deletedObjects[$index]['kept'] = "";
        $deletedObjects[$index]['removed'] = "";


        $pickedObject = null;

        if( $pickFilter !== null )
        {
            if( isset($upperHashMap[$index]) )
            {
                foreach( $upperHashMap[$index] as $object )
                {
                    if( $pickFilter->matchSingleObject(array('object' => $object, 'nestedQueries' => &$nestedQueries)) )
                    {
                        $pickedObject = $object;
                        break;
                    }
                }
                if( $pickedObject === null )
                    $pickedObject = reset($upperHashMap[$index]);

                echo "   * using object from upper level : '{$pickedObject->name()}'\n";
            }
            else
            {
                foreach( $hash as $object )
                {
                    if( $pickFilter->matchSingleObject(array('object' => $object, 'nestedQueries' => &$nestedQueries)) )
                    {
                        $pickedObject = $object;
                        break;
                    }
                }
                if( $pickedObject === null )
                    $pickedObject = reset($hash);

                echo "   * keeping object '{$pickedObject->name()}'\n";
            }
        }
        else
        {
            if( isset($upperHashMap[$index]) )
            {
                $pickedObject = reset($upperHashMap[$index]);
                echo "   * using object from upper level : '{$pickedObject->name()}'\n";
            }
            else
            {
                $pickedObject = reset($hash);
                echo "   * keeping object '{$pickedObject->name()}'\n";
            }
        }


        // Merging loop finally!
        foreach( $hash as $objectIndex => $object )
        {
            /** @var Tag $object */
            if( isset($object->ancestor) )
            {
                $ancestor = $object->ancestor;
                $ancestor_different_color = "";

                if( !$ancestor->isTag() )
                {
                    echo "    - SKIP: object name '{$object->name()}' has one ancestor which is not TAG object\n";
                    continue;
                }

                /** @var Tag $ancestor */
                #if( $upperLevelSearch && !$ancestor->isGroup() && !$ancestor->isTmpAddr() && ($ancestor->isType_ipNetmask() || $ancestor->isType_ipRange() || $ancestor->isType_FQDN()) )
                if( $upperLevelSearch && !$ancestor->isTmp() )
                {
                    if( $object->sameValue($ancestor) ) //same color
                    {
                        if( $dupAlg == 'identical' )
                            if( $pickedObject->name() != $ancestor->name() )
                            {
                                echo "    - SKIP: object name '{$object->name()}' is not IDENTICAL to object name from upperlevel '{$pickedObject->name()}' \n";
                                continue;
                            }

                        echo "    - object '{$object->name()}' merged with its ancestor, deleting this one... ";
                        $deletedObjects[$index]['kept'] = $pickedObject->name();
                        if( $deletedObjects[$index]['removed'] == "" )
                            $deletedObjects[$index]['removed'] = $object->name();
                        else
                            $deletedObjects[$index]['removed'] .= "|" . $object->name();
                        $object->replaceMeGlobally($ancestor);
                        if( $merger->apiMode )
                            $object->owner->API_removeTag($object);
                        else
                            $object->owner->removeTag($object);

                        echo "OK!\n";

                        echo "         anchestor name: '{$ancestor->name()}' DG: ";
                        if( $ancestor->owner->owner->name() == "" ) print "'shared'";
                        else print "'{$ancestor->owner->owner->name()}'";
                        print  "  color: '{$ancestor->getColor()}' \n";

                        if( $pickedObject === $object )
                            $pickedObject = $ancestor;

                        $countRemoved++;

                        if( $mergeCountLimit !== FALSE && $countRemoved >= $mergeCountLimit )
                        {
                            echo "\n *** STOPPING MERGE OPERATIONS NOW SINCE WE REACHED mergeCountLimit ({$mergeCountLimit})\n";
                            break 2;
                        }

                        continue;
                    }
                    else
                        $ancestor_different_color = "with different color";


                }
                echo "    - object '{$object->name()}' cannot be merged because it has an ancestor " . $ancestor_different_color . "\n";

                echo "         anchestor name: '{$ancestor->name()}' DG: ";
                if( $ancestor->owner->owner->name() == "" ) print "'shared'";
                else print "'{$ancestor->owner->owner->name()}'";
                print  "  color: '{$ancestor->getColor()}' \n";

                #unset($deletedObjects[$index]);
                $deletedObjects[$index]['removed'] .= "|->ERROR ancestor: '" . $object->name() . "' cannot be merged";

                continue;
            }

            if( $object === $pickedObject )
                continue;

            if( $dupAlg != 'identical' )
            {
                echo "    - replacing '{$object->_PANC_shortName()}' ...\n";
                mwarning("implementation needed for TAG");
                //Todo;SWASCHKUT
                #$object->__replaceWhereIamUsed($merger->apiMode, $pickedObject, TRUE, 5);

                echo "    - deleting '{$object->_PANC_shortName()}'\n";
                $deletedObjects[$index]['kept'] = $pickedObject->name();
                if( $deletedObjects[$index]['removed'] == "" )
                    $deletedObjects[$index]['removed'] = $object->name();
                else
                    $deletedObjects[$index]['removed'] .= "|" . $object->name();
                if( $merger->apiMode )
                {
                    $object->owner->API_removeTag($object);
                }
                else
                {
                    $object->owner->removeTag($object);
                }

                $countRemoved++;

                if( $mergeCountLimit !== FALSE && $countRemoved >= $mergeCountLimit )
                {
                    echo "\n *** STOPPING MERGE OPERATIONS NOW SINCE WE REACHED mergeCountLimit ({$mergeCountLimit})\n";
                    break 2;
                }
            }
            else
                echo "    - SKIP: object name '{$object->name()}' is not IDENTICAL\n";
        }
    }

    echo "\n\nDuplicates removal is now done. Number of objects after cleanup: '{$store->count()}' (removed {$countRemoved} addresses)\n\n";

    echo "\n\n***********************************************\n\n";

}

echo "\n\n";

$merger->save_our_work();


if( isset(PH::$args['exportcsv']) )
{
    foreach( $deletedObjects as $obj_index => $object_name )
    {
        if( !isset($object_name['kept']) )
            print_r($object_name);
        print $obj_index . "," . $object_name['kept'] . "," . $object_name['removed'] . "\n";
    }
}


echo "\n************* END OF SCRIPT " . basename(__FILE__) . " ************\n\n";

