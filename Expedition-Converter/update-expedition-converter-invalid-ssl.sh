#!/usr/bin/env bash

cd /tools/Expedition-Converter
git -c user.name=test -c user.email=test@test.com stash
git clean -f
git -c http.sslVerify=false pull origin master