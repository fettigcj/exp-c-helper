<?php

include 'Functions.php';
// findSupportedArgs($pathToPan_Configurator);
// include 'PAN-C.Helper.SupportedArguments.php';
include "$pathToPan_Configurator/utils/common/actions.php";


print "<HTML>
<HEAD>
	<TITLE>Pan-Configurator Helper</TITLE>
	<STYLE>
		table				
		{border-collapse: collapse;}
		/*tr:hover
		{background-color: gray;}*/
		.argument:nth-of-type(even)
	 	{background-color: khaki;}
		.action:nth-of-type(even)
		{background-color: lemonchiffon;}
		/*.argument:hover
		{background-color: gray; }*/
		/*.action:hover
		{background-color: gray; }*/
	</STYLE>
</HEAD>
<BODY>
<CENTER>

<div style=\"overflow-x:auto;\">
<H2>
	Pan-Configurator Helper
</H2>
<TABLE WIDTH=100%>
<TR>
	<TH COLSPAN=2><CENTER>Listing of Supported actions:</CENTER></TH>
</TR>
<TR>
	<TH ALIGN=Left WIDTH=20%>Action Name</TH>
	<TH>
		<TABLE WIDTH=100%>
		<TR>
			<TH WIDTH=33% ALIGN=LEFT>Argument - Argument Type</TH>
			<TH WIDTH=33% ALIGN=LEFT>Default Value</TH>
			<TH WIDTH=33% ALIGN=LEFT>Choices</TH>
		</TR>
		</TABLE>
	</TH>
</TR>
<!--EndHeaderRow-->

";


ksort(RuleCallContext::$supportedActions);
foreach(RuleCallContext::$supportedActions as &$action )
{
// start a row for each action. Open the next cell in anticipation of arguments.
print "\n<!--StartAction-->
<TR CLASS=\"action\">
	<TD>{$action['name']}</TD>
	<TD>
";

if( isset($action['args']) )
{
//if there's an argument for the current action  start a table for it.
print "
		<!--StartArguments-->
		<TABLE WIDTH=100% >";
$first = true;
$count=1;
foreach($action['args'] as $argName => &$arg)
            {
		// if( $first )
		//Start a row for the argument
		print"
			<TR CLASS=\"argument\">
				<TD WIDTH=33%>#$count $argName:{$arg['type']}</TD>
				<TD WIDTH=33%>{$arg['default']}</TD>
				<TD WIDTH=33%>";

                if( isset($arg['choices']) )
                {
			print  list_to_string($arg['choices']);
                }
		print "</TD>
			</TR>\n";
                $count++;
                $first = false;

            }
        // Wrap up the arguments sub-table, still within the "Arguments are set" if statement
	print "		</TABLE>
		<!--EndArguments-->
";
	}

	print"	</TD>
</TR>
<!--EndAction-->\n";
    }
print "</TABLE></DIV></CENTER>";

?>
